import * as del from "del";
import { NextFunction, Request, Response } from "express";
import * as mkdirp from "mkdirp";
import { Types } from "mongoose";
import * as multer from "multer";
import * as fs from "fs-extra";
import paths from "./../config/path";
import commentController from "./comment";
import { isArrayUnique } from "./../../src/_lib/array";
import { InvoiceModel } from "./../../src/models/Invoice";
import { ReturnModel } from "./../../src/models/Return";
import { ObjectID } from "bson";
import { UserModel } from "../models/User";
import { flatten } from "../_lib/flatObjOnly";
import { isVariantPriceInRange } from "../_global/business";
import { Config } from "../types/app";
import { Passport } from "passport";
import { RSA_NO_PADDING } from "constants";
import invoice from "../routes/user/admin/management/invoice";
//import return from "../routes/user/admin/management/return";

export class DailystockController {
    public models;
    public userPassport: Passport;

    // config;
    constructor(private config) {
        // this.config = config;
        this.models = config.models;
        this.userPassport = config.passport.userPassport.getUserPassport();
    }

    

    public async dailyStock(
        req: Request,
        res: Response,
        next: NextFunction
    ) {

        console.log("DailyStock")
        console.log(" - request : "+req)
         try {
          //  const {name,sku,userqty,qty } = req.body;
          const {name,sku,Updated_qty,current_qty } = req.body;
            console.log("REQUEST",req.body);
            const query = await new this.models.Dailystock({
            name,sku,Updated_qty,current_qty
            }).save()
            this.updateQuantity(sku,Updated_qty);
              return res.status(201).send({query});


        } catch (e) {
            return next(e);
        }
    }
    updateQuantity(sku,Updated_qty){
       // console.log(code,qty);
        console.log("Code",sku);
        console.log("Qty",Updated_qty);
        this.models.Product.update({"detail.sku":sku }, 
        { $inc: { "stock.qty":Updated_qty, "brief.stock": Updated_qty }                 
            }).then(reslt=>{
                console.log("Log Created-->",reslt)
            })  .catch(err => {
                console.log("error",err);
            });

    }

}
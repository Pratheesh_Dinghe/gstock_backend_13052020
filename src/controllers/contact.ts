/*import * as nodemailer from "nodemailer";
import { Request, Response } from "express";
import * as dotenv from "dotenv";

export let postContact = (req: Request, res: Response) => {
  // req.assert("name", "Name cannot be blank").notEmpty();
  // req.assert("email", "Email is not valid").isEmail();
  // req.assert("message", "Message cannot be blank").notEmpty();

  // const errors = req.validationErrors();

  // if (errors) {

  //   return res.json("/contact");
  // }
  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: "gstocktest123@gmail.com",
      pass: "CYGA6677"
    }
  });

  const mailOptions = {
    from: "Gtsock@gmail.com",
    to: "xianyic@gmail.com",
    subject: "Thank you for shopping with us!",
    text: "Your order is being processed!"
  };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
      return res.status(400).send("error");
    } else {
      console.log("Email sent: " + info.response);
      return res.status(200).send("sent");
    }
  });

  // const mailOptions = {
  //   to: "your@email.com",
  //   from: `${req.body.name} <${req.body.email}>`,
  //   subject: "Contact Form",
  //   text: req.body.message
  // };

  // transporter.sendMail(mailOptions, (err) => {
  //   if (err) {

  //     return res.json("/contact");
  //   }

  //   res.json("/contact");
  // });
  
};*/



import * as del from "del";
import { NextFunction, Request, Response } from "express";
import * as mkdirp from "mkdirp";
import { Types } from "mongoose";
import * as multer from "multer";
import * as fs from "fs-extra";
import paths from "./../config/path";
import commentController from "./comment";
import { isArrayUnique } from "./../../src/_lib/array";
import { InvoiceModel } from "./../../src/models/Invoice";
import { ReturnModel } from "./../../src/models/Return";
import { ObjectID } from "bson";
import { UserModel } from "../models/User";
import { flatten } from "../_lib/flatObjOnly";
import { isVariantPriceInRange } from "../_global/business";
import { Config } from "../types/app";
import { Passport } from "passport";
import { RSA_NO_PADDING } from "constants";
import invoice from "../routes/user/admin/management/invoice";

import * as nodemailer from "nodemailer";
import * as Handlebars from "handlebars";
import * as dateFormat from "dateformat";
import user from "./management/user";
//import return from "../routes/user/admin/management/return";

export class ContactController {
  public models;
  public userPassport: Passport;

  // config;
  constructor(private config) {
    // this.config = config;
    this.models = config.models;
    this.userPassport = config.passport.userPassport.getUserPassport();
  }


  public async createContact(
    req: Request,
    res: Response,
    next: NextFunction
  ) {

    console.log("contact")
    console.log(" - request : ", req.body)

    try {

      const { FirstName, LastName, email, telephone, message, buyer_id } = req.body;
      console.log(req.body);

      //  const { emails } = req.body.email;
      const query = await new this.models.Contact({
        FirstName, LastName, email, telephone, message, buyer_id
      }).save();
      /*---------------------------------start------------*/
      //if (query.length > 0) {
      console.log("inside query")
      const root = "./src/emailTemplate";
      const templatePath = `${root}/Contact/contact.html`;

      let mailOptions = {};
      const smtpTransport = nodemailer.createTransport({
        port: 587,
        host: "smtp.office365.com",
        secure: false,
        auth: {
          user: "no-reply@gstock.sg",
          pass: "GSto2819"
        },
        tls: {
          rejectUnauthorized: false
        }
      });
      fs.readFile(templatePath, (err, chunk) => {
        if (err) {
          return next(err);
        }
        const html = chunk.toString("utf8");
        const template = Handlebars.compile(html);

        const current_time = new Date();
        const data = {
          fromEmail: "no-reply@gstock.sg",
      //    mail_obj: query.FirstName,
          mail_linkId: query.message,
          time: dateFormat(current_time, "shortTime")
        };
        const result = template(data);
        mailOptions = {
          to: "concierge@dinghe.sg",
          from: "no-reply@gstock.sg",
          subject: "Customer Enquires",
          html: result
        };
        const sendEmail = new Promise((resolve, reject) => {
          smtpTransport.sendMail(mailOptions, (error, response: any) => {
            if (error) {
              console.log(error);
              reject(error);
            } else {
              console.log(response);
              resolve(response);
            }
          });
        });
      });
      // -------------------------------------------
      return res.status(200).send({ message: "Please check your mail!" });
      //  } else {
      //   return res.status(400).send({ message: "Please enter valid email id" });
      //  }
      /*------------------------------------end-------------*/
      //return res.status(201).send("Saved");
    } catch (e) {
      return next(e);
    }

  }
  /*---------------------------------------------email-------------------
  
  public async send_email(req: Request, res: Response, next: NextFunction) {
    try {
      const { FirstName, LastName, email, telephone, message,buyer_id } = req.body;
      console.log("EMAIL", req.body);
      const user1 = await this.models.Contact.find({
        "email": req.body.email
      }).select("req.body.FirstName");
      if (user1.length > 0) {
        const root = "./src/emailTemplate";
        //   const imagesPath = `${root}/welcome/images`;
        const templatePath = `${root}/Contact/contact.html`;
  
        let mailOptions = {};
        const smtpTransport = nodemailer.createTransport({
          port: 587,
          host: "smtp.office365.com",
          secure: false,
          auth: {
            user: "no-reply@gstock.sg",
            pass: "GSto2819"
          },
          tls: {
            rejectUnauthorized: false
          }
        });
        fs.readFile(templatePath, (err, chunk) => {
          if (err) {
            return next(err);
          }
          const html = chunk.toString("utf8");
          const template = Handlebars.compile(html);
       
          //    const mail_link = " http://localhost:4200/#/reset/" + email;
         // const mail_link = "https://gstock.sg//#/reset/" + email;
  
          const current_time = new Date();
          const data = {
            fromEmail: "no-reply@gstock.sg",
            mail_obj:req.body.FirstName,
            mail_linkId: req.body.message,
            time: dateFormat(current_time, "shortTime")
          };
          const result = template(data);
          mailOptions = {
            to: email,
            from: "no-reply@gstock.sg",
            subject: "Password Reset For Gstock",
            html: result
          };
          const sendEmail = new Promise((resolve, reject) => {
            smtpTransport.sendMail(mailOptions, (error, response: any) => {
              if (error) {
                console.log(error);
                reject(error);
              } else {
                console.log(response);
                resolve(response);
              }
            });
          });
        });
        // -------------------------------------------
        return res.status(200).send({ message: "Please check your mail!" });
      } else {
        return res.status(400).send({ message: "Please enter valid email id" });
      }
    } catch (error) {
      return next(error);
    }
  }
  
  /*------------------------------------------------ends-------------------*/

  public async loadAddress(req: Request,
    res: Response,
    next: NextFunction) {
    try {
      // const { _id } = req.params.user_id;
      console.log(req.params.user_id);
      const result = await this.models.Contact.findOne({ buyer_id: new Object(req.params.user_id) });
      return res.status(200).send(result);

    } catch (error) {
      return next(error);
    }

  }


}

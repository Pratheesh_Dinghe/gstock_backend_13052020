import * as del from "del";
import { NextFunction, Request, Response } from "express";
import * as mkdirp from "mkdirp";
import { Types } from "mongoose";
import * as multer from "multer";
import * as fs from "fs-extra";
import paths from "../config/path";
import { isArrayUnique } from "../_lib/array";
import { ObjectID } from "bson";
import { UserModel } from "../models/User";
import { flatten } from "../_lib/flatObjOnly";
import { isVariantPriceInRange } from "../_global/business";
import { Config } from "../types/app";
import { Passport } from "passport";
import { RSA_NO_PADDING } from "constants";
import * as nodemailer from "nodemailer";
import * as Handlebars from "handlebars";
const sgMail = require("@sendgrid/mail");

export class

UpdateWinnersController {
    public models;
    public userPassport: Passport;

    constructor(private config) {
        this.models = config.models;
        this.userPassport = config.passport.userPassport.getUserPassport();
    }
    public async checkEmail(req: Request,
        res: Response,
        next: NextFunction) {

        try {
            const user = await this.models.User.findOne({"credential.email": req.body.email });
            if (!user)
                return res.status(200).send({
                    message: "invalid"
                });
            return res.status(200).send(user);
        }
        catch (err) {
            return next(err);
        }
    }

    public async getUpdateWinners(req: Request,
        res: Response,
        next: NextFunction) {
        try {
            const refund = await this.models.UpdateWinners.find().sort("-createdAt");
            return res.status(200).send(refund);

        } catch (error) {
            return next(error);
        }
    }

     // Save UpdateWinners and Update Credit Wallet
     public async save(req: Request,
        res: Response,
        next: NextFunction) {
            try {
                const { name,
                    email,
                    mobile,
                    amount,
                    } = req.body;
                console.log(req.body);
                // const winner = await this.models.UpdateWinners({name, email, mobile, amount}).save();
                // const newEntry = new this.models.CreditWallet({
                //     buyer_id: req.body._id,
                //     memo: "Gstock Launch",
                //     amount: req.body.amount,
                //     status: "Earned",
                //     ref_id: winner._id
                // });
                // await newEntry.save();
                // return res.status(200).send(winner);
            } catch (error) {
                return next(error);
            }
    }
    public async updateById(req: Request,
        res: Response,
        next: NextFunction) {
            try {
                console.log(req.body);
                // const winners = await this.models.UpdateWinners.update({_id: req.body._id}, {"$set": {amount: req.body.amount, mobile: req.body.mobile}});
                // const newEntry = await this.models.CreditWallet.update({ref_id: req.body._id}, {"$set": {amount: req.body.amount}});
                // console.log(winners);
                // console.log(newEntry);
            // return res.status(200).send(winners);
            } catch (error) {
                return next(error);
            }
        }

    public async deleteById(req: Request, res: Response, next: NextFunction) {

        try {
            // const { _id } = req.query;
            // console.log("id", _id);
            // const winnerFound = await this.models.UpdateWinners.findById(_id);
            // if (!winnerFound)
            //     return res.status(409).json({ message: "Record not found" });
            // const del = await this.models.UpdateWinners.findByIdAndRemove(_id);
            // const cw = await this.models.CreditWallet.findOneAndRemove({
            //     ref_id: _id
            //   });
            // return res.status(200).json(del);
        } catch (err) {
            res.status(500);
            return next(err);
        }
    }

    public async bulkEmail(req: Request, res: Response, next: NextFunction) {
        sgMail.setApiKey(process.env.SENDGRID_API_KEY);
        console.log(process.env.SENDGRID_API_KEY);
        const mailList = req.body;
        const mail = [];
        const welcomemsg = [];
        mailList.forEach(async element => {
          mail.push(element.email);
          const { email, first_name } = element;
          const password = "123456";
          const userBase = {
            credential: {
              email,
              password
            }, profile: {
              first_name
            }
          };
          console.log("element", element.email);
          const user = await new this.models.User(userBase).save();
           welcomemsg.push({
            to: element.email,
            from: "no-reply@gstock.sg",
            templateId: "d-6716bb44a8d24325a6ab288d302e1b26",
            dynamic_template_data: {
              email: element.email
            },
          });
          await sgMail.send(welcomemsg).then(() => {
            console.log("welcomemsg sent successfully!", welcomemsg);

          }).catch(error => {
            console.log(error);
          });
        });

        const childmsg = {
          to: mail,
          from: "no-reply@gstock.sg",
          templateId: "d-888a1362ef1044e2ab274a8660825501",
        };

        await sgMail.sendMultiple(childmsg).then(() => {
          console.log("childmsg sent successfully!");


        }).catch(error => {
          console.log(error);
        });



        return res.status(200).json("success");
      }

}
import * as del from "del";
import { NextFunction, Request, Response } from "express";
import * as dateFormat from "dateformat";

import * as mkdirp from "mkdirp";
import { Types } from "mongoose";
import * as multer from "multer";
import * as fs from "fs-extra";
import paths from "../config/path";
import { isArrayUnique } from "../_lib/array";
import { ObjectID } from "bson";
import { UserModel } from "../models/User";
import { flatten } from "../_lib/flatObjOnly";
import { isVariantPriceInRange } from "../_global/business";
import { Config } from "../types/app";
import { Passport } from "passport";
import { RSA_NO_PADDING } from "constants";
import * as nodemailer from "nodemailer";
import * as Handlebars from "handlebars";
import pickedListSchema from "../models/PickedList";


export class PickedListController {
    public models;
    public userPassport: Passport;

    constructor(private config) {
        this.models = config.models;
        this.userPassport = config.passport.userPassport.getUserPassport();
    }

    public async getPickedList(req: Request,
        res: Response,
        next: NextFunction) {

        try {
            const selectedOrders = await this.models.PickedList.find({ delivery_status: "picked_list" }).populate({
                path: "buyer_id merchant_id",
                select: "contact profile credential.email",
                model: this.models.User,
            });
            return res.status(200).send(selectedOrders);
        }
        catch (err) {
            return next(err);
        }
    }
    // public async getPacked(req: Request,
    //     res: Response,
    //     next: NextFunction) {

    //     try {
    //         const selectedOrders = await this.models.PickedList.find({$or: [{delivery_status: "Packed"}, {delivery_status: "hold"}]}).populate({
    //             path: "buyer_id merchant_id",
    //             select: "contact profile credential.email",
    //             model: this.models.User,
    //         });
    //         return res.status(200).send(selectedOrders);
    //     }
    //     catch (err) {
    //         return next(err);
    //     }
    // }

    public async getPacked(req: Request,
        res: Response,
        next: NextFunction) {

        try {
            const selectedOrders = await this.models.PickedList.find({delivery_status: "Packed"}).populate([{
                path: "buyer_id merchant_id",
                select: "contact profile credential.email",
                model: this.models.User,
            },
            {
                path: "products.product._id",
                select: "supplier brief.code stock.unit",
                model: this.models.Product,
            },
]);
            return res.status(200).send(selectedOrders);
        }
        catch (err) {
            return next(err);
        }
    }

         // Get Order Status
         public async getOrderStatus(req: Request,
            res: Response,
            next: NextFunction) {
                try {

                    const picked = await this.models.PickedList.find();
                    return res.status(200).send(picked);

                } catch (error) {
                    return next(error);
                }
        }

    public async getPackedOrHold(req: Request,
        res: Response,
        next: NextFunction) {

        try {
            const selectedOrders = await this.models.PickedList.find({$or: [{delivery_status: "Packed"}, {delivery_status: "hold"}]}).populate({
                path: "buyer_id merchant_id",
                select: "contact profile credential.email",
                model: this.models.User,
            });
            return res.status(200).send(selectedOrders);
        }
        catch (err) {
            return next(err);
        }
    }

    public async getDelivered(req: Request,
        res: Response,
        next: NextFunction) {

        try {
            const selectedOrders = await this.models.PickedList.find({ delivery_status: "delivered" }).populate({
                path: "buyer_id merchant_id",
                select: "contact profile credential.email",
                model: this.models.User,
            });
            return res.status(200).send(selectedOrders);
        }
        catch (err) {
            return next(err);
        }
    }

    public async getOnDelivery(req: Request,
        res: Response,
        next: NextFunction) {

        try {
            const selectedOrders = await this.models.PickedList.find({ delivery_status: "on_delivery" }).populate({
                path: "buyer_id merchant_id",
                select: "contact profile credential.email",
                model: this.models.User,
            });
            return res.status(200).send(selectedOrders);
        }
        catch (err) {
            return next(err);
        }
    }


    public async  getSupplieremail(supplier) {
        try {
            const email = await this.models.Supplier.find(
                { "name": supplier },
            );
            console.log("just check getemail", email[0].email1);
            return email;
        } catch (error) {
            // return next(error);
        }
    }


    public async getPicked(req: Request,
        res: Response,
        next: NextFunction) {
        try {
            const selectedOrders = await this.models.PickedList.find({ delivery_status: "picked" }).populate([{
                path: "buyer_id merchant_id",
                select: "contact profile credential.email",
                model: this.models.User,
            },
            {
                path: "products.product._id",
                select: "supplier brief.code stock.unit",
                model: this.models.Product,
            },

            ]);
            return res.status(200).send(selectedOrders);

        } catch (error) {
            return next(error);
        }
    }
    // Generate Picking List and Send Mail to Suppliers

    public async generatePdf(req: Request, res: Response, next: NextFunction) {
        try {

            const orderIds: any = [];
            const delivery_details = [];
            const selectedOrders = req.body;

            console.log(selectedOrders.length);
            const productList: any = [];
            const today = new Date();
            let email;
            const tomorrow = new Date(today.getTime() + (24 * 60 * 60 * 1000));
            console.log("REACHED");
            if (selectedOrders.length) {
                for (const key in selectedOrders) {
                    orderIds.push(selectedOrders[key]._id);
                    delivery_details.push({
                        order_id: selectedOrders[key]["_id"],
                        order_no: selectedOrders[key]["OrderNo"],
                        order_date: selectedOrders[key]["createdAt"],
                        delivery_type: selectedOrders[key]["delivery.shipping_type"],
                        deli_status: "picked",
                    });
                    for (const prod in selectedOrders[key].products) {
                        if (selectedOrders[key].products[prod].status === "picked") {
                            productList.push({
                                order_id: selectedOrders[key]["_id"],
                                order_no: selectedOrders[key]["OrderNo"],
                                order_date: dateFormat(selectedOrders[key]["createdAt"], "mediumDate"),
                                products: selectedOrders[key].products[prod].product.brief,
                                supplier: selectedOrders[key].products[prod].product._id,
                                quantity: selectedOrders[key].products[prod].variants[0]["order_qty"],
                                picked: selectedOrders[key].products[prod].picked_qty,
                            });
                        }

                    }
                }
                // Send mail to Suppliers
                console.log("selectedOrders", selectedOrders);
                const grouped = {};
                console.log("productList", productList);
                for (let i = 0; i < productList.length; i++) {
                    console.log("supplier email");

                    if (productList[i].supplier.supplier["supplier"]) {
                        console.log("supplier email case1", productList[i].supplier.supplier["supplier"]);
                        email = await this.getSupplieremail(productList[i].supplier.supplier["supplier"]);
                        // console.log("supplier email check1", email[0].email1)

                    } else if (productList[i].supplier.supplier) {
                        console.log("supplier email case2", productList[i].supplier.supplier);
                        email = await this.getSupplieremail(productList[i].supplier.supplier);

                        //   console.log("supplier email check2",email)
                    }
                    const suppliertype = email[0].type;
                    console.log("supplier type check", suppliertype);
                    // const sender = productList[i].supplier.supplier.email1;
                    const sender = email[0].email1;
                    // if (productList[i].supplier.supplier.type === "Consignment") {
                    if (suppliertype === "Consignment") {
                        if (!grouped[sender]) { grouped[sender] = []; }
                        grouped[sender].push(productList[i]);
                    }
                }

                (async () => {
                    for (const key in grouped) {
                        const list = grouped[key];
                        const orderlist = {};
                        const productlist = [];
                        const finalProducts = {};
                        const orders: any = [];
                        let supplier_details;
                        console.log("list", list);

                        if (list[0].supplier.supplier["supplier"]) {

                            supplier_details = await this.getSupplieremail(list[0].supplier.supplier["supplier"]);
                            console.log("supplier address1", supplier_details);
                        } else if (list[0].supplier.supplier) {
                            supplier_details = await this.getSupplieremail(list[0].supplier.supplier);
                            console.log("supplier address2", supplier_details);
                        }
                        //   let supplier_details = await Supplier.findById(list[0].supplier.supplier._id)
                        // let supplier_details = email[0];
                        for (let i = 0; i < list.length; i++) {
                            const order = list[i].order_no;
                            if (!orderlist[order]) orderlist[order] = [{
                                order_date: list[i].order_date,
                            }];
                            orderlist[order].push(list[i]);
                        }
                        list.reduce(function (res, value) {
                            if (!res[value.products.code]) {
                                res[value.products.code] = { products: value.products, qty: 0, unit: value.supplier.stock.unit };
                                productlist.push(res[value.products.code]);
                            }

                            // console.log("supplier",supplier_details.city);
                            res[value.products.code].qty += value.picked;
                            return res;
                        }, {});

                        console.log("details", grouped[key]);
                        const selproducts = [];
                        const orderdata = grouped[key];
                        console.log("productlist", productlist);
                        const delivery = grouped[key];
                        let purchaseOrderCount = await this.models.PurchaseOrder.find().count();
                        purchaseOrderCount++;
                        const count = purchaseOrderCount.toString();
                        const order_no = count.padStart(3, "0");
                        let totalcost = 0;
                        for (let j = 0; j < productlist.length; j++) {
                            const item = productlist[j].products;
                            console.log("item", item.price, productlist[j].qty);
                            selproducts.push({
                                name: item.name,
                                code: item.code,
                                qty: productlist[j].qty,
                                unit: item.stock.unit,
                                cost: item.price,
                                totalCost: item.price * productlist[j].qty,
                                discount: 0
                            });
                            totalcost = totalcost + (item.price * productlist[j].qty);
                        }

                        const savedPurchaseOrder = await new this.models.PurchaseOrder({
                            "orderNo": "PO-" + order_no,
                            "doNo": delivery.SequenceNo,
                            "orderDate": new Date(),
                            "doDate": new Date(),
                            "supplier": supplier_details[0].name,
                            "invNo": null,
                            "invDate": new Date(),
                            "subTotal": totalcost,
                            "gst": 0,
                            "totalCost": totalcost,
                            "item": selproducts,
                            po_type: "auto",
                            supplier_type: supplier_details[0].type
                        }).save();
                        const root = "./src/emailTemplate";
                        const templatePath = `${root}/delivery_process.html`;
                        const logo = "./src/public/img/67.jpg";
                        let mailOptions = {};
                        const smtpTransport = nodemailer.createTransport({
                            port: 587,
                            host: "smtp.office365.com",
                            secure: false,
                            auth: {
                                user: "no-reply@gstock.sg",
                                pass: "GSto2819"
                            },
                            tls: {
                                rejectUnauthorized: false
                            }
                        });

                        fs.readFile(templatePath, (err, chunk) => {
                            if (err) {
                                return next(err);
                            }
                            const html = chunk.toString("utf8");
                            const template = Handlebars.compile(html);
                            Handlebars.registerHelper("inc", function (value, options) {
                                return parseInt(value) + 1;
                            });
                            console.log("supplier_details", supplier_details);
                            const data = {
                                fromEmail: "no-reply@gstock.sg",
                                mail_obj: productlist,
                                supplier: supplier_details[0],
                                logo,
                                delivery_date: dateFormat(tomorrow, "mediumDate"),
                            };
                            const result = template(data);
                            mailOptions = {
                                // to: [key],
                                to:  "dhania@dinghe.sg",
                                   bcc: ["dhania@dinghe.sg", "pratheesh@dinghe.sg"],
                                // bcc: ["dessa@dinghe.sg", "angi@dinghe.sg", "dhania@dinghe.sg", "pratheesh@dinghe.sg", "toh@dinghe.sg", "jovi@dinghe.sg", "eric@dinghe.sg"],
                                from: "no-reply@gstock.sg",
                                subject: "Order Details (" + dateFormat(today, "mediumDate") + ")",
                                html: result
                            };
                            const sendEmail = new Promise((resolve, reject) => {
                                smtpTransport.sendMail(mailOptions, (error, response: any) => {
                                    if (error) {
                                        console.log("eror", error);
                                        reject(error);
                                    } else {
                                        resolve(response);
                                    }
                                });
                            });
                            sendEmail
                                .then(function (successMessage) {
                                    console.log("Success-->", successMessage);
                                })
                                .catch(function (errorMessage) {
                                    // error handler function is invoked
                                    for (const id in orderlist) orders.push({ "_id": id });
                                    const failedEmailsLog = new this.models.FailedEmailsLog({
                                        supplier: key,
                                        orders
                                    }).save().then(reslt => {
                                        console.log("Log Created-->");
                                    });
                                    console.log("error--->", errorMessage);
                                });
                        });
                    }
                })();

                // Update Delivery Status of each orders
                const orders = await this.models.Order.update({ _id: { $in: orderIds } }, {
                    $set: { delivery_status: "picked_list" }
                }, { multi: true });
                const pickedList = await this.models.PickedList.update({ _id: { $in: orderIds } }, {
                    $set: { delivery_status: "picked_list" }
                }, { multi: true });


                // Create Picking List in Delivery Process
                // const deliveryProcess = await new this.models.DeliveryProcess({
                //     deli_date: tomorrow,
                //     from_date: fromDate,
                //     to_date: toDate,
                //     order_list: delivery_details,
                // }).save();
                return res.status(200).json({ orders: selectedOrders });
            } else
                return res.status(200).json({ orders:
                     [] });
        } catch (error) {
            console.log(error);
            return res.status(500).send(error);
        }
    }
    public async savePickedList(req: Request,
        res: Response,
        next: NextFunction) {
        try {
            console.log("picked list", req.body);
            const pickedFound = await this.models.PickedList.findById(req.body._id);
            const update = await this.models.Order.update({ _id: req.body._id }, { "$set": { delivery_status: "picked" } });

            if (!pickedFound) {
                const pickedList = await this.models.PickedList(req.body).save();
                return res.status(200).send(pickedList);
            }
            console.log("not_found");
            const pickedList = await this.models.PickedList.findByIdAndUpdate(req.body._id, { $set: req.body });
            return res.status(200).send(pickedList);

        } catch (error) {
            return next(error);
        }
    }

    public async updateDeliveryStatus(req: Request,
        res: Response,
        next: NextFunction) {
        try {
            const orders = req.body;
            console.log(req.body);
            for (const key in orders) {
                if (orders[key].hasOwnProperty("delivery_status")) {
                    const delivery = await this.models.PickedList.update({ "_id": orders[key]._id }, {
                        "$set": {
                            "delivery_status": orders[key].delivery_status
                        }
                    });
                    console.log("delivery", delivery);
                    const order = await this.models.Order.update({ _id: orders[key]._id }, {
                        "$set": {
                            "delivery_status": orders[key].delivery_status
                        }
                    });
                }
            }
            return res.status(200).send({ status: "Delivery status Updated" });
        } catch (error) {
            return next(error);
        }
    }



}
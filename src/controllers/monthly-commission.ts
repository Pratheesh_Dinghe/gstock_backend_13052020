import * as del from "del";
import { NextFunction, Request, Response } from "express";
import * as mkdirp from "mkdirp";
import { Types } from "mongoose";
import * as multer from "multer";
import * as fs from "fs-extra";
import paths from "./../config/path";
import commentController from "./comment";
import { isArrayUnique } from "./../../src/_lib/array";
import { InvoiceModel } from "./../../src/models/Invoice";
import { ObjectID } from "bson";
import { UserModel } from "../models/User";
import { flatten } from "../_lib/flatObjOnly";
import { isVariantPriceInRange } from "../_global/business";
import { Config } from "../types/app";
import { Passport } from "passport";
import { RSA_NO_PADDING } from "constants";
// mport {paypal} from "paypal-rest-sdk";
const paypal = require("paypal-rest-sdk");

paypal.configure({
    mode: "sandbox", // Sandbox or live
    client_id: "AbbJpZwma-XeGUOWwtlT8D8-VGiQMYnYqAMDcTDi-2tvXhJxIQnDcS_MnWoamjL2eKCT8sc3FoMArk5x",
    client_secret: "EFEZwGcP_bAv6hHF59Wi-xGdFk3CCTv8EEHjKZUMvcrFjRdwyP6q5yXqDolli4XlBKKiMKUn--Y2neRM"
});


export class MonthlyCommissionController {
    public models;
    public userPassport: Passport;
    // config;
    constructor(private config) {
        // this.config = config;
        this.models = config.models;
        this.userPassport = config.passport.userPassport.getUserPassport();
    }
    public async getCommission(req: Request, res: Response, next: NextFunction) {
        const fromDate = new Date(req.body.fromDate);
        const toDate = new Date(req.body.toDate);
        // var fromDate = new Date();
        //     fromDate.setDate(1);
        //     fromDate.setMonth(fromDate.getMonth()-1);
        //     fromDate.setUTCHours(0,0,0,0);
        // var toDate = new Date();
        //     toDate.setDate(1);
        //     toDate.setUTCHours(0,0,0,0);
        const condition = { "From": { "$gte": fromDate }, "To": { "$lte": toDate }, "status": "active" };
        console.log("condition", condition);
        const commission = await this.models.MonthlyCommission.aggregate([
            { $match: condition },
            {
                $group: {
                    _id: "$Root",
                    amount: {
                        $sum: "$SalesAmount"
                    },
                }
            }
        ]);
        const result = await this.models.User.populate(commission, {
            path: "_id",
            select: "credential.email profile.first_name"
        });
        console.log("commission", commission);
        return res.status(200).send({ list: result });
    }

    // public async  approveCommission(req: Request, res: Response, next: NextFunction) {
    //     try {
    //         const statusUpdate = await this.models.MonthlyCommission.update({ Root: req.body.buyer }, {
    //             $set: {
    //                 status: "approved",
    //             }
    //         }, { multi: true });
    //         const commissionPaid = await new this.models.CommissionPaid(req.body
    //         ).save();
    //         console.log("UpdateCreditWallet");
    //         const newEntry = new this.models.CreditWallet({
    //             buyer_id: req.body.buyer,
    //             memo: "commission",
    //             amount: req.body.amount,
    //             status: "Earned",
    //             ref_id: req.body.ap_user
    //         });
    //         console.log("CREDIT", newEntry);
    //         await newEntry.save();
    //         return res.status(201).send({ item: commissionPaid });
    //     } catch (e) {
    //         return next(e);
    //     }
    // }

    public async approveCommission(
        req: Request,
        res: Response,
        next: NextFunction
      ) {
        try {
          const statusUpdate = await this.models.MonthlyCommission.update(
            { Root: req.body.commission[0].buyer },
            {
              $set: {
                status: "approved"
              }
            },
            { multi: true }
          );
          const commissionPaid = await new this.models.CommissionPaid(
            req.body
          ).save();
          console.log("UpdateCreditWallet", req.body);

          console.log("UpdateCreditWallet1", req.body.buyer);

          console.log("UpdateCreditWallet222", req.body.commission[0].buyer);
          const newEntry = new this.models.CreditWallet({
            buyer_id: req.body.commission[0].buyer,
            memo: "commission",
            amount: req.body.commission[0].amount,
            status: "Earned",
            ref_id: req.body.ap_user
          });
          console.log("CREDIT", newEntry);
          await newEntry.save();
          return res.status(201).send({ item: commissionPaid });
        } catch (e) {
          return next(e);
        }
      }

    public async setCommissionPercentage(req: Request, res: Response, next: NextFunction) {
        console.log(req.body);
        const commissionPercentage = await new this.models.CommissionPercentage(req.body
        ).save();
        return res.status(201).send({ item: commissionPercentage });
    }
    public async getCommissionPercentage(req: Request, res: Response, next: NextFunction) {
        console.log(req.body);
        const commissionPercentage = await this.models.CommissionPercentage.findOne();

        if (commissionPercentage) return res.status(201).send({ item: commissionPercentage });
        else return res.status(201).send({ item: {} });
    }

    public async withdrawFromWallet(req: Request, res: Response, next: NextFunction) {
        const sender_batch_id = Math.random().toString(36).substring(9);
        const create_payout_json = {
            "sender_batch_header": {
                "sender_batch_id": sender_batch_id,
                "email_subject": "You have a payment"
            },

            "items": [
                {
                    "recipient_type": "EMAIL",
                    "amount": {
                        "value": "100.00",
                        "currency": "USD"
                    },
                    "receiver": "sb-gkzug401195@personal.example.com",
                    "note": "Thank you.",
                    "sender_item_id": "item_3"
                }
            ]
        };

        const sync_mode = "false";
        let batch_header;

        paypal.payout.create(create_payout_json, sync_mode, function (error, payout) {
            if (error) {
                console.log(error);
                throw error;
            } else {
                console.log("Create Single Payout Response");
                console.log(payout.links[0].href);

                batch_header = payout.batch_header;
                const payoutId = batch_header["payout_batch_id"];
                console.log(payoutId, "payoutId");
                paypal.payout.get(payoutId, function (error, payout) {
                    if (error) {
                        console.log(error);
                        throw error;
                    } else {
                        console.log("Get Payout Response");
                        console.log(JSON.stringify(payout));
                        res.send();
                        // res.redirect(payout.links[0].href);
                    }
                });
            }
        });

        //   var payoutId = "HGLTZ9WR8G7HU"


    }

}
//
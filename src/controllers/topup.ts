import * as del from "del";
import { NextFunction, Request, Response } from "express";
import { Passport } from "passport";
import { RSA_NO_PADDING } from "constants";
import * as nodemailer from "nodemailer";
import * as Handlebars from "handlebars";


export class TopupController {
    public models;
    public userPassport: Passport;

    constructor(private config) {
        this.models = config.models;
        this.userPassport = config.passport.userPassport.getUserPassport();
    }

     // Save  Topup Credit Wallet
     public async save(req: Request,
        res: Response,
        next: NextFunction) {
            try {
                const{amount, paypal} = req.body;

                console.log(req.body);
                const topup = await this.models.Topup({amount, paypal}).save();
                const newEntry = new this.models.CreditWallet({
                    buyer_id: req.body._id,
                    memo: "Topup",
                    amount: req.body.amount,
                    status: "Earned",
                    ref_id: topup._id,
                    type: 1
                });
                await newEntry.save();
                return res.status(200).send(topup);
            } catch (error) {
                return next(error);
            }
    }

}
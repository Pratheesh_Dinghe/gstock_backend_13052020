import * as del from "del";
import { NextFunction, Request, Response } from "express";
import * as mkdirp from "mkdirp";
import { Types } from "mongoose";
import * as multer from "multer";
import * as fs from "fs-extra";
import paths from "../config/path";
import { isArrayUnique } from "../_lib/array";
import { ObjectID } from "bson";
import { UserModel } from "../models/User";
import { flatten } from "../_lib/flatObjOnly";
import { isVariantPriceInRange } from "../_global/business";
import { Config } from "../types/app";
import { Passport } from "passport";
import { RSA_NO_PADDING } from "constants";
import * as nodemailer from "nodemailer";
import * as Handlebars from "handlebars";

export class RefundController {
  public models;
  public userPassport: Passport;

  constructor(private config) {
    this.models = config.models;
    this.userPassport = config.passport.userPassport.getUserPassport();
  }
  public async getRefundNo(req: Request, res: Response, next: NextFunction) {
    try {
      const result = await this.models.Refund.findOne()
        .sort({ refund_date: -1 })
        .limit(1);
      if (result) return res.status(200).send(result);
      else return res.status(200).send({});
    } catch (err) {
      return next(err);
    }
  }

  public async getRefund(req: Request, res: Response, next: NextFunction) {
    try {
      const refund = await this.models.Refund.find()
        .sort("-refund_date")
        .populate({
          path: "ref_id",
          select: "delivery",
          model: this.models.Order
        });
      return res.status(200).send(refund);
    } catch (error) {
      return next(error);
    }
  }

  // Save Refund and Update Credit Wallet
  public async approveRefund(req: Request, res: Response, next: NextFunction) {
    try {
      console.log("req.bodyApprove", req.body.item);
      const refund = await this.models.Refund.update(
        { _id: req.body._id },
        { $set: { status: "Approved" } }
      );
      const newEntry = new this.models.CreditWallet({
        buyer_id: req.body.buyer,
        memo: "refund",
        amount: req.body.total,
        status: "Earned",
        ref_id: req.body._id
      });
      await newEntry.save();
      for (let i = 0; i < req.body.item.length; i++) {
        //  this.updateprQuantity(req.body.item[i].code,req.body.item[i].qty);
        this.updateQuantity(req.body.item[i].code, req.body.item[i].refund_qty);
        console.log("---Ccode---", req.body.item[i].code);
      }
      return res.status(200).send(req.body);
    } catch (error) {
      return next(error);
    }
  }

  // Save Refund and Update Credit Wallet
  public async saveRefund(req: Request, res: Response, next: NextFunction) {
    try {
      console.log(req.body);
      const refund = await this.models.Refund(req.body).save();
      return res.status(200).send(refund);
    } catch (error) {
      return next(error);
    }
  }

  public async approveSingleId(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { _id } = req.params.id;
      console.log(req.params.id);
      const result = await this.models.Refund.findById(req.params.id).populate({
        path: "ref_id",
        select: "delivery",
        model: this.models.Order
      });
      // this.updateQuantity(req.body.item[i].code, req.body.item[i].qty);
      return res.status(200).send(result);
    } catch (error) {
      return next(error);
    }
  }
  updateQuantity(sku, userqty) {
    // console.log(code,qty);
    console.log("Code", sku);
    console.log("Qty", userqty);

    this.models.Product.update(
      { "brief.code": sku },
      {
        $inc: { "stock.qty": userqty, "brief.stock": userqty }
      }
    )
      .then(reslt => {
        console.log("Log Created-->", reslt);
      })
      .catch(err => {
        console.log("error", err);
      });
  }
}

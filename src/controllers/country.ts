import { NextFunction, Request, Response } from "express";
import { Types } from "mongoose";
// import { countryModel } from "../models/country";

/**
 * Country controller.
 */

export default config => {
  const { models } = config;

  async function getAllCountry(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      console.log("Get Add on package");
      const { path } = req.query;
      const query = {};
      // if (path) query["path"] = { $regex: path, $options: "i" };
      const result = await models.Country.find(query);
      // .select("path commission")
      // .populate("attributes")
      // .lean();

      console.log(result);
      return res.status(200).send(result);
    } catch (e) {
      return next(e);
    }
  }

  return {
    getAllCountry
  };
};

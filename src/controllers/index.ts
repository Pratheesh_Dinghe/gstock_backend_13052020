import { CartController } from "./cart";
import { ProductController } from "./product";
import { UserController } from "./user";
import { EmailController } from "./management/email";
import attributeController from "./attribute";
import categoryController from "./category";
import commentController from "./comment";
import orderController from "./order";
import promotionController from "./promotion";
import rewardProductController from "./reward-product";
import userMgmtController from "./management/user";
import adminController from "./user/admin/admin";
import logisticController from "./logistic";
import { Config } from "../types/app";
import countryController from "./country";
import deliveryController from "./delivery";
import { InvoiceController } from "./invoice";
import { ReturnController } from "./return";
import { DailystockController } from "./dailystock";
import { StockController } from "./stock";
import { StockverifyController } from "./stockverify";
import { DeliveryProcessController } from "./delivery-process";
import { MonthlyCommissionController } from "./monthly-commission";
import { InventoryController } from "./inventory";
import { purchaseReturnController } from "./Purchasereturn";
import { ImageSaveController } from "./Image";
import { RefundController } from "./refund";
import { OrderApprovalController } from "./order-approval";
import { ContactController } from "./contact";
import { CreditWalletApprovalController } from "./credit-wallet-approval";
import { UpdateWinnersController } from "./update-winners";
import { TopupController } from "./topup";
import { TransferController } from "./transfer";
// import { CompanyController } from "./company";
// import { EmployeeController } from "./employee";
import { PickedListController } from "./pickedList";
// import pickedList from "../routes/user/admin/management/pickedList";
import { SalesPromotionController } from "./sales-promotion";
export {
  attributeController,
  categoryController,
  CartController,
  orderController,
  ProductController,
  promotionController,
  rewardProductController,
  EmailController,
  userMgmtController,
  logisticController,
  UserController,
  countryController,
  DeliveryProcessController,
  MonthlyCommissionController,
  InventoryController,
  deliveryController,
  InvoiceController,
  ReturnController,
  DailystockController,
  StockverifyController,
  StockController,
  purchaseReturnController,
  ImageSaveController,
  RefundController,
  OrderApprovalController,
  ContactController,
  CreditWalletApprovalController,
  UpdateWinnersController,
  TopupController,
  TransferController,
  // CompanyController,
  // EmployeeController,
  PickedListController,
  SalesPromotionController
};

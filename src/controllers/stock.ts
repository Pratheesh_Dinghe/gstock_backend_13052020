import * as del from "del";
import { NextFunction, Request, Response } from "express";
import * as mkdirp from "mkdirp";
import { Types, mongo } from "mongoose";
import * as multer from "multer";
import * as fs from "fs-extra";
import paths from "./../config/path";
import commentController from "./comment";
import { isArrayUnique } from "./../../src/_lib/array";
import { InvoiceModel } from "./../../src/models/Invoice";
import { ObjectID } from "bson";
import { UserModel } from "../models/User";
import { flatten } from "../_lib/flatObjOnly";
import { isVariantPriceInRange } from "../_global/business";
import { Config } from "../types/app";
import { Passport } from "passport";
import { RSA_NO_PADDING, ESPIPE } from "constants";
import invoice from "../routes/user/admin/management/invoice";


export class StockController {
    public models;
    public userPassport: Passport;
    // config;
    constructor(private config) {
        // this.config = config;
        this.models = config.models;
        this.userPassport = config.passport.userPassport.getUserPassport();
    }


    public async createStock(req: Request, res: Response, next: NextFunction) {
        try {
            const { refNo,
                stockInDate,
                purchaseInvNo,
                purchaseDate,
                totalCost,
                item } = req.body;
            console.log("ITEM", req.body.item);
            console.log(req.body);
            const savedStock = await new this.models.Stock({
                "refNo": refNo,
                "date": stockInDate,
                "purchasInvNo": purchaseInvNo,
                "purchaseDate": purchaseDate,
                "totalCost": totalCost,
                "item": item,
                "Status": "Active",
            }).save();
            console.log("--CodeUpdate--", req.body.item[0].code);
            console.log("--CodeUpdate--", req.body.item[0].qty);
            for (let i = 0; i < req.body.item.length; i++) {
                //  this.updateprQuantity(req.body.item[i].code,req.body.item[i].qty);
                this.updateQuantity(req.body.item[i].code, req.body.item[i].qty);
                console.log("---Ccode---", req.body.item[i].code);
            }
            //  this.updateQuantity(req.body.item[0].code,req.body.item[0].qty);
            console.log("--savedStock--", savedStock);
            return res.status(201).send({ "stock_id": savedStock._id });
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }

    updateQuantity(sku, userqty) {
        // console.log(code,qty);
        console.log("Code", sku);
        console.log("Qty", userqty);

        this.models.Product.update({ "brief.code": sku },
            {
                $inc: { "stock.qty": userqty, "brief.stock": userqty }
            }).then(reslt => {
                console.log("Log Created-->", reslt);
            }).catch(err => {
                console.log("error", err);
            });

    }

    public async getAllStockDetails(req: Request, res: Response, next: NextFunction) {
        try {
            return res.status(200).send(await this.models.Stock.find());
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }

    // public async getStockReport(req: Request, res: Response, next: NextFunction) {
    //     try {
    //         return res.status(200).send(await this.models.Product.find());
    //     } catch (e) {
    //         console.log(e);
    //         return next(e);
    //     }
    // }
    public async getStockReport(req: Request, res: Response, next: NextFunction) {
        try {
          return res
            .status(200)
            .send(await this.models.Product.find({ is_active: true }));

          // return res.status(200).send(await this.models.Product.find());
        } catch (e) {
          console.log(e);
          return next(e);
        }
      }
    public async  getAllOrder(req: Request,
        res: Response,
        next: NextFunction) {
        const fromDate = req.body.fromDate;
        const toDate = req.body.toDate;
        console.log("getPaidOrders");
        console.log("- fromDate : " + fromDate);
        console.log("- toDate : " + toDate);
        try {
            const dbQuery = {};
            const populate = {};



            if (fromDate && toDate) {
                const _fromDate = new Date(fromDate);
                const _toDate = new Date(toDate);
                _toDate.setDate(_toDate.getDate() + 1);
                dbQuery["createdAt"] = { $gte: _fromDate, $lt: _toDate };
            } else if (fromDate) {
                const _fromDate = new Date(fromDate);
                dbQuery["createdAt"] = { $gte: _fromDate };
            } else if (toDate) {
                const _toDate = new Date(toDate);
                _toDate.setDate(_toDate.getDate() + 1);
                dbQuery["createdAt"] = { $lt: _toDate };
            }
            dbQuery["status"] = "Paid";

            return res.status(200).send(await this.models.Order.find(dbQuery));
            // const orders = await Order.find();
            // return res.status(200).send(orders);

        } catch (error) {
            return next(error);
        }
    }

    public async getCreditWallet(req: Request, res: Response, next: NextFunction) {

        try {
            console.log("getCreditWallet");
            const populate = {
                path: "buyer_id",
                select: "contact profile credential.email",
                model: this.models.User
            };
            //   console.log("hai")
            const grouped = {};
            //   console.log("CREDIT HISTORY 12",  .user.id);
            const wallet = await this.models.CreditWallet.find({ status: { $nin: "Pending" } }).populate({
                path: "buyer_id",
                select: "contact profile credential.email",
                model: this.models.User
            }).sort("-createdAt");
            // sort("createdAt")
            console.log("wallet", wallet.length);

            for (let i = 0; i < wallet.length; i++) {
                console.log("wallet -" + [i], wallet[i]);
                console.log("id", wallet[i].buyer_id._id);
                const sender = wallet[i].buyer_id._id;
                console.log("sender", sender);
                if (!grouped[sender]) { grouped[sender] = []; }
                console.log("group", grouped);
                const result1 = await this.models.CreditWallet.aggregate([
                    {
                        $match: {
                            buyer_id: Types.ObjectId(sender),
                            status: "Earned"
                        }
                    },
                    {
                        $group: {
                            _id: null,
                            totalAmount: { $sum: "$amount" }
                        }
                    }
                ]);
               console.log("result1", result1);
                const result2 = await this.models.CreditWallet.aggregate([
                    {
                        $match: {
                            buyer_id: Types.ObjectId(sender),
                            status: "Used"
                        }
                    },
                    {
                        $group: {
                            _id: null,
                            totalAmount: { $sum: "$amount" }
                        }
                    }
                ]);
                console.log("result2", result2);
                const result3 = await this.models.CreditWallet.aggregate([
                    {
                        $match: {
                            buyer_id: Types.ObjectId(sender),
                            status: "Withdrawed"
                        }
                    },
                    {
                        $group: {
                            _id: null,
                            totalAmount: { $sum: "$amount" }
                        }
                    }
                ]);
                console.log("result3", result3);

                let res1 = 0;
                let res2 = 0;
                let res3 = 0;

                if (result1.length > 0) {
                    res1 = result1[0]["totalAmount"];
                }
                if (result2.length > 0) {
                    res2 = result2[0]["totalAmount"];
                }
                if (result3.length > 0) {
                    res3 = result3[0]["totalAmount"];
                }

                const result = res1 - res2 - res3;
                // const result = 0;

                console.log("TOTALL", result);
                grouped[sender].push({ wallet: wallet[i], total: result });
            }
            console.log("grouped123", grouped);
            return res.status(200).send(grouped

            );
        } catch (error) {
            return next(error);
        }
        //  }
    }

    public async  loadUserList(req: Request,
        res: Response,
        next: NextFunction) {
        try {
           const month = req.params.month;
            const Orders = await this.models.Order.aggregate([
                {
                    $group: {
                        _id: { buyer_id: "$buyer_id", month: { $month: "$createdAt" }, status: "$status" },
                        total: { $sum: "$total.store_ap" },
                        buyer_id: { $first: "$buyer_id" },
                        status: { $first: "$status" },
                        updatedAt: { $last: "$createdAt" }
                    }
                },
                {
                    $project: {
                        _id: 0,
                        buyer_id: "$buyer_id",
                        total: "$total",
                        month: { $month: "$updatedAt" },
                        status: "$status",
                        date: "$updatedAt"
                    }
                },
                { $match: { month: 2, status: "Paid", total: { $gte: 30 } } }
            ]);
            const users = [];
            for (let i = 0; i < Orders.length; i++) {
                const item = Orders[i];
                const user = await this.models.User.findOne({ "credential.user_group": "buyer", "profile": { $ne: null }, _id: new mongo.ObjectID(item.buyer_id) });
                users.push(user);
            }

            res.send(users);

        } catch (err) {
            console.log(err);
            res.status(500).send(err);
        }
    }

    public async  getAllOrderdetails(req: Request,
        res: Response,
        next: NextFunction) {
        const fromDate = req.body.fromDate;
        const toDate = req.body.toDate;
        console.log("getPaidOrders");
        console.log("- fromDate : " + fromDate);
        console.log("- toDate : " + toDate);
        try {
            const dbQuery = {};
            let populate = {};

            populate = [{
                path: "buyer_id merchant_id",
                select: "contact profile credential.email",
                model: this.models.User,
            },
            {
                path: "products.product._id",
                select: "supplier brief.code stock.unit",
                model: this.models.Product,
            }
            ];

            if (fromDate && toDate) {
                const _fromDate = new Date(fromDate);
                const _toDate = new Date(toDate);
                _toDate.setDate(_toDate.getDate() + 1);
                dbQuery["createdAt"] = { $gte: _fromDate, $lt: _toDate };
            } else if (fromDate) {
                const _fromDate = new Date(fromDate);
                dbQuery["createdAt"] = { $gte: _fromDate };
            } else if (toDate) {
                const _toDate = new Date(toDate);
                _toDate.setDate(_toDate.getDate() + 1);
                dbQuery["createdAt"] = { $lt: _toDate };
            }
            dbQuery["status"] = "Paid";
            const orders = await this.models.Order
                .find(dbQuery)
                .populate(populate)
                .sort("-createdAt");
            //  return res.status(200).send(await this.models.Order.find(dbQuery))
            // const orders = await Order.find();
            return res.status(200).send(orders);

        } catch (error) {
            return next(error);
        }
    }
    public async  getJoinReport(req: Request,
        res: Response,
        next: NextFunction) {
        try {
            console.log(req.body);
            let todate = new Date(req.body.to);
            todate = new Date(Date.UTC(todate.getFullYear(), todate.getMonth(), todate.getDate() + 1, 12, 0, 0, 0));
            const utctodate = todate.toUTCString();
            let fromdate = new Date(req.body.from);
            fromdate = new Date(Date.UTC(fromdate.getFullYear(), fromdate.getMonth(), fromdate.getDate() + 1, 0, 0, 0, 0));
            const utcfromdate = fromdate.toUTCString();
            console.log(utcfromdate, utctodate);
            let users;
            // if (req.body.to == req.body.from) {
            //     console.log(req.body.to == req.body.from)
            //      users =  await this.models.User.find({ createdAt: { $eq: utctodate }, "credential.user_group": "buyer" })
            // } else{
                 users =  await this.models.User.find({ createdAt: { $lte: utctodate, $gte: utcfromdate }, "credential.user_group": "buyer" });
            // }


            const empDetails = [];

            for (let i = 0; i < users.length; i++) {
                const Details = users[i];
                let parent = { "profile": { first_name: " ", last_name: " " } };
                const user = await this.models.Commissiontree.findById(Details._id);
                if (user != null && user.parent != null) {
                    parent = await this.models.User.findById(user.parent);
                    // console.log("parent", user);
                }
                // console.log("parent", emps, parent)
                if (Details) {
                    // console.log(Details.credential.email);
                    // console.log("empDetails", empDetails)
                    empDetails.push({
                        createdAt: Details.createdAt, email: Details.credential.email,
                        parent: parent.profile.first_name + " " + parent.profile.last_name,
                        name: Details.profile.first_name + " " + Details.profile.last_name
                    });
                }

            }
            // console.log(empDetails);
            res.send({ result: empDetails });

        } catch (error) {
            console.log(error);
            res.send(error);
        }

    }
    public async  checkUserTree(req: Request,
        res: Response,
        next: NextFunction) {
        try {
            const users = await this.models.User.find({ "credential.user_group": "buyer" });
            console.log(users.length);
            const difUsers = [];
            for (let i = 0; i < users.length; i++) {
                const user = users[i];
                const tree = await this.models.Commissiontree.find({ _id: user._id });
                if (tree.length !== 0) {
                    console.log("yes", tree);
                } else {
                    console.log("no");
                    difUsers.push(user);
                    const node = new this.models.Commissiontree({
                        _id: user._id,
                        user_name: user.profile.first_name + "" + user.profile.last_name,
                        parent: "5dce86b04490201703543d6e",
                        someadditionalattr: "gstock",
                        order: 10
                    });
                    const newNode = await node.save();
                }
            }
            res.send(difUsers);

        } catch (err) {
            console.log(err);
            res.status(500).send(err);
        }
    }

    // public async generatecommissionReport(req: Request, res: Response, next: NextFunction) {
    //     // TO GET THE DESCENDANTS OF A PATRICULAR NODE.
    //     try {
    //         console.log("PARENT3", req.body);
    //         const Result = [];
    //         const tmpstack1 = [];
    //         let TEMP2 = [];
    //         if (typeof req.body.month !== "string") {
    //             res.sendStatus(400);
    //             return;
    //           }
    //         const month = req.body.month.split("-")[1];
    //         let totalCommission = 0;
    //         // await this.UpdateTotalPurchase();
    //         // const totalPurchase = await this.models.Order.aggregate([
    //         //     {
    //         //         $project: {
    //         //             purchase: "$total.store_ap",
    //         //             month: { $month: "$createdAt" },
    //         //             buyer: "$buyer_id"
    //         //         }
    //         //     },
    //         //     { $match: { month: parseInt(month) } }
    //         // ]);

    //         // console.log(totalPurchase, month);
    //         const userdata = await this.models.User.findById(req.body.user);
    //         const it = await this.models.Commissiontree.find({ parent: req.body.user });
    //         // tmpstack1.push(it);
    //         // Result.push({ leval1: it }); // Level 1
    //         console.log("Result1", it);
    //         const Result2 = [];
    //         const Result3 = [];
    //         const Result4 = [];
    //         const Result5 = [];
    //         const Resultfull_1 = [];
    //         const Resultfull_2 = [];
    //         const Resultfull_3 = [];
    //         const Resultfull_4 = [];
    //         const Resultfull_5 = [];

    //         Resultfull_1.push(it);


    //         for (let w = 0; w < it.length; w++) {
    //             const item = it[w];
    //             const userOrders = [];
    //             const totalPurchase = await this.models.Order.aggregate([
    //                 {
    //                     $project: {
    //                         purchase: "$total.store_ap",
    //                         month: { $month: "$createdAt" },
    //                         buyer: "$buyer_id",
    //                         OrderNo: "$OrderNo",
    //                         status: "$status",
    //                         products: "$products"
    //                     }
    //                 },
    //                 { $match: { month: parseInt(month), buyer: new mongo.ObjectID(item._id), status: "Paid" } }
    //             ]);
    //             if (totalPurchase.lenght != 0) {
    //                 // console.log("level1", totalPurchase, item);
    //                 for (let j = 0; j < totalPurchase.length; j++) {
    //                     const purchase = totalPurchase[j];
    //                     // console.log(purchase.products)
    //                     let cost = 0, price = 0, margin = 0;

    //                     for (let l = 0; l < purchase.products.length; l++) {
    //                         const product = purchase.products[l];
    //                         console.log(product.product._id);
    //                         const rateDt = await this.models.Product.findById(product.product._id);
    //                         const qty = product.variants[0].order_qty;
    //                         cost = cost + rateDt.brief.cost * qty;
    //                         price = price + product.product.brief.price * qty;
    //                         const teCost = rateDt.brief.cost * qty;
    //                         const tePrice = product.product.brief.price * qty;
    //                         margin = margin + tePrice - teCost;
    //                         console.log("rateDt", rateDt.brief.price);
    //                     }
    //                     const dataset = {
    //                         OrderNo: purchase.OrderNo, user_name: item.user_name, price, cost,
    //                         commission: (margin * 10) / 100, _id: item.parent, margin
    //                     };
    //                     totalCommission = totalCommission + dataset.commission;
    //                     userOrders.push(dataset);

    //                 }

    //             }
    //             if (userOrders.length != 0) {
    //                 tmpstack1.push(userOrders);
    //             }

    //         }
    //         Result.push({ leval1: tmpstack1 }); // Level 1
    //         // console.log("tmpstack1", tmpstack1)
    //         TEMP2 = Resultfull_1.pop();
    //         // console.log(TEMP2)
    //         for (let i = 0; i < TEMP2.length; i++) {
    //             const TEMP3 = await this.models.Commissiontree.find({
    //                 parent: TEMP2[i]._id
    //             }); // Level 2
    //             // console.log("2", TEMP3);
    //             Resultfull_2.push(TEMP3);
    //             let userOrders = [];
    //             for (let k = 0; k < TEMP3.length; k++) {
    //                 const item = TEMP3[k];
    //                 userOrders = [];
    //                 const totalPurchase = await this.models.Order.aggregate([
    //                     {
    //                         $project: {
    //                             purchase: "$total.store_ap",
    //                             month: { $month: "$createdAt" },
    //                             buyer: "$buyer_id",
    //                             OrderNo: "$OrderNo",
    //                             status: "$status",
    //                             products: "$products"
    //                         }
    //                     },
    //                     { $match: { month: parseInt(month), buyer: new mongo.ObjectID(item._id), status: "Paid" } }
    //                 ]);
    //                 if (totalPurchase.lenght != 0) {
    //                     // console.log("level2", totalPurchase, item);
    //                     for (let j = 0; j < totalPurchase.length; j++) {
    //                         const purchase = totalPurchase[j];
    //                         let cost = 0, price = 0, margin = 0;

    //                         for (let l = 0; l < purchase.products.length; l++) {
    //                             const product = purchase.products[l];
    //                             console.log(product.product._id);
    //                             const rateDt = await this.models.Product.findById(product.product._id);
    //                             const qty = product.variants[0].order_qty;
    //                             cost = cost + rateDt.brief.cost * qty;
    //                             price = price + product.product.brief.price * qty;
    //                             const teCost = rateDt.brief.cost * qty;
    //                             const tePrice = product.product.brief.price * qty;
    //                             margin = margin + tePrice - teCost;
    //                             console.log("rateDt", rateDt.brief.price);
    //                         }
    //                         const dataset = {
    //                             OrderNo: purchase.OrderNo, user_name: item.user_name, price, cost,
    //                             commission: (margin * 8) / 100, _id: item.parent, margin
    //                         };
    //                         totalCommission = totalCommission + dataset.commission;
    //                         userOrders.push(dataset);
    //                     }

    //                 }
    //                 if (userOrders.length != 0) {
    //                     // console.log("dataset", userOrders)
    //                     Result2.push(userOrders);
    //                 }

    //             }


    //         }
    //         Result.push({ leval2: Result2 });

    //         for (let m = 0; m < Resultfull_2.length; m++) {
    //             const temp = Resultfull_2[m];
    //             for (let i = 0; i < temp.length; i++) {
    //                 const TEMP3 = await this.models.Commissiontree.find({
    //                     parent: temp[i]._id
    //                 }); // Level 3
    //                 Resultfull_3.push(TEMP3);
    //                 let userOrders = [];
    //                 for (let k = 0; k < TEMP3.length; k++) {
    //                     const item = TEMP3[k];
    //                     userOrders = [];
    //                     const totalPurchase = await this.models.Order.aggregate([
    //                         {
    //                             $project: {
    //                                 purchase: "$total.store_ap",
    //                                 month: { $month: "$createdAt" },
    //                                 buyer: "$buyer_id",
    //                                 OrderNo: "$OrderNo",
    //                                 status: "$status",
    //                                 products: "$products"
    //                             }
    //                         },
    //                         { $match: { month: parseInt(month), buyer: new mongo.ObjectID(item._id), status: "Paid" } }
    //                     ]);
    //                     if (totalPurchase.lenght != 0) {
    //                         // console.log("www", totalPurchase, item);
    //                         for (let j = 0; j < totalPurchase.length; j++) {
    //                             const purchase = totalPurchase[j];
    //                             let cost = 0, price = 0, margin = 0;

    //                             for (let l = 0; l < purchase.products.length; l++) {
    //                                 const product = purchase.products[l];
    //                                 console.log(product.product._id);
    //                                 const rateDt = await this.models.Product.findById(product.product._id);
    //                                 const qty = product.variants[0].order_qty;
    //                                 cost = cost + rateDt.brief.cost * qty;
    //                                 price = price + product.product.brief.price * qty;
    //                                 const teCost = rateDt.brief.cost * qty;
    //                                 const tePrice = product.product.brief.price * qty;
    //                                 margin = margin + tePrice - teCost;
    //                                 console.log("rateDt", rateDt.brief.price);
    //                             }
    //                             const dataset = {
    //                                 OrderNo: purchase.OrderNo, user_name: item.user_name, price, cost,
    //                                 commission: (margin * 6) / 100, _id: item.parent, margin
    //                             };
    //                             totalCommission = totalCommission + dataset.commission;
    //                             userOrders.push(dataset);
    //                         }

    //                     }
    //                     if (userOrders.length != 0) {
    //                         // console.log("dataset", userOrders)
    //                         Result3.push(userOrders);
    //                     }
    //                 }



    //             }
    //         }
    //         Result.push({ leval3: Result3 });
    //         for (let m = 0; m < Resultfull_3.length; m++) {
    //             const temp = Resultfull_3[m];
    //             for (let i = 0; i < temp.length; i++) {
    //                 const TEMP3 = await this.models.Commissiontree.find({
    //                     parent: temp[i]._id
    //                 }); // Level 4
    //                 Resultfull_4.push(TEMP3);

    //                 let userOrders = [];
    //                 for (let k = 0; k < TEMP3.length; k++) {
    //                     const item = TEMP3[k];
    //                     userOrders = [];
    //                     const totalPurchase = await this.models.Order.aggregate([
    //                         {
    //                             $project: {
    //                                 purchase: "$total.store_ap",
    //                                 month: { $month: "$createdAt" },
    //                                 buyer: "$buyer_id",
    //                                 OrderNo: "$OrderNo",
    //                                 status: "$status",
    //                                 products: "$products"
    //                             }
    //                         },
    //                         { $match: { month: parseInt(month), buyer: new mongo.ObjectID(item._id), status: "Paid" } }
    //                     ]);
    //                     if (totalPurchase.lenght != 0) {
    //                         // console.log("www", totalPurchase, item);
    //                         for (let j = 0; j < totalPurchase.length; j++) {
    //                             const purchase = totalPurchase[j];
    //                             let cost = 0, price = 0, margin = 0;

    //                             for (let l = 0; l < purchase.products.length; l++) {
    //                                 const product = purchase.products[l];
    //                                 console.log(product.product._id);
    //                                 const rateDt = await this.models.Product.findById(product.product._id);
    //                                 const qty = product.variants[0].order_qty;
    //                                 cost = cost + rateDt.brief.cost * qty;
    //                                 price = price + product.product.brief.price * qty;
    //                                 const teCost = rateDt.brief.cost * qty;
    //                                 const tePrice = product.product.brief.price * qty;
    //                                 margin = margin + tePrice - teCost;
    //                                 console.log("rateDt", rateDt.brief.price);
    //                             }
    //                             const dataset = {
    //                                 OrderNo: purchase.OrderNo, user_name: item.user_name, price, cost,
    //                                 commission: (margin * 4) / 100, _id: item.parent, margin
    //                             };
    //                             totalCommission = totalCommission + dataset.commission;
    //                             userOrders.push(dataset);
    //                         }

    //                     }
    //                     if (userOrders.length != 0) {
    //                         // console.log("dataset", userOrders)
    //                         Result4.push(userOrders);
    //                     }
    //                 }


    //             }
    //         }
    //         Result.push({ leval4: Result4 });

    //         for (let m = 0; m < Resultfull_4.length; m++) {
    //             const temp = Resultfull_4[m];
    //             for (let i = 0; i < temp.length; i++) {
    //                 const TEMP3 = await this.models.Commissiontree.find({
    //                     parent: temp[i]._id
    //                 });
    //                 // Resultfull_5.push(TEMP3);
    //                 let userOrders = [];
    //                 for (let k = 0; k < TEMP3.length; k++) {
    //                     const item = TEMP3[k];
    //                     userOrders = [];
    //                     const totalPurchase = await this.models.Order.aggregate([
    //                         {
    //                             $project: {
    //                                 purchase: "$total.store_ap",
    //                                 month: { $month: "$createdAt" },
    //                                 buyer: "$buyer_id",
    //                                 OrderNo: "$OrderNo",
    //                                 status: "$status",
    //                                 products: "$products"
    //                             }
    //                         },
    //                         { $match: { month: parseInt(month), buyer: new mongo.ObjectID(item._id), status: "Paid" } }
    //                     ]);
    //                     if (totalPurchase.lenght != 0) {
    //                         // console.log("www", totalPurchase, item);
    //                         for (let j = 0; j < totalPurchase.length; j++) {
    //                             const purchase = totalPurchase[j];
    //                             let cost = 0, price = 0, margin = 0;

    //                             for (let l = 0; l < purchase.products.length; l++) {
    //                                 const product = purchase.products[l];
    //                                 console.log(product.product._id);
    //                                 const rateDt = await this.models.Product.findById(product.product._id);
    //                                 const qty = product.variants[0].order_qty;
    //                                 cost = cost + rateDt.brief.cost * qty;
    //                                 price = price + product.product.brief.price * qty;
    //                                 const teCost = rateDt.brief.cost * qty;
    //                                 const tePrice = product.product.brief.price * qty;
    //                                 margin = margin + tePrice - teCost;
    //                                 console.log("rateDt", rateDt.brief.price);
    //                             }
    //                             const dataset = {
    //                                 OrderNo: purchase.OrderNo, user_name: item.user_name, price, cost,
    //                                 commission: (margin * 2) / 100, _id: item.parent, margin
    //                             };
    //                             totalCommission = totalCommission + dataset.commission;
    //                             userOrders.push(dataset);
    //                         }

    //                     }
    //                     if (userOrders.length != 0) {
    //                         // console.log("dataset", userOrders)
    //                         Result5.push(userOrders);
    //                     }
    //                 }
    //                 // if (userOrders.length != 0) {
    //                 //     // console.log("dataset", userOrders)
    //                 //     Result5.push(userOrders);
    //                 // }

    //             }
    //         }
    //         Result.push({ leval5: Result5 });
    //         // console.log("Result", Result)
    //         return res.status(200).send({
    //             Result,
    //             totalCommission
    //         });
    //     } catch (error) {
    //         console.log(error);
    //         return next(error);
    //     }
    // }


    public async generatecommissionReport(req: Request, res: Response, next: NextFunction) {
        // TO GET THE DESCENDANTS OF A PATRICULAR NODE.
        try {
            console.log("PARENT3", req.body);
            const Result = [];
            const tmpstack1 = [];
            let TEMP2 = [];
            const month = req.body.month.split("-")[1];
            let totalCommission = 0;
            // await this.UpdateTotalPurchase();
            // const totalPurchase = await this.models.Order.aggregate([
            //     {
            //         $project: {
            //             purchase: "$total.store_ap",
            //             month: { $month: "$createdAt" },
            //             buyer: "$buyer_id"
            //         }
            //     },
            //     { $match: { month: parseInt(month) } }
            // ]);

            // console.log(totalPurchase, month);
            const userdata = await this.models.User.findById(req.body.user);
            const commisUser = await this.models.Commissiontree.findOne({ _id: req.body.user });
            const it = await this.models.Commissiontree.find({ parent: req.body.user });
            // tmpstack1.push(it);
            // Result.push({ leval1: it }); // Level 1
            console.log("Result1", it);
            const Result2 = [];
            const Result3 = [];
            const Result4 = [];
            const Result5 = [];
            const Resultfull_1 = [];
            const Resultfull_2 = [];
            const Resultfull_3 = [];
            const Resultfull_4 = [];
            const Resultfull_5 = [];
            const userOrds = [];
            Resultfull_1.push(it);
            const totalLevel1 = await this.models.Order.aggregate([
                {
                    $project: {
                        purchase: "$total.store_ap",
                        month: { $month: "$createdAt" },
                        buyer: "$buyer_id",
                        OrderNo: "$OrderNo",
                        status: "$status",
                        products: "$products"
                    }
                },
                { $match: { month: parseInt(month), buyer: new mongo.ObjectID(req.body.user), status: "Paid" } }
            ]);
            if (totalLevel1.lenght != 0) {

                // console.log("level1", totalPurchase, item);
                for (let j = 0; j < totalLevel1.length; j++) {
                    const purchase = totalLevel1[j];
                    // console.log(purchase.products)
                    let cost = 0, price = 0, margin = 0;

                    for (let l = 0; l < purchase.products.length; l++) {
                        const product = purchase.products[l];
                        console.log(product.product._id);
                        const rateDt = await this.models.Product.findById(product.product._id);
                        const qty = product.variants[0].order_qty;
                        cost = cost + rateDt.brief.cost * qty;
                        price = price + product.product.brief.price * qty;
                        const teCost = rateDt.brief.cost * qty;
                        const tePrice = product.product.brief.price * qty;
                        margin = margin + tePrice - teCost;
                        console.log("rateDt", rateDt.brief.price);
                    }
                    const dataset = {
                        OrderNo: purchase.OrderNo, user_name: commisUser.user_name, price, cost,
                        commission: (margin * 10) / 100, _id: commisUser.parent, margin
                    };
                    console.log("datasetfirs", dataset, commisUser);
                    totalCommission = totalCommission + dataset.commission;
                    userOrds.push(dataset);
                }

            }
            Result.push({ leval1: [userOrds] });
            for (let w = 0; w < it.length; w++) {
                const item = it[w];
                const userOrders = [];
                const totalPurchase = await this.models.Order.aggregate([
                    {
                        $project: {
                            purchase: "$total.store_ap",
                            month: { $month: "$createdAt" },
                            buyer: "$buyer_id",
                            OrderNo: "$OrderNo",
                            status: "$status",
                            products: "$products"
                        }
                    },
                    { $match: { month: parseInt(month), buyer: new mongo.ObjectID(item._id), status: "Paid" } }
                ]);
                if (totalPurchase.lenght != 0) {
                    // console.log("level1", totalPurchase, item);
                    for (let j = 0; j < totalPurchase.length; j++) {
                        const purchase = totalPurchase[j];
                        // console.log(purchase.products)
                        let cost = 0, price = 0, margin = 0;

                        for (let l = 0; l < purchase.products.length; l++) {
                            const product = purchase.products[l];
                            console.log(product.product._id);
                            const rateDt = await this.models.Product.findById(product.product._id);
                            const qty = product.variants[0].order_qty;
                            cost = cost + rateDt.brief.cost * qty;
                            price = price + product.product.brief.price * qty;
                            const teCost = rateDt.brief.cost * qty;
                            const tePrice = product.product.brief.price * qty;
                            margin = margin + tePrice - teCost;
                            console.log("rateDt", rateDt.brief.price);
                        }
                        const dataset = {
                            OrderNo: purchase.OrderNo, user_name: item.user_name, price, cost,
                            commission: (margin * 8) / 100, _id: item.parent, margin
                        };
                        totalCommission = totalCommission + dataset.commission;
                        userOrders.push(dataset);

                    }

                }
                if (userOrders.length != 0) {
                    tmpstack1.push(userOrders);
                }

            }
            Result.push({ leval2: tmpstack1 }); // Level 1
            // console.log("tmpstack1", tmpstack1)
            TEMP2 = Resultfull_1.pop();
            // console.log(TEMP2)
            for (let i = 0; i < TEMP2.length; i++) {
                const TEMP3 = await this.models.Commissiontree.find({
                    parent: TEMP2[i]._id
                }); // Level 2
                // console.log("2", TEMP3);
                Resultfull_2.push(TEMP3);
                let userOrders = [];
                for (let k = 0; k < TEMP3.length; k++) {
                    const item = TEMP3[k];
                    userOrders = [];
                    const totalPurchase = await this.models.Order.aggregate([
                        {
                            $project: {
                                purchase: "$total.store_ap",
                                month: { $month: "$createdAt" },
                                buyer: "$buyer_id",
                                OrderNo: "$OrderNo",
                                status: "$status",
                                products: "$products"
                            }
                        },
                        { $match: { month: parseInt(month), buyer: new mongo.ObjectID(item._id), status: "Paid" } }
                    ]);
                    if (totalPurchase.lenght != 0) {
                        // console.log("level2", totalPurchase, item);
                        for (let j = 0; j < totalPurchase.length; j++) {
                            const purchase = totalPurchase[j];
                            let cost = 0, price = 0, margin = 0;

                            for (let l = 0; l < purchase.products.length; l++) {
                                const product = purchase.products[l];
                                console.log(product.product._id);
                                const rateDt = await this.models.Product.findById(product.product._id);
                                const qty = product.variants[0].order_qty;
                                cost = cost + rateDt.brief.cost * qty;
                                price = price + product.product.brief.price * qty;
                                const teCost = rateDt.brief.cost * qty;
                                const tePrice = product.product.brief.price * qty;
                                margin = margin + tePrice - teCost;
                                console.log("rateDt", rateDt.brief.price);
                            }
                            const dataset = {
                                OrderNo: purchase.OrderNo, user_name: item.user_name, price, cost,
                                commission: (margin * 6) / 100, _id: item.parent, margin
                            };
                            totalCommission = totalCommission + dataset.commission;
                            userOrders.push(dataset);
                        }

                    }
                    if (userOrders.length != 0) {
                        // console.log("dataset", userOrders)
                        Result2.push(userOrders);
                    }

                }


            }
            Result.push({ leval3: Result2 });

            for (let m = 0; m < Resultfull_2.length; m++) {
                const temp = Resultfull_2[m];
                for (let i = 0; i < temp.length; i++) {
                    const TEMP3 = await this.models.Commissiontree.find({
                        parent: temp[i]._id
                    }); // Level 3
                    Resultfull_3.push(TEMP3);
                    let userOrders = [];
                    for (let k = 0; k < TEMP3.length; k++) {
                        const item = TEMP3[k];
                        userOrders = [];
                        const totalPurchase = await this.models.Order.aggregate([
                            {
                                $project: {
                                    purchase: "$total.store_ap",
                                    month: { $month: "$createdAt" },
                                    buyer: "$buyer_id",
                                    OrderNo: "$OrderNo",
                                    status: "$status",
                                    products: "$products"
                                }
                            },
                            { $match: { month: parseInt(month), buyer: new mongo.ObjectID(item._id), status: "Paid" } }
                        ]);
                        if (totalPurchase.lenght != 0) {
                            // console.log("www", totalPurchase, item);
                            for (let j = 0; j < totalPurchase.length; j++) {
                                const purchase = totalPurchase[j];
                                let cost = 0, price = 0, margin = 0;

                                for (let l = 0; l < purchase.products.length; l++) {
                                    const product = purchase.products[l];
                                    console.log(product.product._id);
                                    const rateDt = await this.models.Product.findById(product.product._id);
                                    const qty = product.variants[0].order_qty;
                                    cost = cost + rateDt.brief.cost * qty;
                                    price = price + product.product.brief.price * qty;
                                    const teCost = rateDt.brief.cost * qty;
                                    const tePrice = product.product.brief.price * qty;
                                    margin = margin + tePrice - teCost;
                                    console.log("rateDt", rateDt.brief.price);
                                }
                                const dataset = {
                                    OrderNo: purchase.OrderNo, user_name: item.user_name, price, cost,
                                    commission: (margin * 4) / 100, _id: item.parent, margin
                                };
                                totalCommission = totalCommission + dataset.commission;
                                userOrders.push(dataset);
                            }

                        }
                        if (userOrders.length != 0) {
                            // console.log("dataset", userOrders)
                            Result3.push(userOrders);
                        }
                    }



                }
            }
            Result.push({ leval4: Result3 });
            for (let m = 0; m < Resultfull_3.length; m++) {
                const temp = Resultfull_3[m];
                for (let i = 0; i < temp.length; i++) {
                    const TEMP3 = await this.models.Commissiontree.find({
                        parent: temp[i]._id
                    }); // Level 4
                    Resultfull_4.push(TEMP3);

                    let userOrders = [];
                    for (let k = 0; k < TEMP3.length; k++) {
                        const item = TEMP3[k];
                        userOrders = [];
                        const totalPurchase = await this.models.Order.aggregate([
                            {
                                $project: {
                                    purchase: "$total.store_ap",
                                    month: { $month: "$createdAt" },
                                    buyer: "$buyer_id",
                                    OrderNo: "$OrderNo",
                                    status: "$status",
                                    products: "$products"
                                }
                            },
                            { $match: { month: parseInt(month), buyer: new mongo.ObjectID(item._id), status: "Paid" } }
                        ]);
                        if (totalPurchase.lenght != 0) {
                            // console.log("www", totalPurchase, item);
                            for (let j = 0; j < totalPurchase.length; j++) {
                                const purchase = totalPurchase[j];
                                let cost = 0, price = 0, margin = 0;

                                for (let l = 0; l < purchase.products.length; l++) {
                                    const product = purchase.products[l];
                                    console.log(product.product._id);
                                    const rateDt = await this.models.Product.findById(product.product._id);
                                    const qty = product.variants[0].order_qty;
                                    cost = cost + rateDt.brief.cost * qty;
                                    price = price + product.product.brief.price * qty;
                                    const teCost = rateDt.brief.cost * qty;
                                    const tePrice = product.product.brief.price * qty;
                                    margin = margin + tePrice - teCost;
                                    console.log("rateDt", rateDt.brief.price);
                                }
                                const dataset = {
                                    OrderNo: purchase.OrderNo, user_name: item.user_name, price, cost,
                                    commission: (margin * 2) / 100, _id: item.parent, margin
                                };
                                totalCommission = totalCommission + dataset.commission;
                                userOrders.push(dataset);
                            }

                        }
                        if (userOrders.length != 0) {
                            // console.log("dataset", userOrders)
                            Result4.push(userOrders);
                        }
                    }


                }
            }
            Result.push({ leval5: Result4 });

            // for (let m = 0; m < Resultfull_4.length; m++) {
            //     const temp = Resultfull_4[m];
            //     for (let i = 0; i < temp.length; i++) {
            //         const TEMP3 = await this.models.Commissiontree.find({
            //             parent: temp[i]._id
            //         });
            //         // Resultfull_5.push(TEMP3);
            //         let userOrders = [];
            //         for (let k = 0; k < TEMP3.length; k++) {
            //             const item = TEMP3[k];
            //             userOrders = [];
            //             const totalPurchase = await this.models.Order.aggregate([
            //                 {
            //                     $project: {
            //                         purchase: "$total.store_ap",
            //                         month: { $month: "$createdAt" },
            //                         buyer: "$buyer_id",
            //                         OrderNo: "$OrderNo",
            //                         status: "$status",
            //                         products: "$products"
            //                     }
            //                 },
            //                 { $match: { month: parseInt(month), buyer: new mongo.ObjectID(item._id), status: "Paid" } }
            //             ]);
            //             if (totalPurchase.lenght != 0) {
            //                 // console.log("www", totalPurchase, item);
            //                 for (let j = 0; j < totalPurchase.length; j++) {
            //                     const purchase = totalPurchase[j];
            //                     let cost = 0, price = 0, margin = 0

            //                     for (let l = 0; l < purchase.products.length; l++) {
            //                         const product = purchase.products[l];
            //                         console.log(product.product._id)
            //                         const rateDt = await this.models.Product.findById(product.product._id);
            //                         const qty = product.variants[0].order_qty;
            //                         cost = cost + rateDt.brief.cost * qty;
            //                         price = price + product.product.brief.price * qty;
            //                         const teCost = rateDt.brief.cost * qty;
            //                         const tePrice = product.product.brief.price * qty;
            //                         margin = margin + tePrice - teCost;
            //                         console.log('rateDt', rateDt.brief.price)
            //                     }
            //                     const dataset = {
            //                         OrderNo: purchase.OrderNo, user_name: item.user_name, price, cost,
            //                         commission: (margin * 2) / 100, _id: item.parent, margin
            //                     };
            //                     totalCommission = totalCommission + dataset.commission;
            //                     userOrders.push(dataset);
            //                 }

            //             }
            //             if (userOrders.length != 0) {
            //                 // console.log("dataset", userOrders)
            //                 Result5.push(userOrders);
            //             }
            //         }
            //         // if (userOrders.length != 0) {
            //         //     // console.log("dataset", userOrders)
            //         //     Result5.push(userOrders);
            //         // }

            //     }
            // }
            // Result.push({ leval5: Result5 });
            // console.log("Result", Result)
            return res.status(200).send({
                Result,
                totalCommission
            });
        } catch (error) {
            console.log(error);
            return next(error);
        }
    }

    public async  loadJoinedUnderCompany(req: Request,
        res: Response,
        next: NextFunction) {
        try {
            const comp = await this.models.Commissiontree.find({ "user_name": "Gstock" });
            const comp_id = comp[0]._id;
            const userDetails = [];
            const users = await this.models.Commissiontree.find({ "parent": comp_id });
            for (let i = 0; i < users.length; i++) {
                const user = users[i];
                const Details = await this.models.User.findById(user._id);
                // console.log("parent", emps, parent)
                if (Details) {
                    // console.log(Details.credential.email);
                    // console.log("empDetails", empDetails)
                    userDetails.push({
                        createdAt: Details.createdAt, email: Details.credential.email,
                        parent: "Gstock",
                        name: Details.profile.first_name + " " + Details.profile.last_name
                    });
                }

            }

            console.log(comp);
            res.send({ result: userDetails });

        } catch (error) {
            console.log(error);
            res.send(error);
        }

    }

    public async getStockRefNo(req: Request, res: Response, next: NextFunction) {
        try {
            const stockCount = await this.models.Stock.find().count();
            console.log("stock", stockCount);
            return res.status(201).send({ "inventoryRefNo": "SI-00" + (stockCount + 1) });
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }

    public async getSingleStockDetail(req: Request, res: Response, next: NextFunction) {
        try {
            const { stockId } = req.query;
            console.log(req.query);
            if (!ObjectID.isValid(stockId)) return res.status(400).send({ message: "Invalid Stock id" });
            return res.status(200).send(await this.models.Stock.findOne({ _id: new ObjectID(stockId) }));
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }

    public async updateStock(req: Request, res: Response, next: NextFunction) {
        try {
            const { stockId } = req.query;

            await this.models.Stock.findByIdAndUpdate(stockId, { $set: req.body });
            return res.status(200).send({ message: "Stock updated" });
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }
    public async deleteStock(req: Request, res: Response, next: NextFunction) {

        try {
            const { _id } = req.query;
            console.log("Stockid", _id);
            const stockFound = await this.models.Stock.findById(_id);
            console.log("stockFoundDoc", stockFound);
            if (!stockFound)
                return res.status(409).json({ message: "Stock not found" });
            //  await this.models.Stock.remove({"_id": _id});
            await this.models.Stock.findByIdAndUpdate(_id, { $set: { "Status": "Cancelled" } });
            console.log("--Codedelete--", stockFound.item[0].code);
            console.log("--Codedelete--", stockFound.item[0].qty);
            //  this.deleteQuantity(stockFound.item[0].code,-stockFound.item[0].qty);
            stockFound.item.forEach((prod) => {
                this.deleteQuantity(prod.code, -prod.qty);
            });
            return res.status(200).send({ "Status": "Cancelled" });
            return res.status(200).json({ message: "Stok is removed" });
        } catch (err) {

            res.status(500);
            return next(err);
        }
    }
    deleteQuantity(sku, userqty) {
        // console.log(code,qty);
        console.log("Code", sku);
        console.log("Qty", userqty);

        this.models.Product.update({ "brief.code": sku },
            {
                $inc: { "stock.qty": userqty, "brief.stock": userqty }
            }).then(reslt => {
                console.log("Log Created-->", reslt);
            }).catch(err => {
                console.log("error", err);
            });

    }
    public async category_fetch(req: Request,
        res: Response,
        next: NextFunction) {
        try {
            console.log(req.body);
            const { category } = req.body;

            const Category = await this.models.Product.find({ "brief.category": category });
            console.log(Category);
            return res.status(200).send(Category);

        } catch (error) {
            return next(error);
        }

    }


    public async category(req: Request,
        res: Response,
        next: NextFunction) {
        try {
            // console.log(req.body);
            // const{category}=req.body;

            const Category = await this.models.Product.find();
            console.log(Category);
            return res.status(200).send(Category);

        } catch (error) {
            return next(error);
        }

    }

    public async hotitems(req: Request, res: Response, next: NextFunction) {
        try {
            console.log("ITEM", req.body);
            const{item} = req.body.item;

            for (let i = 0; i < req.body.item.length; i++) {
                const singleitem = await this.models.HotItem.find({"item.code": req.body.item[i].code});
                console.log(singleitem, "single");
                if (singleitem.length == 0) {
                const hotItems = await new this.models.HotItem({
                    "item": req.body.item[i]
                }).save();
            } else {
                console.log(singleitem, "singleitem");
                return res.status(200).send({"message": "already exist in hotitem", "data": singleitem});
            }
            }

           // console.log(hotItems, "hotItems")
            return res.status(200).send({"message": "Hotitem added"});
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }

    public async gethotitems(req: Request, res: Response, next: NextFunction) {
        try {

         const    productList = [];
            const HotItems = await this.models.HotItem.find(
                {
                }
                );

              const Items = await this.models.Product.populate(HotItems, {
                path: "item.product_id",
                distinct: "products.product._id",
              });



        for (const key in Items) {
            for (let i = 0; i < Items[key].item.length; i++) {
              productList.push(
                Items[key].item[i].product_id
                ,
              );
            }
          }

             console.log(Items);
            return res.status(200).send(productList);
             } catch (e) {
            console.log(e);
            return next(e);
        }
    }

    public async gethotitemsList(req: Request, res: Response, next: NextFunction) {
        try {
               const HotItems = await this.models.HotItem.find(
                {
                }
                );
               return res.status(200).send(HotItems);
             } catch (e) {
            console.log(e);
            return next(e);
        }
    }
    public async deletehotitems(req: Request, res: Response, next: NextFunction) {
        try {
            console.log(req.query, "query");
            const query = req.query;
            console.log(query._id, "query._id");

         const deletehotitems = await this.models.HotItem.remove({"item.product_id": new ObjectID(query._id)});
         return res.status(200).send({message: "deleted"});
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }
}

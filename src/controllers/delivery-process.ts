import * as del from "del";
import { NextFunction, Request, Response } from "express";
import * as mkdirp from "mkdirp";
import { Types } from "mongoose";
import * as multer from "multer";
import * as fs from "fs-extra";
import paths from "../config/path";
import commentController from "./comment";
import { isArrayUnique } from "../_lib/array";
import { ObjectID } from "bson";
import { UserModel } from "../models/User";
import { flatten } from "../_lib/flatObjOnly";
import { isVariantPriceInRange } from "../_global/business";
import { Config } from "../types/app";
import { Passport } from "passport";
import { RSA_NO_PADDING } from "constants";

export class DeliveryProcessController {
    public models;
    public userPassport: Passport;

    constructor(private config) {
        this.models = config.models;
        this.userPassport = config.passport.userPassport.getUserPassport();
    }

    public async updateProduct(req: Request,
        res: Response,
        next: NextFunction) {
            try {
                let new_supp: any = {};
                console.log("req", req.body);
                const supplier =  req.body.name;
                const supp = await this.models.Supplier.findOne({name: supplier});
                new_supp = {
                    supplier: supp.name,
                    type: supp.type,
                    email1: supp.email1,
                    _id: supp._id
                };
                console.log("new data", new_supp);
                const updated_supp = await this.models.Product.update({supplier}, {$set: {suppliers: new_supp}}, {new: true, overwrite: true});
                return res.status(200).send({supplier: updated_supp});
            } catch (error) {
                return next(error);
            }
        }
    public async updateDeliveryStatus(req: Request,
        res: Response,
        next: NextFunction) {
            try {
                const orders = req.body;
                for (const key in orders) {
                    if (orders[key].hasOwnProperty("deli_status")) {
                        const delivery = await this.models.DeliveryProcess.update({"order_list.order_id": orders[key].order_id}, {"$set": {
                            "order_list.$.deli_status": orders[key].deli_status
                        }});
                        const order = await this.models.Order.update({_id: orders[key].order_id}, {"$set": {
                            "delivery_status": orders[key].deli_status
                        }});
                    }
                }
                return res.status(200).send({status: "Delivery status Updated"});
            } catch (error) {
                return next(error);
            }
        }

    // Get all Picked List
    public async getAllPickedList(req: Request,
        res: Response,
        next: NextFunction) {
            try {
                const dbQuery = {};
                const fromDate = req.body.fromDate;
                const toDate = req.body.toDate;
                dbQuery["deli_date"] = { $gte: fromDate, $lte: toDate };
                console.log(dbQuery);
                const picked = await this.models.DeliveryProcess.find(dbQuery).sort("-deli_date");
                return res.status(200).send(picked);

            } catch (error) {
                return next(error);
            }
    }

     // Get Order Status
     public async getOrderStatus(req: Request,
        res: Response,
        next: NextFunction) {
            try {
                const dbQuery = {};
                const fromDate = req.body.fromDate;
                const toDate = req.body.toDate;
                if (fromDate && toDate) {
                    dbQuery["order_list.order_date"] = { $gte: fromDate, $lte: toDate };
                }
                const picked = await this.models.DeliveryProcess.find(dbQuery).sort("-deli_date");
                return res.status(200).send(picked);

            } catch (error) {
                return next(error);
            }
    }


}
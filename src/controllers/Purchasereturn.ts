import * as del from "del";
import { NextFunction, Request, Response } from "express";
import * as mkdirp from "mkdirp";
import { Types } from "mongoose";
import * as multer from "multer";
import * as fs from "fs-extra";
import paths from "./../config/path";
import commentController from "./comment";
import { isArrayUnique } from "./../../src/_lib/array";
import { InvoiceModel } from "./../../src/models/Invoice";
import { ReturnModel } from "./../../src/models/Return";
import { ObjectID } from "bson";
import { UserModel } from "../models/User";
import { flatten } from "../_lib/flatObjOnly";
import { isVariantPriceInRange } from "../_global/business";
import { Config } from "../types/app";
import { Passport } from "passport";
import { RSA_NO_PADDING } from "constants";
import invoice from "../routes/user/admin/management/invoice";
//import return from "../routes/user/admin/management/return";

export class purchaseReturnController {
    public models;
    public userPassport: Passport;

    // config;
    constructor(private config) {
        // this.config = config;
        this.models = config.models;
        this.userPassport = config.passport.userPassport.getUserPassport();
    }

    public async updatePR(req: Request,
        res: Response,
        next: NextFunction) {
        try {
            const status = req.body.status
            console.log("body", req.body);
            if (status == "active") {
                const { Invoice_date,
                    Invoice_No,
                    OderDate,
                    Return_no,
                    Invoice_no,
                    Return_date,
                    PO_N0,
                    total,
                    gst,
                    gst_perc,
                    net_total, } = req.body;

                await this.models.PurchaseReturn.update({ Return_no: req.body.Return_no }, {
                    $set: {
                        "item": req.body.item,
                        Invoice_date,
                        Invoice_No,
                        OderDate,
                        Return_no,
                        Invoice_no,
                        Return_date,
                        PO_N0,
                        total,
                        gst,
                        gst_perc,
                        net_total,
                    }
                });
            } else {
                await this.models.PurchaseReturn.update({ Return_no: req.body.Return_no }, {
                    $set: {
                        status
                    }
                });
            }
            for(let i=0;i<req.body.item.length;i++){
                this.updateQuantity(req.body.item[i].code,req.body.item[i].qty);
                console.log("---Ucode---",req.body.item[i].code)
            }
            //  this.updateQuantity(req.body.item.code,-req.body.item.qty);
            // console.log("Code",req.body.item.code);
            //    console.log("qty",item[0].qty));
            return res.status(201).send({ item: req.body });
        } catch (error) {
            console.log("error", error);
            return next(error);
        }
    }

      updateQuantity(sku,userqty){
        // console.log(code,qty);
        console.log("Code",sku);
        console.log("Qty",userqty);
 
         this.models.Product.update({"brief.code":sku }, 
         { $inc: { "stock.qty":userqty, "brief.stock": userqty }                 
             }).then(reslt=>{
                 console.log("Log Created-->",reslt)
             })  .catch(err => {
                 console.log("error",err);
             });
 
     }


    public async createPurchaseReturn(
        req: Request,
        res: Response,
        next: NextFunction
    ) {

        console.log("purchaseSalesReturn")
        console.log(" - request : " + req)
      //  console.log("item", req.body.item.qty);
        try {
            const {
                Invoice_date,
                Invoice_No,
                OderDate,
                Return_no,
                Invoice_no,
                Return_date,
                PO_N0,
                total,
                gst,
                gst_perc,
                net_total,


            } = req.body;
            console.log(req.body);
            const query = await new this.models.PurchaseReturn({
                "item": req.body.item,
                //"status": "active",
                "Status": "Active",
                Invoice_date,
                Invoice_No,
                OderDate,
                Return_no,
                //   Invoice_no,
                Return_date,
                PO_N0,
                total,
                gst,
                gst_perc,
                net_total,


            }).save();
            for(let i=0;i<req.body.item.length;i++){
                this.updateprQuantity(req.body.item[i].code,req.body.item[i].qty);
                console.log("---Ccode---",req.body.item[i].code)
            }
         //   this.updateprQuantity(req.body.item.code,-req.body.item.qty);
            //console.log("PRCode",req.body.item.code);
            return res.status(201).send({ item: req.body });
           
             // return res.status(201).send(query);
                //   .then(item => {
                //     console.log("--CodeUpdate--",item[0].code);
                //     console.log("--CodeUpdate--",item[0].qty);
                //     this.updateQuantity(item[0].code,-item[0].qty);
                //      return res.status(201).send({ item: item });
                //   })
                //   .catch(err => {
                //      return res.status(400).send("unable to save to database");
                //  });
        } catch (e) {
            return next(e);
        }
    }
    updateprQuantity(sku,userqty){
         // console.log(code,qty);
         console.log("Code",sku);
        console.log("Qty",userqty);
 
          this.models.Product.update({"brief.code":sku }, 
          { $inc: { "stock.qty":userqty, "brief.stock": userqty }                 
              }).then(reslt=>{
                 console.log("Log Created-->",reslt)
             })  .catch(err => {
                 console.log("error",err);
              });
 
      }
    public async getReturnOrderNo(req: Request, res: Response, next: NextFunction) {
        try {
            let purchaseReturnCount = await this.models.PurchaseReturn.find().count();
            purchaseReturnCount++;
            var count = purchaseReturnCount.toString();
            const Return_no = count.padStart(3, '0');
            return res.status(201).send({ "Return_no": "PR-" + (Return_no) });
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }
    public async getAllPRDetails(req: Request, res: Response, next: NextFunction) {
        try {
            return res.status(200).send(await this.models.PurchaseReturn.find().sort("Return_no"));
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }
    public async deletePR(req: Request, res: Response, next: NextFunction) {

        try {
            const { _id } = req.query;
            const prFound = await this.models.PurchaseReturn.findById(_id);
            if (!prFound)
                return res.status(409).json({ message: "Purchase Return not found" });
                await this.models.PurchaseReturn.findByIdAndUpdate(_id, { $set: {"Status":"Cancelled"} });
           // await this.models.PurchaseReturn.remove({ "_id": _id });
           console.log("--Codedelete--",prFound.item[0].code);
           console.log("--Codedelete--",prFound.item[0].qty);
          // this.deletePrQuantity(prFound.item[0].code,prFound.item[0].qty);
        //   for(let i=0;i<req.body.item.length;i++){
        //     this.deletePrQuantity(prFound.item[i].code,prFound.item[i].qty);
        //  //   this.updateQuantity(req.body.item[i].code,req.body.item[i].qty);
        //     console.log("---Ucode---",req.body.item[i].code)
        // }
        prFound.item.forEach((prod) => {
            this.deletePrQuantity(prod.code, prod.qty)
        });
                 return res.status(200).send({"Status":"Cancelled"});
                return res.status(200).json({ message: "Purchase Return is removed" });
        } catch (err) {

            res.status(500);
            return next(err);
        }

    }
    deletePrQuantity(sku,userqty){
        // console.log(code,qty);
        console.log("Code",sku);
        console.log("Qty",userqty);
    
         this.models.Product.update({"brief.code":sku }, 
         { $inc: { "stock.qty":userqty, "brief.stock": userqty }                 
             }).then(reslt=>{
                 console.log("Log Created-->",reslt)
             })  .catch(err => {
                 console.log("error",err);
             });
    
     }
    public async getSinglePRDetail(req: Request, res: Response, next: NextFunction) {
        try {
            const query = req.query;
            if (!ObjectID.isValid(query._id)) return res.status(400).send({ message: "Invalid Purchase Return id" });
            return res.status(200).send(await this.models.PurchaseReturn.findOne({ _id: new ObjectID(query._id) }));
        }
        catch (e) {
            console.log(e);
            return next(e);
        }
    }




}
import * as del from "del";
import { NextFunction, Request, Response } from "express";
import * as mkdirp from "mkdirp";
import { Types } from "mongoose";
import * as multer from "multer";
import * as fs from "fs-extra";
import paths from "../config/path";
import commentController from "./comment";
import { isArrayUnique } from "../_lib/array";
import { PRODUCT_STATUS } from "../_status/status";
import { ProductModel } from "../models/Product";
import { ObjectID } from "bson";
import { UserModel } from "../models/User";
import { flatten } from "../_lib/flatObjOnly";
import { isVariantPriceInRange } from "../_global/business";
import { Config } from "../types/app";
import { Passport } from "passport";
import * as nodemailer from "nodemailer";
import * as Handlebars from "handlebars";
import * as dateFormat from "dateformat";

/**
 * Product app controller.
 */
export class InventoryController {
    public models;
    public userPassport: Passport;
    // config;
    constructor(private config) {
        // this.config = config;
        this.models = config.models;
        this.userPassport = config.passport.userPassport.getUserPassport();
    }

    public async sendEmail(req: Request, res: Response, next: NextFunction) {
        try {
            const { orderNo,
                doNo,
                orderDate,
                doDate,
                supplier,
                invNo,
                invDate,
                subTotal,
                gst,
                totalCost,
                item } = req.body;
            console.log(req.body);
            await this.models.PurchaseOrder.update({orderNo}, { $set: {email_status: "sent"} });

            const today = new Date();
            const supplier_details = await this.models.Supplier.find(
                { "name": supplier },
            );
            const productlist = [];
            item.forEach(element => {
                productlist.push({
                    products: {
                        name: element.name,
                        code: element.code
                    },
                    qty: element.qty,
                    unit: element.unit
                });
            });

            // Send purchase order summary to supplier
            const tomorrow = new Date(today.getTime() + (24 * 60 * 60 * 1000));
            const root = "./src/emailTemplate";
            const templatePath = `${root}/delivery_process.html`;
            const logo = "./src/public/img/67.jpg";
            let mailOptions = {};
            const smtpTransport = nodemailer.createTransport({
                port: 587,
                host: "smtp.office365.com",
                secure: false,
                auth: {
                    user: "no-reply@gstock.sg",
                    pass: "GSto2819"
                },
                tls: {
                    rejectUnauthorized: false
                }
            });

            fs.readFile(templatePath, (err, chunk) => {
                if (err) {
                    return next(err);
                }
                const html = chunk.toString("utf8");
                const template = Handlebars.compile(html);
                Handlebars.registerHelper("inc", function (value, options) {
                    return parseInt(value) + 1;
                });
                console.log("supplier_details", supplier_details);
                console.log("products", productlist);
                const data = {
                    fromEmail: "no-reply@gstock.sg",
                    mail_obj: productlist,
                    supplier: supplier_details[0],
                    logo,
                    delivery_date: dateFormat(tomorrow, "mediumDate"),
                };
                const result = template(data);
               mailOptions = {
                    to: [supplier_details[0].email1] ,
                    bcc: ["pratheesh@dinghe.sg"],
                    // bcc: ["dessa@dinghe.sg", "angi@dinghe.sg", "dhania@dinghe.sg", "pratheesh@dinghe.sg", "jovi@dinghe.sg", "eric@dinghe.sg"],
                    from: "no-reply@gstock.sg",
                    subject: "Order Details (" + dateFormat(today, "mediumDate") + ")",
                    html: result
                };
                const sendEmail = new Promise((resolve, reject) => {
                    smtpTransport.sendMail(mailOptions, (error, response: any) => {
                        if (error) {
                            console.log("eror", error);
                            reject(error);
                        } else {
                            resolve(response);
                        }
                    });
                });
                sendEmail
                    .then(function (successMessage) {
                        console.log("Success-->", successMessage);
                    })
                    .catch(function (errorMessage) {
                        // error handler function is invoked
                        console.log("error--->", errorMessage);
                    });
            });
            return res.status(201).send({ "purchaseOrder_id": "success"  });
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }
    public async createPO(req: Request, res: Response, next: NextFunction) {
        try {
            const { orderNo,
                doNo,
                orderDate,
                doDate,
                supplier,
                invNo,
                invDate,
                subTotal,
                gst,
                totalCost,
                item } = req.body;
            console.log(req.body);
            const savedPurchaseOrder = await new this.models.PurchaseOrder({
                "orderNo": orderNo,
                "doNo": doNo,
                "orderDate": orderDate,
                "doDate": doDate,
                "supplier": supplier,
                "invNo": invNo,
                "invDate": invDate,
                "subTotal": subTotal,
                "gst": gst,
                "totalCost": totalCost,
                "item": item
            }).save();

            return res.status(201).send({ "purchaseOrder_id": savedPurchaseOrder._id });
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }
    public async createGrn(req: Request, res: Response, next: NextFunction) {
        try {
            let _status = 1;
            const grn = await new this.models.GoodsReceived(req.body).save();
            const po = await this.models.PurchaseOrder.findOne({ orderNo: req.body.poNo });
            await req.body.item.forEach(element => {
                po.item.forEach(ele => {
                    if (ele.code == element.code) {
                        ele.delivered = element.delivered + parseInt(element.rcvd_qty);
                        if (ele.qty == ele.delivered) { ele.status = "delivered"; }
                    }
                });
                this.updateQuantity(element.code, element.rcvd_qty);
            });
            await po.item.forEach(element => {
                if (element.status == "active") _status = 0;
            });
            if (_status) po.status = "delivered";
            const updated_po = await this.models.PurchaseOrder.findByIdAndUpdate(po._id, { $set: { item: po.item, status: po.status } });
            return res.status(201).send({ item: po.item });
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }
    public async getAllGrn(req: Request, res: Response, next: NextFunction) {
        try {
            return res.status(200).send(await this.models.GoodsReceived.find());
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }

    public async getOrderNo(req: Request, res: Response, next: NextFunction) {
        try {
            let purchaseOrderCount = await this.models.PurchaseOrder.find().count();
            purchaseOrderCount++;
            const count = purchaseOrderCount.toString();
            const order_no = count.padStart(3, "0");
            return res.status(201).send({ "orderNo": "PO-" + (order_no) });
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }

    public async getGrnNo(req: Request, res: Response, next: NextFunction) {
        try {
            let GoodsReceivedCount = await this.models.GoodsReceived.find().count();
            GoodsReceivedCount++;
            const count = GoodsReceivedCount.toString();
            const grn_no = count.padStart(3, "0");
            return res.status(201).send({ "grnNo": "GRN-" + (grn_no) });
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }
    public async getGrnById(req: Request, res: Response, next: NextFunction) {
        try {
            const query = req.query;
            console.log(req.query);
            if (!ObjectID.isValid(query._id)) return res.status(400).send({ message: "Invalid Purchase Order id" });
            return res.status(200).send(await this.models.GoodsReceived.findOne({ _id: new ObjectID(query._id) }));
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }
    public async getAllPODetails(req: Request, res: Response, next: NextFunction) {
        try {
            if (req.query.type == "auto") {
                return res.status(200).send(await this.models.PurchaseOrder.find({ po_type: "auto" }));
            } else if (req.query.type == "manual") {
                return res.status(200).send(await this.models.PurchaseOrder.find({ po_type: { $ne: "auto" } }));
            } else {
                return res.status(200).send(await this.models.PurchaseOrder.find());
            }

        } catch (e) {
            console.log(e);
            return next(e);
        }
    }
    public async getAllActivePo(req: Request, res: Response, next: NextFunction) {
        try {
            return res.status(200).send(await this.models.PurchaseOrder.find({ status: "approved" }));
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }
    public async getAllActivePoAuto(req: Request, res: Response, next: NextFunction) {
        try {
            return res.status(200).send(await this.models.PurchaseOrder.find({ status: "approved", po_type: "auto" }));
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }
    public async getAllSuppliers(req: Request, res: Response, next: NextFunction) {
        try {
            return res.status(200).send(await this.models.Supplier.find());
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }
    public async getSinglePODetail(req: Request, res: Response, next: NextFunction) {
        try {
            const query = req.query;
            if (!ObjectID.isValid(query._id)) return res.status(400).send({ message: "Invalid Purchase Order id" });
            return res.status(200).send(await this.models.PurchaseOrder.findOne({ _id: new ObjectID(query._id) }));
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }

    public async updatePO(req: Request, res: Response, next: NextFunction) {
        try {
            const { _id } = req.query;
            const status = req.body.status;
            const poInGrn = await (this.models.GoodsReceived.find({ poId: _id, status: "active" }));
            if (poInGrn.length)
                return res.status(200).json({ status: "not updated" });
            // const old_Po = await this.models.PurchaseOrder.findById(_id);
            await this.models.PurchaseOrder.findByIdAndUpdate(_id, { $set: req.body });
            // req.body.item.forEach((prod) => {
            //     old_Po.item.forEach((selected) => {
            //         if (prod.code == selected.code) {
            //             const qty = selected.qty - prod.qty;
            //             console.log(qty)
            //             this.updateQuantity(prod.code, qty)
            //         }
            //     })
            // });
            return res.status(200).send({ message: "Stock updated" });
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }
    public async approvePO(req: Request, res: Response, next: NextFunction) {
        try {
            console.log(req.body);
            await this.models.PurchaseOrder.findByIdAndUpdate(req.body, { $set: {approve_status: "approved", status: "approved"} });

            return res.status(200).send({ message: "PO approved" });
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }

    public async deletePO(req: Request, res: Response, next: NextFunction) {

        try {
            const { _id } = req.query;
            const poFound = await this.models.PurchaseOrder.findById(_id);
            const poInGrn = await (this.models.GoodsReceived.find({ poId: _id, status: "active" }));
            if (!poFound)
                return res.status(409).json({ message: "Purchase Order not found" });
            if (poInGrn.length)
                return res.status(200).json({ status: "not updated" });
            await this.models.PurchaseOrder.findByIdAndUpdate(_id, { $set: { status: "Cancelled" } });
            //  poFound.item.forEach((prod) => {
            //     this.updateQuantity(prod.code, prod.qty)
            // });
            poFound.status = "Cancelled";
            return res.status(200).json({ po: poFound });
        } catch (err) {

            res.status(500);
            return next(err);
        }
    }
    public async deleteGrn(req: Request, res: Response, next: NextFunction) {
        try {
            const { _id } = req.query;
            const grnFound = await this.models.GoodsReceived.findById(_id);
            if (!grnFound)
                return res.status(409).json({ message: "Purchase Order not found" });
            await this.models.GoodsReceived.findByIdAndUpdate(_id, { $set: { status: "Cancelled" } });
            grnFound.item.forEach((prod) => {
                this.updateQuantity(prod.code, -prod.rcvd_qty);
            });
            grnFound.status = "Cancelled";
            return res.status(200).json({ grn: grnFound });
        } catch (err) {

            res.status(500);
            return next(err);
        }
    }
    updateQuantity(code, qty) {
        console.log(code, qty);
        this.models.Product.update({ "brief.code": code },
            {
                $inc: { "stock.qty": qty, "brief.stock": qty }
            }).then(reslt => {
                console.log("Log Created-->", reslt);
            }).catch(err => {
                console.log("error", err);
            });

    }
}
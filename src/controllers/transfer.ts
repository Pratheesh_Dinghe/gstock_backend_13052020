import * as del from "del";
import { NextFunction, Request, Response } from "express";
import * as mkdirp from "mkdirp";
import { Types } from "mongoose";
import * as multer from "multer";
import * as fs from "fs-extra";
import paths from "../config/path";
import { isArrayUnique } from "../_lib/array";
import { ObjectID } from "bson";
import { UserModel } from "../models/User";
import { flatten } from "../_lib/flatObjOnly";
import { isVariantPriceInRange } from "../_global/business";
import { Config } from "../types/app";
import { Passport } from "passport";
import { RSA_NO_PADDING } from "constants";
import * as nodemailer from "nodemailer";
import * as Handlebars from "handlebars";


export class TransferController {
    public models;
    public userPassport: Passport;

    constructor(private config) {
        this.models = config.models;
        this.userPassport = config.passport.userPassport.getUserPassport();
    }


     // Transfer to Credit Wallet
     public async save(req: Request,
        res: Response,
        next: NextFunction) {
            try {
                const { name,
                    email,
                    mobile,
                    amount,
                    transfer_to,
                    transfer_from
                    } = req.body;
                console.log(req.body);
                const transfer = await this.models.Transfer(req.body).save();
                const newEntry = new this.models.CreditWallet({
                    buyer_id: transfer_to,
                    memo: "Transfer",
                    amount: req.body.amount,
                    status: "Earned",
                    ref_id: transfer._id,
                    type: 1
                });
                await newEntry.save();
                const fromEntry = new this.models.CreditWallet({
                    buyer_id: transfer_from,
                    memo: "Transfer to " + email,
                    amount: req.body.amount,
                    status: "Used",
                    ref_id: transfer._id,
                    type: 1
                });
                await fromEntry.save();
                return res.status(200).send(transfer);
            } catch (error) {
                return next(error);
            }
    }
}
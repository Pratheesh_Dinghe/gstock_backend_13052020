import * as del from "del";
import { NextFunction, Request, Response } from "express";
import * as mkdirp from "mkdirp";
import { Types } from "mongoose";
import * as multer from "multer";
import * as fs from "fs-extra";
import paths from "./../config/path";
import commentController from "./comment";
import { isArrayUnique } from "./../../src/_lib/array";
import { PRODUCT_STATUS } from "../../src/_status/status";
import productSchema, { ProductModel } from "./../../src/models/Product";
import { ObjectID, ObjectId } from "bson";
import { UserModel } from "../models/User";
import { flatten } from "../_lib/flatObjOnly";
import { isVariantPriceInRange } from "../_global/business";
import { Config } from "../types/app";
import { Passport } from "passport";
// const fs = require("fs");
// const fastcsv = require("fast-csv");
/**
 * Product app controller.
 */
export class ProductController {
    public models;
    public userPassport: Passport;
    // config;
    constructor(private config) {
        // this.config = config;
        this.models = config.models;
        this.userPassport = config.passport.userPassport.getUserPassport();
    }
    public async queryFactory(q) {
        const {
            limit = 3,
            page = 1,
            select = "brief",
            name = "",
            low,
            high,
            category,
            status,
            store_name,
            intervals,
            sortby = "-date",
            product_id,
            brand,
            related = false
        } = q;
        const query: any = {
            /**
             * Default query
             */
            "brief.stock": { $gte: 1 },
            "brief.name": { $regex: name, $options: "i" },
            "pricing.discount_price": { "$nin": [null] },
        };
        if (low && !isNaN(low)) {
            query["pricing.discount_price"]["$gte"] = Number(low);
        }
        if (high && !isNaN(high)) {
            query["pricing.discount_price"]["$lte"] = Number(high);
        }
        // Find merchant based on store_name maybe large results in memory
        if (store_name) {
            const merchantID = await this.models.User.find({ "store.name": { $regex: store_name, $options: "i" } }, { select: "_id" }).lean() as Array<{ _id: string }>;
            query["merchant_id"] = { $in: merchantID.map(e => Types.ObjectId(e._id)) };
        }
        if (brand) {
            query["detail.product_brand"] = brand;
        }
        if (category) {
            const sub = category.split("*");
            query["brief.category"] = sub.length > 1 ? { $regex: new RegExp(`^${sub[0]}.+${sub[1]}`), $options: "i" } : { $regex: category, $options: "i" };
        }
        if (intervals) {
            const intervalsArray = intervals.split("/");
            // if (2 !== intervalsArray.length) return res.status(400).send({ message: "Invalid intervals format. Please ensure xxxx-xxxx." });
            const start = new Date(intervalsArray[0]);
            const end = new Date(intervalsArray[1]);
            query["updatedAt"] = { $gte: start, $lte: end };
        }

        let sort = "-" === sortby[0] ? "-" : "";
        query["status"] = status ? status : { $in: Object.keys(PRODUCT_STATUS).map(k => PRODUCT_STATUS[k]) };
        switch ("-" === sort ? sortby.slice(1) : sortby) {
            case "pricing":
                sort += "pricing.discount_price";
                break;
            case "date":
                sort += "updatedAt";
                break;
            default: sort += "pricing.discount_price";
        }
        // switch (role) {
        //     case "merchant": {
        //         // query["merchant_id"] = Types.ObjectId(req.locals.user.id);
        //         break;
        //     }
        //     case "buyer": {
        //         /**
        //          * Buyer will find all stock that is approved
        //          */
        //         query["is_active"] = true;
        //         query["status"] = { $in: [PRODUCT_STATUS.Approved] };
        //         break;
        //     }
        //     case "approved": {
        //         query["status"] = { $in: [PRODUCT_STATUS.Approved] };
        //         break;
        //     }
        //     case "admin": {
        //         query["status"] = undefined !== status ? status : { $in: Object.keys(PRODUCT_STATUS).map(k => PRODUCT_STATUS[k]) };
        //         query["brief.stock"] = { $exists: true };
        //         query["pricing.discount_price"]["$nin"] = [];
        //         break;
        //     }
        //     default: break;
        // }
    }
    public async getRelevantProduct(query) {
        if (query["brief.category"]) {
            delete query["brief.name"];
        }
        const r = await this.models.Product.aggregate([
            {
                $match: query
            },
            {
                "$group": {
                    "_id": {
                        "category": "$brief.category"
                    },
                    count: { $sum: 1 }
                }
            }
        ]).exec();
        return r;
    }
    // public async productList(
    //     req: Request,
    //     res: Response,
    //     next: NextFunction
    // ) {
    //     try {
    //         console.log("Details_", req.body);
    //         const user = await this.models.User.findOne({ "_id": new ObjectId(req.body.id) });
    //         const supplier = await this.models.Supplier.findOne({ "ic_no": user.profile.Uen_no });
    //         console.log("Supplier", supplier._id);
    //         console.log("USER", user.profile.Uen_no);
    //         const products = await this.models.Product.find({ "supplier._id": supplier._id });
    //         // const products = await this.models.Product.find({"supplier.supplier":req.body.supplier});
    //         //    for(let i=0;i<req.body.length;i++){
    //         //     if(req.body[i].supplier){
    //         //         const products = await this.models.Product.find({"supplier":req.body.supplier});
    //         //         return res.status(200).send(products);
    //         //     }
    //         //    }
    //         console.log("Products:", products);
    //         return res.status(200).send(products);
    //         //  return res.status(200).send(user);
    //     } catch (error) {
    //         console.log(error);
    //         return next(error);
    //     }
    // }
    public async productList(
        req: Request,
        res: Response,
        next: NextFunction
    ) {
        try {
            console.log("Details_", req.body);
            const user = await this.models.User.findOne({ "_id": new ObjectId(req.body.id) });
            const supplier = await this.models.Supplier.findOne({ "ic_no": user.profile.Uen_no });
            console.log("Supplier", supplier._id);
            console.log("USER", user.profile.Uen_no);
            //   query["is_active"] = true;
            //             query["status"] = { $in: [PRODUCT_STATUS.Approved] };
            const products = await this.models.Product.find({ $and: [{ "supplier._id": supplier._id }, {"is_active" : true}] });
            // const products = await this.models.Product.find({"supplier.supplier":req.body.supplier});
            //    for(let i=0;i<req.body.length;i++){
            //     if(req.body[i].supplier){
            //         const products = await this.models.Product.find({"supplier":req.body.supplier});
            //         return res.status(200).send(products);
            //     }
            //    }
            console.log("Products:", products);
            return res.status(200).send(products);
            //  return res.status(200).send(user);
        } catch (error) {
            console.log(error);
            return next(error);
        }
    }

    public async SaveStockManaging(
        req: Request,
        res: Response,
        next: NextFunction
    ) {
        try {
            console.log("Details_", req.body);
            const user = await this.models.User.findOne({ "_id": new ObjectId(req.body.user) });
            const supplier = await this.models.Supplier.findOne({ "ic_no": user.profile.Uen_no });

            const dataset = req.body.data;

            const update = await this.models.Product.findByIdAndUpdate(dataset._id, { "stock_avilability": dataset.status });

            const save = await new this.models.StockManaging({
                product_id: new ObjectId(dataset._id),
                product_name: dataset.name,
                product_code: dataset.sku,
                status: dataset.status,
                supplier: supplier._id,
                supplier_name: supplier.name
            }).save();
            return res.status(200).send({ resp: "saved" });
        } catch (error) {
            console.log(error);
            return res.status(500).send(error);
        }

    }

    public async getproduct(req: Request, res: Response, next: NextFunction) {
        try {
            return res.status(200).send(await this.models.Product.find());
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }


    public getProductsSearch(role) {

        console.log("check product search admin");
        return async (req: Request, res: Response, next: NextFunction) => {
            try {


                const {
                    limit = 4,
                    page = 1,
                    select = "brief",
                    name = "",
                    low,
                    high,
                    category,
                    status,
                    store_name,
                    intervals,
                    sortby = "-date",
                    product_id,
                    brand,
                    related = false,
                    start,
                    end,
                    international
                } = req.query;

                console.log("request----", req.query, product_id);
                if (product_id) {
                    return res.status(200).send(await this.models.Product.findById(product_id));
                }
                let query = {};
                // {
                //     "brief.attributes.value": { $regex: name, $options: "i" }
                // }
                if (brand) {
                    console.log("brand check");
                    query = {

                        "brief.stock": { $gte: 0 },
                        // "$or": [{ "brief.name": { $regex: name, $options: "i" } }, { "detail.product_brand": brand }],
                        "$or": [{ "brief.name": { $regex: name, $options: "i" } }, { "detail.product_brand": { $regex: brand, $options: "i" } }],

                        "pricing.discount_price": { "$nin": [null] },
                    };
                    // query["detail.product_brand"] = brand;
                } else {
                    query = {

                        "brief.stock": { $gte: 1 },
                        "brief.name": { $regex: name, $options: "i" },
                        "pricing.discount_price": { "$nin": [null] },
                    };
                }
                if (low && !isNaN(low)) {
                    query["pricing.discount_price"]["$gte"] = Number(low);
                }
                if (high && !isNaN(high)) {
                    query["pricing.discount_price"]["$lte"] = Number(high);
                }
                // Find merchant based on store_name
                if (store_name) {
                    const merchantID = await this.models.User.find({ "store.name": { $regex: store_name, $options: "i" } }, { select: "_id" }).lean() as Array<{ _id: string }>;
                    query["merchant_id"] = { $in: merchantID.map(e => Types.ObjectId(e._id)) };
                }

                if (category) {
                    console.log("CATEGORY SEARCH");
                    const sub = category.split("*");
                    if (sub.length > 1) {
                        const category = new RegExp(`^${sub[0]}.+${sub[1]}`);
                        query["$or"].push({ "brief.category": { $regex: category, $options: "i" } });
                    } else {
                        query["$or"].push({ "brief.category": { $regex: category, $options: "i" } });
                    }
                }
                if (intervals) {
                    console.log("INTERVALSSSS");
                    const intervalsArray = intervals.split("/");
                    if (2 !== intervalsArray.length) return res.status(400).send({ message: "Invalid intervals format. Please ensure xxxx-xxxx." });
                    const start = new Date(intervalsArray[0]);
                    const end = new Date(intervalsArray[1]);
                    console.log(start, end);
                    query["updatedAt"] = { $gte: start, $lte: end };
                }

                let sort = "-" === sortby[0] ? "-" : "";
                query["status"] = status ? status : { $in: Object.keys(PRODUCT_STATUS).map(k => PRODUCT_STATUS[k]) };
                switch ("-" === sort ? sortby.slice(1) : sortby) {
                    case "pricing":
                        sort += "pricing.discount_price";
                        break;
                    case "date":
                        sort += "updatedAt";
                        break;
                    default: sort += "pricing.discount_price";
                }
                switch (role) {
                    case "merchant": {
                        query["merchant_id"] = Types.ObjectId(req.locals.user.id);
                        break;
                    }
                    case "buyer": {
                        /**
                         * Buyer will find all stock that is approved
                         */
                        query["is_active"] = true;
                        query["status"] = { $in: [PRODUCT_STATUS.Approved] };
                        if (international) {
                            query["international"] = international;
                        }

                        break;
                    }
                    case "approved": {
                        query["status"] = { $in: [PRODUCT_STATUS.Approved] };
                        break;
                    }
                    case "admin": {
                        query["status"] = undefined !== status ? status : { $in: Object.keys(PRODUCT_STATUS).map(k => PRODUCT_STATUS[k]) };
                        query["brief.stock"] = { $exists: true };
                        query["pricing.discount_price"]["$nin"] = [];
                        query["is_active"] = true;
                        break;
                    }
                    default: break;
                }
                const options = {
                    populate: [
                        {
                            path: "merchant_id",
                            select: "email store createdAt",
                            model: this.models.User
                        },
                        { path: "category_id", model: this.models.Category },
                    ],
                    lean: true,
                    page: Number(page),
                    limit: Number(limit),
                    sort,
                    select: select.split("/").join(" ") + " merchant_id createdAt stock_avilability"
                };
                if (start) {
                    query["createdAt"] = { "$gte": new Date(start) };
                }
                if (end) {
                    query["createdAt"] = { "$lt": new Date(end) };
                }
                if (start && end) {
                    query["createdAt"] = {
                        "$gte": new Date(start),
                        "$lt": new Date(end)
                    };
                }
                console.log("SINGAPORE", query);

                const result = await this.models.Product.aggregate([
                    {
                        "$match": query
                    },
                    {
                        "$group": {
                            "_id": null,
                            "max_price": { "$max": "$pricing.discount_price" },
                            "min_price": { "$min": "$pricing.discount_price" }
                        }
                    }
                ]).exec();
                const products = await this.models.Product.paginate(query, options);
                // console.log("STOCK UNIT", products)
                // console.log("INTERNATIONAL", query)

                return res.status(200).send({
                    ...products,
                    related: related ? await this.getRelevantProduct(query) : [],
                    ...result[0]
                });
            } catch (error) {
                console.log(error);
                return next(error);
            }
        };
    }



    public getProducts(role) {

        console.log("Hello Merchant");
        return async (req: Request, res: Response, next: NextFunction) => {
            try {


                const {
                    limit,
                    page = 1,
                    select = "brief",
                    name = "",
                    low,
                    high,
                    category,
                    status,
                    store_name,
                    intervals,
                    sortby = "-date",
                    product_id,
                    brand,
                    related = false,
                    start,
                    end,
                    international,
                    sku,
                    supplier,
                    stock_avilability,
                    isSubCateg
                } = req.query;

                // console.log("ayyyyayyooooo11", req.query);
                if (product_id) {
                    return res.status(200).send(await this.models.Product.findById(product_id));
                }
                const query: any = {
                    /**
                       * Default query
                       */
                    "brief.stock": { $gte: -100 },
                    "brief.name": { $regex: name, $options: "i" },
                    "pricing.discount_price": { "$nin": [null] },
                };
                // {
                //     "brief.attributes.value": { $regex: name, $options: "i" }
                // }
                if (low && !isNaN(low)) {
                    query["pricing.discount_price"]["$gte"] = Number(low);
                }
                if (high && !isNaN(high)) {
                    query["pricing.discount_price"]["$lte"] = Number(high);
                }
                // Find merchant based on store_name
                if (store_name) {
                    const merchantID = await this.models.User.find({ "store.name": { $regex: store_name, $options: "i" } }, { select: "_id" }).lean() as Array<{ _id: string }>;
                    query["merchant_id"] = { $in: merchantID.map(e => Types.ObjectId(e._id)) };
                }
                if (supplier) {
                  query["supplier.supplier"] = supplier;
                //   query['supplier']=supplier;
                }
                if (brand) {
                    console.log("brand check 33");

                    // query["detail.product_brand"] = brand;
                     query["detail.product_brand"] = { $regex: brand, $options: "i" };
                    //  "detail.product_brand": { $regex: brand, $options: "i" }
                }
                // if (category) {
                //     const sub = category.split("*");
                //     if (sub.length > 1) {
                //         const category = new RegExp(`^${sub[0]}.+${sub[1]}`);
                //         query["brief.category"] = { $regex: category, $options: "i" };
                //     } else {
                //         query["brief.category"] = { $regex: category, $options: "i" };
                //     }
                // }

                if (category) {
                    console.log("STOP", category);
                    if (isSubCateg) {
                          console.log("check isSubCateg");
                        const sub = category.split("*");
                         if (sub.length > 1) {
                             const category = new RegExp(`^${sub[0]}.+${sub[1]}`);
                            console.log("subcategory search", category);
                            query["brief.category"] = category;
                         } else {
                              console.log("subcategory search11", category);
                            query["brief.category"] = category;
                        }
                    }
                    else {
                        console.log("111");
                        const sub = category.split("*");
                        if (sub.length > 1) {
                            const category = new RegExp(`^${sub[0]}.+${sub[1]}`);
                            query["brief.category"] = { $regex: category, $options: "i" };
                        } else {
                            query["brief.category"] = { $regex: category, $options: "i" };
                        }
                    }
                }
                if (intervals) {
                    console.log("INTERVALSSSS");
                    const intervalsArray = intervals.split("/");
                    if (2 !== intervalsArray.length) return res.status(400).send({ message: "Invalid intervals format. Please ensure xxxx-xxxx." });
                    const start = new Date(intervalsArray[0]);
                    const end = new Date(intervalsArray[1]);
                    console.log(start, end);
                    query["updatedAt"] = { $gte: start, $lte: end };
                }
                if (stock_avilability) {
                    query["stock_avilability"] = true;
                }
                let sort = "-" === sortby[0] ? "-" : "";
                query["status"] = status ? status : { $in: Object.keys(PRODUCT_STATUS).map(k => PRODUCT_STATUS[k]) };
                switch ("-" === sort ? sortby.slice(1) : sortby) {
                    case "pricing":
                        sort += "pricing.discount_price";
                        break;
                    case "date":
                        sort += "updatedAt";
                        break;
                    default: sort += "pricing.discount_price";
                }
                switch (role) {
                    case "merchant": {
                        query["merchant_id"] = Types.ObjectId(req.locals.user.id);
                        break;
                    }
                    case "buyer": {
                        /**
                         * Buyer will find all stock that is approved
                         */
                        query["is_active"] = true;
                        query["status"] = { $in: [PRODUCT_STATUS.Approved] };
                        if (international) {
                            query["international"] = international;
                        }

                        break;
                    }
                    case "approved": {
                        query["status"] = { $in: [PRODUCT_STATUS.Approved] };
                        break;
                    }
                    case "admin": {
                        query["status"] = undefined !== status ? status : { $in: Object.keys(PRODUCT_STATUS).map(k => PRODUCT_STATUS[k]) };
                        query["brief.stock"] = { $exists: true };
                        query["pricing.discount_price"]["$nin"] = [];
                        query["is_active"] = true;
                        if (sku) {
                            // query["detail.sku"] = sku;
                         query["detail.sku"] = { $regex: sku, $options: "i" };

                        }
                        break;
                    }
                    default: break;
                }
                const options = {
                    populate: [
                        {
                            path: "merchant_id",
                            select: "email store createdAt",
                            model: this.models.User
                        },
                        { path: "category_id", model: this.models.Category },
                    ],
                    lean: true,
                    page: Number(page),
                    limit: Number(limit),
                    sort,
                    select: select.split("/").join(" ") + " merchant_id createdAt stock_avilability"
                };
                if (start) {
                    query["createdAt"] = { "$gte": new Date(start) };
                }
                if (end) {
                    query["createdAt"] = { "$lt": new Date(end) };
                }
                if (start && end) {
                    query["createdAt"] = {
                        "$gte": new Date(start),
                        "$lt": new Date(end)
                    };
                }
                // console.log("SINGAPORE", query,supplier);

                // const result = await this.models.Product.aggregate([
                //     {
                //         "$match": {
                //             query
                //         }
                //         // "$match":{
                //         //     "$and":[
                //         //       {"$or":[
                //         //         {"supplier.supplier":supplier},
                //         //         {"supplier":supplier}
                //         //       ]},
                //         //       {query},
                //         //     ]
                //         //  }
                //     },
                //     {
                //         "$group": {
                //             "_id": null,
                //             "max_price": { "$max": "$pricing.discount_price" },
                //             "min_price": { "$min": "$pricing.discount_price" }
                //         }
                //     }
                // ]).exec();


  const result = await this.models.Product.aggregate([
                    {
                        "$match": query
                    },
                    {
                        "$group": {
                            "_id": null,
                            "max_price": { "$max": "$pricing.discount_price" },
                            "min_price": { "$min": "$pricing.discount_price" }
                        }
                    }
                ]).exec();


                // console.log("result",result);
                const products = await this.models.Product.paginate(query, options);
                // console.log("STOCK UNIT", products)
                console.log("INTERNATIONAL", query);
                console.log("entha pblm", products);

                return res.status(200).send({
                    ...products,
                    related: related ? await this.getRelevantProduct(query) : [],
                    ...result[0]
                });
            } catch (error) {
                return next(error);
            }
        };
    }

    public async isProductActiveOrInPendingOrder(req: Request, res: Response, next: NextFunction) {
        const { product } = req.body;
        try {
            const product_id = product.map(id => Types.ObjectId(id));
            const inOrder = await this.models.Order.findOne({
                "products.product._id": { $in: product_id },
                "status": { $ne: "GR" }
            }).lean();
            if (inOrder) { return res.status(400).send({ "message": "This product has pending order" }); }
            const isActive = await this.models.Product.findOne(
                {
                    "_id": { $in: product_id },
                    is_active: true
                }).lean() as ProductModel;
            if (isActive) { return res.status(400).send({ "message": "This product is still active" }); }

            next();
        } catch (error) {
            return next(error);
        }
    }

    public adminGetProducts() {
        return this.getProducts("admin");
    }
    public async adminUpdateProductStatus(req: Request, res: Response, next: NextFunction) {
        try {
            const { product_id, status } = req.body;
            const productIds = [].concat(product_id).map(id => Types.ObjectId(id));
            const contentToUpdate = {
                status: "Approved" === status ? PRODUCT_STATUS.Approved : PRODUCT_STATUS.Rejected,
            };
            const product = await this.models.Product.update({ _id: { $in: productIds } }, { $set: { ...contentToUpdate, is_active: "Approved" === status } }, { multi: true });
            await this.models.Variant.update({ product_id: { $in: productIds } }, { $set: contentToUpdate }, { multi: true });
            return res.status(200).json({ message: `Product ${"Approved" === status ? "approved" : "rejected"}!` });
        } catch (e) {
            res.status(400);
            return next(e);
        }
    }

    /**
     *
     * Admin Create Project
     *
     * */

    public async adminCreateProduct(
        req: Request,
        res: Response,
        next: NextFunction
    ) {
        try {
            // const { category, category_id, attributes, country = { name: '', code: '' }, international } = req.body;
            const { category, category_id, attributes, country, countryofOrigin, supplier, deliveryCost, international } = req.body;
            console.log("SUPPLIER ADDED", req.body);
            console.log("iDDD", supplier._id);
            console.log("iDDD", supplier.supplier);
            const product = await new this.models.Product({
                merchant_id: Types.ObjectId("5a017ec2818c02c9a798e8d9"),
                category_id: Types.ObjectId(category_id),
                "brief.category": category,
                "brief.attributes": attributes,
                //   "supplier._id": supplier._id,
                // "supplier.supplier":supplier.supplier,
                international,
                //   country: { name: country.name, code: country.code }
                country,
                countryofOrigin,
                supplier: { _id: supplier._id, supplier: supplier.supplier },
                deliveryCost
            }).save();
            return res.status(201).send({ product_id: product.id });
        } catch (e) {
            return next(e);
        }
    }


    // public async adminUpdateProducts(req: Request, res: Response, next: NextFunction) {
    //     console.log("UPDATE PRODUCT")
    //     const { product_id: productId } = req.query;
    //     const blacklist = ["status", "merchant_id", "is_active"];
    //     if (!isArrayUnique(blacklist.concat(Object.keys(req.body)))) return res.status(200).send({ message: "Invalid Update" });
    //     const update = flatten(req.body, ".", "attributes");
    //     if (update["stock.qty"] < 1) { return res.status(400).send({ message: "Stock must be more than 0" }); }
    //     if (update["stock.qty"]) update["brief.stock"] = update["stock.qty"];
    //     try {
    //         const product = await this.models.Product.findByIdAndUpdate({
    //             "_id": productId,
    //         }, { $set: update }).lean();
    //         return res.status(200).json({ "message": "Product updated!" });
    //     } catch (err) {
    //         res.status(400);
    //         return next(err);
    //     }
    // }






    public async adminUpdateProducts(
        req: Request,
        res: Response,
        next: NextFunction
    ) {
        console.log("1111111111");
        console.log("NEW PRODUCT UPDATESSS111111111111", req.body);
        // const merchantId = req.locals.user.id;
        console.log("Welcome" + "Update Products");
        const update = flatten(req.body, ".", "attributes");
        console.log("adminUpdateProducts");
        console.log(update);
        const {
            product_id,
            "stock.qty": stockQty,
            "stock.unit": unit,
            "brief.short_description": shortDescription,
            "brief.product_name": productName,
            "brief.discount": discount,
            "brief.price": productPrice,
            "brief.discount_rate": discountrate,
            "brief.discount_price": discountprice,
            "brief.cost": cost,
            "detail.product_brand": productBrandName,
            "detail.sku": sku,
            "pricing.discount_rate": discount_rate,
            "pricing.discount_rate1": discountrate1,
            "pricing.discount_price1": discountprice1,
            "pricing.discount_rate2": discountrate2,
            "pricing.discount_price2": discountprice2,
            "commission.level1": level1,
            "commission.level2": level2,
            "commission.level3": level3,
            "commission.level4": level4,
            "commission.level5": level5
        } = update;
        if (stockQty) {
            update["brief.stock"] = update["stock.qty"]; // To be backward compatitble with old product imported.
        }

        // check if pricing is valid
        // if (!discount) {
        //     update["pricing.discount_rate"] = 0;
        // }
        if (discount_rate) {
            if (discount_rate < 0 || discount_rate >= 100) {
                return res
                    .status(400)
                    .send({ message: "Disocunt rate must be within 1-99 range" });
            }
        }
        // update["detail.product_brand"] = productBrandName;
        if (productPrice) {
            update["pricing.discount_price"] =
                productPrice * (1 - (discount_rate || 0) / 100);
        }
        try {
            console.log("Product" + product_id);
            const product = await this.models.Product.findOneAndUpdate(
                {
                    _id: Types.ObjectId(product_id)
                },
                { $set: update }
            );
            console.log("product pricing" + product.pricing);
            if (!product) {
                return res.status(404).json({ message: "Product not found" });
            }
            // Check if product base price is changed,

            if (productPrice && productPrice !== product.brief.price) {
                // if yes set all variant to base price to product base price
                console.log("Updating all variant base price");
                await this.models.Variant.update(
                    { product_id: Types.ObjectId(product_id) },
                    { $set: { price: productPrice } },
                    { multi: true }
                );
            }
            return res.status(200).send({ message: "Product updated!" });
        } catch (err) {
            console.log(err);
            res.status(400);
            return next(err);
        }
    }
    public async adminDeleteSingleProduct(req: Request, res: Response, next: NextFunction) {
        const { product_id } = req.query;
        try {
            const product = await this.models.Product.findByIdAndRemove(product_id);
            await this.models.Variant.remove({ product_id: Types.ObjectId(product_id) });
            fs.removeSync(
                `${paths.appDir}/${paths.userUploadDir}/${product.merchant_id}/products/${product_id}/`
            );
            return res.status(200).send({ message: "Product deletion successful" });
        } catch (error) {
            return next(error);
        }
    }
    public async adminFindOrphangeProducts(req: Request, res: Response, next: NextFunction) {
        await this.models.Product.find().select("_id");
    }

    public merchantGetProduct() {
        return this.getProducts("merchant");
    }
    public async merchantCreateProduct(req: Request, res: Response, next: NextFunction) {
        try {
            const {
                category,
                category_id,
                attributes
            } = req.body;
            const product = await new this.models.Product({
                merchant_id: Types.ObjectId(req.locals.user.id),
                category_id: Types.ObjectId(category_id),
                "brief.category": category,
                "brief.attributes": attributes
            }).save();
            return res.status(201).send({ "product_id": product.id });
        } catch (e) {
            return next(e);
        }
    }
    public async  merchantUpdateProduct(req: Request, res: Response, next: NextFunction) {
        const merchantId = req.locals.user.id;
        const update = flatten(req.body, ".", "attributes");
        const {
            product_id,
            "stock.qty": stockQty,
            "brief.short_description": shortDescription,
            "brief.product_name": productName,
            "brief.discount": discount,
            "brief.price": productPrice,
            "detail.product_brand": productBrandName,
            "pricing.discount_rate": discountRate,
        } = update;
        if (stockQty) {
            update["brief.stock"] = update["stock.qty"]; // To be backward compatitble with old product imported.
        }

        // check if pricing is valid
        if (!discount) {
            update["pricing.discount_rate"] = 0;
        }
        if (discountRate < 0 || discountRate >= 100) {
            return res.status(400).send({ message: "Disocunt rate must be within 1-99 range" });
        }

        update["pricing.discount_price"] = productPrice * (1 - (discountRate || 0) / 100);
        try {
            const product = await this.models.Product.findOneAndUpdate(
                {
                    "_id": Types.ObjectId(product_id),
                    "merchant_id": Types.ObjectId(merchantId)
                },
                { $set: update });
            if (!product) {
                return res.status(404).json({ message: "Product not found" });
            }
            // Check if product base price is changed,

            if (productPrice && productPrice !== product.brief.price) {
                // if yes set all variant to base price to product base price
                console.log("Updating all variant base price");
                await this.models.Variant.update(
                    { product_id: Types.ObjectId(product_id) },
                    { $set: { price: productPrice } },
                    { multi: true }
                );
            }
            return res.status(200).send({ message: "Product updated!" });
        } catch (err) {
            res.status(400);
            return next(err);
        }
    }
    // public async merchantUpdateProductActive(req: Request, res: Response, next: NextFunction) {
    //     const { product_id } = req.query;
    //     const merchant_id = req["user_id"];
    //     if (!res.locals.user.is_active) {
    //         return res.status(400).send({ message: "Inactivated merchant has no authorization to perform this action" });
    //     }

    //     const product = await this.models.Product.findOne({ "_id": Types.ObjectId(product_id), "merchant_id": Types.ObjectId(merchant_id) });
    //     if (!product.is_active) {
    //         if (product.stock.qty < 1) { return res.status(400).send({ message: "Insufficient quantity" }); }
    //     }
    //     product.is_active = !product.is_active;
    //     await product.save();
    //     return res.status(200).send({ message: "Product status changed" });
    // }

    public async merchantUpdateProductActive(
        req: Request,
        res: Response,
        next: NextFunction
    ) {
        console.log("INACTIVEEE");
        const { product_id } = req.query;
        // const merchant_id = req["user_id"];
        const merchant_id = "5a017ec2818c02c9a798e8d9";
        console.log("ACTIVEEEEEEEEEEEEEEEE", merchant_id);
        // Commented as this is for GLUX Modification , GLUX Itself is the Merchant.
        // console.log("IS_ACTIVE", res.locals.user.is_active);
        // if (!res.locals.user.is_active) {
        //   return res.status(400).send({
        //     message:
        //       "Inactivated merchant has no authorization to perform this action"
        //   });
        // }
        console.log("ACTIVEEEEEEEEEEEEEEEE");
        const product = await this.models.Product.findOne({
            _id: Types.ObjectId(product_id),
            merchant_id: Types.ObjectId(merchant_id)
        });
        if (!product.is_active) {
            if (product.stock.qty < 1) {
                return res.status(400).send({ message: "Insufficient quantity" });
            }
        }
        product.is_active = !product.is_active;
        await product.save();
        return res.status(200).send({ message: "Product status changed" });
    }
    public async merchantDeleteProduct(req: Request, res: Response, next: NextFunction) {
        try {
            const { product } = req.body;
            const product_id = product.map(id => Types.ObjectId(id));
            await this.models.Product.remove({
                "merchant_id": Types.ObjectId(req["user_id"]),
                "_id": { $in: product_id }
            }).lean();
            await this.models.Variant.remove({ product_id: { $in: product_id } });
            product.forEach(
                (product_id) => {
                    fs.removeSync(
                        `${paths.appDir}/${paths.userUploadDir}/${req["user_id"]}/products/${product_id}/`
                    );
                }
            );
            return res.status(201).send({ message: "Product deleted" });
        } catch (e) {
            return next(e);
        }
    }

    public async putStoreName(req: Request, res: Response, next: NextFunction) {
    }
    // merchantGetImageUrl: async (req: Request, res: Response, next: NextFunction) => {
    //     try {
    //         fs.readdir(paths.appDir + paths.userUploadDir + "/" + req["user_id"] + "/products/" + req.params["productId"] + "/", (error, paths) => { return res.status(200).json(paths); });
    //     } catch (err) {
    //         res.status(400);
    //         return next(err);
    //     }
    // },
    public async  merchantUploadProductImageLimit(req: Request, res: Response, next: NextFunction) {
        const { productId } = req.params;
        try {
            if (productId === undefined) return res.status(400).send({ message: "Invalid product ID" });
            // this upload function is trigger once per image.
            const theProduct = await this.models.Product.findById(productId);
            if (!theProduct) return res.status(409).send({ message: "product not found" });
            if ((theProduct.brief.images.length + 1) >= 12) return res.status(400).json({ message: "Maximum upload number is 12." });
            return next();
        } catch (err) {
            next(err);
        }
    }
    public async  merchantUploadProductImage(req: Request, res: Response, next: NextFunction) {
        console.log("SALES RETURN IMAGE UPLOAD");
        console.log("IMGE UPLOAD", req.files);
        const { productId } = req.params;
        await this.models.Product.findByIdAndUpdate(productId, { $addToSet: { "brief.images": req.files[0].originalname } });
        return res.status(200).json({ message: "Image uploaded" });
    }
    public async  merchantUploadProductDescriptionImage(req: Request, res: Response, next: NextFunction) {
        return res.status(200).json({ "link": `https://gstock.sg/buyer.gstock/uploads/user/${req["user_id"]}/products/${req["product_id"]}/description/${req["originalName"]}` });
    }
    public async  merchantRemoveProductImage(req: Request, res: Response, next: NextFunction) {
        try {
            console.log("IMAGE REMOVED", req.locals);
            const { remove } = req.body;
            const { productId } = req.params;
            // console.log(`${paths.appDir + paths.userUploadDir}/${req.locals.user.id}/products/${productId}`);
            // console.log("USERID", req.locals.user.id);
            console.log("appDir", paths.appDir);
            console.log("userUploadDir", paths.userUploadDir);
            const d = [].concat(remove).map(e => `${paths.appDir + paths.userUploadDir}/undefined/products/${productId}/${e}`);
            await this.models.Product.findByIdAndUpdate(
                productId,
                { $pull: { "brief.images": { $in: [].concat(remove) } } });
            const filesRemovedFromDisk = await del(d);
            return res.status(200).json({ message: filesRemovedFromDisk.length + " files removed from disk" });
        } catch (err) {
            return next(err);
        }
    }

    public async  merchantCreateVariant(req: Request, res: Response, next: NextFunction) {
        try {
            const {
                variants: variantsSubmitted,
                product_id
            } = req.body;
            const user_id = req["user_id"];
            // check price
            if (!variantsSubmitted.length) {
                return res.status(200).send({ message: "0 variant added" });
            }
            const product = await this.models.Product.findById(product_id);
            if (!product) {
                return res.status(404).send({ "message": "Product not found" });
            }

            const outOfRangeVariant = variantsSubmitted.filter(v => !isVariantPriceInRange(product.brief.price, v.price));
            if (outOfRangeVariant.length) {
                return res.status(400).send({ "message": "Variant price out of range. Must be -50% to 100% of base price." });
            }
            const variantsFound = await this.models.Variant.find({
                "product_id": Types.ObjectId(product_id),
                "merchant_id": Types.ObjectId(user_id)
            });
            if (variantsFound) {
                const optionValuesNew = variantsFound.concat(variantsSubmitted).map(item => item["option_value"]);
                if (!isArrayUnique(optionValuesNew)) {
                    return res.status(409).send({ "message": "Variant name and value conflicts. Please ensure you provide unique variant name for a single product" });
                }
            }
            const result = await this.models.Variant.insertMany(
                variantsSubmitted.map(e => ({
                    ...e,
                    product_id,
                    merchant_id: user_id
                })
                ));
            return res.status(200).send({ message: `${variantsSubmitted.length} variant added`, data: result });
        } catch (err) {
            res.status(400);
            return next(err);
        }
    }
    public async  merchantGetVariant(req: Request, res: Response, next: NextFunction) {
        try {
            const { product_id } = req.query;
            const user_id = req["user_id"];
            const result = await this.models.Variant.find({
                "product_id": Types.ObjectId(product_id),
                "merchant_id": Types.ObjectId(user_id)
            });
            return res.status(200).send(result);
            // return res.status(200).send({ message: `${variantsSubmitted.length} variant added` });
        } catch (err) {
            res.status(400);
            return next(err);
        }
    }
    public async merchantUpdateVariant(req: Request, res: Response, next: NextFunction) {
        try {
            const { variant_id } = req.body;
            // check quantity
            console.log(req.body);
            const user_id = req["user_id"];
            const variant = await this.models.Variant.findOne({
                _id: variant_id,
                merchant_id: user_id
            });
            const product = await this.models.Product.findById(variant.product_id);
            // check price
            if (req.body.price && !isVariantPriceInRange(product.brief.price, req.body.price)) { return res.status(400).send({ message: "Variant price should be between -50% and +100% of product listing price." }); }
            await this.models.Variant.findOneAndUpdate(
                {
                    _id: variant_id,
                    merchant_id: user_id
                }, { $set: req.body });
            if (!variant) { return res.status(400).json({ message: "No variant of such product is found. Please go to product page to add its variants" }); }
            return res.status(200).json({ message: "Variant updated!" });
        } catch (err) {
            res.status(400);
            return next(err);
        }
    }
    public async merchantDeleteVariant(req: Request, res: Response, next: NextFunction) {
        const { variant_id } = req.body;
        console.log(variant_id);
        try {
            const result = await this.models.Variant.findByIdAndRemove(variant_id);
            if (!result) { return res.status(404).send({ message: "No such variant found!" }); }
            return res.status(200).send({ message: "Variant removed" });
        } catch (error) {
            return next(error);
        }
    }

    // public getBuyerProducts() {
    //     // console.log("ok");
    //     return this.getProducts("buyer");
    // }
    public getApprovedProducts() {
        return this.getProducts("buyer");
    }
    public getProductSearch() {
        return this.getProductsSearch("buyer");
    }
    public async  getProductsVariants(req: Request, res: Response, next: NextFunction) {

        try {
            const { product_id } = req.query;
            // "status": PRODUCT_STATUS.Approved
            if (!ObjectID.isValid(product_id)) return res.status(400).send({ message: "Invalid product id" });
            // if (!result.length) { return res.status(200).send([]); }
            // const pendingStockOfVaraints = await getPendingStock(models, product_id, true);
            // pendingStockOfVaraints.forEach(p => {
            //     const r = result.find(r => JSON.stringify(r.id) === JSON.stringify(p._id.variant_id));
            //     console.log(r);
            //     r.stock -= p.total;
            // });
            return res.status(200).send(await this.models.Variant.find({ product_id: new ObjectID(product_id) }));
        } catch (error) {
            return next(error);
        }
    }
    // public async  upload(req: Request, res: Response, next: NextFunction) {

    //     try {
    //         const {Product, Supplier, Category} = this.config.models;
    //         console.log(req.file);
    //         let path = "";
    //         const storage = multer.diskStorage({ // multers disk storage settings
    //             destination (req, file, cb) {
    //                 cb(null, "uploads/");
    //             },
    //             filename (req, file, cb) {
    //                 console.log("file----", file);
    //                 const datetimestamp = Date.now();
    //                 cb(null, file.fieldname + "-" + datetimestamp + "." + file.originalname.split(".")[file.originalname.split(".").length - 1]);
    //             }
    //         });
    //         const upload = multer({ storage }).single("file");
    //         upload(req, res, function (err) {
    //             console.log("request", req.file);
    //             if (err) {
    //                 // An error occurred when uploading
    //                 console.log(err);
    //                 return res.status(422).send("an Error occured");
    //             }
    //             // No error occured.
    //             const stream = fs.createReadStream(req.file.path);
    //             const csvData = [];
    //             const saveData = [];
    //             const csvStream = fastcsv
    //             .parse()
    //             .on("data", function(data) {
    //                 console.log("dta", data);
    //                const re = /And/gi;
    //                 const str = data[7];
    //                 const newstr = str.replace(re, "&");
    //                 csvData.push({
    //                     merchant_id: "5a017ec2818c02c9a798e8d9",
    //                     visibility: data[0],
    //                     "brief.name": data[1],
    //                     "brief.code": data[2],
    //                     "brief.short_description": data[3],
    //                     "brief.price": data[4],
    //                     "brief.cost": data[5],
    //                     "brief.stock": data[6],
    //                     "brief.category": "," + newstr + "," + data[8] + ",",
    //                     "detail.long_description": data[9],
    //                     "detail.barcode": data[10],
    //                     "detail.sku": data[11],
    //                     "detail.product_brand": data[12],
    //                     "pricing.discount_rate": data[13],
    //                     "pricing.discount_price": data[14],
    //                     "pricing.final_price":  data[15],
    //                     "pricing.min_price":  data[16],
    //                     "pricing.max_price":  data[17],
    //                     "stock.qty": data[18],
    //                     "stock. min_qty": data[19],
    //                     "stock.unit": data[20],
    //                     is_active: true,
    //                     country: data[21],
    //                     countryofOrigin: data[22],
    //                     "supplier.supplier": data[23],
    //                     deliveryCost: data[24],
    //                 });
    //             })
    //             .on("end", function() {
    //                 csvData.shift();
    //                 csvData.map((i) => {
    //                     Supplier.findOne({name: i["supplier.supplier"]}).then(function(supplier) {
    //                       if (supplier) i["supplier._id"] = supplier._id;
    //                         const cata = i["brief.category"];
    //                         Category.findOne({path: cata})
    //                         .then(function (cat) {
    //                             if (cat) i["category_id"] = cat._id;
    //                             Product.find({"brief.code": i["brief.code"]})
    //                             .then(function(prod) {
    //                                 if (!prod.length) {
    //                                     console.log("product not exist");
    //                                     const product = new Product(i);
    //                                     product.save()
    //                                     .then((result) => {
    //                                         saveData.push(result);
    //                                     }).catch((err) => {
    //                                         console.error(err);
    //                                         res.status(500).json({err});
    //                                     });
    //                                 } else {
    //                                     console.log("product exist", prod[0]._id);
    //                                     Product.update({ _id: (prod[0]._id) }, { $set:  i  })
    //                                     .then((result) => {
    //                                         console.log("result", result);
    //                                     })
    //                                     .catch((err) => {
    //                                         console.error(err);
    //                                         res.status(500).json({err});
    //                                     });
    //                                 }
    //                             });
    //                         }).catch((err) => {
    //                             console.error(err);
    //                             res.status(500).json({err});
    //                         });
    //                     }).catch((err) => {
    //                         console.error(err);
    //                         res.status(500).json({err});
    //                     });
    //                 });
    //             });
    //             if (saveData.length === csvData.length) {
    //                 stream.pipe(csvStream);
    //                 path = req.file.path;
    //                 return res.send("Upload Completed for " + path);
    //             }
    //         });

    //     } catch (error) {
    //         return next(error);
    //     }
    // }

    public getSingleProductDetail(requester) {

        console.log("GET PRODUCT DETAILS1111111111111");
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const { product_id } = req.query;
                console.log("PRODUCTID1212121", product_id);
                if (!ObjectID.isValid(product_id)) return res.status(400).send({ message: "Invalid product id" });
                const comments = await commentController(this.config.role).getComments(product_id);
                const query = {
                    _id: Types.ObjectId(product_id),
                };
                switch (requester) {
                    case "buyer": {
                        query["status"] = PRODUCT_STATUS.Approved;
                        query["is_active"] = true;
                    }
                        break;
                }
                const p = await this.models.Product.findOne(query).populate({ path: "category_id", populate: { path: "attributes" } }).lean() as ProductModel;
                if (!p) { return res.status(404).send(p); }
                console.log("PRODUCTID1212121", p);
                // await this.models.User.populate(p, { path: "merchant_id", select: "credential.email commission store profile" });
                // console.log(`The product base price is ${p.brief.price}, discounted price is ${p.pricing.discount_price} and commission rate is ${p.category_id["commission"]}`);
                // p["reward_pts"] = (p.brief.price * p.category_id["commission"] / 100 * 5).toFixed(2);
                // p["comments"] = comments;
                // console.log("HELLOOOo");
                // const variants: any = await this.models.Variant.find({
                //     "product_id": Types.ObjectId(product_id),
                //     // "status": PRODUCT_STATUS.Approved
                // }).lean();
                // variants.forEach(v => v["reward_pts"] = (v.price * p.category_id["commission"] / 100 * 5).toFixed(2));
                // console.log("GET PRODUCT DETAILS222222222");
                return res.status(200).send(
                    {
                        product: p
                        // ,
                        // variants,
                    }
                );
                // const r = await getPendingStock(models, product_id, false);
                // if (r.length) pendingStock = r[0].total;
            } catch (err) {
                return next(err);
            }
        };
    }
    public async getProductBySupplier(req: Request, res: Response, next: NextFunction) {
        try {
            const supplier = req.body;
            console.log(req.body);
            const products = await this.models.Product.find({
                $or:
                    [
                        {
                            "supplier.supplier": req.body.supplier
                        },
                        {
                            supplier: req.body.supplier
                        }
                    ]

            });
            return res.status(201).send(products);
        } catch (e) {
            return next(e);
        }
    }
    public async  getRelevantProductFromStore(req: Request, res: Response, next: NextFunction) {
        try {
            console.log("RELEVANT PRODUCTS");
            const { product_id, self = 0 } = req.query;
            if (!ObjectID.isValid(product_id)) return res.status(400).send({ message: "Invalid product id" });
            const {
                brief,
                merchant_id
            } = await this.models.Product.findById(product_id);
            const category = brief.category.split(",")[1];
            const query = {
                "brief.stock": { $gte: 0 },
                _id: { $ne: new ObjectID(product_id) },
                "brief.category": { $regex: category },
                status: PRODUCT_STATUS.Approved,
                is_active: true,
                // merchant_id: !parseInt(self) ? { "$ne": merchant_id } : merchant_id
            };
            return res.status(200).send(await this.models.Product.find(query).limit(4).sort("-createdAt"));
        } catch (error) {
            return next(error);
        }
    }
    public async getAllSupplierProduct(req: Request, res: Response, next: NextFunction) {
        try {
            let products = [];
            const grouped = {};

             const supplier = await this.models.Supplier.find();
             const supplierId = [];
             for (let i = 0; i < supplier.length; i++) {
                supplierId.push(supplier[i]._id);
              //  const sender = supplier[i]._id

             }
             products = await this.models.Product.find({ "supplier._id": supplierId, is_active: true});
            //  for(let j=0;j<products.length;j++){
            //     const sender = products[j].supplier._id;
            //     if (!grouped[sender]) { grouped[sender] = []; }
            //     console.log("group", grouped)
            //     grouped[sender].push({ wallet: products[j], });
            //  }
             console.log("supplierId", supplierId);
             console.log("supplier", supplier);
             console.log("supplier.lenght", supplier.length);
             console.log("products", products);
             console.log("products.length", products.length);

          // return res.status(201).send(grouped);
          return res.status(201).send(products);
        } catch (e) {
            return next(e);
        }
    }

    public async getProductBySku(req: Request, res: Response, next: NextFunction) {
        try {

             console.log("sku", req.body);
            // const sku=await this.models.Product.find({"details.sku":req.body.sku})
             const products = await this.models.Product.find({"brief.code": req.body.sku});
            // console.log("SKUproducts",products)
             return res.status(201).send(products);
        } catch (e) {
            return next(e);
        }
    }

    public async getProductByName(req: Request, res: Response, next: NextFunction) {
        try {
            const supplier = req.body;
            console.log("NAME", req.body);
            // const sku=await this.models.Product.find({"details.sku":req.body.sku})
           const products = await this.models.Product.find({"brief.name": req.body.name});
            console.log("Nameproducts", products);
            return res.status(201).send(products);
        } catch (e) {
            return next(e);
        }
    }

    public async getNormalSupplierProduct(req: Request, res: Response, next: NextFunction) {
        try {

            let products = [];
            const grouped = {};
            // const sku=await this.models.Product.find({"details.sku":req.body.sku})

             const supplier = await this.models.Supplier.find({type: "Normal"});
             const supplierId = [];
             for (let i = 0; i < supplier.length; i++) {
                supplierId.push(supplier[i]._id);
              //  const sender = supplier[i]._id

             }
             products = await this.models.Product.find({
                "supplier._id": supplierId,
                is_active: true});
            //  for(let j=0;j<products.length;j++){
            //     const sender = products[j].supplier._id;
            //     if (!grouped[sender]) { grouped[sender] = []; }
            //     console.log("group", grouped)
            //     grouped[sender].push({ wallet: products[j],type:"Normal" });
            //  }
             console.log("supplierId", supplierId);
             // $and: [{ type: "Normal" },{ type: "Consignment"}]
             console.log("supplier", supplier);
             console.log("supplier.lenght", supplier.length);
             console.log("products", products);
             console.log("products.length", products.length);
             return res.status(201).send(products);
           // return res.status(201).send(grouped);
        } catch (e) {
            return next(e);
        }
    }
    public async getConsignmentSupplierProduct(req: Request, res: Response, next: NextFunction) {
        try {

            let products = [];
            const grouped = {};
            // const sku=await this.models.Product.find({"details.sku":req.body.sku})

             const supplier = await this.models.Supplier.find({type: "Consignment"});
             const supplierId = [];
             for (let i = 0; i < supplier.length; i++) {
                supplierId.push(supplier[i]._id);
              //  const sender = supplier[i]._id

             }
             products = await this.models.Product.find({
                "supplier._id": supplierId,
                is_active: true});
            //  for(let j=0;j<products.length;j++){
            //     const sender = products[j].supplier._id;
            //     if (!grouped[sender]) { grouped[sender] = []; }
            //     console.log("group", grouped)
            //     grouped[sender].push({ wallet: products[j],type:"Consignment" });
            //  }
             console.log("supplierId", supplierId);
             // $and: [{ type: "Normal" },{ type: "Consignment"}]
             console.log("supplier", supplier);
             console.log("supplier.lenght", supplier.length);
             console.log("products", products);
             console.log("products.length", products.length);
             return res.status(201).send(products);
            // return res.status(201).send(grouped);
        } catch (e) {
            return next(e);
        }
    }


}
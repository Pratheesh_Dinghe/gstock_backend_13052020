
import * as del from "del";
import { NextFunction, Request, Response } from "express";
import * as mkdirp from "mkdirp";
import { Types } from "mongoose";
import * as multer from "multer";
import * as fs from "fs-extra";
import paths from "./../config/path";
import commentController from "./comment";
import { isArrayUnique } from "./../../src/_lib/array";
import { InvoiceModel } from "./../../src/models/Invoice";
import { ObjectID } from "bson";
import { UserModel } from "../models/User";
import { flatten } from "../_lib/flatObjOnly";
import { isVariantPriceInRange } from "../_global/business";
import { Config } from "../types/app";
import { Passport } from "passport";
import { RSA_NO_PADDING } from "constants";
import invoice from "../routes/user/admin/management/invoice";

export class StockverifyController {
    public models;
    public userPassport: Passport;
    // config;
    constructor(private config) {
        // this.config = config;
        this.models = config.models;
        this.userPassport = config.passport.userPassport.getUserPassport();
    }


    public async createStockverify(req: Request, res: Response, next: NextFunction) {
        try {
            console.log("REQUESTbody----",req.body);
            const { name,code,brand,stock,Discrepancy,updted_stk,verifydate,verfNo} = req.body;
            const savedStock = await new this.models.Stockverify({
            name,code,brand,stock,Discrepancy,updted_stk,verifydate,verfNo
           }).save();
        //  this.updateQuantity(code,-Discrepancy);
        this.updateQuantity(code,Discrepancy);
             return res.status(201).send(savedStock);
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }

    updateQuantity(sku,userqty){
        // console.log(code,qty);
        console.log("Code",sku);
        console.log("Qty",userqty);
 
         this.models.Product.update({"brief.code":sku }, 
         { $inc: { "stock.qty":userqty, "brief.stock": userqty }                 
             }).then(reslt=>{
                 console.log("Log Created-->",reslt)
             })  .catch(err => {
                 console.log("error",err);
             });
 
     }
     public async getStockVerfNo(req: Request, res: Response, next: NextFunction){
        try {
            let stockCount = await this.models.Stockverify.find().count();
            console.log("stock",stockCount);
            return res.status(201).send({ "inventoryverfNo": "ADJ-00"+ (stockCount+1 ) });
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }
    public async getAllStockverifyDetail(req: Request, res: Response, next: NextFunction){
        try {
            return res.status(200).send(await this.models.Stockverify.find()); 
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }

    public async getStockverifyDetailById(req: Request, res: Response, next: NextFunction) {
        try {
            const { stockId } = req.query;
            console.log(req.query);
            if (!ObjectID.isValid(stockId)) return res.status(400).send({ message: "Invalid Stock id" });
            return res.status(200).send(await this.models.Stockverify.findOne({ _id: new ObjectID(stockId) }));
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }

}

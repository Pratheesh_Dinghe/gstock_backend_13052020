import * as del from "del";
import { NextFunction, Request, Response } from "express";
import * as mkdirp from "mkdirp";
import { Types } from "mongoose";
import * as multer from "multer";
import * as fs from "fs-extra";
import paths from "./../config/path";
import commentController from "./comment";
import { isArrayUnique } from "./../../src/_lib/array";
import { InvoiceModel } from "./../../src/models/Invoice";
import { ObjectID } from "bson";
import { UserModel } from "../models/User";
import { flatten } from "../_lib/flatObjOnly";
import { isVariantPriceInRange } from "../_global/business";
import { Config } from "../types/app";
import { Passport } from "passport";
import { RSA_NO_PADDING } from "constants";
import invoice from "../routes/user/admin/management/invoice";

export class InvoiceController {
    public models;
    public userPassport: Passport;
    // config;
    constructor(private config) {
        // this.config = config;
        this.models = config.models;
        this.userPassport = config.passport.userPassport.getUserPassport();
    }
    public async getLatest(req: Request,
        res: Response,
        next: NextFunction) {

        try {
            const result = await this.models.Invoice.findOne().sort({ invoice_date: -1 }).limit(1);
            if (result) return res.status(200).send(result);
            else return res.status(200).send({});
        }
        catch (err) {
            return next(err);
        }
    }

    public async getInvoiceById(req: Request,
        res: Response,
        next: NextFunction) {
        try {
            const { _id } = req.params.id;
            const result = await this.models.Invoice.findById(req.params.id);
            return res.status(200).send(result);
        } catch (error) {
            return next(error);
        }
    }

    public async getAllInvoices(req: Request,
        res: Response,
        next: NextFunction) {
        try {
            const invoices = await this.models.Invoice.find().sort("-invoice_date");
            return res.status(200).send(invoices);

        } catch (error) {
            return next(error);
        }
    }

    public async updateSalesInvoice(req: Request,
        res: Response,
        next: NextFunction) {
        try {
            const status = req.body.status
            if (status == "active") {
                const { name, address, city, phone, net_total, total, gst_perc, gst, discount, invoice_date, invoice_no } = req.body;
                const old_invoice = await this.models.Invoice.findOne({ invoice_no: req.body.invoice_no });
                await this.models.Invoice.update({ invoice_no: req.body.invoice_no }, {
                    $set: {
                        "item": req.body.item,
                        net_total, total, gst_perc, gst, discount, name, address, city, phone, invoice_date, status
                    }
                });

                req.body.item.forEach((prod) => {
                    old_invoice.item.forEach((selected) => {
                        if (prod.code == selected.code) {
                            const qty = selected.qty - prod.qty;
                            console.log(qty)
                            this.updateQuantity(prod.code, qty)
                        }
                    })
                });
            } else {
                await this.models.Invoice.update({ invoice_no: req.body.invoice_no }, { $set: { status } });
                req.body.item.forEach((prod) => {
                    this.updateQuantity(prod.code, prod.qty)
                });
            }
            return res.status(201).send({ item: req.body });
        } catch (error) {
            console.log("error", error);
            return next(error);
        }
    }


    public async createSalesInvoice(
        req: Request,
        res: Response,
        next: NextFunction) {
        try {
            const { name, address, city, phone, net_total, total, gst_perc, gst, discount, invoice_date, invoice_no } = req.body;
            const invoice = await new this.models.Invoice({
                "item": req.body.item,
                "status": "active",
                net_total, total, gst_perc, gst, discount, name, address, city, phone, invoice_date, invoice_no
            }).save();
            req.body.item.forEach((prod) => {
                this.updateQuantity(prod.code, -prod.qty)
            });
            return res.status(201).send({ item: req.body });
        } catch (e) {
            return next(e);
        }
    }

    updateQuantity(code, qty) {
        console.log(code, qty);
        this.models.Product.update({ "brief.code": code },
            {
                $inc: { "stock.qty": qty, "brief.stock": qty }
            }).then(reslt => {
                console.log("Log Created-->", reslt)
            }).catch(err => {
                console.log("error", err);
            });

    }

    public async getAllSalesReturns(
        req: Request,
        res: Response,
        next: NextFunction) {
        try {
            const items = await this.models.BuyerSalesReturn.find();
            console.log(items)
            return res.status(201).send({ items });
        } catch (e) {
            console.log(e)
            return next(e);
        }
    }

    public async getSalesReturnbyid(
        req: Request,
        res: Response,
        next: NextFunction) {
        try {
            const item = await this.models.BuyerSalesReturn.findById(req.params.id);
            // console.log(items)
            return res.status(201).send({ item });
        } catch (e) {
            console.log(e)
            return next(e);
        }

    }

    public async updateReturnStatus(
        req: Request,
        res: Response,
        next: NextFunction) {
        try {
            const item = await this.models.BuyerSalesReturn.findByIdAndUpdate(req.body._id, { status: req.body.status });
            // console.log(items)
            return res.status(201).send({ result: "updated" });
        } catch (e) {
            console.log(e)
            return next(e);
        }
    }

}
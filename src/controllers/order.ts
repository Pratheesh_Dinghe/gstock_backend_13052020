import { NextFunction, Request, Response } from "express";
import { Types, mongo } from "mongoose";
import * as nodemailer from "nodemailer";
import * as fs from "fs";
import * as Handlebars from "handlebars";
import * as dateFormat from "dateformat";
// import { Config } from "./../app";
import { sendEmail } from "./../emailTemplate/email_manager";
import { db } from "./../firebase";
import commentCtrl from "./comment";
import { ORDER_STATUS, PRODUCT_STATUS, COMMISSION_STATUS } from "./../../src/_status/status";
import { calculateDiscountPrice } from "../_global/business";
import { MerchantCommission } from "../models/User";
import { DeliveryProcessController } from ".";
import { BuyerModels } from "../models";
import productSchema from "../models/Product";
const debug = require("debug")("express:router");

interface OrderAccepted {

    carts: Array<string>;
    orders:
    [{
        merchant_id: string,
        products: [{
            product_id: string;
            "variants": [
                {
                    cart_id: string,
                    variant_id: string;
                    "qty": number
                }
            ]
        }],
        delivery: [{
            S_address: string;
            S_unit_no: string;
            S_postal_code: string;
            shipping_fee: number;
            shipping_type: string;
            S_contact_no: string;
            S_recepient: string;
            shipment_id: string;
            Shipping_date: string;
            Shipping_time: String;
            B_address: string;
            B_unit_no: string;
            B_postal_code: string;
            B_contact_no: string;
            B_recepient: string;
        }]
    }];
    promo_codes: Array<string>;
}
/*}
 * Order controller.
 */
function decimal(num, sf = 2) {
    return Math.round(num * Math.pow(10, sf)) / Math.pow(10, sf);
}

export default (config) => {
    const {
        User,
        Product,
        Category,
        Variant,
        Order,
        Promotion,
        RewardPts,
        Cart,
        Complaint,
        CreditWallet,
        DeliveryProcess,
        FailedEmailsLog,
        Supplier,
        DeliveryAddress,
        PurchaseOrder,
        Jobs
    } = config.models;
    async function getDeliveryDetail(req: Request, res: Response, next: NextFunction) {
        // if (!req.body.delivery) return res.status(400).json({ "message": "Invalid delivery form." });
        // console.log("getDeliveryDetail");
        // console.log("2222222222222222");
        return next();
    }
    async function getOrderDetail(req: Request, res: Response, next: NextFunction) {
        try {
            // console.log("getOrderDetail");
            // console.log("333333333333", req.body);
            const {
                // promo_codes: promoCodes,
                orders
            } = req.body as OrderAccepted;
            await User.populate(orders, { path: "merchant_id", select: "commission" });
            await Product.populate(orders, { path: "products.product_id", select: "brief detail pricing category_id" });
            // await Category.populate(orders, { path: "products.product_id.category_id", select: "commission" });
            await Variant.populate(orders, { path: "products.variants.variant_id" });
            res.locals.orderDetails = orders;
            return next();
        } catch (error) {
            console.log(error);
            throw ("Failed to get order details");
        }
    }


    // async function GetDeliveryCharge(req: Request, res: Response, next: NextFunction) {
    //     try {
    //         console.log("GetDeliveryCharge")


    //     }
    //     catch (error) {
    //         console.log(error)
    //         throw ("Failed to get order details");
    //     }
    // }
    async function getPromotionDetailFromPromoCode(req: Request, res: Response, next: NextFunction) {
        try {
            // console.log("getPromotionDetailFromPromoCode");

            // console.log("44444444444444444");
            const { promo_codes: promoCodes } = req.body as OrderAccepted;
            const promotions = await Promotion.find({ promo_code: { $in: promoCodes }, status: "Active" });
            // console.log("44444", res.locals.user);
            const firstOrderOnlyPromo = promotions.filter(p => p.first_order);
            if (firstOrderOnlyPromo.length) {
                const order = await Order.findOne({
                    buyer_id: res.locals.user.id,
                    "promotion.promo_code": { $in: firstOrderOnlyPromo.map(f => f.promo_code) }
                });
                if (order) { return res.status(400).send({ "message": "You have used promotion code." }); }
            }
            // console.log(res.locals);
            res.locals.promotions = promotions;
            next();
        } catch (error) {
            console.log(error);
            throw ("Failed to verify promotion codes");
        }
    }
    async function processStore(req: Request, res: Response, next: NextFunction) {
        // console.log("processStore");
        console.log("555555555555", req.body);

        // console.log("delivery check", req.body.delivery);
        // const userId = req.body.user_id;
        const userId = req["user_id"];
        // console.log("BUYER IDD", req.body.user_id);
        const creditamnt = req.body.creditwallet_amnt;
        // console.log("sample", creditamnt);

        const SeqNo = await Order.aggregate([{ $group: { _id: null, max: { $max: "$SequenceNo" } } }]);
        // console.log("Sequence Value", SeqNo);
        // console.log("Sequence Value", SeqNo == [] ? "001" : SeqNo[0].max + 1);
        // console.log("SeqNo", SeqNo);
        const seq = SeqNo.length == 0 ? 1 : SeqNo[0].max + 1;

        // var str = "" + seq;
        // var pad = "0000";
        // var ans = pad.substring(0, pad.length - str.length) + str;
        // var sequenNo = SeqNo.length == 0 ? "ORD-001" : "ORD-" + ans;
        // const sequenNo = 1002;
        // const seq = 10;

        const str = "" + seq;
        const sequenNo = SeqNo.length == 0 ? "ORD-1" : "ORD-" + str;
        // var sequenNo = 1;
        // var seq = 1;
        const {
            orderDetails,
            promotions
        } = res.locals;
        const carts = [];
        const ordersForReport = orderDetails.map(
            store => {
                const {
                    memo,
                    merchant_id: {
                        _id: merchantId
                    },
                    delivery
                } = store;
                const {
                    cart,
                    products,
                    freeShipping
                } = processProducts(store, promotions);
// console.log("processStore-products", products[0]["product"]);
// console.log("processStore-products", products[1]["product"]);

                carts.push(...cart);
                const total = calculateTotal(products);
                const {
                    product_total_bp: productTotalBefPromo,
                    product_total_ap: productTotalAftPromo,
                    commission_amount: commissionAmount

                } = total;
                total["store_bp"] = productTotalBefPromo;
                total["store_ap"] = productTotalAftPromo + delivery.shipping_fee;
                total["commission"] = commissionAmount;
                total["creditwallet_amnt"] = creditamnt;
                Object.keys(total).forEach(k => total[k] = decimal(total[k]));
                return {
                    memo,
                    buyer_id: userId,
                    OrderNo: sequenNo,
                    SequenceNo: seq,
                    merchant_id: merchantId,
                    free_shipping: freeShipping,
                    delivery,
                    products,
                    total,
                    promotions
                };
            });
        res.locals.cart = carts;
        res.locals.orderForReport = ordersForReport;
        next();
    }
    function processProducts(store, promotions) {
        console.log("processProductsbrief", store.products.brief );
        const cart = [];
        let freeShipping = false;
        const products = store.products
            .map(p => {
                // product["_id"] = productId;
                // const commission = calculateCommission(category, specialCommissionAssignedToMerchant);
                const {
                    freeShipping: free_shipping,
                    product_total_bp,
                    product_total_ap,
                    variantsAftProc,
                    cart: cartsFromVariant,
                    total_commission: commission,
                    total_reward_pts: reward_pts
                } = processVariants(store, p, promotions);
                if (freeShipping || free_shipping) { freeShipping = true; }
                cart.push(...cartsFromVariant);
                console.log("variantsAftProc", variantsAftProc);



                // if ( variantsAftProc[0].promo_id) {
                //     if (  variantsAftProc[0].promo_id == undefined) {
                //     // p.product_id.brief.price = variantsAftProc[0].promo_price;
                //     }
                //     else
                //     p.product_id.brief.price = variantsAftProc[0].promo_price;
                // }
               console.log("PRODUCT-AFTERVARIENTPRICE", p);
                return {
                    purchase: {
                        product_total_bp,
                        product_total_ap,
                        commission,
                        commission_amount: product_total_bp * commission,
                        /* total * commission_rate * reward_pts_rate * 100 */
                        reward_pts
                        // : calculateRewardPts(product_total_bp, commission, 0.05)
                    },
                    product: p.product_id,
                    variants: variantsAftProc
                    // products: p
                };
            });
        console.log("processProducts finished-02", products);
        // console.log("processProducts finished-Sub-0", products[0]["product"]);
        // console.log("processProducts finished-Sub-1", products[1]["product"]);
        return {
            products,
            cart,
            freeShipping
        };
    }

    // function processVariants(store, baseProduct, promotions) {
    //     // console.log("PROCESS VARIANTSSS");
    //     const {
    //         merchant_id: {
    //             _id: merchantId
    //             // commission: specialCommissionAssignedToMerchant
    //         }
    //     } = store;
    //     const {
    //         product_id: product,
    //         variants: variantsBefProc
    //     } = baseProduct;
    //     // console.log("product", product);
    //     const {
    //         category_id: category,
    //         category_id: {
    //             _id: categoryId
    //         },
    //         brief: {
    //             discount,
    //             price: priceBefDiscount,
    //         },
    //         pricing: {
    //             discount_rate,
    //             discount_price
    //         }
    //     } = product;
    //     // console.log("CHECK");
    //     const variantsHolder = [];
    //     const cart = [];
    //     let product_total_bp = 0;
    //     let product_total_ap = 0;
    //     let total_commission = 0;
    //     let total_reward_pts = 0;
    //     let freeShipping = false;

    //     variantsBefProc.forEach(
    //         (v) => {
    //             const {
    //                 variant_id: variant,
    //                 qty,
    //                 cart_id
    //             } = v;
    //             const price = variant ? variant["price"] : discount_price;
    //             const priceAftDiscount = decimal(discount ? calculateDiscountPrice(price, discount_rate) : price);
    //             let toPush = { "order_qty": qty };
    //             const totalPriceAfterDiscount = priceAftDiscount * qty;
    //             product_total_bp += totalPriceAfterDiscount;
    //             // const commission = decimal(calculateCommission(category, specialCommissionAssignedToMerchant) * totalPriceAfterDiscount);
    //             const commission = 0;
    //             total_commission += commission;
    //             const priceAftDiscountAndCommission = totalPriceAfterDiscount - commission;
    //             const reward_pts = decimal(calculateRewardPts(priceAftDiscountAndCommission, 0.05));
    //             total_reward_pts += reward_pts;
    //             const {
    //                 product_ap,
    //                 free_shipping
    //             } = calPriceAftPromo(promotions, categoryId, merchantId, priceAftDiscount);
    //             if (free_shipping || freeShipping) { freeShipping = free_shipping; }
    //             product_total_ap += product_ap * qty;
    //             if (undefined !== variant) toPush = {
    //                 ...toPush,
    //                 ...variant.toObject(),
    //                 commission,
    //                 reward_pts,
    //             };
    //             variantsHolder.push(toPush);
    //             cart.push(cart_id);
    //         }
    //     );
    //     return {
    //         cart,
    //         total_commission,
    //         total_reward_pts,
    //         product_total_ap,
    //         product_total_bp,
    //         variantsAftProc: variantsHolder,
    //         freeShipping
    //     };
    // }

    function processVariants(store, baseProduct, promotions) {
        // console.log("PROCESS VARIANTSSS", baseProduct.variants);
        const {
            merchant_id: {
                _id: merchantId
                // commission: specialCommissionAssignedToMerchant
            }
        } = store;
        const {
            product_id: product,
            variants: variantsBefProc
        } = baseProduct;
        // console.log("variants", this.variants);
        // console.log("product", product);
        const {
            category_id: category,
            category_id: {
                _id: categoryId
            },
            brief: {
                discount,
                price: priceBefDiscount,
            },
            pricing: {
                discount_rate,
                discount_price
            }
        } = product;
        console.log("CHECK");
        const variantsHolder = [];
        const cart = [];
        let product_total_bp = 0;
        let product_total_ap = 0;
        let total_commission = 0;
        let total_reward_pts = 0;
        let freeShipping = false;

        // console.log("SpecialvariantsBefProc", variantsBefProc);
let i = 0;
        variantsBefProc.forEach(
            (v) => {
                const {
                    variant_id: variant,
                    qty,
                    cart_id,
                    promo_id,
                    promo_price
                } = v;
                let price = variant ? variant["price"] : discount_price;
                i = ++i;
                console.log("promo_id -" + i, promo_id);
                if (promo_id) {
                    price = promo_price;
                }
                console.log("price", price);
                const priceAftDiscount = decimal(discount ? calculateDiscountPrice(price, discount_rate) : price);
                let toPush = { "order_qty": qty };
                if (promo_id) {
                    toPush["promo_id"] = promo_id;
                    toPush["promo_price"] = promo_price;
                }
                const totalPriceAfterDiscount = priceAftDiscount * qty;
                product_total_bp += totalPriceAfterDiscount;
                // const commission = decimal(calculateCommission(category, specialCommissionAssignedToMerchant) * totalPriceAfterDiscount);
                const commission = 0;
                total_commission += commission;
                const priceAftDiscountAndCommission = totalPriceAfterDiscount - commission;
                const reward_pts = decimal(calculateRewardPts(priceAftDiscountAndCommission, 0.05));
                total_reward_pts += reward_pts;
                const {
                    product_ap,
                    free_shipping
                } = calPriceAftPromo(promotions, categoryId, merchantId, priceAftDiscount);
                if (free_shipping || freeShipping) { freeShipping = free_shipping; }
                product_total_ap += product_ap * qty;
                if (undefined !== variant) toPush = {
                    ...toPush,
                    ...variant.toObject(),
                    commission,
                    reward_pts,
                };
                variantsHolder.push(toPush);
                variantsHolder.push();
                cart.push(cart_id);
            }
          );
        console.log("variantsHolder" , variantsHolder);
        return {
            cart,
            total_commission,
            total_reward_pts,
            product_total_ap,
            product_total_bp,
            variantsAftProc: variantsHolder,
            freeShipping,
        };
    }
    async function removeItemFromCart(cart, user_id) {
        // console.log("REMOVE FROM CART");
        const carts = [];
        cart.map(c => carts.push(c.product._id));
        // console.log(cart);
        try {
            return await Cart.remove({
                product_id: { $in: carts },
                buyer_id: Types.ObjectId(user_id)
            });
        } catch (error) {
            throw (error);
        }
    }
    async function updatePendingStock(ordersInserted, pending_stock) {
        const r = pending_stock.map(e =>
            ({
                order_id: ordersInserted.find(o => o.merchant_id.equals(e.merchant_id))._id,
                ...e
            })
        );
        await Promise.all(
            r.map((l) => Product.findByIdAndUpdate(l.product_id, {
                $push: {
                    "stock.pending": l
                }
            })));
        return r;
    }
    async function updateRewardPts(ordersInserted, user_id) {
        let totalPts = 0;
        const rewardPts = ordersInserted.map(
            o => {
                totalPts += o.total.reward_pts;
                return {
                    buyer_id: user_id,
                    order_id: o._id,
                    pts: o.total.reward_pts,
                    status: "Pending"
                };
            }
        );
        await RewardPts.insertMany(rewardPts);
        return totalPts;
    }
    async function sendEmailAndNotification(user, payload) {
        const {
            orderStatus,
            ordersInserted: orders,
            totalRewardPts
        } = payload;
        sendEmail("NEW_ORDER", user, { totalRewardPts });
        await sendEmailAndNotificationToMerchant("MER_ORDER", user, { orders, orderStatus });
    }
    async function sendEmailAndNotificationToMerchant(emailType, merchant, payload) {
        // const {
        //     orderStatus,
        //     orders,
        //     totalRewardPts
        // } = payload;
        // sendEmail("MER_ORDER", merchant, { orderStatus });
        // const newNewsKey = db.ref("/news").push().key;
        // const updates = {};
        // return Promise.all(orders.map((m) => {
        //     updates[`/news/${m.merchant_id}/${newNewsKey}`] = {
        //         "order_id": m._id,
        //         "type": "order",
        //         "create_date": new Date(),
        //         "read": false,
        //         "status": orderStatus
        //     };
        //     return db.ref("/").update(updates);
        // }));
    }
    // async function sendEmailAndNotification(ordersInserted, user_id, totalRewardPts) {
    //     const user = await User.findById(user_id);
    //     console.log(totalRewardPts);
    //     sendEmail("NEW_ORDER", user, { totalRewardPts: totalRewardPts });
    //     sendEmail("MER_NEW_ORDER", user, { totalRewardPts: totalRewardPts });
    //     const newNewsKey = db.ref("/news").push().key;
    //     const updates = {};
    //     await Promise.all(ordersInserted.map((m) => {
    //         updates[`/news/${m.merchant_id}/${newNewsKey}`] = {
    //             "order_id": m._id,
    //             "type": "order",
    //             "create_date": new Date(),
    //             "read": false,
    //             "status": "Pending"
    //         };
    //         return db.ref("/").update(updates);
    //     }));
    // }
    function calPriceAftPromo(promotions, categoryId, merchantId, up) {
        let totalPromo = 0;
        let fs = false;
        if (promotions && !promotions.length) {
            return {
                free_shipping: false,
                product_ap: up
            };
        }
        promotions.forEach(promotion => {
            const { freeShipping, accPromoAmt } = productPromotionAmt(up, promotion, categoryId, merchantId);
            totalPromo += accPromoAmt;
            fs = freeShipping;
        });
        // const { product_ap, freeShipping } = ;
        return {
            free_shipping: fs,
            product_ap: up - totalPromo
        };
    }
    function productPromotionAmt(product_bp, promotion, categoryId, merchantId) {
        const { kind, promo_type, value } = promotion;
        switch (kind) {
            case "storewide": {
                debug("This is a store wide");
                const { freeShipping, accPromoAmt } = computePromoAmt(product_bp, promo_type, value);
                debug("This is a store wide", freeShipping);
                return {
                    accPromoAmt,
                    freeShipping
                };
            }
            case "category": {
                // console.log("This is a promotion by category");
                const found = promotion.target.findIndex(e => e === categoryId);
                if (found > 0) {
                    const { freeShipping, accPromoAmt } = computePromoAmt(product_bp, promo_type, value);
                    return {
                        accPromoAmt,
                        freeShipping
                    };
                }
                break;
            }
            case "merchant": {
                console.log("This is a promotion by merchant");
                console.log(`Checking if ${merchantId} is participating in promotion`);
                const found = promotion.target.findIndex((e: Types.ObjectId) => e.equals(merchantId));
                if (found > 0) {
                    console.log("Yes he is", found);
                    const { freeShipping, accPromoAmt } = computePromoAmt(product_bp, promo_type, value); accPromoAmt;
                } else {
                    console.log("This merchant is not participating in promotion.", promotion);
                }
                return {
                    accPromoAmt: 0,
                    freeShipping: false
                };
            }
            default: throw ("unkown promotion kind");
        }
        // console.log(`Product price before promotion is ${product_bp}. After checking all promotion, the total deduction amount is ${totalPromo}. Product price after promotion is ${product_bp - totalPromo}`);
        // return {
        //     product_ap: product_bp - totalPromo,
        //     freeShipping: fs
        // };
    }
    function computePromoAmt(price, promoType, value) {
        console.log(`Received product price is ${price}, promotion type is ${promoType} the value is ${value}`);
        let freeShipping = false;
        let accPromoAmt = 0;
        switch (promoType) {
            case "fs": {
                freeShipping = true;
                break;
            }
            case "pd": {
                accPromoAmt = decimal(price * Number.parseFloat(value) / 100);
                console.log(`A product with price ${price} after ${value}% of discount is ${accPromoAmt}`);
                break;
            }
            case "fa": {
                accPromoAmt = Number.parseFloat(value);
                break;
            }
        }
        return {
            freeShipping,
            accPromoAmt,
        };
    }
    function calculateTotal(products) {
        // console.log(products);
        return products.map(e => e.purchase).reduce(
            (a, b) => ({
                reward_pts: a.reward_pts + b.reward_pts || 0,
                commission_amount: a.commission_amount + b.commission_amount || 0,
                product_total_bp: a.product_total_bp + b.product_total_bp || 0,
                product_total_ap: a.product_total_ap + b.product_total_ap || 0,
            }));
    }
    function calculateRewardPts(priceAftDiscountAndCommission, rewardRate) {
        return priceAftDiscountAndCommission * rewardRate * 100;
    }
    function calculateCommission(category, specialCommissionAssignedToMerchant: [MerchantCommission]) {
        if (!category) { throw ("Invalid category"); }
        const defaultRate = category["commission"];
        const specialRate = specialCommissionAssignedToMerchant.find(s => s.category_id.equals(category._id));
        return specialRate ? specialRate.rate / 100 : defaultRate / 100;
    }

    function
    convertUTCDateToLocalDate(date) {
        if (date) {
            const newDate = new Date(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
            return newDate;
        }return "";
    }
    // async function placeOrder(req: Request, res: Response, next: NextFunction) {
    //     // a lot complicated lookup and requries no optimization
    //     try {
    //         console.log("placeOrder", req.body);
    //         console.log("placeOrder11111", req.body.creditwallet_amnt);
    //         const {
    //             orderDetails,
    //             promotions,
    //             cart,
    //             orderForReport,
    //             user
    //         } = res.locals;
    //         console.log(res.locals);
    //         const ordersInserted = await Order.insertMany(orderForReport);
    //         console.log("user", user);
    //         // const totalRewardPts = await updateRewardPts(ordersInserted, user.id);
    //         // commission Insert
    //         // await updateCommssion(cart, order_id, user.id);
    //         //
    //         const order_id = [].concat(ordersInserted).map(o => o._id);
    //         const order_no = [].concat(ordersInserted).map(o => o.OrderNo);
    //         await UpdateCreditWallet(req.body.creditwallet_amnt, order_id, user.id);
    //         // await sendEmailAndNotification(user, { orderStatus: "Pending", ordersInserted, totalRewardPts });
    //         console.log("heeee");
    //         return res.status(200).json({order_id, order_no});
    //     } catch (error) {
    //         console.log(error);
    //         return next(error);
    //     }
    // }

    async function placeOrder(req: Request, res: Response, next: NextFunction) {
        // a lot complicated lookup and requries no optimization
        try {
            console.log("placeOrder", req.body);
            console.log("placeOrder -2222");
            // console.log("placeOrder11111", req.body.creditwallet_amnt);
            const {
                orderDetails,
                promotions,
                cart,
                orderForReport,
                user
            } = res.locals;
            console.log("res.locals", res.locals);
            // console.log(orderForReport[0]);

// Modifications

const prod = [];
            orderForReport[0].products.forEach(element => {
                const ele = JSON.parse(JSON.stringify(element));
                prod.push(ele);
            });
            const updatedOrder = prod.map(order => {
                const promoPrice = order.variants[0].promo_price;
                if (promoPrice) {
                    order.product.brief.price = promoPrice;
                }
                return order;
            });
            orderForReport[0].products = updatedOrder;
console.log("updatedOrder", updatedOrder);
            orderForReport[0].payment_method = req.body.paymentType;
                if (orderForReport[0].delivery.shipping_type === "normal") {
                let shipping_date ;
                shipping_date = new Date();
                shipping_date.setHours(shipping_date.getHours() + 8);
                console.log("CONVERTED DATE", convertUTCDateToLocalDate(shipping_date) );
                const order_date = shipping_date.getDay();
                console.log("order_date", order_date, shipping_date);
                if (order_date == 0) orderForReport[0].delivery.Shipping_date = shipping_date.setDate(shipping_date.getDate() + 2);
                else if (order_date == 6)  orderForReport[0].delivery.Shipping_date = shipping_date.setDate(shipping_date.getDate() + 3);
                else if (order_date == 5) {
                  if (shipping_date.getHours() >= 14)
                  orderForReport[0].delivery.Shipping_date = shipping_date.setDate(shipping_date.getDate() + 4);
                  else  orderForReport[0].delivery.Shipping_date = shipping_date.setDate(shipping_date.getDate() + 1);
                }
                else {
                    console.log("shipping_date.getHours()-1", shipping_date.getHours());
                  if (shipping_date.getHours() >= 14) {
                    console.log("shipping_date.getHours()", shipping_date.getHours());
                  orderForReport[0].delivery.Shipping_date = shipping_date.setDate(shipping_date.getDate() + 2);
                  }
                  else  orderForReport[0].delivery.Shipping_date = shipping_date.setDate(shipping_date.getDate() + 1);
                }
            } else {
             let  shipping_date;
               shipping_date = new Date( orderForReport[0].delivery.Shipping_date);
               shipping_date.setHours(shipping_date.getHours() + 8);
               orderForReport[0].delivery.Shipping_date = shipping_date;

                console.log("delivery.Shipping_date",    shipping_date);
                const shippingdate = new Date() ;
                shippingdate.setHours(shippingdate.getHours() + 8);
                const prev_date = new Date(orderForReport[0].delivery.Shipping_date);
                const orderdate = shippingdate.getDay();
                console.log("date", prev_date.getDate(), shippingdate.getDate());
                if ( (shippingdate.getHours() >= 14 && (prev_date.getDate() === shippingdate.getDate() || prev_date.getDate()  === shippingdate.getDate() + 1 )
                ) || (shippingdate.getHours() < 14 && prev_date.getDate() === shippingdate.getDate())) {
                   if (orderdate == 0) {
                    console.log("4");
                   orderForReport[0].delivery.Shipping_date = shippingdate.setDate(shippingdate.getDate() + 2);
                   }
                    else if (orderdate == 6) {
                        console.log("6");
                        orderForReport[0].delivery.Shipping_date = shippingdate.setDate(shippingdate.getDate() + 3);
                    }
                    else if (orderdate == 5) {
                    if (shippingdate.getHours() >= 14) {
                        console.log("8");
                    orderForReport[0].delivery.Shipping_date = shippingdate.setDate(shippingdate.getDate() + 4);
                    }
                    else {
                        console.log("10");
                        orderForReport[0].delivery.Shipping_date = shippingdate.setDate(shippingdate.getDate() + 1);
                    }
                    }
                    else {
                        if (shippingdate.getHours() >= 14) {
                            console.log("12");
                        orderForReport[0].delivery.Shipping_date = shippingdate.setDate(shippingdate.getDate() + 2);
                        }
                        else {
                            console.log("16");
                             orderForReport[0].delivery.Shipping_date = shippingdate.setDate(shippingdate.getDate() + 1);
                        }
                    }
                }
            }
            console.log( "Shipping Date", orderForReport[0].delivery.Shipping_date);
            // console.log("orderForReport", orderForReport[]);
            const ordersInserted = await Order.insertMany(orderForReport);

            const order_id = [].concat(ordersInserted).map(o => o._id);
            const order_no = [].concat(ordersInserted).map(o => o.OrderNo);
                if (req.body.creditwallet_amnt > 0) {
                            await UpdateCreditWallet(req.body.creditwallet_amnt, order_id, user.id);
                }
            if (req.body.paymentType === "PayNow" || req.body.paymentType === "Bank Transfer" ) {
                await payNowMail(order_id);
                console.log("req.body.paymentType", req.body.paymentType);
            }
            return res.status(200).json({order_id, order_no});
        } catch (error) {
            console.log(error);
            return next(error);
        }
    }

    async function payNowMail(order_id) {
        const populate = {
            path: "buyer_id merchant_id",
            select: "contact profile credential.email",
            model: User
          };
        const orders = await Order.findById(order_id).populate(populate);


        //  console.log("ORDERS", orders);
         const root = "./src/emailTemplate";
        //  console.log(root);
         const imagesPath = `${root}/welcome/images`;
         const templatePath = `${root}/pending-order.html`;
         let mailOptions = {};
         const smtpTransport = nodemailer.createTransport({
           port: 587,
           host: "smtp.office365.com",
           secure: false,
           auth: {
             user: "no-reply@gstock.sg",
             pass: "GSto2819"
           },
           tls: {
             rejectUnauthorized: false
           }
         });
         fs.readFile(templatePath, (err, chunk) => {
           if (err) {
             console.log(err);
            //  return next(err);
           }
           const html = chunk.toString("utf8");
           const template = Handlebars.compile(html);

           Handlebars.registerHelper("distanceFixed", function(distance) {
                      return distance.toFixed(2);
           });

           const current_time = new Date();
           const someDate = new Date();
           const numberOfDaysToAdd = 3;
           someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
           const dd = someDate.getDate();
           const mm = someDate.getMonth() + 1;
           const y = someDate.getFullYear();
           const paypal = "-";
           let payment_type;
           const deliveryDate = dd + "/" + mm + "/" + y;
           payment_type = orders.payment_method;
        //    if (req.body.paypal) {
        //      paypal = req.body.paypal.paymentID;
        //      payment_type = "Paypal"; }
        //   else {payment_type = "PayNow"; }
           const imgSrc =
             "https://gstockservices.glux.sg/uploads/user/undefined/products/";
        //    console.log(orders);
           // const shippingdate = convertUTCDateToLocalDate(orders.delivery.Shipping_date);
           // console.log(shippingdate, "SHIPPING DATE");
           let shipping_date;
           if (orders.delivery.shipping_type === "normal")
             shipping_date = orders.delivery.Shipping_date;
           else
             shipping_date = convertUTCDateToLocalDate(orders.delivery.Shipping_date);
           const data = {
             fromEmail: "no-reply@gstock.sg",
             mail_obj: orders,
             paypal,
             type: payment_type,
             productArray: orders.products,
             time: dateFormat(current_time, "shortTime"),
             order_date: dateFormat(orders.createdAt, "mediumDate"),
             // shipping_date: dateFormat(orders.delivery.Shipping_date, "mediumDate"),
             // shippingdate,
             shipping_date: dateFormat(shipping_date, "mediumDate"),
             imgSrc,
             deliveryDate
           };
           const result = template(data);
        //    console.log(orders.buyer_id.credential.email, "email");
           mailOptions = {
               // to:'simi.caxigo@gmail.com',
            //    to: orders.buyer_id.credential.email,
               to: "pratheesh@dinghe.sg",
            // bcc: ["manju.caxigo@gmail.com","simi.caxigo"],

             // bcc: ["manju.caxigo@gmail.com"],
             // to: 'vishnupuliyarakkal@gmail.com',
             // bcc: ["dessa@dinghe.sg", "toh@dinghe.sg"],
            //  bcc: ["pratheesh@dinghe.sg", "dhania@dinghe.sg"],
             bcc: ["angi@dinghe.sg"],
             from: "no-reply@gstock.sg",
             subject: "Thank you for placing your order",
             html: result
           };
           const sendEmail = new Promise((resolve, reject) => {
             smtpTransport.sendMail(mailOptions, (error, response: any) => {
               if (error) {
                 console.log(error);
                 reject(error);
               } else {
                 console.log(response);
                 resolve(response);
               }
             });
           });

         });
        //  console.log("Order1.Produts", orders.products);
        //  console.log("orders1.buyer_id", orders.buyer_id);
        await removeItemFromCarts(orders.products, orders.buyer_id._id);
    }

    async function removeItemFromCarts(cart, user_id) {
        // console.log("REMOVE FROM CART");
        const carts = [];
        cart.map(c => carts.push(c.product._id));
         try {
             return await Cart.remove({
                product_id: { $in: carts },
                buyer_id: Types.ObjectId(user_id)
            });
        } catch (error) {
            throw (error);
        }
    }
    async function UpdateCreditWallet(
        walletamnt, orderId, user_id
    ) {
        try {

            // console.log("UpdateCreditWallet");
            const newEntry = new CreditWallet({
                buyer_id: user_id,
                memo: "Wallet",
                amount: walletamnt,
                status: "Pending",
                ref_id: orderId
                });
            // console.log("CREDIT", newEntry);
            await newEntry.save();

        } catch (error) {
            // return next(error);
        }
    }
    // async function updateCommssion(cart, orderId, user_id) {
    //     const populateProduct = {
    //         path: "product_id ",
    //         select: "commission",
    //         model: Product
    //     };
    //     const cartDetails = await Cart.find({ "_id": { $in: cart } }).populate(populateProduct);
    //     const commission =  cartDetails.map(
    //         o => {
    //             const comm = JSON.parse(JSON.stringify(o.product_id));
    //             // console.log('commissionLevel1', comm)
    //             return {
    //                 order_id: new ObjectID(orderId[0]),
    //                 buyer_id: new ObjectID(user_id),
    //                 Sharedbuyer_id: getSharedUser(o.link),
    //                 product_id: o.product_id["_id"],
    //                 level: "1",
    //                 link: o.link,
    //                 commission_level: comm.commission,
    //                 status: "Active"
    //             };
    //         }
    //     );
    //     console.log("commission", commission);
    //     console.log("commission", commission[0].Sharedbuyer_id);
    //     const commissionFiltered = commission.filter(element => {
    //         return element.link;
    //     });
    //     if (commissionFiltered && commissionFiltered.length > 0) {
    //         commissionFiltered.level = 0;
    //               // return 0;
    //     const usercommissions = await UserCommission.insertMany(commissionFiltered);
    //     console.log("usercommissions", usercommissions);
    //         const commissionId = [].concat(usercommissions).map(o => o._id);
    //         console.log("commission", commissionId);
    //         const shareId =   await UserCommission.findOne({"buyer_id": new ObjectID(commission[0].Sharedbuyer_id)});
    //         console.log("shareId", shareId);
    //         console.log("SharedbuyerID", shareId.level);
    //         if (shareId) {
    //             const Sharedlevel = parseInt(shareId.level) + 1;
    //                 await UserCommission.update({ _id: { $in: commissionId } }, {
    //                 $set: {
    //                         level: Sharedlevel
    //                     }
    //             }, { multi: true });
    //         }
    //          return shareId;
    //         }
    //     else
    //         return 0;
    //   }
    function getOrder(role) {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {

                // console.log("ORDER LIST");
                const {
                    select,
                    order_id,
                    start,
                    end
                } = req.query;
                const query = {};
                let populate = {};

                switch (role) {
                    case "buyer": {
                        query["buyer_id"] = req["user_id"];
                        query["status"] = "Paid";
                        // console.log("BUYERIDDDDD", req["user_id"]);
                        populate = [{
                            path: "merchant_id",
                            select: "store",
                            model: User
                        },
                        {
                            path: "promotions._id",
                            select: "",
                            model: Promotion
                        }];
                        break;
                    }
                    case "merchant": {
                        query["merchant_id"] = req["user_id"];
                        populate = {
                            path: "buyer_id",
                            select: "profile credential.email",
                            model: User
                        };
                        break;
                    }
                    case "admin": {
                        populate = {
                            path: "buyer_id merchant_id",
                            select: "contact profile credential.email",
                            model: User,
                            status
                        };
                        break;
                    }
                    default: throw ("No such role");
                }
                if (start & end) {
                    query["createdAt"] = { $gte: start, $lte: end };

                }

                // console.log("Bodytest", req.query);
                if (order_id) {
                    const orders = await Order.findById(order_id).populate(populate);
                    return res.status(200).send(orders);
                }
                const orders = await Order.find(query).populate(populate).sort("-createdAt");
                // console.log(orders);
                return res.status(200).send(orders);
            } catch (error) {
                return next(error);
            }
        };
    }




    //  async function getSalesReturnOrders1(req: Request, res: Response, next: NextFunction) {
    //     try {
    //         console.log("Orders");
    //         const result = await Order.find(req.query);
    //         console.log(result);
    //         return res.status(200).send(result);
    //     } catch (e) {
    //         return next(e);
    //     }
    // }

    //     function getSalesReturnOrders() {
    //      return async (req: Request, res: Response, next: NextFunction) => {
    //          try {
    //               console.log("getSalesReturnOrders");
    //              const result = await Order.find(req.query);
    //              console.log(result);
    //        return res.status(200).send(result);
    //              } catch (error) {
    //             return next(error);
    //         }

    //         };

    //  }


    // --------------------------------------------------


    // function getOrderNos(req: Request,
    //     res: Response,
    //     next: NextFunction) {
    //     try {
    //         const { _id } = req.body;
    //         const result = await Order find(req.params.id);
    //         return res.status(200).send(result);
    //     } catch (error) {
    //         return next(error);
    //     }
    // }
    async function getOrderNos(req: Request,
        res: Response,
        next: NextFunction) {
        try {

            const orders = await Order.find();
            return res.status(200).send(orders);

        } catch (error) {
            return next(error);
        }
    }

    async function getAllOrder(req: Request,
        res: Response,
        next: NextFunction) {
        try {

            const orders = await Order.find();
            return res.status(200).send(orders);

        } catch (error) {
            return next(error);
        }
    }


    function getOrderNo(role) {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                // console.log("getOrderNOOOOOOOOOOOOOOOOOOOOOOOOOOO");
                // role = "buyer";

                const {
                    select,
                    order_id,
                    start,
                    end
                } = req.query;
                const query = {};
                let populate = {};

                // console.log("Body", req.query);
                switch (role) {
                    case "buyer": {
                        // commented By Pratheesh ON 291118
                        const userId = req.query.user_id;
                        // console.log("BUYERIDDDDD1111", req["user_id"]);
                        query["buyer_id"] = req["user_id"];
                        // console.log("buyer");
                        // console.log(userId);
                        // query["buyer_id"] = userId;
                        query["status"] = "Paid";
                        // console.log("Bodydasgdasgsdfgfs", req.query);
                        // query["buyer_id"] = "5c7b87a809203a36eca41d9f";
                        populate = [{
                            path: "merchant_id",
                            select: "store",
                            model: User
                        },
                        {
                            path: "promotions._id",
                            select: "",
                            model: Promotion
                        }];
                        break;
                    }
                    case "merchant": {
                        query["merchant_id"] = req["user_id"];
                        populate = {
                            path: "buyer_id",
                            select: "profile credential.email",
                            model: User
                        };
                        break;
                    }
                    case "admin": {
                        populate = {
                            path: "buyer_id merchant_id",
                            select: "contact profile credential.email",
                            model: User
                        };
                        break;
                    }
                    default: throw ("No such role");
                }
                if (start & end) { query["createdAt"] = { $gte: start, $lte: end }; }
                if (order_id) {
                    const orders = await Order.findById(order_id).populate(populate);
                    return res.status(200).send(orders);
                }
                const orders = await Order.find(query).populate(populate).sort("-createdAt");
                return res.status(200).send(orders);
            } catch (error) {
                return next(error);
            }
        };
    }

    function getPaidOrders(userRole) {
        return async (req: Request, res: Response, next: NextFunction) => {

            const fromDate = new Date(req.body.fromDate);
            const toDate = req.body.toDate;
            // console.log("getPaidOrders");
            // console.log("- role : " + userRole);
            // console.log("- fromDate : " + fromDate);
            // console.log("- toDate : " + toDate);
            // moment(time, format).tz(zone).format('YYYY/MM/DD HH:mm:ss ZZ');

            try {
                const dbQuery = {};
                let populate = {};

                switch (userRole) {

                    case "buyer": {
                        dbQuery["buyer_id"] = req["user_id"];
                        populate = [{
                            path: "merchant_id",
                            select: "store",
                            model: User
                        },
                        {
                            path: "promotions._id",
                            select: "",
                            model: Promotion
                        }];
                        break;
                    }
                    case "merchant": {
                        dbQuery["merchant_id"] = req["user_id"];
                        populate = {
                            path: "buyer_id",
                            select: "profile credential.email",
                            model: User
                        };
                        break;
                    }
                    case "admin": {
                        // console.log("SUPPLIER NAME");
                        populate = [{
                            path: "buyer_id merchant_id",
                            select: "contact profile credential.email",
                            model: User,
                        },
                        {
                            path: "products.product._id",
                            select: "supplier brief.code stock.unit",
                            model: Product,
                        },

                        ];

                        break;
                    }
                    default: throw ("No such role");
                }
                // console.log("- fromDate check : " + fromDate);
                // console.log("- toDate check : " + toDate);
                // dbQuery["createdAt"] = { fromDate, toDate };
                dbQuery["createdAt"] = { $gte: new Date(fromDate).toISOString(), $lte: new Date(toDate).toISOString() };
                dbQuery["status"] = "Paid";
                dbQuery["delivery_status"] = "not_picked";
                // console.log("- dbQuery : ", dbQuery);
                const orders = await Order
                    .find(dbQuery)
                    .populate(populate)
                    // .deepPopulate(products.product,)
                    .sort("-createdAt");
                return res.status(200).send(orders);
            } catch (error) {
                return next(error);
            }
        };
    }
    /**
     * delete
     * @param role
     */
    /*   function getPickDate(role) {
           console.log("getPickDate")
           return async (req: Request, res: Response, next: NextFunction) => {
               try {
                   console.log(req.body);
                   console.log(req.query);
                   var fromDate = req.body.date;
                   var toDate = req.body.date2;
                   //write checks to verify if input data is undefined
                   console.log(toDate + "-" + fromDate);
                   //const { date } = req.body;
                   //const { select, order_id, start, end } = req.query;
                   //console.log(req.query);
                   const query = {};
                   let populate = {};

                   switch (role) {
                       case "buyer": {
                           query["buyer_id"] = req["user_id"];
                           populate = [{
                               path: "merchant_id",
                               select: "store",
                               model: User
                           },
                           {
                               path: "promotions._id",
                               select: "",
                               model: Promotion
                           }];
                           break;
                       }
                       case "merchant": {
                           query["merchant_id"] = req["user_id"];
                           populate = {
                               path: "buyer_id",
                               select: "profile credential.email",
                               model: User
                           };
                           break;
                       }
                       case "admin": {

                           populate = {

                               path: "buyer_id merchant_id",
                               select: "contact profile credential.email",
                               model: User,

                           };
                           break;
                       }
                       default: throw ("No such role");
                   }


                   if (fromDate & toDate) {
                       query["createdAt"] = { $gte: fromDate, $lte: toDate };
                   }

                   /*if (order_id) {
                       const orders = await Order.findById(order_id).populate(populate);
                       return res.status(200).send(orders);
                   }*-----------------/*
                   const orders = await Order.find(query).populate(populate).sort("-createdAt");
                   return res.status(200).send(orders);
               } catch (error) {

                   return next(error);
               }
           };
       }*/

    /*  if (start & end)
                    { query["createdAt"] = { $gte: start, $lte: end };

                   }*/
    /* query["status"] = "Paid";*/

    /* if (order_id) {
         const orders = await Order.findById(order_id).populate(populate);
         console.log(orders);
         return res.status(200).send(orders);
      find(query) .populate(populate); .sort("-createdAt");
     }*/
    //  query["status"] = "Paid";
    //  const orders = await Order.find(query).populate(populate).sort("-createdAt");
    /*     const Orders = await Order.find({status:"Paid"}).populate(populate).sort("-createdAt");
           console.log("ORDERDSSSSSS",Orders);
           return res.status(200).send(Orders);
       } catch (error) {
           console.log(error);
           return next(error);
       }*/

    async function merchantCancelOrder(req: Request, res: Response, next: NextFunction) {
    }

    async function buyerMadePayment(req: Request, res: Response, next: NextFunction) {
        try {
            // console.log("ERRRRRRRRRRRRRR", req.body);
            // console.log("buyerMadePayment");
            const {
                order_id,
                paypal
            } = req.body;
            const orderIds = [].concat(order_id).map(o => Types.ObjectId(o));
            await Order.update({ _id: { $in: orderIds } }, {
                $set: {
                    status: "Paid",
                    commission_status: COMMISSION_STATUS.Approved,
                    paypal
                }
            }, { multi: true });

            await CreditWallet.update({ ref_id: { $in: orderIds } }, {
                $set: {
                    status: "Used"
                }
            });
            // sendEmailAndNotificationToMerchant("MER_ORDER", res.locals.user, ORDER_STATUS.Paid);
            // const pendingStock = await getPendingStock(orderIds);
            // await substractPendingStock(pendingStock);
            await RewardPts.update({ order_id: { $in: orderIds } }, { $set: { status: "Awarded" } }, { multi: true });
            // return res.status(200).json({ "message": "Status update to paid and reward points credited." });
            res.locals.orderIds = orderIds;
            const order = await Order.findById(order_id);
            // console.log("order sdhsdjhsg", order, order_id);
            await removeItemFromCart(order.products, order.buyer_id);
            next();
        } catch (error) {
            console.log(error);
            return next(error);
        }
    }
    async function getPendingStock(req: Request, res: Response, next: NextFunction) {
        // console.log("Calculating pending stock");
        const { orderIds } = res.locals;
        const pendingStock = await Order.aggregate([
            {
                $match: {
                    "status": { $eq: "Paid" },
                    "_id": { $in: orderIds }
                }
            },
            {
                $unwind: "$products"
            },
            {
                $unwind: "$products.variants"
            },
            {
                $group: {
                    "_id": "$products.product._id",
                    result: {
                        $push: {
                            product_id: "$products.product._id",
                            variant_id: "$products.variants._id",
                            "total": { $sum: "$products.variants.order_qty" }
                        }
                    }
                }
            },
            {
                "$unwind": "$result"
            },
            {
                $project: {
                    product_id: "$result.product_id",
                    variant_id: "$result.variant_id",
                    "total": "$result.total"
                }
            }]
        );
        res.locals.pendingStock = pendingStock;
        next();
    }
    async function substractPendingStock(req: Request, res: Response, next: NextFunction) {
        const { pendingStock } = res.locals;
        const variant = [];
        const product = [];
        const r = await Promise.all(pendingStock.map(p => {
            const {
                product_id: productId,
                variant_id: variantId,
                total } = p;
            if (!variantId) {
                product.push(Types.ObjectId(productId));
                return Product.findByIdAndUpdate(productId, { $inc: { "brief.stock": -total, "stock.qty": -total } });
            } else {
                variant.push(Types.ObjectId(variantId));
                return Variant.findByIdAndUpdate(variantId, { $inc: { "stock": -total } });
            }
        }));
        await Product.update({ _id: { $in: product }, "brief.stock": { "$lte": 0 } }, { $set: { is_active: false } });
        await Variant.update({ _id: { $in: variant }, "stock": { "$lte": 0 } }, { $set: { is_active: false } });
        return next();
    }
    async function buyerConfirmOrder(req: Request, res: Response, next: NextFunction) {
        try {
            const { order_id } = req.query;
            const buyerId = req["user_id"];
            const order = await Order.findOne(
                {
                    "_id": Types.ObjectId(order_id),
                    "buyer_id": Types.ObjectId(buyerId)
                });
            if (!order) { return next("No such order or illegal access"); }
            if (ORDER_STATUS.Delivering !== order.status) return next("Invalid order status");
            order.status = ORDER_STATUS.GoodReceived;
            order["commission_status"] = COMMISSION_STATUS.Released;
            await order.save();
            return res.status(200).send("Item delivered and received.");
        } catch (error) {
            return next(error);
        }
    }
    async function merchantProccedToNextStageOfOrder(req: Request, res: Response, next: NextFunction) {
        try {
            const { shipment_id, order_id } = req.body;
            const user_id = req["user_id"];
            const order = await Order.findById(order_id);
            switch (order["status"]) {
                case ORDER_STATUS.GoodReceived:
                    return res.status(200).send("Goods have already benn received.");
                case ORDER_STATUS.AwaitingDelivery:
                    if (!shipment_id) return next("Shipment id is required");
                    await Order.findByIdAndUpdate(order_id, {
                        $set: {
                            "delivery.shipment_id": shipment_id,
                            "status": ORDER_STATUS.Delivering
                        }
                    });
                    return res.status(200).send("Sending request to detemine if good is delivered");
                case ORDER_STATUS.Delivering:
                    return res.status(200).send("Goods are delivering");
                case ORDER_STATUS.Paid:
                    await Order.findByIdAndUpdate(order_id, shipment_id ? {
                        $set: {
                            "delivery.shipment_id": shipment_id, "status": ORDER_STATUS.Delivering
                        }
                    } : { $set: { "status": ORDER_STATUS.AwaitingDelivery } });
                    return res.status(200).send("Your item status has been changed");
                default:
                    return next("Invalid status for operation");
            }

        } catch (error) {
            return next(error);
        }
    }
    async function buyerPostComplaint(req: Request, res: Response, next: NextFunction) {
        try {
            const {
                order_id,
                comment
            } = req.body;
            const buyer_id = req["user_id"];
            if (await Complaint.findOne({
                order_id: Types.ObjectId(order_id),
                buyer_id: Types.ObjectId(buyer_id)
            })) {
                return res.status(409).send({ message: "Complaint existed" });
            }

            const newComplaint = new Complaint({
                order_id,
                comment,
                buyer_id
            });
            await newComplaint.save();
            return res.status(200).send({ "message": "Complaint created." });
        } catch (err) {
            return next(err);
        }
    }
    async function report(req: Request, res: Response, next: NextFunction) {
        const commission = await
            Order.aggregate(
                // Limit to relevant documents and potentially take advantage of an index
                [{
                    $match: {
                        "commission_status": "Approved",
                        "merchant_id": req["user_id"]
                    }
                },
                {
                    $group: {
                        _id: null,
                        total: { $sum: "$total.commission" }
                    }
                }]
            );
        const order = await Order.aggregate(
            [{
                $match: {
                    "merchant_id": req["user_id"]
                }
            },
            {
                $group: {
                    _id: null,
                    qty: { $sum: 1 },
                    amt: { $sum: "$total.store_ap" }
                }
            }]);
        const paid = await Order.aggregate(
            [{
                $match: {
                    "status": "Paid",
                    "merchant_id": req["user_id"]
                }
            },
            {
                $group: {
                    _id: null,
                    qty: { $sum: 1 },
                    amt: { $sum: "$total.store_ap" }
                }
            }]);
        const result = {
            total: {
                order: {
                    all: order,
                    paid,
                    AD: await Order.count({ "merchant_id": req["user_id"], "status": "AD" }),
                    DG: await Order.count({ "merchant_id": req["user_id"], "status": "DG" }),
                    GR: await Order.count({ "merchant_id": req["user_id"], "status": "GR" }),
                },
                products: {
                    qty: await Product.count({ "merchant_id": req["user_id"] }),
                    "pending": await Product.count({ "merchant_id": req["user_id"], "status": "Pending" }),
                    "approved": await Product.count({ "merchant_id": req["user_id"], "status": "Approved" }),
                    "rejected": await Product.count({ "merchant_id": req["user_id"], "status": "Rejected" }),
                },
                commission: {
                    approved: commission
                },
            }
        };
        return res.status(200).send(result);
    }
    async function pendingStock(req: Request, res: Response, next: NextFunction) {
        const products = await Product.update({ "brief.price": { $exists: false } }, { $set: { status: PRODUCT_STATUS.Rejected } }, { multi: true });

        return res.status(200).send("ok");
    }
    async function deleteOrder(req: Request, res: Response, next: NextFunction) {
        try {
            const { order } = req.body;

            const order_id = [].concat(order).map(_id => Types.ObjectId(_id));
            const orderRemoved = await Order.remove({ "_id": { $in: order_id } });
            debug(`orderRemoved ${order_id}`);
            await RewardPts.remove({ "order_id": { $in: order_id } });
            await Complaint.remove({ "order_id": { $in: order_id } });

            return res.status(200).send({
                "message": "Order removed"
            });
        } catch (e) {
            debug(`error ${e}`);
            return next(e);
        }
    }
    async function adminSearchOrder(req: Request, res: Response, next: NextFunction) {
        const orders = await Order.find(req.body.query).populate("buyer_id merchant_id", "contact profile", User);
        return res.status(200).send(orders);
    }
    async function adminUpdateOrderStatus(req: Request, res: Response, next: NextFunction) {
        try {

            const { order_id } = req.query;
            const { status } = req.body;
            if (["AD", "DG", "GR"].indexOf(status) < 0) {
                return next(`Unkown status ${status}`);
            }
            const result = await Order.findByIdAndUpdate(order_id, { $set: { status } });
            if (!result) { res.status(200).send("No result found"); }
            return res.status(200).send(`Order status updated to ${status}`);
        } catch (e) {
            res.status(500);
            return next(e);
        }
    }

    async function getSupplierDetails(req: Request, res: Response, next: NextFunction) {
        try {
            const order_details = req.body;
            for (const element of order_details) {
                for (const ele of element.products) {
                    if (ele.product._id.supplier.supplier) {
                        const supplier = await Supplier.findOne({ name: ele.product._id.supplier.supplier });
                        ele.product._id["supplier_type"] = supplier.type;
                        ele.product._id["contact"] = supplier.contact;
                    }
                    else if (ele.product._id.supplier) {
                        const supplier = await Supplier.findOne({ name: ele.product._id.supplier });
                        ele.product._id["supplier_type"] = supplier.type;
                        ele.product._id["contact"] = supplier.contact;
                    }
                }
            }
            return res.status(200).send(order_details);
        }
        catch (err) {
            return next(err);
        }
    }
    async function getAllJobs(req: Request, res: Response, next: NextFunction) {
        try {
            const orders = await Order.find();
            const job1 = [], job2 = [], job3 = [], job4 = [], job5 = [], job6 = [], job7 = [], job8 = [], job9 = [], job10 = [], job11 = [];
            const job12 = [], job13 = [], job14 = [], job15 = [], job16 = [];
            const job17 = [], job18 = [], job19 = [], job20 = [], job21 = [], job22 = [], job23 = [], job24 = [], job25 = [], job26 = [], job27 = [], job28 = [];
            // console.log(orders);
            for (let i = 0; i < orders.length; i++) {
                const items = orders[i];
                // console.log(items.delivery.S_postal_code);
                const pincode = "" + items.delivery.S_postal_code[0] + items.delivery.S_postal_code[1];
                if (["01", "02", "03", "04", "05", "06"].includes(pincode)) {
                    job1.push(items);
                } else if (["07", "08"].includes(pincode)) {
                    job2.push(items);
                } else if (["14", "15", "16"].includes(pincode)) {
                    job3.push(items);
                } else if (["09", "10"].includes(pincode)) {
                    job4.push(items);
                } else if (["11", "12", "13"].includes(pincode)) {
                    job5.push(items);
                } else if (["17"].includes(pincode)) {
                    job6.push(items);
                } else if (["18", "19"].includes(pincode)) {
                    job7.push(items);
                } else if (["20", "21"].includes(pincode)) {
                    job8.push(items);
                } else if (["22", "23"].includes(pincode)) {
                    job9.push(items);
                } else if (["24", "25", "26", "27"].includes(pincode)) {
                    job10.push(items);
                } else if (["28", "29", "30"].includes(pincode)) {
                    job11.push(items);
                } else if (["31", "32", "33"].includes(pincode)) {
                    job12.push(items);
                } else if (["34", "35", "36", "37"].includes(pincode)) {
                    job13.push(items);
                } else if (["38", "39", "40", "41"].includes(pincode)) {
                    job14.push(items);
                } else if (["42", "43", "44", "45"].includes(pincode)) {
                    job15.push(items);
                } else if (["46", "47", "48"].includes(pincode)) {
                    job16.push(items);
                } else if (["49", "50", "81"].includes(pincode)) {
                    job17.push(items);
                } else if (["51", "52"].includes(pincode)) {
                    job18.push(items);
                } else if (["53", "54", "55", "82"].includes(pincode)) {
                    job19.push(items);
                } else if (["56", "57"].includes(pincode)) {
                    job20.push(items);
                } else if (["58", "59"].includes(pincode)) {
                    job21.push(items);
                } else if (["60", "61", "62", "63", "64"].includes(pincode)) {
                    job22.push(items);
                } else if (["65", "66", "67", "68"].includes(pincode)) {
                    job23.push(items);
                } else if (["69", "70", "71"].includes(pincode)) {
                    job24.push(items);
                } else if (["72", "73"].includes(pincode)) {
                    job25.push(items);
                } else if (["77", "78"].includes(pincode)) {
                    job26.push(items);
                } else if (["75", "76"].includes(pincode)) {
                    job27.push(items);
                } else if (["79", "80"].includes(pincode)) {
                    job28.push(items);
                }

            }
            const result = [];
            result.push(job1, job2, job3, job4, job5, job6, job7, job8, job9, job10, job11, job12, job13, job14, job15, job16, job17, job18, job19, job20, job21, job22, job23, job24, job25, job26, job27, job28);
            res.send({ item: result });
        } catch (error) {
            console.log(error);
            res.send(error);
        }
    }

    async function SaveJobs(req: Request, res: Response, next: NextFunction) {
        try {
            const dataset = req.body;
            for (let i = 0; i < dataset.length; i++) {
                const item = dataset[i];
                let jobCount = await Jobs.find().count();
                jobCount++;
                const count = jobCount.toString();
                const order_no = count.padStart(3, "0");
                // console.log(item);
                const orderdata = [];
                for (let k = 0; k < item.length; k++) {
                    const orders = item[k];
                    orderdata.push({
                        OrderNo: orders.OrderNo,
                        OrderDate: orders.createdAt,
                        OrderId: orders._id,
                        S_Address: orders.delivery.S_address,
                        netPrice: orders.total.store_ap,
                        customerPhone: orders.delivery.S_contact_no,
                        customerName: orders.delivery.S_recepient,
                        numberOfItems: orders.products.length
                    });
                }
                // console.log(orderdata);
                const jobs = await new Jobs({
                    orders: orderdata,
                    JobId: order_no,
                    status: "Created"
                }).save();
            }
            res.send({ result: "saved" });

        } catch (error) {
            res.send(error);
            console.log(error);
        }
    }

    async function getSavedJobs(req: Request, res: Response, next: NextFunction) {
        try {

            const jobs = await Jobs.find({ status: "Created" });
            // console.log("jobs", jobs);
            res.send({ result: jobs });

        } catch (error) {
            res.send(error);
            console.log(error);
        }
    }
    async function UpdateJobStatus(req: Request, res: Response, next: NextFunction) {
        try {
            // console.log(req.body);
            const jobs = await Jobs.findByIdAndUpdate(req.body.id, { status: "Processing" });
            // console.log("jobs", jobs);
            res.send({ result: "success" });

        } catch (error) {
            res.send(error);
            console.log(error);
        }
    }






    async function adminGetOrderComplain(req: Request, res: Response, next: NextFunction) {
        try {
            // console.log("inside");
            return res.status(200).send(await Complaint.find());
        }
        catch (err) {
            return next(err);
        }
    }
    async function adminGetOrderPaid(req: Request, res: Response, next: NextFunction) {

        try {
            const orders = await Order.find({ status: "Paid" }).populate(
                {
                    path: "buyer_id",
                    select: "profile.first_name",
                    model: User
                },
            );
            return res.status(200).send(orders);
        }
        catch (err) {
            return next(err);
        }
    }
    async function getOrderPending(req: Request, res: Response, next: NextFunction) {

        try {
            const orders = await Order.find({ $and: [
                    {status: "Pending"},
                       {
                           $or: [
                               {payment_method : "PayNow", },
                               {payment_method : "Bank Transfer", }
                           ],
                       }
                   ]
             }).populate(
                {
                    path: "buyer_id",
                    select: "profile.first_name contact",
                    model: User
                },
            );
            return res.status(200).send(orders);
        }
        catch (err) {
            return next(err);
        }
    }

    async function updateOrderStatus(req: Request, res: Response, next: NextFunction) {
        // console.log("updateOrderStatus");
        try {
            const {
                order_id,
                status
            } = req.body;
            // console.log("updateOrderStatus", req.body);
            const orderIds = [].concat(order_id).map(o => Types.ObjectId(o));
            await Order.update({ _id: { $in: orderIds } }, {
                $set: {
                    status

                }
            }, { multi: true });
            // sendEmailAndNotificationToMerchant("MER_ORDER", res.locals.user, ORDER_STATUS.Paid);
            // const pendingStock = await getPendingStock(orderIds);
            // await substractPendingStock(pendingStock);
            // await RewardPts.update({ order_id: { $in: orderIds } }, { $set: { status: "Awarded" } }, { multi: true });
            return res.status(200).json({ "message": "Status Changed" });
            // res.locals.orderIds = orderIds;
            // next();
        } catch (error) {
            return res.status(500).send(error);
        }
    }

    // Get supplier email

    async function getSupplieremail(supplier) {
        try {
            // return res.status(200).find(await models.ProductAttribute.find());
            const email = await Supplier.find(
                { "name": supplier },
            );
            // console.log(category_id);
            // console.log("just check getemail", email[0].email1);
            return email;
        } catch (error) {
            // return next(error);
        }
    }


    // Generate Picking List and Send Mail to Suppliers

    async function generateOrdersList(req: Request, res: Response, next: NextFunction) {
        try {
            // console.log("generateOrdersList");
            const orderIds: any = [];
            const delivery_details = [];
            const selectedOrders = req.body.selectedOrders;
            const toDate = req.body.toDate;
            const fromDate = req.body.fromDate;
            const productList: any = [];
            const today = new Date();
            let email;
            const tomorrow = new Date(today.getTime() + (24 * 60 * 60 * 1000));
            if (selectedOrders.length) {
                for (const key in selectedOrders) {
                    orderIds.push(selectedOrders[key]._id);
                    delivery_details.push({
                        order_id: selectedOrders[key]["_id"],
                        order_no: selectedOrders[key]["OrderNo"],
                        order_date: selectedOrders[key]["createdAt"],
                        delivery_type: selectedOrders[key]["delivery.shipping_type"],
                        deli_status: "picked",
                    });
                    for (const prod in selectedOrders[key].products) {
                        productList.push({
                            order_id: selectedOrders[key]["_id"],
                            order_no: selectedOrders[key]["OrderNo"],
                            order_date: dateFormat(selectedOrders[key]["createdAt"], "mediumDate"),
                            products: selectedOrders[key].products[prod].product.brief,
                            supplier: selectedOrders[key].products[prod].product._id,
                            quantity: selectedOrders[key].products[prod].variants[0]["order_qty"],
                        });
                    }
                }
                // Send mail to Suppliers
                // console.log("selectedOrders", selectedOrders);
                const grouped = {};
                for (let i = 0; i < productList.length; i++) {
                    // console.log("supplier email");

                    if (productList[i].supplier.supplier["supplier"]) {
                        // console.log("supplier email case1", productList[i].supplier.supplier["supplier"]);
                        email = await getSupplieremail(productList[i].supplier.supplier["supplier"]);
                        // console.log("supplier email check1", email[0].email1)

                    } else if (productList[i].supplier.supplier) {
                        // console.log("supplier email case2", productList[i].supplier.supplier);
                        email = await getSupplieremail(productList[i].supplier.supplier);

                        //   console.log("supplier email check2",email)
                    }
                    const suppliertype = email[0].type;
                    // console.log("supplier type check", suppliertype);
                    // const sender = productList[i].supplier.supplier.email1;
                    const sender = email[0].email1;
                    // if (productList[i].supplier.supplier.type === "Consignment") {
                    if (suppliertype === "Consignment") {
                        if (!grouped[sender]) { grouped[sender] = []; }
                        grouped[sender].push(productList[i]);
                    }
                }

                (async () => {
                    for (const key in grouped) {
                        const list = grouped[key];
                        const orderlist = {};
                        const productlist = [];
                        const finalProducts = {};
                        const orders: any = [];
                        let supplier_details;
                        // console.log("list", list);

                        if (list[0].supplier.supplier["supplier"]) {

                            supplier_details = await getSupplieremail(list[0].supplier.supplier["supplier"]);
                            // console.log("supplier address1", supplier_details);
                        } else if (list[0].supplier.supplier) {
                            supplier_details = await getSupplieremail(list[0].supplier.supplier);
                            // console.log("supplier address2", supplier_details);
                        }
                        //   let supplier_details = await Supplier.findById(list[0].supplier.supplier._id)
                        // let supplier_details = email[0];
                        for (let i = 0; i < list.length; i++) {
                            const order = list[i].order_no;
                            if (!orderlist[order]) orderlist[order] = [{
                                order_date: list[i].order_date,
                            }];
                            orderlist[order].push(list[i]);
                        }
                        list.reduce(function (res, value) {
                            if (!res[value.products.code]) {
                                res[value.products.code] = { products: value.products, qty: 0, unit: value.supplier.stock.unit };
                                productlist.push(res[value.products.code]);
                            }

                            // console.log("supplier",supplier_details.city);
                            res[value.products.code].qty += value.quantity;
                            return res;
                        }, {});

                        // console.log("details", grouped[key]);
                        const selproducts = [];
                        const orderdata = grouped[key];
                        // console.log("productlist", productlist);
                        const delivery = grouped[key];
                        let purchaseOrderCount = await PurchaseOrder.find().count();
                        purchaseOrderCount++;
                        const count = purchaseOrderCount.toString();
                        const order_no = count.padStart(3, "0");
                        let totalcost = 0;
                        for (let j = 0; j < productlist.length; j++) {
                            const item = productlist[j].products;
                            // console.log("item", item.price, productlist[j].qty);
                            selproducts.push({
                                name: item.name,
                                code: item.code,
                                qty: productlist[j].qty,
                                unit: item.stock.unit,
                                cost: item.price,
                                totalCost: item.price * productlist[j].qty,
                                discount: 0

                            });
                            totalcost = totalcost + (item.price * productlist[j].qty);
                        }

                        const savedPurchaseOrder = await new PurchaseOrder({
                            "orderNo": "PO-" + order_no,
                            "doNo": delivery.SequenceNo,
                            "orderDate": new Date(),
                            "doDate": new Date(),
                            "supplier": supplier_details[0].name,
                            "invNo": null,
                            "invDate": new Date(),
                            "subTotal": totalcost,
                            "gst": 0,
                            "totalCost": totalcost,
                            "item": selproducts,
                            po_type: "auto",
                            supplier_type: supplier_details[0].type
                        }).save();


                        const root = "./src/emailTemplate";
                        const templatePath = `${root}/delivery_process.html`;
                        const logo = "./src/public/img/67.jpg";
                        let mailOptions = {};
                        const smtpTransport = nodemailer.createTransport({
                            port: 587,
                            host: "smtp.office365.com",
                            secure: false,
                            auth: {
                                user: "no-reply@gstock.sg",
                                pass: "GSto2819"
                            },
                            tls: {
                                rejectUnauthorized: false
                            }
                        });

                        fs.readFile(templatePath, (err, chunk) => {
                            if (err) {
                                return next(err);
                            }
                            const html = chunk.toString("utf8");
                            const template = Handlebars.compile(html);
                            Handlebars.registerHelper("inc", function (value, options) {
                                return parseInt(value) + 1;
                            });
                            // console.log("supplier_details", supplier_details);
                            const data = {
                                fromEmail: "no-reply@gstock.sg",
                                mail_obj: productlist,
                                supplier: supplier_details[0],
                                logo,
                                delivery_date: dateFormat(tomorrow, "mediumDate"),
                            };
                            const result = template(data);
                            // console.log("temp dta check", data);
                            // console.log("test check Key", [key]);
                            mailOptions = {
                                to: [key],
                                   bcc: ["dhania@dinghe.sg", "pratheesh"],
                                // bcc: ["dessa@dinghe.sg", "angi@dinghe.sg", "dhania@dinghe.sg", "pratheesh@dinghe.sg", "toh@dinghe.sg", "jovi@dinghe.sg", "eric@dinghe.sg"],
                                from: "no-reply@gstock.sg",
                                subject: "Order Details (" + dateFormat(today, "mediumDate") + ")",
                                html: result
                            };
                            const sendEmail = new Promise((resolve, reject) => {
                                smtpTransport.sendMail(mailOptions, (error, response: any) => {
                                    if (error) {
                                        console.log("eror", error);
                                        reject(error);
                                    } else {
                                        resolve(response);
                                    }
                                });
                            });
                            sendEmail
                                .then(function (successMessage) {
                                    console.log("Success-->", successMessage);
                                })
                                .catch(function (errorMessage) {
                                    // error handler function is invoked
                                    for (const id in orderlist) orders.push({ "_id": id });
                                    const failedEmailsLog = new FailedEmailsLog({
                                        supplier: key,
                                        orders
                                    }).save().then(reslt => {
                                        console.log("Log Created-->");
                                    });
                                    console.log("error--->", errorMessage);
                                });
                            // });
                        });
                    }
                })();

                // Update Delivery Status of each orders
                const orders = await Order.update({ _id: { $in: orderIds } }, {
                    $set: { delivery_status: "picked" }
                }, { multi: true });

                // Create Picking List in Delivery Process
                const deliveryProcess = await new DeliveryProcess({
                    deli_date: tomorrow,
                    from_date: fromDate,
                    to_date: toDate,
                    order_list: delivery_details,
                }).save();
                return res.status(200).json({ "message": "Status Changed" });
            } else return res.status(200).json({ "message": "No Data" });

        } catch (error) {
            console.log(error);
            return res.status(500).send(error);
        }
    }




    async function saveShiptoaddress(
        req: Request,
        res: Response,
        next: NextFunction
    ) {
        // console.log("saveShiptoaddress");
        try {


            // console.log(req.body);
            const user_id = res.locals.user.id;
            const { S_location, S_unitno, S_postalcode, S_shipto, S_contactnumber } = req.body;
            const ShipToBody = {
                buyer_id: Types.ObjectId(user_id)
            };
            // console.log("ShipToBody", ShipToBody);
            const result = await DeliveryAddress.findOneAndUpdate(
                {
                    buyer_id: Types.ObjectId(user_id)
                },
                {
                    "S_location": req.body.S_location,
                    "S_unitno": req.body.S_unitno,
                    "S_postalcode": req.body.S_postalcode,
                    "S_shipto": req.body.S_shipto,
                    "S_contactnumber": req.body.S_contactnumber

                }, { upsert: true, new: true }, (err, doc) => {
                    return res.status(200).send("Updated");
                });

        }
        catch (error) {
            console.log("ERROR", error);
            res.status(500);
            return next(error);
        }
    }


    async function save_ship(
        req: Request,
        res: Response,
        next: NextFunction
    ) {
        // console.log("saveShiptoaddress");
        try {
            // console.log("saveShip", req.body);
            const user_id = res.locals.user.id;
            const {
                S_location,
                S_unitno,
                S_postalcode,
                S_shipto,
                S_contactnumber,
                // B_location,
                // B_unitno,
                // B_postalcode,
                // B_shipto,
                // B_contactnumber
            } = req.body;
            const ShipToBody = {
                buyer_id: Types.ObjectId(user_id)
            };
            // console.log("ShipToBody", ShipToBody);
            const result = await DeliveryAddress.findOneAndUpdate(
                {
                    buyer_id: Types.ObjectId(user_id)
                },
                {
                    "S_location": req.body.S_location,
                    "S_unitno": req.body.S_unitno,
                    "S_postalcode": req.body.S_postalcode,
                    "S_shipto": req.body.S_shipto,
                    "S_contactnumber": req.body.S_contactnumber,
                    // "B_location": req.body.B_location,
                    // "B_unitno": req.body.B_unitno,
                    // "B_postalcode": req.body.B_postalcode,
                    // "B_shipto": req.body.B_shipto,
                    // "B_contactnumber": req.body.B_contactnumber

                }, { upsert: true, new: true }, (err, doc) => {
                    return res.status(200).send("Updated");
                });

        }
        catch (error) {
            // console.log("ERROR", error);
            res.status(500);
            return next(error);
        }
    }
    async function save_bill(
        req: Request,
        res: Response,
        next: NextFunction
    ) {
        // console.log("saveBilltoaddress");
        try {
            // console.log("saveBilltoaddress");
            // console.log("BODY-----------------", req.body);
            const user_id = res.locals.user.id;
            const { B_location, B_unitno, B_postalcode, B_shipto, B_contactnumber,
            } = req.body;
            const ShipToBody = {
                buyer_id: Types.ObjectId(user_id)
            };
            // console.log("saveBilltoaddress", ShipToBody);
            const result1 = await DeliveryAddress.findOneAndUpdate(
                {
                    buyer_id: Types.ObjectId(user_id)
                },
                {

                    "B_location": req.body.B_location,
                    "B_unitno": req.body.B_unitno,
                    "B_postalcode": req.body.B_postalcode,
                    "B_shipto": req.body.B_shipto,
                    "B_contactnumber": req.body.B_contactnumber,

                }, { upsert: true, new: true }, (err, doc) => {
                    return res.status(200).send("Updated");
                });
        } catch (error) {
            console.log("ERROR", error);
            res.status(500);
            return next(error);
        }
    }



    async function load_Address(
        req: Request,
        res: Response,
        next: NextFunction
    ) {
        try {
            // console.log(req.params);
            const result = await DeliveryAddress.findOne({ buyer_id: new mongo.ObjectId(req.params.userid) });
            // console.log("RESULT", result);
            return res.status(200).send({ result });
        } catch (error) {
            console.log("ERROR", error);
            res.status(500);
            return next(error);
        }
    }




    async function loadAddress(
        req: Request,
        res: Response,
        next: NextFunction
    ) {
        try {
            // console.log(req.params);
            const result = await DeliveryAddress.findOne({ buyer_id: new mongo.ObjectId(req.params.userid) });
            return res.status(200).send({ result });
        } catch (error) {
            console.log("ERROR", error);
            res.status(500);
            return next(error);
        }
    }

    async function saveBilltoaddress(
        req: Request,
        res: Response,
        next: NextFunction
    ) {
        // console.log("saveBilltoaddress");
        try {
            // console.log("saveBilltoaddress");
            // console.log(req.body);
            const user_id = res.locals.user.id;
            const { B_location, B_unitno, B_postalcode, B_shipto, B_contactnumber } = req.body;
            const ShipToBody = {
                buyer_id: Types.ObjectId(user_id)
            };
            // console.log("saveBilltoaddress", ShipToBody);
            const result1 = await DeliveryAddress.findOneAndUpdate(
                {
                    buyer_id: Types.ObjectId(user_id)
                },
                {
                    "B_location": req.body.B_location,
                    "B_unitno": req.body.B_unitno,
                    "B_postalcode": req.body.B_postalcode,
                    "B_shipto": req.body.B_shipto,
                    "B_contactnumber": req.body.B_contactnumber

                }, { upsert: true, new: true }, (err, doc) => {
                    return res.status(200).send("Updated");
                });
        } catch (error) {
            console.log("ERROR", error);
            res.status(500);
            return next(error);
        }
    }

    function getPaidOrder(userRole) {
        return async (req: Request, res: Response, next: NextFunction) => {

            // console.log("getPaidOrders");
            // console.log("- role : " + userRole);
            // moment(time, format).tz(zone).format('YYYY/MM/DD HH:mm:ss ZZ');

            try {
                const dbQuery = {};
                let populate = {};

                switch (userRole) {

                    case "buyer": {
                        dbQuery["buyer_id"] = req["user_id"];
                        populate = [{
                            path: "merchant_id",
                            select: "store",
                            model: User
                        },
                        {
                            path: "promotions._id",
                            select: "",
                            model: Promotion
                        }];
                        break;
                    }
                    case "merchant": {
                        dbQuery["merchant_id"] = req["user_id"];
                        populate = {
                            path: "buyer_id",
                            select: "profile credential.email",
                            model: User
                        };
                        break;
                    }
                    case "admin": {
                        // console.log("SUPPLIER NAME");
                        populate = [{
                            path: "buyer_id merchant_id",
                            select: "contact profile credential.email",
                            model: User,
                        },
                        {
                            path: "products.product._id",
                            select: "supplier brief.code stock.unit",
                            model: Product,
                        },

                        ];

                        break;
                    }
                    default: throw ("No such role");
                }
                              dbQuery["status"] = "Paid";
                dbQuery["delivery_status"] = "not_picked";
                // console.log("- dbQuery : ", dbQuery);
                const orders = await Order
                    .find(dbQuery)
                    .populate(populate)
                    // .deepPopulate(products.product,)
                    .sort("-createdAt");
                return res.status(200).send(orders);
            } catch (error) {
                return next(error);
            }
        };
    }

    return {
        getDeliveryDetail,
        getOrderDetail,
        getPromotionDetailFromPromoCode,
        processStore,
        buyerConfirmOrder,
        getPaidOrders: getPaidOrders("admin"),
        adminGetOrder: getOrder("admin"),
        getPaidOrder: getPaidOrder("admin"),
        placeOrder,
        pendingStock,
        buyerMadePayment,
        getPendingStock,
        substractPendingStock,
        buyerPostComplaint,
        buyerGetOrder: getOrder("buyer"),
        merchantGetOrder: getOrder("merchant"),
        merchantProccedToNextStageOfOrder,
        buyerPostComment: commentCtrl(config.role).postComments,
        report,
        adminSearchOrder,
        adminUpdateOrderStatus,
        adminGetOrderComplain,
        adminGetOrderPaid,
        deleteOrder,
        updateOrderStatus,
        generateOrdersList,
        buyerGetOrderNo: getOrderNo("buyer"),
        saveShiptoaddress,
        saveBilltoaddress,
        loadAddress,
        load_Address,
        save_bill,
        save_ship,
        getOrderNos,
        getAllOrder,
        getOrderPending,
        getSupplierDetails,
        getAllJobs,
        SaveJobs,
        getSavedJobs,
        UpdateJobStatus,
        convertUTCDateToLocalDate,

    };
};

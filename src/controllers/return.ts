import * as del from "del";
import { NextFunction, Request, Response } from "express";
import * as mkdirp from "mkdirp";
import { Types } from "mongoose";
import * as multer from "multer";
import * as fs from "fs-extra";
import paths from "./../config/path";
import commentController from "./comment";
import { isArrayUnique } from "./../../src/_lib/array";
import { InvoiceModel } from "./../../src/models/Invoice";
import { ReturnModel } from "./../../src/models/Return";
import { ObjectID } from "bson";
import { UserModel } from "../models/User";
import { flatten } from "../_lib/flatObjOnly";
import { isVariantPriceInRange } from "../_global/business";
import { Config } from "../types/app";
import { Passport } from "passport";
import { RSA_NO_PADDING } from "constants";
import invoice from "../routes/user/admin/management/invoice";
//import return from "../routes/user/admin/management/return";

export class ReturnController {
    public models;
    public userPassport: Passport;

    // config;
    constructor(private config) {
        // this.config = config;
        this.models = config.models;
        this.userPassport = config.passport.userPassport.getUserPassport();
    }

    public async updateSalesReturn( req: Request,
        res: Response,
        next: NextFunction){
            try {
                const status = req.body.status
                console.log("body",req.body);
                if(status=="active"){
                    const {Customer, net_total, total, gst, gst_perc ,invoice_date, invoice_no, return_no, return_date  } = req.body;
               
                    await this.models.Return.update({invoice_no:req.body.invoice_no }, {
                        $set: {
                            "item" : req.body.item,
                            Customer, net_total, total, gst, gst_perc ,invoice_date, invoice_no, return_no, return_date 
                        }
                    });
                }else{
                 //   await this.models.Return.update({return_no:req.body.return_no }, {
                    await this.models.Return.update({invoice_no:req.body.invoice_no }, {
                        $set: {
                           status
                        }
                    });
                }
                for(let i=0;i<req.body.item.length;i++){
                    this.updaterQuantity(req.body.item[i].code,req.body.item[i].qty);
                    console.log("---Ucode---",req.body.item[i].code)
                }
                return res.status(201).send({item:req.body});
            } catch (error) {
                console.log("error",error);
                return next(error);
            }
        }

    public async createSalesReturn(
        req: Request,
        res: Response,
        next: NextFunction
    ) {

        console.log("createSalesReturn")
        console.log(" - request : ",req.body)
        //  console.log(req.body.item);
        try {
            const { Customer, net_total, total, gst, gst_perc ,invoice_date, invoice_no, return_no, return_date } = req.body;
            console.log(req.body);
            const query = await new this.models.Return({
                "item": req.body.item,
                "status": "active",
                net_total, total, gst, Customer, invoice_date,gst_perc, invoice_no, return_no, return_date
            }).save();
            for(let i=0;i<req.body.item.length;i++){
                this.updaterQuantity(req.body.item[i].code,req.body.item[i].qty);
                console.log("---Ccode---",req.body.item[i].code)
            }
               // .then(item => {
                   
                    //  for(let i=0;i<=req.body.item.length;i++){
                    //      this.updaterQuantity(req.body.item[i].code,req.body.item[i].qty);
                    //      console.log("---Rcode---",req.body.item[i].code)
                    //  }
                    // this.updateQuantity(req.body.item[0].code,req.body.item[0].qty);
                    return res.status(201).send({ item: req.body });

                // })

                //  .catch(err => {
                //     return res.status(400).send("unable to save to database");
                //  });
        } catch (e) {
            return next(e);
        }
    }

    updaterQuantity(sku,userqty){
        // console.log(code,qty);
        console.log("Code",sku);
        console.log("Qty",userqty);
 
         this.models.Product.update({"brief.code":sku }, 
         { $inc: { "stock.qty":userqty, "brief.stock": userqty }                 
             }).then(reslt=>{
                 console.log("Log Created-->",reslt)
             })  .catch(err => {
                 console.log("error",err);
             });
 
     }
    public async getLatest(req: Request,
        res: Response,
        next: NextFunction){
            
            try {
                const result = await this.models.Return.findOne().sort({return_date: -1}).limit(1);
                   //return res.status(200).send(result);
                   if (result) return res.status(200).send(result);
                   else return res.status(200).send({});
            }
            catch (err) {
                return next(err);
            }

    }

   

    public async getAllReturn(req: Request,
        res: Response,
        next: NextFunction){
            try{
                const returns = await this.models.Return.find().sort("-return_date");
                return res.status(200).send(returns);
    
            }catch (error) {
                return next(error);
            }
           
    }

    public async getReturnById(req: Request,
        res: Response,
        next: NextFunction){
            try{
                const { _id } = req.params.id;
                console.log(req.params.id);
                const result = await this.models.Return.findById(req.params.id);
                return res.status(200).send(result);

            }catch (error) {
                return next(error);
            }
           
    }



}
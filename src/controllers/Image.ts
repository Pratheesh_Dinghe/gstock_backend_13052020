import * as del from "del";
import { NextFunction, Request, Response } from "express";
import * as mkdirp from "mkdirp";
import { Types } from "mongoose";
import * as fs from "fs-extra";
import paths from "./../config/path";
import commentController from "./comment";
import { isArrayUnique } from "./../../src/_lib/array";
import { InvoiceModel } from "./../../src/models/Invoice";
import { ReturnModel } from "./../../src/models/Return";
import { ObjectID } from "bson";
import { UserModel } from "../models/User";
import { flatten } from "../_lib/flatObjOnly";
import { isVariantPriceInRange } from "../_global/business";
import { Config } from "../types/app";
import { Passport } from "passport";
import { RSA_NO_PADDING } from "constants";
import invoice from "../routes/user/admin/management/invoice";
//import return from "../routes/user/admin/management/return";
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var multer = require('multer');
var DIR = './uploads/';

export class ImageSaveController {
    public models;
    public userPassport: Passport;

    // config;
    constructor(private config) {
        // this.config = config;
        this.models = config.models;
        this.userPassport = config.passport.userPassport.getUserPassport();
    }

    public async createImage(
        req: Request,
        res: Response,
        next: NextFunction
    ) {
        var path = '';

        var storage = multer.diskStorage({ //multers disk storage settings
            destination: function (req, file, cb) {
                cb(null, './uploads/');
            },
            filename: function (req, file, cb) {
                var datetimestamp = Date.now();
                cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1]);
            }
        });

        var upload = multer({ storage: storage }).single('image');
        upload(req, res, function (err) {
            if (err) {
                // An error occurred when uploading
                console.log(err);
                return res.status(422).send("an Error occured")
            }
            // No error occured.
            path = req.file.path;
            return res.send("Upload Completed for " + path);
        });
    }
    /*
  var storage = multer.diskStorage({ //multers disk storage settings
      destination: function (req, file, cb) {
          cb(null, './uploads/');
      },
      filename: function (req, file, cb) {
          var datetimestamp = Date.now();
          cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1]);
      }
  });
  
  
  var upload = multer({ //multer settings
      storage: storage
  }).any();
  
  upload(req, res, function (err) {
      console.log(req.file);
      if (err) {
          res.status(400).send(
              "eroror"
          )
          //res.json({ error_code: 1, err_desc: err });
          return;
      }
      res.status(200).send(
          "upload success"
      )
      //res.json({ error_code: 0, err_desc: null });
  });

  /*
  try {
      const {
          test
      } = req.body;
      console.log(req.body);
      const query = await new this.models.ImageSave({
          // name,type
      }).save()
          .then(item => {
              return res.status(201).send({ item: item });
          })
          .catch(err => {
              return res.status(400).send("unable to save to database");
          });
  } catch (e) {
      return next(e);
  }
  */
}

import { NextFunction, Request, Response, Router } from "express";
import generateEmail from "../../emailTemplate/template";
import * as nodemailer from "nodemailer";
import * as fs from "fs";
import * as multer from "multer";
import paths from "./../../config/path";
import * as mkdirp from "mkdirp";
import { FILESIZE, FILETYPES } from "../../_global/variables";
import { Config } from "../../types/app";
import { generateWelcomeEmail } from "../../emailTemplate/welcome";
import * as crypto from "crypto";
import { Models } from "../../models";
import { UserModel } from "../../models/User";
import * as Handlebars from "handlebars";
import * as sgMail from "@sendgrid/mail";
async function getRandomBytes() {
  return new Promise<string>((resolve, reject) => {
    crypto.randomBytes(16, (err, buf) => {
      if (err) {
        reject(err);
      }
      resolve(buf.toString("hex"));
    });
  });
}
export class EmailController {
  models: Models;
  htmlStorage = multer.diskStorage({
    destination(req: Request, file, cb) {
      const fileUrl = `./dist/email_template/${req.body.type}/html/`;
      cb(undefined, fileUrl);
      // mkdirp(paths.appDir + fileUrl, () => cb(undefined, paths.appDir + fileUrl));
    },
    filename(req: Request, file, cb) {
      try {
        // file.originalname
        cb(undefined, "template.html");
      } catch (err) {
        cb(err, "/" + req.params.productId + "-" + file.originalname + ".png");
      }
    }
  });
  constructor(config) {
    this.models = config.models;
  }
  static replaceVariable(html, variable, value) {
    return html.replace(`{{{${variable}}}}`, value);
  }
  async getEmailTemplate(type, user: UserModel, additionalInfo) {
    try {
      const root = "./src/emailTemplate";

      const x = {
        WELCOME: async () => {
          const {
            credential: { user_group, email },
            profile: { first_name },
            store: { name: storeName }
          } = user;
          return {
            templatePath: `${root}/welcome/welcome.html`,
            imagesPath: `${root}/welcome/images`,
            templateVariable: "USER",
            value: "buyer" === user_group ? first_name : storeName,
            subject: "Welcome to Gstock"
          };
        },
        FORGOT: async () => {
          const token = await getRandomBytes();
          const { email } = additionalInfo;
          const user = await this.models.User.findOne({
            "credential.email": email
          }).select("credential profile");
          // const html = "buyer" === user.credential.user_group ? `<p>Hello,\n\nClick the below link https://gstock.sg/#/user/reset/${email}/${token} to reset ${email}.\n</p>` : `<p>Hello,\n\nClick the below link https://merchant.gstock.sg/reset/${token} to reset ${email}.\n</p>`;
          const html =
            "buyer" === user.credential.user_group
              ? `<p>Hi ${user.profile.first_name},<br>\n\nYou've recently asked to reset the password for this Gstock account :    <b> ${email} </b><br><br>To update your password, click the button below : <br> <a style="    font-size: 16px;
                    font-family: Helvetica,Helvetica neue,Arial,Verdana,sans-serif;
                    font-weight: none;
                    color: #ffffff;
                    text-decoration: none;
                    background-color: #008d36;
                    border-top: 11px solid #008d36;
                    border-bottom: 11px solid #008d36;
                    border-left: 20px solid #008d36;
                    border-right: 20px solid #008d36;
                    border-radius: 5px;
                    display: inline-block;
                    " href="https://gstock.sg/#/user/reset/${email}/${token}"> Reset my password</a> \n</p>`
              : `<p>Hello,\n\nClick the below link https://merchant.gstock.sg/reset/${token} to reset ${email}.\n</p>`;

          return {
            subject: "Password reset for Gstock",
            html,
            token
          };
        }
      };
      return await x[type]();
      // case "NEW_ORDER":
      //     template = generateNewOrderEmail(email, totalRewardPts);
      //     break;
      // case "MER_ORDER":
      //     template = generateMerchantNewOrderEmail(email, orderStatus);
      //     break;
    } catch (error) {
      throw error;
    }
  }
  getHtmlStorage() {
    return multer({
      storage: this.htmlStorage
      // limits: { fileSize: FILESIZE.image },
      // fileFilter: function (req, file, cb) {
      //     const filetypes = FILETYPES.html;
      //     const mimetype = filetypes.test(file.mimetype);
      //     if (mimetype) return cb(null, true);
      //     cb(new Error("Error: File upload only supports the following filetypes - " + filetypes), false);
      // }
    });
  }
  async getEmailContent(req: Request, res: Response, next: NextFunction) {
    try {
      const welcome = await this.models.Email.findOne({
        type: "Welcome",
        position: "content"
      });
      return res.status(200).send(welcome);
    } catch (error) {
      return next(error);
    }
  }
  async uploadEmailHTML(req: Request, res: Response, next: NextFunction) {
    try {
      const welcome = await this.models.Email.findOne({
        type: "Welcome",
        position: "content"
      });
      return res.status(200).send(welcome);
    } catch (error) {
      return next(error);
    }
  }
  async uploadEmailAssets(req: Request, res: Response, next: NextFunction) {
    try {
      const welcome = await this.models.Email.findOne({
        type: "Welcome",
        position: "content"
      });
      return res.status(200).send(welcome);
    } catch (error) {
      return next(error);
    }
  }

  sendEmail(type) {
    return async (req: Request, res: Response, next: NextFunction) => {
      console.log("sendmail", type);
      const user = res.locals.user
        ? await this.models.User.findById(res.locals.user.id).select(
            "+credential"
          )
        : null;
      console.log(user);
      const {
        html,
        templatePath,
        imagesPath,
        token,
        value,
        templateVariable,
        subject
      } = await this.getEmailTemplate(type, user, req.body);
      console.log("sendmail", type);
      const smtpTransport = nodemailer.createTransport({
        port: 587,
        host: "smtp.office365.com",
        secure: false,
        auth: {
          user: "no-reply@gstock.sg",
          pass: "GSto2819"
        },
        tls: {
          rejectUnauthorized: false
        }
      });
      let mailOptions;
      // console.log(html)
      if (!html) {
        console.log(templatePath);
        fs.readFile(templatePath, (err, chunk) => {
          if (err) {
            return next(err);
          }
          const html = chunk.toString("utf8");
          const template = Handlebars.compile(html);
          // html = EmailController.replaceVariable(html, templateVariable, value);
          // const files = fs.readdirSync(imagesPath + "/");
          // files.forEach(fn => html = html.replace(`images/${fn}`, `cid:unique@nodemailer.com-${fn}`));
          // const attachments = files.map(filename => ({
          //     filename,
          //     path: `${imagesPath}/${filename}`,
          //     cid: `unique@nodemailer.com-${filename}` // same cid value as in the html img src
          // }));
          const data = {
            mail_obj: user.profile.first_name + " " + user.profile.last_name,
            mail_linkId: "https://gstock.sg"
          };
          const result = template(data);
          mailOptions = {
            to: req.body.email,
            from: "no-reply@gstock.sg",
            subject,
            html: result
          };
          const sendEmail = new Promise((resolve, reject) => {
            smtpTransport.sendMail(mailOptions, (error, response: any) => {
              if (error) {
                console.log(error);
                reject(error);
              } else {
                console.log(response);
                resolve(response);
              }
            });
          });
          return res.status(200).send({ message: "ok" });
        });
      } else {
        console.log("req.body.email", req.body.email);
        mailOptions = {
          to: req.body.email,
          from: "no-reply@gstock.sg",
          subject,
          html
        };
        const sendEmail = new Promise((resolve, reject) => {
          smtpTransport.sendMail(mailOptions, (error, response: any) => {
            if (error) {
              console.log(error);
              reject(error);
            } else {
              console.log(response);
              resolve(response);
            }
          });
        });
        return res.status(200).send({ message: "ok" });
        5;
      }
    };
  }

  async updateEmailContent(req: Request, res: Response, next: NextFunction) {
    try {
      await this.models.Email.findOneAndUpdate(
        { type: "Welcome", position: "content" },
        { content: req.body.content }
      );
      return res.status(200).send({ message: "ok" });
    } catch (error) {
      return next(error);
    }
  }
  async getCampaignTemplates(req: Request, res: Response, next: NextFunction) {
    try {
      const templates = await this.models.CampaignEmailTemplates.find();

      return res.json(templates);
    } catch (error) {
      return next(error);
    }
  }
  async saveCampaignTemplate(req: Request, res: Response, next: NextFunction) {
    try {
      const storage = multer.diskStorage({
        destination: "src/uploads/Promotion",
        filename(req, file, cb) {
          const datetimestamp = Date.now();
          cb(
            null,
            file.fieldname +
              "-" +
              datetimestamp +
              "." +
              file.originalname.split(".")[
                file.originalname.split(".").length - 1
              ]
          );
        }
      });

      const upload = multer({ storage }).single("header_image");
      console.log("upload", upload);
      upload(req, res, async err => {
        if (err) {
          console.log(err);
          return res.status(422).send("an Error occured");
        }

        if (!req.body.title || !req.body.email_body) {
          return res.status(400).send("Invalid data");
        }

        const path = req.file.path;
        console.log("path", path);

        const path1 = path.replace(/\\/g, "/");
        const path2 = path1.substring(4);
        // Path = Path.replace(/\\/g, "|")
        console.log("path1", path1);
        const template = await new this.models.CampaignEmailTemplates({
          header_image: "https://gstockadminservices.glux.sg/" + path2,
          title: req.body.title,
          name: req.body.title
            .toLowerCase()
            .replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, "")
            .replace(/\s/, "_"),
          subject: req.body.subject,
          body: req.body.email_body,
          footer: req.body.footer
        }).save();

        return res.json(template);
      });
    } catch (error) {
      return next(error);
    }
  }

  // async sendCampaignEmail(req: Request, res: Response, next: NextFunction) {
  //   try {
  //     console.log("Campaign Emails");
  //     const template = await this.models.CampaignEmailTemplates.findById(
  //       req.params.id
  //     );

  //     if (!template) {
  //       return res.status(400).send("Invalid template id");
  //     }

  //     let emails;
  //     if (req.body.testEmail) {
  //       emails = [req.body.testEmail];
  //     } else {
  //       const users = await this.models.User.find();
  //       // const emails = users.map(u => u.get("email")).filter(e => e);
  //       // emails = ["adeeb@diezon.com", "hello@adeebbasheer.com", "adeebbasheert@gmail.com, "];
  //       emails = [
  //         "dhanyavs.22@gmail.com",
  //         "prithwijith@gmail.com",
  //         "dhania@dinghe.sg",
  //         "pratheesh@dinghe.sg"
  //       ];
  //     }

  //     sgMail.setApiKey(
  //       "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"
  //     );
  //     //   const msg = {
  //     //     to: emails,
  //     //     from: "no-reply@gstock.sg",
  //     //     subject: template.subject || template.title,
  //     //     text: template.title,
  //     //     // templateId: "d-f43daeeaef504760851f727007e0b5d0",
  //     //     templateId: "d-20d03f7ca31d419d82ed329a8ac12ed4",

  //     //     dynamic_template_data: {
  //     //       title: template.title,
  //     //       header_image: template.header_image,

  //     //       email_body: template.body,
  //     //       footer: template.footer
  //     //     }
  //     //     // mailSettings: {
  //     //     //     sandboxMode: {
  //     //     //         enable: true
  //     //     //     }
  //     //     // }
  //     //   };

  //     //   console.log("email check", msg);
  //     //   const sendEmail = await sgMail.send(msg, true);

  //     sgMail.setApiKey(
  //       "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"
  //     );
  //     const msg = {
  //       to: emails,
  //       from: "no-reply@gstock.sg",
  //       subject: template.subject || template.title,
  //       html: template.title,
  //       templateId: "d-20d03f7ca31d419d82ed329a8ac12ed4",
  //       dynamic_template_data: {
  //         subject: template.subject || template.title,
  //         title: template.title,
  //         header_image: template.header_image,
  //         // header_image:
  //         //   "https://gstockservices.glux.sg/uploads/Promotion/12.jpg",

  //         email_body: template.body,
  //         footer: template.footer
  //       }
  //       // mailSettings: {
  //       //     sandboxMode: {
  //       //         enable: true
  //       //     }
  //       // }
  //     };
  //     const sendEmail = await sgMail.send(msg, true);

  //     // return res.send("Email sent to " + emails.length + " users");
  //     return res.status(200).json("success");
  //   } catch (error) {
  //     return next(error);
  //   }
  // }
  // async sendCampaignEmail(req: Request, res: Response, next: NextFunction) {
  //   try {
  //     console.log("Campaign Emails");
  //     const template = await this.models.CampaignEmailTemplates.findById(
  //       req.params.id
  //     );

  //     if (!template) {
  //       return res.status(400).send("Invalid template id");
  //     }
  //     let emails;
  //     if (req.body.testEmail) {
  //       emails = [req.body.testEmail];
  //     } else {
  //       const users = await this.models.User.find();
  //       console.log("users list", users);
  //       const emails = users.map(u => u.get("credential.email")).filter(e => e);
  //       console.log("email list", emails);
  //       // emails = ["adeeb@diezon.com", "hello@adeebbasheer.com", "adeebbasheert@gmail.com, "];
  //       //   emails = [
  //       //     "dhanyavs.22@gmail.com",
  //       //     "prithwijith@gmail.com",
  //       //     "dhania@dinghe.sg",
  //       //     "pratheesh@dinghe.sg"
  //       //     // "abc22@gmail.com",
  //       //     // "jhothi@dinghe.sg"
  //       //   ];
  //     }

  //     sgMail.setApiKey(
  //       "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"
  //     );

  //     const msg = {
  //       to: emails,
  //       from: "no-reply@gstock.sg",
  //       subject: template.subject || template.title,
  //       html: template.title,
  //       templateId: "d-20d03f7ca31d419d82ed329a8ac12ed4",
  //       dynamic_template_data: {
  //         subject: "{ADV}" + template.subject || template.title,
  //         title: template.title,
  //         header_image: template.header_image,

  //         email_body: template.body,
  //         footer: template.footer
  //       }
  //     };
  //     const sendEmail = await sgMail.send(msg, true);

  //     return res.status(200).json("success");
  //   } catch (error) {
  //     return next(error);
  //   }
  // }


  async sendCampaignEmail(req: Request, res: Response, next: NextFunction) {
    try {
      const difUsers = [];
      const interval = 500;
      const count = Number;
      console.log("Campaign Emails");
      const template = await this.models.CampaignEmailTemplates.findById(
        req.params.id
      );

      if (!template) {
        return res.status(400).send("Invalid template id");
      }
      const emails1 = [];
      let emails = [];
      const emails2 = [];
      if (req.body.testEmail) {
        emails = [req.body.testEmail];
      } else {
        const users = await this.models.User.find();
        const count = await this.models.User.find().count();
        const init = 0;
        console.log("users list", count);

        // ------------------------------------------------------------
        let i, j;
        let temparray = [];
        for (i = 0, j = count; i < j; i += interval) {
          difUsers.length = 0;
          console.log("iiiiii", i);
          temparray = users.slice(i, i + interval);
          temparray = temparray
            .map((u) => u.get("credential.email").toString())
            .filter((e) => e);

          difUsers.push(temparray);

          console.log("INTERVAL" + i, difUsers[0]);

          sgMail.setApiKey(
            "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"
          );

          // console.log("INTERVAL" + i, difUsers[0]);

          const msg = {
            to: difUsers[0],
            from: "no-reply@gstock.sg",
            // bcc: ["dhania@dinghe.sg", "pratheesh@dinghe.sg"],
            // cc: ["dhanyavs.22@gmail.com"],
            subject: template.subject || template.title,
            html: template.title,
            templateId: "d-20d03f7ca31d419d82ed329a8ac12ed4",
            dynamic_template_data: {
              subject: "{ADV}" + template.subject || template.title,
              title: template.title,
              header_image: template.header_image,

              email_body: template.body,
              footer: template.footer,
            },
            hideWarnings: true,
          };

          const sendEmail = await sgMail
            .sendMultiple(msg)
            .then(() => {
              console.log("emails sent successfully!");
            })
            .catch((error) => {
              console.log("Error --->", error);
            });
        }
        // -------------------------------------------------------

        // const emails = users
        //   .map((u) => u.get("credential.email").toString())
        //   .filter((e) => e)
        //   .slice(1000, 1500);
        // difUsers.push(emails);
        // console.log("email list", difUsers[0]);
      }

      // sgMail.setApiKey(
      //   "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"
      // );

      if (req.body.testEmail) {
        sgMail.setApiKey(
          "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"
        );
        const msg = {
          to: emails,
          from: "no-reply@gstock.sg",
          // bcc: ["dhania@dinghe.sg", "pratheesh@dinghe.sg"],
          // cc: ["dhanyavs.22@gmail.com"],
          subject: template.subject || template.title,
          html: template.title,
          templateId: "d-20d03f7ca31d419d82ed329a8ac12ed4",
          dynamic_template_data: {
            subject: "{ADV}" + template.subject || template.title,
            title: template.title,
            header_image: template.header_image,

            email_body: template.body,
            footer: template.footer,
          },
          hideWarnings: true,
        };

        const sendEmail = await sgMail
          .sendMultiple(msg)
          .then(() => {
            console.log("emails sent successfully!");
          })
          .catch((error) => {
            console.log("Error --->", error);
          });
      }
      // else {
      // const msg = {
      //   to: difUsers[0],
      //   from: "no-reply@gstock.sg",
      //   // bcc: ["dhania@dinghe.sg", "pratheesh@dinghe.sg"],
      //   // cc: ["dhanyavs.22@gmail.com"],
      //   subject: template.subject || template.title,
      //   html: template.title,
      //   templateId: "d-20d03f7ca31d419d82ed329a8ac12ed4",
      //   dynamic_template_data: {
      //     subject: "{ADV}" + template.subject || template.title,
      //     title: template.title,
      //     header_image: template.header_image,

      //     email_body: template.body,
      //     footer: template.footer,
      //   },
      //   hideWarnings: true,
      // };

      // const sendEmail = await sgMail
      //   .sendMultiple(msg)
      //   .then(() => {
      //     console.log("emails sent successfully!");
      //   })
      //   .catch((error) => {
      //     console.log("Error --->", error);
      //   });
      // }

      return res.status(200).json("success");
    } catch (error) {
      return next(error);
    }
  }
  async getCampaignTemplateDetails(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      console.log("getCampaignTemplateDetails", req.body);
      const campaigntemp = await this.models.CampaignEmailTemplates.findById(
        req.body.id
      );

      return res.status(200).send({
        data: campaigntemp
      });
    } catch (error) {
      console.log(error);
      return next(error);
    }
  }
  async updateCampaignTemplate(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      console.log("update check", req.body.title);

      // // --------------------------image -------------------------------

      const storage = multer.diskStorage({
        destination: "src/uploads/Promotion",
        filename(req, file, cb) {
          const datetimestamp = Date.now();
          cb(
            null,
            file.fieldname +
              "-" +
              datetimestamp +
              "." +
              file.originalname.split(".")[
                file.originalname.split(".").length - 1
              ]
          );
        }
      });
      const upload = multer({ storage }).single("header_image");

      upload(req, res, async err => {
        if (req.file != undefined) {
          console.log("update checdsfsdfdsfk", req.file);

          const path = req.file.path;

          const path1 = path.replace(/\\/g, "/");
          const path2 = path1.substring(4);
          const header_image =
             "https://gstockadminservices.glux.sg/" + path2;
            // header_image: "https://gstockadminservices.glux.sg/" + path2,
            // req.protocol + "://" + req.get("host") + "/" + path2;
            // header_image: "https://gstockadminservices.glux.sg/" + path2,
          console.log("header_image3232323", header_image);

          const imagefile = header_image;

          console.log("image file", imagefile);

          // ---------------------------image ------------------------------

          // const supplier = await this.models.CampaignEmailTemplates.findByIdAndUpdate(
          //   req.body.id,
          //   {
          //     ...req.body.data
          //   }
          // );

          const supplier1 = await this.models.CampaignEmailTemplates.update(
            { _id: req.body._id },
            {
              $set: {
                header_image: imagefile,
                title: req.body.title,
                subject: req.body.subject,
                body: req.body.email_body,
                footer: req.body.footer
              }
            }
          );
        } else {
          const supplier1 = await this.models.CampaignEmailTemplates.update(
            { _id: req.body._id },
            {
              $set: {
                // header_image: imagefile,
                title: req.body.title,
                subject: req.body.subject,
                body: req.body.email_body,
                footer: req.body.footer
              }
            }
          );
        }
      });

      return res.status(200).send({
        message: "Supplier Updated."
      });
    } catch (error) {
      console.log(error);
      return next(error);
    }
  }
}

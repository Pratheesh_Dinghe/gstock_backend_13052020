import { isArrayUnique } from "../../_lib/array";
import { NextFunction, Request, Response, Router } from "express";
import { Types, mongo } from "mongoose";
import { Config, BuyerModels } from "../../types/app";
import { ObjectId } from "bson";
import * as bcrypt from "bcrypt-nodejs";
import * as nodemailer from "nodemailer";
import * as Handlebars from "handlebars";
// const ObjectId = require('mongodb').ObjectID;
import * as fs from "fs";
const moment = require("moment");
// import Node from "./schema";
/*var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var multer = require('multer');
var mkdirp = require('mkdirp');

var DIR = './uploads/return/';
 export default config => {
   const { models } = config;


  *-------------fileupload start--------------------*
  async function createImage(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    var path = '';
    //console.log("Req",req.params.id)
    //const { _id } = req.params.id;
    //`${environment.apiUrl}/uploads/user/undefined/products/salesreturn/${
    var storage = multer.diskStorage({ //multers disk storage settings
      destination: function (req, file, cb) {
        var dest = './uploads/return/'+req.params.id+'/';
        mkdirp.sync(dest);
        cb(null, dest);
      },
      filename: function (req, file, cb) {
        var datetimestamp = Date.now();

        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1]);
      }
    });

    var upload = multer({ storage: storage }).single('image');
   //var upload = multer({ storage: storage }).multiple('image');
    upload(req, res, function (err) {
      if (err) {
        // An error occurred when uploading
        console.log(err);
        return res.status(422).send("an Error occured")
      }
      // No error occured.
      path = req.file.path;
      return res.send("Upload Completed for " + path);
    });
  }
  *-------------fileupload ends-------------*/

// import Node from "./schema";

// export default config => {
//   const { models } = config;

const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const multer = require("multer");
const mkdirp = require("mkdirp");

const DIR = "./uploads/return/";
export default config => {
  const { models } = config;

  /*-------------fileupload start--------------------*/
  async function createImage(req: Request, res: Response, next: NextFunction) {
    let path = "";
    // console.log("Req",req.params.id)
    // const { _id } = req.params.id;
    // `${environment.apiUrl}/uploads/user/undefined/products/salesreturn/${
    const storage = multer.diskStorage({
      // multers disk storage settings
      destination(req, file, cb) {
        const dest = "./uploads/return/" + req.params.id + "/";
        mkdirp.sync(dest);
        cb(null, dest);
      },
      filename(req, file, cb) {
        const datetimestamp = Date.now();

        cb(
          null,
          file.fieldname +
          "-" +
          datetimestamp +
          "." +
          file.originalname.split(".")[
          file.originalname.split(".").length - 1
          ]
        );
      }
    });

    const upload = multer({ storage }).single("image");
    // var upload = multer({ storage: storage }).multiple('image');
    upload(req, res, function(err) {
      if (err) {
        // An error occurred when uploading
        console.log(err);
        return res.status(422).send("an Error occured");
      }
      // No error occured.
      path = req.file.path;
      return res.send("Upload Completed for " + path);
    });
  }
  /*-------------fileupload ends-------------*/

  function changeStatus(is_active) {
    return async (req: Request, res: Response, next: NextFunction) => {
      try {
        const { _id } = req.query;
        const user = await models.User.findByIdAndUpdate(_id, {
          $set: { is_active }
        }).select("credential");
        if (!user) return res.status(409).json({ message: "User not found." });
        if ("merchant" === user.credential.user_group)
          await models.Product.update(
            { merchant_id: Types.ObjectId(_id) },
            { $set: { is_active } },
            { multi: true }
          );
        return res.status(200).json({
          message: `Account ${is_active ? "activated" : "suspended"}.`
        });
      } catch (err) {
        res.status(500);
        return next(err);
      }
    };
  }
  function deleteAccount() {
    return async (req: Request, res: Response, next: NextFunction) => {
      try {
        const { _id } = req.query;
        const userFound = await models.User.findById(_id);
        if (!userFound)
          return res.status(409).json({ message: "User not found" });
        const isMerchant = "merchant " === userFound.credential.user_group;
        const merchant = { merchant_id: Types.ObjectId(_id) };
        const buyer = { buyer_id: Types.ObjectId(_id) };
        if (userFound.is_active) {
          return res
            .status(400)
            .send({ message: "User needs to be suspended before deletion." });
        }
        const pendingOrder = await models.Order.findOne({
          ...(isMerchant ? merchant : buyer),
          status: { $nin: ["GR"] }
        }).lean();
        if (pendingOrder)
          return res.status(400).send({ message: "Pending order found" });
        const removedUser = await models.User.findByIdAndRemove(_id);
        if (isMerchant) {
          await models.Product.remove({ ...(isMerchant ? merchant : buyer) });
          await models.Variant.remove({ ...(isMerchant ? merchant : buyer) });
        } else {
          await (<BuyerModels>models).Cart.remove({
            ...(isMerchant ? merchant : buyer)
          });
          await models.RewardPts.remove({ ...(isMerchant ? merchant : buyer) });
        }
        return res.status(200).json({ message: "Account is removed" });
      } catch (err) {
        res.status(500);
        return next(err);
      }
    };
  }
  function activateAccount() {
    return changeStatus(true);
  }
  function suspendAccount() {
    return changeStatus(false);
  }

  // async function UpdateTotalPurchase() {

  //   try {
  //     const date = new Date();
  //     const month = date.getMonth();

  //     const totalPurchase = await models.Order.aggregate([
  //       {
  //         $project: {
  //           total: { $sum: "$total.store_ap" },
  //           month: { $month: "$createdAt" },
  //           buyer: "$buyer_id"
  //         }
  //       },
  //       { $match: { month } }
  //     ]);
  //     for (let i = 0; i < totalPurchase.length; i++) {
  //       const item = totalPurchase[i];
  //       console.log("item", item);
  //       const id = "" + item.buyer;
  //       const doc = await models.Commissiontree.findOne({ "_id": id });
  //       console.log(id, doc);
  //       // doc.someadditionalattr = "tree"
  //       doc.totalPurchase = item.total;
  //       doc.save();
  //     }
  //     // console.log('totalPurchase', totalPurchase)
  //     return totalPurchase;
  //   } catch (error) {
  //     console.log(error);
  //     return error;
  //   }

  // }

  async function UpdateTotalPurchase() {

    try {
      const date = new Date();
      let month = date.getMonth();
      month = month + 1;
      console.log("month", month);
      const totalPurchase = await models.Order.aggregate([
        {
          $group: {
            _id: { buyer_id: "$buyer_id", month: { $month: "$createdAt" }, status: "$status" },
            total: { $sum: "$total.store_ap" },
            buyer_id: { $first: "$buyer_id" },
            status: { $first: "$status" },
            updatedAt: { $last: "$createdAt" }
          }
        },
        {
          $project: {
            _id: 0,
            buyer_id: "$buyer_id",
            total: "$total",
            month: { $month: "$updatedAt" },
            status: "$status",
            date: "$updatedAt"
          }
        },
        { $match: { month, status: "Paid" } }
      ]);
      console.log("totalPurchase", totalPurchase);
      await models.Commissiontree.updateMany({}, { $set: { totalPurchase: 0 } });
      for (let i = 0; i < totalPurchase.length; i++) {
        const item = totalPurchase[i];
        console.log("item", item);
        const id = item.buyer_id;
        const doc = await models.Commissiontree.findOne({ "_id": new mongo.ObjectID(id) });
        // console.log('doc', id, doc);
        // doc.someadditionalattr = "tree"
        if (doc !== null) {
          doc.totalPurchase = item.total.toFixed(2);
          doc.save();
        }

      }
      // console.log('totalPurchase', totalPurchase)
      return totalPurchase;
    } catch (error) {
      console.log(error);
      return error;
    }

  }
  async function createNewUser(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      req.sanitize("email").normalizeEmail({ gmail_remove_dots: false });
      const {
        credential: { email },
        store: { name: storeName },
        profile: { DOB }
      } = req.body;
      const errors = await req.getValidationResult();
      if (!errors.isEmpty()) {
        return res.status(400).send(errors.array({ onlyFirstError: true })[0]);
      }
      const existingUser = await models.User.findOne({
        "credential.email": email
      });
      if (existingUser) {
        return res.status(409).send("Account existed");
      }
      const existingStoreName = await models.User.findOne({
        "store.name": storeName
      });
      if (existingStoreName) {
        return res.status(409).send("Store existed");
      }
      const dateArray = DOB.split("-");
      req.body.profile.DOB = new Date(
        new Date(dateArray[2], dateArray[1] - 1, dateArray[0])
      );
      await new models.User(req.body).save();
      return res.status(201).json("User Created");
    } catch (e) {
      res.status(500);
      return next(e);
    }
  }
  async function getUserDetail(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    console.log("USER DEATILS");
    console.log("user varunnillaaa");
    const {
      offset = 0,
      limit = 5,
      store_name,
      _id,
      user_group,
      email,
      name
    } = req.query;
    const query = {};

    //  console.log("user varunnillaaa", req.query);
    if (email) {
      query["credential.email"] = { $regex: email, $options: "i" };
      0;
    }
    if (name) {
      query["profile.first_name"] = { $regex: name, $options: "i" };
    }
    if (user_group) {
      query["credential.user_group"] = user_group;
    }
    if (store_name) {
      query["store.name"] = { $regex: store_name, $options: "i" };
    }
    try {
      if (_id) {
        console.log("id", _id);
        query["_id"] = Types.ObjectId(_id);
      }

      console.log("user varunnillaaa", req.query);
      const result = await models.User.paginate(query, {
        lean: true,
        offset: Number(offset),
        limit: Number(limit)
        // select: "-detail credential -credential.password"
      });

      console.log("user vannoo", result);
      if (_id) {
        result.docs[0]["product_count"] = await models.Product.count({
          merchant_id: Types.ObjectId(_id)
        });
        await models.Category.populate(result.docs, {
          path: "commission.category_id"
        });
      }

      console.log("user vannoo", result);
      return res.status(200).send(result);
    } catch (err) {
      return next(err);
    }
  }
  async function updateUserDetail(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    console.log(
      "updateUserDetailupdateUserDetailupdateUserDetailupdateUserDetail"
    );
    try {
      const { profile, bank, store, password } = req.body;
      console.log("UPDATE", req.body);
      console.log("UPDATE", req.query._id);
      const user = await models.User.findById(req.query._id).select(
        "+crendetial"
      );

      user.profile = profile;
      user.bank = bank;
      user.store = store;
      user.credential.password = password;
      console.log("USERR", user);
      await user.save();
      return res.status(200).send({ message: "User updated!" });
    } catch (err) {
      res.status(500);
      return next(err);
    }
  }
  async function updateMerchantAssignedCommission(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { category, rate, merchant_id } = req.body;
      const pathFound = await models.Category.find({
        path: { $regex: `^${category}` }
      });
      const merchant = await models.User.findById(merchant_id).select(
        "commission"
      );
      const commission = pathFound.map(p => ({
        category_id: p._id,
        rate: rate as number
      }));
      merchant.commission.push(...commission);
      if (
        !isArrayUnique(
          merchant.commission.map(c => c.category_id.toHexString())
        )
      ) {
        return next("Duplicated category commission setting!");
      }
      await merchant.save();
      await models.Category.populate(merchant, {
        path: "commission.category_id"
      });
      return res.status(200).send(merchant);
    } catch (error) {
      return next(error);
    }
  }
  async function deleteMerchantAssignedCommission(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { category, merchant_id } = req.body;
      const updated = await models.User.findByIdAndUpdate(
        merchant_id,
        { $pull: { commission: { category_id: { $in: category } } } },
        { new: true }
      )
        .select("commission")
        .lean();
      await models.Category.populate(updated, {
        path: "commission.category_id"
      });
      return res.status(200).send(updated);
    } catch (error) {
      return next(error);
    }
  }
  async function getBuyerCreditWalletList(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const records = await models.CreditWallet.aggregate(
        // Limit to relevant documents and potentially take advantage of an index
        [
          {
            $group: {
              _id: "$buyer_id",
              total: { $sum: "$amount" },
              updatedAt: { $last: "$updatedAt" }
            }
          },
          {
            $project: {
              _id: 0,
              buyer_id: "$_id",
              total: "$total",
              updatedAt: "$updatedAt"
            }
          }
        ]
      );
      const poplutatedRecords = await models.User.populate(records, {
        path: "buyer_id"
      });
      return res.status(200).send(poplutatedRecords);
    } catch (error) {
      return next(error);
    }
  }
  // async function getBuyerCreditWallet(
  //   req: Request,
  //   res: Response,
  //   next: NextFunction
  // ) {
  //   try {
  //     const { buyer_id } = req.query;
  //     console.log("CREDIT WALLET", req.query);
  //     console.log("buyer_id", buyer_id);
  //     console.log(
  //       `Recevied ${buyer_id}=>converted to ${Types.ObjectId(buyer_id)}`
  //     );
  //     // const findTotal = await models.CreditWallet.aggregate([
  //     //   {
  //     $match: {
  //       buyer_id: Types.ObjectId(buyer_id);
  //     }
  //     const result1 = await models.CreditWallet.aggregate([
  //       {
  //         $match: {
  //           buyer_id: Types.ObjectId(buyer_id),
  //           status: "Earned"
  //         }
  //       },
  //       {
  //         $group: {
  //           _id: null,
  //           totalAmount: { $sum: "$amount" }
  //         }
  //       }
  //     ]);

  //     const result2 = await models.CreditWallet.aggregate([
  //       {
  //         $match: {
  //           buyer_id: Types.ObjectId(buyer_id),
  //           status: "Used"
  //         }
  //       },
  //       {
  //         $group: {
  //           _id: null,
  //           totalAmount: { $sum: "$amount" }
  //         }
  //       }
  //     ]);

  //     const result3 = await models.CreditWallet.aggregate([
  //       {
  //         $match: {
  //           buyer_id: Types.ObjectId(buyer_id),
  //           status: "Withdrawed"
  //         }
  //       },
  //       {
  //         $group: {
  //           _id: null,
  //           totalAmount: { $sum: "$amount" }
  //         }
  //       }
  //     ]);

  //     const result =
  //       result1[0]["totalAmount"] -
  //       result2[0]["totalAmount"] -
  //       result3[0]["totalAmount"];
  //     // const result = {
  //     //   $subtract: [result1[0]["totalAmount"], result2[0]["totalAmount"]]
  //     // };
  //     console.log("TOTALL", result);
  //     return res.status(200).send({
  //       // data: {
  //       //   history: await models.CreditWallet.find({
  //       //     buyer_id: Types.ObjectId(buyer_id)
  //       //   }),
  //       //   ...result[0]
  //       // }
  //       result
  //     });
  //   } catch (error) {
  //     return next(error);
  //   }
  // }


  async function getBuyerCreditWallet(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { buyer_id } = req.query;
      console.log("CREDIT WALLET", req.query);
      console.log("buyer_id", buyer_id);
      console.log(
        `Recevied ${buyer_id}=>converted to ${Types.ObjectId(buyer_id)}`
      );
      // const findTotal = await models.CreditWallet.aggregate([
      //   {
      $match: {
        buyer_id: Types.ObjectId(buyer_id);
      }
      const result1 = await models.CreditWallet.aggregate([
        {
          $match: {
            buyer_id: Types.ObjectId(buyer_id),
            status: "Earned"
          }
        },
        {
          $group: {
            _id: null,
            totalAmount: { $sum: "$amount" }
          }
        }
      ]);
      console.log("result1", result1);
      const result2 = await models.CreditWallet.aggregate([
        {
          $match: {
            buyer_id: Types.ObjectId(buyer_id),
            status: "Used"
           }
        },
        {
          $group: {
            _id: null,
            totalAmount: { $sum: "$amount" }
          }
        }
      ]);
      console.log("result2", result2);
      const result3 = await models.CreditWallet.aggregate([
        {
          $match: {
            buyer_id: Types.ObjectId(buyer_id),
            status: "Withdrawed"
          }
        },
        {
          $group: {
            _id: null,
            totalAmount: { $sum: "$amount" }
          }
        }
      ]);
      console.log("result3", result3);
      let resultFinal = 0;
      let res1 = 0;
      let res2 = 0;
      let res3 = 0;

      if (result1.length > 0) {
        res1 = result1[0]["totalAmount"];
      }
      if (result2.length > 0) {
        res2 = result2[0]["totalAmount"];
      }
      if (result3.length > 0) {
        res3 = result3[0]["totalAmount"];
      }

      // let result = res1 - res2 - res3;
      resultFinal = res1 - res2 - res3;
      // if (resultFinal < 0 ) {
      //   resultFinal = 0;
      // }
      const result = resultFinal.toFixed(2);
      return res.status(200).send({
           result
      });
    } catch (error) {
      return next(error);
    }
  }

  async function saveBuyerBankInfo(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    console.log("saveBuyerBankInfo");
    try {
      const {
        buyer_id,
        bank_name,
        Acc_No,
        Name,
        Mobile_No,
        Withdrawal_Amnt
      } = req.body;
      console.log("bankdetails", req.body);

      const existbuyer = await models.BuyerBankInfo.findOne({
        buyer_id
      }).lean();
      console.log("existbuyer", existbuyer);

      if (existbuyer) {
        console.log("test cheyyanu");
        // const item = await models.BuyerBankInfo.findByIdAndUpdate(
        //   req.body._id,
        //   {
        //     bank_name: req.body.bank_name,
        //     Acc_No: req.body.Acc_No,
        //     Name: req.body.Name,
        //     Mobile_No: req.body.Mobile_No
        //   }
        // );

        await models.BuyerBankInfo.findByIdAndUpdate(existbuyer._id, {
          $set: {
            bank_name: req.body.bank_name,
            Acc_No: req.body.Acc_No,
            Name: req.body.Name,
            Mobile_No: req.body.Mobile_No
          }
        });
      } else {
        await new models.BuyerBankInfo({
          buyer_id,
          bank_name,
          Acc_No,
          Name,
          Mobile_No
        }).save();
      }
      if (req.body.Withdrawal_Amnt > 0) {
        await new models.WalletWithdrawal({
          buyer_id,
          bank_name,
          Acc_No,
          Name,
          Mobile_No,
          Withdrawal_Amnt,
          Status: "Requested"
        }).save();
      }
      console.log("Bankinfo saved");
      return res.status(201).json({ message: "Bankinfo saved!" });
    } catch (error) {
      console.log("ERROR", error);
      res.status(500);
      return next(error);
    }
  }

  function setImmediatePromise() {
    return new Promise(resolve => {
      setImmediate(() => resolve());
    });
  }

  async function getWalletWithdrawalDetails(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      console.log("getWalletWithdrawalDetails", req.query);
      const result = await models.BuyerBankInfo.find(req.query);
      console.log("wallet approval", result);
      return res.status(200).send({ result });
      // return result;
    } catch (e) {
      return next(e);
    }
  }

  function isEmpty(value) {
    return value == null || value.length === 0;
  }

  //   async function p() {
  //     // Await puts the rest of this function in the .then() of the promise
  //     const docs = await collection.find({ id: { "$in": item}});

  //     const singleDocNames = [];
  //     for(var i = 0; i < docs.length; i++) {
  //         // ... synchronous code unchanged ...
  //     }

  //     // Resolve the outer promise with the final result
  //     return singleDocNames;
  // });

  // // async functions can be treated like promises
  // p().then(console.log);

  // const forLoop = async (_: any) => {
  //   console.log('Start')

  //   for (let index = 0; index < fruitsToGet.length; index++) {
  //     const fruit = fruitsToGet[index];
  //     const numFruit = await getNumFruit(fruit);
  //     console.log(numFruit);
  //   }
  //   console.log('End');
  // };

  // const getUserDetails = (callback) => {
  //   const tmpstack1  = [];
  //   const tmpstack2  = [];
  //   let counter = 0;
  //   console.log("CONSTANT FOR LOOP");
  //     for ( let i = 0; i < tmpstack1[0].length; i++) {
  //       console.log("LENGTH", tmpstack1[0].length);
  //       console.log("LENGTH" + i, i);
  //       const cnode = tmpstack1.pop();
  //       const child = await models.Commissiontree.find({parent: cnode[i]._id});
  //       // await setImmediatePromise();
  //       console.log("child", child);
  //       // stack.push(child);
  //       tmpstack2.push(child);
  //       counter++;
  //           if (counter == tmpstack1[0].length) {
  //               return callback(tmpstack2);
  //           }
  //      }
  // };


  // async function showtrees(req: Request, res: Response, next: NextFunction) {
  //   // TO GET THE DESCENDANTS OF A PATRICULAR NODE.
  //   try {
  //     // console.log("PARENT", req.body);
  //     const Result = [];
  //     const tmpstack1 = [];
  //     let TEMP2 = [];
  //     await UpdateTotalPurchase();
  //     const userdata = await models.User.findById(req.body.parent);
  //     const it = await models.Commissiontree.find({ parent: req.body.parent });
  //     tmpstack1.push(it);
  //     Result.push(it); // Level 1
  //     // console.log("Result1", Result);
  //     const Result2 = [];
  //     const Result3 = [];
  //     const Result4 = [];
  //     const Result5 = [];

  //     TEMP2 = tmpstack1.pop();
  //     for (let i = 0; i < TEMP2.length; i++) {
  //       const TEMP3 = await models.Commissiontree.find({
  //         parent: TEMP2[i]._id
  //       }); // Level 2
  //       // console.log("2", TEMP3);
  //       Result2.push(TEMP3);
  //       Result.push(TEMP3);
  //     }

  //     for (let m = 0; m < Result2.length; m++) {
  //       const temp = Result2[m];
  //       for (let i = 0; i < temp.length; i++) {
  //         const TEMP3 = await models.Commissiontree.find({
  //           parent: temp[i]._id
  //         }); // Level 3
  //         Result3.push(TEMP3);
  //         Result.push(TEMP3);
  //       }
  //     }

  //     for (let m = 0; m < Result3.length; m++) {
  //       const temp = Result3[m];
  //       for (let i = 0; i < temp.length; i++) {
  //         const TEMP3 = await models.Commissiontree.find({
  //           parent: temp[i]._id
  //         }); // Level 4
  //         Result4.push(TEMP3);
  //         Result.push(TEMP3);
  //       }
  //     }

  //     for (let m = 0; m < Result4.length; m++) {
  //       const temp = Result4[m];
  //       for (let i = 0; i < temp.length; i++) {
  //         const TEMP3 = await models.Commissiontree.find({
  //           parent: temp[i]._id
  //         }); // Level 5
  //         // console.log("2", TEMP3);
  //         Result5.push(TEMP3);
  //         Result.push(TEMP3);
  //       }
  //     }
  //     return res.status(200).send({
  //       Result,
  //       userdata
  //     });
  //   } catch (error) {
  //     console.log(error);
  //     return next(error);
  //   }
  // }

  async function showtrees(req: Request, res: Response, next: NextFunction) {
    // TO GET THE DESCENDANTS OF A PATRICULAR NODE.
    try {
      // console.log("PARENT", req.body);
      const Result = [];
      const tmpstack1 = [];
      let TEMP2 = [];
      await UpdateTotalPurchase();
      const userdata = await models.User.findById(req.body.parent);
      const userTotal = await models.Commissiontree.findOne({ _id: userdata._id });
      console.log("userTotal", userTotal);
      // userdata.totalPurchase = userTotal.totalPurchase;
      const it = await models.Commissiontree.find({ parent: req.body.parent });
      tmpstack1.push(it);
      Result.push(it); // Level 1
      // console.log("Result1", Result);
      const Result2 = [];
      const Result3 = [];
      const Result4 = [];
      const Result5 = [];

      TEMP2 = tmpstack1.pop();
      for (let i = 0; i < TEMP2.length; i++) {
        const TEMP3 = await models.Commissiontree.find({
          parent: TEMP2[i]._id
        }); // Level 2
        // console.log("2", TEMP3);
        Result2.push(TEMP3);
        Result.push(TEMP3);
      }

      for (let m = 0; m < Result2.length; m++) {
        const temp = Result2[m];
        for (let i = 0; i < temp.length; i++) {
          const TEMP3 = await models.Commissiontree.find({
            parent: temp[i]._id
          }); // Level 3
          Result3.push(TEMP3);
          Result.push(TEMP3);
        }
      }

      for (let m = 0; m < Result3.length; m++) {
        const temp = Result3[m];
        for (let i = 0; i < temp.length; i++) {
          const TEMP3 = await models.Commissiontree.find({
            parent: temp[i]._id
          }); // Level 4
          Result4.push(TEMP3);
          Result.push(TEMP3);
        }
      }

      for (let m = 0; m < Result4.length; m++) {
        const temp = Result4[m];
        for (let i = 0; i < temp.length; i++) {
          const TEMP3 = await models.Commissiontree.find({
            parent: temp[i]._id
          }); // Level 5
          // console.log("2", TEMP3);
          Result5.push(TEMP3);
          Result.push(TEMP3);
        }
      }
      return res.status(200).send({
        Result,
        userdata,
        userTotal
      });
    } catch (error) {
      console.log(error);
      return next(error);
    }
  }
  async function getSalesReturnOrders(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      console.log("getSalesReturnOrders", req.query);
      const result = await models.Order.find({ OrderNo: req.query._id });
      // const result = await models.Order.find(req.query);
      console.log("SALES RETURN ORDERS", result);
      return res.status(200).send({ result });
      // return result;
    } catch (e) {
      return next(e);
    }
  }

  async function createBuyerSalesReturn(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    console.log("maduthuuuuu");
    console.log("buyer:", req.body);
    try {
      console.log("createBuyerSalesReturn12");
      const userId = req["user_id"];
      let RetNo;
      let SeqNo;
      let stockCount;
      const zero = 0;
      console.log("createBuyerSalesReturn12", req.body);
      // const SeqNo = await this.models.Buyersalesreturn.aggregate({
      //   $group: { _id: null, max: { $max: "$SequenceNo" } }
      // });
      // console.log("Sequence Value", SeqNo);
      const {
        //  buyer_id,
        user_id,
        buyer_id,
        ref_id,
        Return_No,
        OrderNo,
        name,
        email,
        Reason,
        Remarks,
        //  Order_amnt,
        Order_amount,
        //   Ret_amnt,
        Ret_amount,
        status,
        products,
        uploadimage
      } = req.body;

      // products: [{…}]

      // status: "Verified"
      // uploadimage: null
      // user_id: "5dbbb6869ad17a23a0f3cf56"

      console.log("dhania", req.body);
      console.log("raghul", req.files);
      stockCount = await models.BuyerSalesReturn.find().count();

      console.log("madhurii", req.body);

      //   Order_amnt,
      //   Ret_amnt,
      //   status,
      //   products
      // } = req.body;
      console.log("dhania", req.body);
      console.log("raghul", req.files);
      stockCount = await models.BuyerSalesReturn.find().count();
      console.log("madhurii", req.body);
      if (stockCount != 0) {
        SeqNo = await models.BuyerSalesReturn.aggregate({
          $group: { _id: null, max: { $max: "$SequenceNo" } }
        });
        if (SeqNo[0].max == null) {
          RetNo = "BET-1";
        } else {
          // SeqNo[0].max = SeqNo[0].max;
          RetNo = "BET-" + (SeqNo[0].max + 1);
          console.log("Sequence Value", RetNo);
        }
      } else {
        RetNo = "BET-1";
        SeqNo = 0;
        console.log("SeqNo", SeqNo);
      }
      if (stockCount == 0) {
        console.log("stockCount");
        const ret = await new this.models.BuyerSalesReturn({
          buyer_id: userId,
          ref_id,
          name,
          email,
          Return_No: RetNo,
          Order_No: OrderNo,
          reason: Reason,
          remarks: Remarks,
          SequenceNo: 1,
          //  Order_amnt,
          Order_amount,
          //   Ret_amnt,
          Ret_amount,
          // Order_amnt,
          // Ret_amnt,
          status,
          products
        }).save();
        return res.status(201).json({ Ret_id: ret._id });
      } else {
        console.log("stockCount1");
        const ret = await new models.BuyerSalesReturn({
          // buyer_id: userId,
          // ref_id:user_id ,
          buyer_id: userId,
          ref_id,
          name,
          email,
          Return_No: RetNo,
          Order_No: OrderNo,
          reason: Reason,
          remarks: Remarks,
          SequenceNo: SeqNo[0].max + 1,
          // Order_amnt,
          //  Ret_amnt,
          // Order_amount,
          // Ret_amount,
          // status,
          // products

          // Order_amnt,
          // Ret_amnt,
          //  Order_amnt,
          Order_amount,
          //   Ret_amnt,
          Ret_amount,
          status,
          products
        }).save();
        // await this.models.BuyerSalesReturn.findByIdAndUpdate(ret._id, {
        //   $addToSet: { uploadimage: req.files[0].originalname }
        // });
        return res.status(201).json({ Ret_id: ret._id });
      }
      // return res.status(201).json({ message: "Buyersalesreturn created!" });
    } catch (error) {
      console.log("ERROR", error);
      res.status(500);
      return next(error);
    }
  }
  async function getOrder(req: Request,
    res: Response,
    next: NextFunction) {
      const date = new Date();
    }

  async function getOrderStatus(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const date = new Date();
      console.log(req.query.id);
      const firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
      const lastdayoflastmonth = new Date();
      lastdayoflastmonth.setMonth(lastdayoflastmonth.getMonth(), 0);
      const firstdayoflastmonth = new Date();
      firstdayoflastmonth.setDate(1);
      firstdayoflastmonth.setMonth(firstdayoflastmonth.getMonth() - 1);
      console.log("TWO", firstDay, date);
      console.log("one", lastdayoflastmonth, firstdayoflastmonth);
      const fromDate = new Date(req.body.fromDate);
      const toDate = new Date(req.body.toDate);
      const condition = { createdAt: { $gte: firstDay, $lte: date }, buyer_id: Types.ObjectId(req.query.id) };
      const condition_prev = { createdAt: { $gte: firstdayoflastmonth, $lte: lastdayoflastmonth }, buyer_id: Types.ObjectId(req.query.id) };
      const current_month = await models.Order.aggregate([
        {
          $match: condition
        },
        {
          $lookup: {
            from: "products",
            localField: "products.product._id",
            foreignField: "_id",
            as: "product_cost"
          }
        },
        { $unwind: "$total" },
        { $unwind: "$product_cost" },
        { $unwind: "$products" },
        { $unwind: "$products.variants" },
        {
          $group: {
            _id: "$buyer_id",
            amount: {
              $sum: "$total.store_ap"
            }
          }
        },
        {
          $match: {
            amount: { $gte: 30 }
          }
        }
      ]);
      if (!current_month.length) {
        const prev = await models.Order.aggregate([
          {
            $match: condition_prev
          },
          {
            $lookup: {
              from: "products",
              localField: "products.product._id",
              foreignField: "_id",
              as: "product_cost"
            }
          },
          { $unwind: "$total" },
          { $unwind: "$product_cost" },
          { $unwind: "$products" },
          { $unwind: "$products.variants" },
          {
            $group: {
              _id: "$buyer_id",

              amount: {
                $sum: "$total.store_ap"
              },
            }
          },
          {
            $match: {
              amount: { $gte: 30 }
            }
          }
        ]);
        console.log("prev", prev);
        return res.status(201).json(prev);
      }
      console.log("current", current_month);
      return res.status(201).json(current_month);

    } catch (error) {
      return next(error);
    }

  }

  function convertUTCDateToLocalDate(date) {
    if (date) {
      const newDate = new Date(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
      return newDate;
    } return  "";
}

async function calculateSales(userid, start, end) {
  try {

    const fromDate = new Date(start);
    const toDate = new Date(end);
    const condition = { "createdAt": { $gt: fromDate, $lt: toDate } };

    const query: any = {
      buyer_id: new ObjectId(userid),
      status: "Paid"
    };
    if (start && end) {
      query["createdAt"] = {
        $gt: fromDate,
        $lt: toDate
      };
    }
  //  console.log("QUERY", query);
    const buyer = await models.Order.aggregate([
      {
        $match: query
      },
      {
        "$lookup": {
          from: "products",
          localField: "products.product._id",
          foreignField: "_id",
          as: "product_cost"
        }
      },
      { $unwind: "$total" },
      { $unwind: { path: "$product_cost", preserveNullAndEmptyArrays: true } },
      { $unwind: "$products" },
      { $unwind: "$products.variants" },
      {
        "$group": {
          _id: "$buyer_id",
          amount: {
            $sum: {
              $cond: [
                { $eq: ["$product_cost._id", "$products.product._id"] },
                {
                  $multiply: [
                    "$products.product.brief.price",
                    "$products.variants.order_qty"
                  ]
                },
                0
              ]
            }
          },
          totalcost: {
            $sum: {
              $cond: [
                { $eq: ["$product_cost._id", "$products.product._id"] },
                {
                  $multiply: [
                    "$product_cost.brief.cost",
                    "$products.variants.order_qty"
                  ]
                },
                0
              ]
            }
          }
        }
      },
      {
        "$project": {
          profit: {
            $subtract: ["$amount", "$totalcost"]
          }
        }
      },
      {
        "$match": {
          profit: { $gt: 0 }
        }
      }
    ]);
    //  const buyer = await models.Order.aggregate([
    //   {
    //     $match: query
    //   },
    //   {
    //     "$lookup": {
    //       from: "products",
    //       localField: "products.product._id",
    //       foreignField: "_id",
    //       as: "product_cost"
    //     }
    //   },

    // ]);
    // let result = [];
    // let profit = 0;

    // if(buyer.length){
    //   for (const order of buyer) {
    //     console.log(order.OrderNo)
    //       for(let i=0;i<order.products.length;i++){
    //         // console.log(order.products[i].variants[0].order_qty)
    //           profit +=(order.products[i].variants[0].order_qty*order.products[i].product.brief.price)-(order.products[i].variants[0].order_qty*order.product_cost[i].brief.cost)
    //           console.log(profit)
    //     };

    //   };
    // }
    // if(profit>0)   result.push({profit:profit})


    return buyer;
  } catch (e) { }
}

  // async function updateCommission( // TO GET THE DESCENDANTS OF A PATRICULAR NODE.
  //   req: Request,
  //   res: Response,
  //   next: NextFunction
  // ) {
  //   try {
  //     console.log("TWO", req.body);
  //     const fromDate = new Date(req.body.fromDate);
  //     const toDate = new Date(req.body.toDate);
  //     // const condition = { createdAt: { $gte: fromDate, $lte: toDate } };
  //     const condition = { "createdAt": { $gt: fromDate, $lt: toDate } };
  //     console.log("condition", condition);
  //     // const buyer = await models.Order.aggregate(
  //     //   { "$match" :
  //     //            {
  //     //             "createdAt": {
  //     //                            $gt: fromDate,
  //     //                            $lt: toDate
  //     //                           }
  //     //            }
  //     //    },
  //     //              {
  //     //     $lookup: {
  //     //       from: "products",
  //     //       localField: "products.product._id",
  //     //       foreignField: "_id",
  //     //       as: "product_cost"
  //     //     }
  //     //   },
  //     //   { $unwind: "$total" },
  //     //   { $unwind: "$product_cost" },
  //     //   { $unwind: "$products" },
  //     //   { $unwind: "$products.variants" },
  //     //   {
  //     //     $group: {
  //     //       _id: "$buyer_id",
  //     //       amount: {
  //     //         $sum: "$total.store_bp"
  //     //       }
  //     //     }
  //     //   },
  //     //   {
  //     //     $match: {
  //     //       amount: { $gte: 0 }
  //     //     }
  //     //   };
  //     // ])

  //     const buyer = await models.Order.aggregate([
  //       {
  //         $match: condition
  //       },
  //       {
  //         "$lookup": {
  //           from: "products",
  //           localField: "products.product._id",
  //           foreignField: "_id",
  //           as: "product_cost"
  //         }
  //       },
  //       // { "$unwind": "$total" },
  //       // { "$unwind": "$product_cost" },
  //       // { "$unwind": "$products" },
  //       // { "$unwind": "$products.variants" },
  //       {
  //         "$group": {
  //           _id: "$buyer_id",
  //           amount: {
  //             $sum: "$total.store_bp"
  //           }
  //         }
  //       },
  //       {
  //         "$match": {
  //           "amount": { $gt: 30 }
  //         }
  //       }
  //     ]);
  //     console.log("CHECKING", buyer);
  //     const result = await models.User.populate(buyer, {
  //       path: "_id",
  //       select: " profile.first_name"
  //     });
  //     let count = 0;
  //     const level = await models.CommissionPercentage.findOne();
  //     console.log(result, level, "ELIGIBLE USERS");
  //     await result.forEach(async ele => {
  //       count = 0;
  //       const stack = [];
  //       const desc = [];
  //       const tmpstack1 = [];
  //       let result2 = [];
  //       // console.log("Count", count);
  //       count = count + 1;
  //       console.log("DESCENDANTS OF A PATRICULAR NODE", ele._id._id);
  //       const it = await models.Commissiontree.find({ parent: ele._id._id });
  //       // const it = await models.Commissiontree.find({parent: "5dc12c0b83d5354d6cfe5bcf"});
  //       const parent = ele._id._id;
  //       // console.log("parent", parent );
  //       const TEMP1 = it;
  //       console.log("LEVEL-01", TEMP1); // Level 1
  //       TEMP1.forEach(async element => {
  //         const result1 = await calculateSales(element._id, fromDate, toDate);
  //         console.log("SALES-1", result1);
  //         if (result1.length && level) {
  //           const monthlycomm1 = new models.MonthlyCommission({
  //             Root: parent,
  //             Level: "1",
  //             From: fromDate,
  //             To: toDate,
  //             ParentBuyer: element.parent,
  //             ChildBuyer: element._id,
  //             SalesAmount: result1[0].profit * (level.l1 / 100)
  //           });
  //           const commission = await monthlycomm1.save();
  //           console.log("SALES-11111");
  //         }
  //       });

  //       tmpstack1.push(it);
  //       const counter = 0;
  //       const child = [];
  //       // let  ResultLevel2 ;

  //       const FETCH2 = [];

  //       for (let i = 0; i < TEMP1.length; i++) {
  //         // console.log("TEMP1-02", TEMP1[i]._id);
  //         const it = await models.Commissiontree.find({ parent: TEMP1[i]._id });
  //         FETCH2.push(it);
  //         for (let j = 0; j < it.length; j++) {
  //           const result1 = await calculateSales(it[j]._id, fromDate, toDate);
  //           if (result1.length && level) {
  //             const monthlycomm1 = new models.MonthlyCommission({
  //               Root: parent,
  //               Level: "2",
  //               From: fromDate,
  //               To: toDate,
  //               ParentBuyer: it[j].parent,
  //               ChildBuyer: it[j]._id,
  //               SalesAmount: result1[0].profit * (level.l2 / 100)
  //             });
  //             const commission = await monthlycomm1.save();
  //             result2 = result1;
  //           }
  //         }
  //       }

  //       const FETCH3 = [];
  //       const temp2 = FETCH2[0];
  //       if (temp2 && temp2.length) {
  //         for (let i = 0; i < temp2.length; i++) {
  //           // console.log("TEMP1-03", temp2[i]._id);
  //           const it = await models.Commissiontree.find({ parent: temp2[i]._id });
  //           FETCH3.push(it);
  //           for (let j = 0; j < it.length; j++) {
  //             const result1 = await calculateSales(it[j]._id, fromDate, toDate);
  //             // console.log("SALES -03", result1);
  //             if (result1.length) {
  //               const monthlycomm1 = new models.MonthlyCommission({
  //                 Root: parent,
  //                 Level: "3",
  //                 From: fromDate,
  //                 To: toDate,
  //                 ParentBuyer: it[j].parent,
  //                 ChildBuyer: it[j]._id,
  //                 SalesAmount: result1[0].profit * (level.l3 / 100)
  //               });
  //               const commission = await monthlycomm1.save();
  //               result2 = result1;
  //             }
  //           }
  //         }
  //       }
  //       // console.log("Ttemp2", temp2);


  //       const FETCH4 = [];
  //       const temp3 = FETCH3[0];
  //       if (temp3 && temp3.length) {
  //               // console.log("Ttemp2", temp3);
  //       for (let i = 0; i < temp3.length; i++) {
  //         // console.log("TEMP1-03", temp3[i]._id);
  //         const it = await models.Commissiontree.find({ parent: temp2[i]._id });
  //         FETCH4.push(it);
  //         for (let j = 0; j < it.length; j++) {
  //           const result1 = await calculateSales(it[j]._id, fromDate, toDate);
  //           // console.log("SALES -03", result1);
  //           if (result1.length) {
  //             const monthlycomm1 = new models.MonthlyCommission({
  //               Root: parent,
  //               Level: "4",
  //               From: fromDate,
  //               To: toDate,
  //               ParentBuyer: it[j].parent,
  //               ChildBuyer: it[j]._id,
  //               SalesAmount: result1[0].profit * (level.l3 / 100)
  //             });
  //             const commission = await monthlycomm1.save();
  //             result2 = result1;
  //           }
  //         }
  //       }
  //       }


  //       // const FETCH5 = [];
  //       const temp4 = FETCH3[0];
  //       // console.log("Ttemp2", temp4);
  //       if (temp4 && temp4.length) {
  //         for (let i = 0; i < temp4.length; i++) {
  //           // console.log("TEMP1-03", temp3[i]._id);
  //           const it = await models.Commissiontree.find({ parent: temp2[i]._id });
  //           // FETCH4.push(it);
  //           for (let j = 0; j < it.length; j++) {
  //             const result1 = await calculateSales(it[j]._id, fromDate, toDate);
  //             // console.log("SALES -03", result1);
  //             if (result1.length) {
  //               const monthlycomm1 = new models.MonthlyCommission({
  //                 Root: parent,
  //                 Level: "5",
  //                 From: fromDate,
  //                 To: toDate,
  //                 ParentBuyer: it[j].parent,
  //                 ChildBuyer: it[j]._id,
  //                 SalesAmount: result1[0].profit * (level.l3 / 100)
  //               });
  //               const commission = await monthlycomm1.save();
  //               result2 = result1;
  //             }
  //           }
  //         }
  //       }
  //     });
  //     return res.status(200).send({ result: "success" });
  //   } catch (error) {
  //     return next(error);
  //   }
  // }

  // async function calculateSales(userid, start, end) {
  //   try {

  //     const fromDate = new Date(start);
  //     const toDate = new Date(end);
  //     // const condition = { createdAt: { $gte: fromDate, $lte: toDate } };
  //     const condition = { "createdAt": { $gt: fromDate, $lt: toDate } };

  //     const query: any = {
  //       buyer_id: new ObjectId(userid)
  //     };
  //     if (start && end) {
  //       query["createdAt"] = {
  //         $gt: fromDate,
  //         $lt: toDate
  //       };
  //     }
  //     // console.log("QUERY", query);
  //     const buyer = await models.Order.aggregate([
  //       {
  //         $match: query
  //       },
  //       {
  //         "$lookup": {
  //           from: "products",
  //           localField: "products.product._id",
  //           foreignField: "_id",
  //           as: "product_cost"
  //         }
  //       },
  //       { $unwind: "$total" },
  //       { $unwind: { path: "$product_cost", preserveNullAndEmptyArrays: true }},
  //       { $unwind: "$products" },
  //       { $unwind: "$products.variants" },
  //       {
  //         "$group": {
  //           _id: "$buyer_id",
  //           amount: {
  //             $sum: {
  //               $cond: [
  //                 { $eq: ["$product_cost._id", "$products.product._id"] },
  //                 {
  //                   $multiply: [
  //                     "$products.product.brief.price",
  //                     "$products.variants.order_qty"
  //                   ]
  //                 },
  //                 0
  //               ]
  //             }
  //           },
  //           totalcost: {
  //             $sum: {
  //               $cond: [
  //                 { $eq: ["$product_cost._id", "$products.product._id"] },
  //                 {
  //                   $multiply: [
  //                     "$product_cost.brief.cost",
  //                     "$products.variants.order_qty"
  //                   ]
  //                 },
  //                 0
  //               ]
  //             }
  //           }
  //         }
  //       },
  //       {
  //         "$project": {
  //           profit: {
  //             $subtract: ["$amount", "$totalcost"]
  //           }
  //         }
  //       },
  //       {
  //         "$match": {
  //           profit: { $gt: 0 }
  //         }
  //       }
  //     ]);
  //     console.log("CALCULATE SALES - BUYER", buyer);
  //     return buyer;
  //   } catch (e) { }
  // }

  async function buildLinkTree(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const root = new models.tree({ name: "1" });
      const header = new models.tree({ name: "2" });
      const main = new models.tree({ name: "3" });
      const foo = new models.tree({ name: "foo" });
      const bar = new models.tree({ name: "bar" });
      const bar1 = new models.tree({ name: "bar" });
      root.children = [header, main];
      main.children = [foo, bar];

      models.tree
        .remove({})
        .then(Promise.all([foo, bar, header, main, root].map(p => p.save())));
      // .then(_ => models.tree.findOne({name: "1"}))
      // .then( r => console.log(r.children[1].children[0].name)) ; // foo
    } catch (e) {
      // error handling
    }
  }

  // async function Recursivenode(
  //   req: Request,
  //   res: Response,
  //   next: NextFunction
  // ) {
  //     try {
  //     const descendants = [];
  //     const stack = [];
  //     const item = models.Commissiontree.findOne({ _id: "Cell_Phones_and_Accessories"});
  //     stack.push(item);
  //     while ( stack.length > 0 ) {
  //       const currentnode = stack.pop();
  //       const children = models.Commissiontree.find({ parent: currentnode._id});
  //       while ( true === children.hasNext()) {
  //       const child = children.next();
  //       descendants.push(child._id);
  //       stack.push(child);
  //  }

  async function buildLink(req: Request, res: Response, next: NextFunction) {
    try {
      const users: any = [];
      let excount = 0;
      // WHILE REGISTERING ALL LINKS WILL BE FORMED AS A TREE-   PARENT - CHILD REPRESENTATION
      console.log("buildLink", req.body);
      const existingUser = await models.Commissiontree.findOne({
        _id: req.body.id
      });
      console.log("EXISTING USER1111", existingUser);
      if (existingUser != null) {
        console.log("check cheyyaanu", existingUser);
        return res.status(200).send(existingUser);
      } else {
        if (req.body.parent == "undefined" || req.body.parent == null) {
          const parent = await models.Commissiontree.findOne({ parent: null });
          if (!isEmpty(parent)) {
            req.body.parent = parent._id;
          }
        }
        const commission = await models.Commissiontree.aggregate([
          {
            $match: {
              parent: req.body.parent
            }
          },
          {
            $group: {
              _id: "parent",
              count: { $sum: 1 }
            }
          }
        ]);
        users[0] = { user: await models.User.findById(req.body.id) };

        if (isEmpty(commission)) {
          isEmpty;
          excount = 0;
        }
        excount = 0;
        const norder = (excount + 1) * 10;
        console.log("Order", norder);
        users[1] = { user: await models.User.findById(req.body.parent) };
        console.log("users--------------", users);
        const parent = users[1].user.profile.first_name;
        const child = users[0].user.profile.first_name;
        console.log("parent,child", parent, child);
        await users.forEach((user, index) => {
          const root = "./src/emailTemplate/commissionEmail";
          let templatePath = "";
          if (!index) {
            templatePath = `${root}/commissionEmailChild.html`;
          } else {
            templatePath = `${root}/commissionEmailParent.html`;
          }
          let mailOptions = {};
          const smtpTransport = nodemailer.createTransport({
            port: 587,
            host: "smtp.office365.com",
            secure: false,
            auth: {
              user: "no-reply@gstock.sg",
              pass: "GSto2819"
            },
            tls: {
              rejectUnauthorized: false
            }
          });

          fs.readFile(templatePath, (err, chunk) => {
            if (err) {
              return next(err);
            }
            const html = chunk.toString("utf8");
            const template = Handlebars.compile(html);
            const data = {
              fromEmail: "no-reply@gstock.sg",
              parent,
              child
            };
            const result = template(data);
            mailOptions = {
              to: user.user.credential.email,
              from: "no-reply@gstock.sg",
              subject: "Gstock Referral Marketing",
              html: result
            };
            const sendEmail = new Promise((resolve, reject) => {
              smtpTransport.sendMail(mailOptions, (error, response: any) => {
                if (error) {
                  console.log("eror", error);
                  reject(error);
                } else {
                  resolve(response);
                }
              });
            });
            sendEmail
              .then(function (successMessage) {
                console.log("Success-->", successMessage);
              })
              .catch(function (errorMessage) {
                // error handler function is invoked
              });
          });
        });
        // Create a new entry in commission tree
        const node = new models.Commissiontree({
          _id: req.body.id,
          user_name: users[0].user.profile.first_name,
          parent: req.body.parent,
          someadditionalattr: req.body.attribute,
          order: norder
        });
        console.log("console2");
        console.log("not savedd");
        console.log("ERRORRRRRRRRR", node);
        const newNode = await node.save();
        console.log("Saved", newNode);
        // return newNode;
        return res.status(200).send(newNode);
      }
    } catch (e) {
      return next(e);
      // error handling
    }
  }

  async function addNewEntryToBuyerCreditWallet(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      console.log("req.body", req.body);
      console.log("req.params", req.body.buyer_id);
      if (!(await models.User.findById(req.body.buyer_id)))
        res.status(404).send({ message: "No such user found" });
      const newEntry = new models.CreditWallet({
        ...req.body,
        ...req.params
      });
      await newEntry.save();
      return res.status(200).send({
        message: "Credit modified."
      });
    } catch (error) {
      return next(error);
    }
  }
  // merchant function

  // async function updateCommission( // TO GET THE DESCENDANTS OF A PATRICULAR NODE.
  //   req: Request,
  //   res: Response,
  //   next: NextFunction
  // ) {
  //   try {
  //     console.log("TWO", req.body);
  //     const fromDate = new Date(req.body.fromDate);
  //     const toDate = new Date(req.body.toDate);
  //     // const condition = { createdAt: { $gte: fromDate, $lte: toDate } };
  //     const condition = { "createdAt": { $gt: fromDate, $lt: toDate },   status: "Paid" };
  //     console.log("condition", condition);

  //     const buyer = await models.Order.aggregate([
  //       {
  //         $match: condition
  //       },

  //       { "$unwind": "$total" },

  //       {
  //         "$group": {
  //           _id: "$buyer_id",
  //           amount: {
  //             $sum: "$total.store_bp"
  //           }
  //         }
  //       },
  //       {
  //         "$match": {
  //           "amount": { $gt: 30 }
  //         }
  //       }
  //     ]);
  //  //   console.log("CHECKING", buyer);

  //     const result = await models.User.populate(buyer, {
  //       path: "_id",
  //       select: " profile.first_name"
  //     });
  //     let count = 0;
  //     const level = await models.CommissionPercentage.findOne();
  //     // console.log(result, "ELIGIBLE USERS");
  //     for (const ele of result) {
  //     // await result.forEach(async ele => {
  //       count = 0;
  //       const stack = [];
  //       const desc = [];
  //       const tmpstack1 = [];
  //       let result2 = [];
  //       // console.log("Count", count);
  //       count = count + 1;
  //     //  console.log("DESCENDANTS OF A PATRICULAR NODE", ele._id._id);
  //       const it = await models.Commissiontree.find({ parent: ele._id._id });
  //       // const it = await models.Commissiontree.find({parent: "5dc12c0b83d5354d6cfe5bcf"});
  //       const parent = ele._id._id;
  //       // console.log("parent", parent );
  //       const TEMP1 = it;
  //       // console.log("LEVEL-01", TEMP1); // Level 1
  //       // for (const element of TEMP1) {
  //       TEMP1.forEach(async element => {
  //         const result1 = await calculateSales(element._id, fromDate, toDate);
  //         //  return res.status(200).send({ result: result1 });
  //         if (result1.length && level) {
  //           // console.log("SALES-1", result1[0].profit * (level.l1 / 100));
  //           const monthlycomm1 = new models.MonthlyCommission({
  //             Root: parent,
  //             Level: "1",
  //             From: fromDate,
  //             To: toDate,
  //             ParentBuyer: element.parent,
  //             ChildBuyer: element._id,
  //             SalesAmount: result1[0].profit * (level.l1 / 100)
  //           });
  //           const commission = await monthlycomm1.save();
  //           console.log("SALES-11111");
  //         }
  //       });

  //       tmpstack1.push(it);
  //       const counter = 0;
  //       const child = [];
  //       // let  ResultLevel2 ;

  //       const FETCH2 = [];
  //       // for (const element of TEMP1) {
  //       for (let i = 0; i < TEMP1.length; i++) {
  //         // console.log("TEMP1-02", TEMP1[i]._id);
  //         const it = await models.Commissiontree.find({ parent: TEMP1[i]._id });
  //         FETCH2.push(it);
  //         // for (const elem of TEMP1) {
  //         for (let j = 0; j < it.length; j++) {
  //           const result1 = await calculateSales( it[j]._id, fromDate, toDate);
  //           if (result1.length && level) {
  //             // console.log("2", result1[0].profit * (level.l2 / 100))
  //             const monthlycomm1 = new models.MonthlyCommission({
  //               Root: parent,
  //               Level: "2",
  //               From: fromDate,
  //               To: toDate,
  //               ParentBuyer: it[j].parent,
  //               ChildBuyer: it[j]._id,
  //               SalesAmount: result1[0].profit * (level.l2 / 100)
  //             });
  //             const commission = await monthlycomm1.save();
  //             result2 = result1;
  //           }
  //         }
  //       }

  //       const FETCH3 = [];
  //       const temp2 = FETCH2[0];
  //       if (temp2 && temp2.length) {
  //         for (let i = 0; i < temp2.length; i++) {
  //           // console.log("TEMP1-03", temp2[i]._id);
  //           const it = await models.Commissiontree.find({ parent: temp2[i]._id });
  //           FETCH3.push(it);
  //           for (let j = 0; j < it.length; j++) {
  //             const result1 = await calculateSales(it[j]._id, fromDate, toDate);
  //             // console.log("SALES -03", result1);
  //             if (result1.length) {
  //               const monthlycomm1 = new models.MonthlyCommission({
  //                 Root: parent,
  //                 Level: "3",
  //                 From: fromDate,
  //                 To: toDate,
  //                 ParentBuyer: it[j].parent,
  //                 ChildBuyer: it[j]._id,
  //                 SalesAmount: result1[0].profit * (level.l3 / 100)
  //               });
  //               const commission = await monthlycomm1.save();
  //               result2 = result1;
  //             }
  //           }
  //         }
  //       }
  //       // console.log("Ttemp2", temp2);


  //       const FETCH4 = [];
  //       const temp3 = FETCH3[0];
  //       if (temp3 && temp3.length) {
  //         // console.log("Ttemp2", temp3);
  //         for (let i = 0; i < temp3.length; i++) {
  //           // console.log("TEMP1-03", temp3[i]._id);
  //           const it = await models.Commissiontree.find({ parent: temp2[i]._id });
  //           FETCH4.push(it);
  //           for (let j = 0; j < it.length; j++) {
  //             const result1 = await calculateSales(it[j]._id, fromDate, toDate);
  //             // console.log("SALES -03", result1);
  //             if (result1.length) {
  //               const monthlycomm1 = new models.MonthlyCommission({
  //                 Root: parent,
  //                 Level: "4",
  //                 From: fromDate,
  //                 To: toDate,
  //                 ParentBuyer: it[j].parent,
  //                 ChildBuyer: it[j]._id,
  //                 SalesAmount: result1[0].profit * (level.l3 / 100)
  //               });
  //               const commission = await monthlycomm1.save();
  //               result2 = result1;
  //             }
  //           }
  //         }
  //       }


  //       // const FETCH5 = [];
  //       const temp4 = FETCH3[0];
  //       // console.log("Ttemp2", temp4);
  //       if (temp4 && temp4.length) {
  //         for (let i = 0; i < temp4.length; i++) {
  //           // console.log("TEMP1-03", temp3[i]._id);
  //           const it = await models.Commissiontree.find({ parent: temp2[i]._id });
  //           // FETCH4.push(it);
  //           for (let j = 0; j < it.length; j++) {
  //             const result1 = await calculateSales(it[j]._id, fromDate, toDate);
  //             // console.log("SALES -03", result1);
  //             if (result1.length) {
  //               const monthlycomm1 = new models.MonthlyCommission({
  //                 Root: parent,
  //                 Level: "5",
  //                 From: fromDate,
  //                 To: toDate,
  //                 ParentBuyer: it[j].parent,
  //                 ChildBuyer: it[j]._id,
  //                 SalesAmount: result1[0].profit * (level.l3 / 100)
  //               });
  //               const commission = await monthlycomm1.save();
  //               result2 = result1;
  //             }
  //           }
  //         }
  //       }
  //     }
  //     return res.status(200).send({ result: "success" });
  //   } catch (error) {
  //     return next(error);
  //   }
  // }

  async function updateCommission( // TO GET THE DESCENDANTS OF A PATRICULAR NODE.
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      console.log("TWO", req.body);
      const fromDate = new Date(req.body.fromDate);
      const toDate = new Date(req.body.toDate);
      // const condition = { createdAt: { $gte: fromDate, $lte: toDate } };
      const condition = { "createdAt": { $gt: fromDate, $lt: toDate }, status: "Paid" };
      console.log("condition", condition);

      const buyer = await models.Order.aggregate([
        {
          $match: condition
        },

        { "$unwind": "$total" },

        {
          "$group": {
            _id: "$buyer_id",
            amount: {
              $sum: "$total.store_bp"
            }
          }
        },
        {
          "$match": {
            "amount": { $gt: 30 }
          }
        }
      ]);
      //   console.log("CHECKING", buyer);

      const result = await models.User.populate(buyer, {
        path: "_id",
        select: " profile.first_name"
      });
      let count = 0;
      const level = await models.CommissionPercentage.findOne();
      console.log(result[0], "ELIGIBLE USERS");
      for (const ele of result) {
        count = 0;
        const stack = [];
        const desc = [];
        const tmpstack1 = [];
        let result2 = [];
        count = count + 1;
        const it = await models.Commissiontree.find({ parent: ele._id._id });
        const parent = ele._id._id;
        const TEMP1 = it;

        const result = await calculateSales(parent, fromDate, toDate);
        //  return res.status(200).send({ result: result1 });
        if (result.length && level) {
          // console.log("SALES-1", result1[0].profit * (level.l1 / 100));
          const monthlycomm1 = new models.MonthlyCommission({
            Root: parent,
            Level: "1",
            From: fromDate,
            To: toDate,
            ParentBuyer: parent,
            ChildBuyer: parent,
            SalesAmount: result[0].profit * (level.l1 / 100)
          });
          const commission = await monthlycomm1.save();
          console.log("SALES-11111");
        }

        TEMP1.forEach(async element => {
          const result1 = await calculateSales(element._id, fromDate, toDate);
          //  return res.status(200).send({ result: result1 });
          if (result1.length && level) {
            // console.log("SALES-1", result1[0].profit * (level.l1 / 100));
            const monthlycomm1 = new models.MonthlyCommission({
              Root: parent,
              Level: "2",
              From: fromDate,
              To: toDate,
              ParentBuyer: element.parent,
              ChildBuyer: element._id,
              SalesAmount: result1[0].profit * (level.l2 / 100)
            });
            const commission = await monthlycomm1.save();
            console.log("SALES-11111");
          }
        });

        tmpstack1.push(it);
        const counter = 0;
        const child = [];
            const FETCH2 = [];
          for (let i = 0; i < TEMP1.length; i++) {
          const it = await models.Commissiontree.find({ parent: TEMP1[i]._id });
          FETCH2.push(it);
          for (let j = 0; j < it.length; j++) {
            const result1 = await calculateSales(it[j]._id, fromDate, toDate);
            if (result1.length && level) {
                const monthlycomm1 = new models.MonthlyCommission({
                Root: parent,
                Level: "3",
                From: fromDate,
                To: toDate,
                ParentBuyer: it[j].parent,
                ChildBuyer: it[j]._id,
                SalesAmount: result1[0].profit * (level.l3 / 100)
              });
              const commission = await monthlycomm1.save();
              result2 = result1;
            }
          }
        }
        console.log("FETCH2 level3", FETCH2);
        const FETCH3 = [];
        if (FETCH2 && FETCH2.length) {
          for (let k = 0; k < FETCH2.length; k++) {
            const temp2 = FETCH2[k];
            for (let i = 0; i < temp2.length; i++) {
              const it = await models.Commissiontree.find({ parent: temp2[i]._id });
              console.log(it, "iteee");
              FETCH3.push(it);
              for (let j = 0; j < it.length; j++) {
                console.log(it[j]._id, "it[j]._id");
                const result1 = await calculateSales(it[j]._id, fromDate, toDate);
                console.log("SALES -03", result1);
                if (result1.length) {
                  const monthlycomm1 = new models.MonthlyCommission({
                    Root: parent,
                    Level: "4",
                    From: fromDate,
                    To: toDate,
                    ParentBuyer: it[j].parent,
                    ChildBuyer: it[j]._id,
                    SalesAmount: result1[0].profit * (level.l4 / 100)
                  });
                  const commission = await monthlycomm1.save();
                  result2 = result1;
                }
              }
            }
          }

        }

        const FETCH4 = [];
        if (FETCH3 && FETCH3.length) {
           for (let k = 0; k < FETCH3.length; k++) {
            const temp3 = FETCH3[k];
            for (let i = 0; i < temp3.length; i++) {
              const it = await models.Commissiontree.find({ parent: temp3[i]._id });
              FETCH4.push(it);
              for (let j = 0; j < it.length; j++) {
                const result1 = await calculateSales(it[j]._id, fromDate, toDate);
                 if (result1.length) {
                  const monthlycomm1 = new models.MonthlyCommission({
                    Root: parent,
                    Level: "5",
                    From: fromDate,
                    To: toDate,
                    ParentBuyer: it[j].parent,
                    ChildBuyer: it[j]._id,
                    SalesAmount: result1[0].profit * (level.l5 / 100)
                  });
                  const commission = await monthlycomm1.save();
                  result2 = result1;
                }
              }
            }
          }

        }

        // if (FETCH4 && FETCH4.length) {
        //   for (let k = 0; k < FETCH4.length; k++) {
        //     const temp4 = FETCH4[k];
        //     for (let i = 0; i < temp4.length; i++) {
        //      const it = await models.Commissiontree.find({ parent: temp4[i]._id });
        //         for (let j = 0; j < it.length; j++) {
        //         const result1 = await calculateSales(it[j]._id, fromDate, toDate);
        //          if (result1.length) {
        //           const monthlycomm1 = new models.MonthlyCommission({
        //             Root: parent,
        //             Level: "5",
        //             From: fromDate,
        //             To: toDate,
        //             ParentBuyer: it[j].parent,
        //             ChildBuyer: it[j]._id,
        //             SalesAmount: result1[0].profit * (level.l3 / 100)
        //           });
        //           const commission = await monthlycomm1.save();
        //           result2 = result1;
        //         }
        //       }
        //     }
        //   }
        // }
      }
      return res.status(200).send({ result: "success" });
    } catch (error) {
      return next(error);
    }
  }
    async function merchantnameFetch(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      console.log("NameFetch", req.body);
      const suppliers = await models.Supplier.find({ ic_no: req.body.id });
      //  return res.status(200).send(suppliers);
      if (!suppliers.length) {
        return res.status(200).send({
          suppliers,
          message: "add different."
        });
      } else {
        return res.status(200).send({
          suppliers,
          message: "okay."
        });
      }
    } catch (error) {
      console.log(error);
      return next(error);
    }
  }
  // ends

  // merchant Login
  async function LoginFetch(req: Request, res: Response, next: NextFunction) {
    try {
      console.log("loginFetch : ");
      console.log("- requestBody", req.body);
      const supplier = await models.Supplier.findOne({
        username: req.body.email
      });
      console.log("SUPPLIER DOC", supplier);
      console.log("SUPPLIER", supplier.Password);
      if (!supplier) {
        res.status(400).send({ message: "The username doesn't exist" });
      }
      if (!bcrypt.compareSync(req.body.password, supplier.Password)) {
        return res.status(400).send({ message: "The password is invalid" });
      }
      console.log("login", supplier);
      return res.status(200).send(supplier);

      // console.log("suppliers:", suppliers)
      //   return res.status(200).send(suppliers);
      /*  if(!suppliers._doc.username==req.body.email&&!suppliers._doc.Password==req.body.password){
          return res.status(200).send({suppliers,
              message:"Login failed"
          });
        }else{
          return res.status(200).send({suppliers,
            message:"Logined Succesfully"
          })
        }
          */
    } catch (error) {
      console.log(error);
      return next(error);
    }
  }
  // end
  async function addnewSupplier(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      console.log("supplier error adichu", req.body);
      const newSupplier = new models.Supplier({
        ...req.body
      });
      console.log("NEW SUPPLIER", req.body);
      const suppliers = await models.Supplier.find({ ic_no: req.body.ic_no });
      const supp = await models.Supplier.find({ name: req.body.name });
      console.log("suppliers:", suppliers);
      if (!suppliers.length && !supp.length) {
        await newSupplier.save();
        return res.status(200).send({
          message: "Supplier Saved."
        });
      } else {
        if (suppliers.length)
          return res.status(200).send({
            message: "Need unique ic no."
          });
        if (supp.length)
          return res.status(200).send({
            message: "Need unique name."
          });
      }
    } catch (error) {
      console.log(error);
      return next(error);
    }
  }
  // merchant update
  /* async function updateMerchantDetails(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      console.log("updateMerchantDetails:-")
      console.log("ID", req.body)

      var hashedPassword = bcrypt.hashSync(req.body.password)

      const order = await models.Supplier.update({ ic_no: req.body.Uen_no }, {
        '$set': {
          password: hashedPassword,
          email: req.body.username,
        }
      });

      console.log(order);
      return res.status(200).send({
        message: "Supplier Saved."
      });
    } catch (error) {
      console.log(error);
      return next(error);
    }
  }*/
  // ends
  async function updatesupplier(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      console.log("supplier error adichu", req.body.data);
      const supplier = await models.Supplier.findByIdAndUpdate(req.body.id, {
        ...req.body.data
      });
      return res.status(200).send({
        message: "Supplier Updated."
      });
    } catch (error) {
      console.log(error);
      return next(error);
    }
  }
  async function getallSuppliers(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const suppliers = await models.Supplier.find({});

      return res.status(200).send({
        data: suppliers
      });
    } catch (error) {
      console.log(error);
      return next(error);
    }
  }

  async function getSupplierData(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      console.log(req.body);
      const suppliers = await models.Supplier.findById(req.body.id);

      return res.status(200).send({
        data: suppliers
      });
    } catch (error) {
      console.log(error);
      return next(error);
    }
  }

  async function getTotalOrderamnt(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const amount = 30;
      const total = await models.Order.aggregate([
        {
          $project: {
            totalAmount: { $sum: "$total.store_bp" },
            buyer: "$buyer_id"
          }
        },
        { $match: { buyer: new ObjectId(req.params.buyer) } }
      ]);
      return res.send({ total });
    } catch (err) {
      return res.send(err);
    }
  }

  async function getActiveUser(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const date = new Date();
      const prvmonth = date.getMonth();
      const amount = 20;
      let activeflag = false;
      console.log(prvmonth);
      const active = await models.Order.aggregate([
        {
          $project: {
            total: "$total",
            month: { $month: "$createdAt" },
            buyer: "$buyer_id"
          }
        },
        { $match: { month: prvmonth, buyer: new ObjectId(req.params.buyer) } }
      ]);
      console.log(active);
      for (let i = 0; i < active.length; i++) {
        const item = active[i];
        if (item.total.store_ap >= amount) {
          activeflag = true;
        } else {
          activeflag = false;
        }
      }
      return res.send({ activeflag });
    } catch (error) {
      console.log(error);
    }
  }
  // async function getEstimatedCredit( // TO GET THE DESCENDANTS OF A PATRICULAR NODE.
  //   req: Request,
  //   res: Response,
  //   next: NextFunction
  // ) {
  //   try {
  //     const { buyer_id } = req.query;
  //     const toDate = new Date(Date.now());
  //     const fromDate = new Date();
  //     let SalesAmount = 0;
  //     fromDate.setDate(1);
  //     fromDate.setHours(0, 0, 0, 0);
  //     console.log(fromDate, toDate);
  //     const condition = { "buyer_id": new ObjectId(buyer_id), "createdAt": { $gt: fromDate, $lt: toDate } };
  //     console.log("ESTIMATED", condition);
  //     const buyer = await models.Order.aggregate([
  //       {
  //         $match: condition
  //       },
  //       {
  //         "$lookup": {
  //           from: "products",
  //           localField: "products.product._id",
  //           foreignField: "_id",
  //           as: "product_cost"
  //         }
  //       },
  //       { "$unwind": "$total" },
  //       { "$unwind": "$product_cost" },
  //       { "$unwind": "$products" },
  //       { "$unwind": "$products.variants" },
  //       {
  //         "$group": {
  //           _id: "$buyer_id",
  //           amount: {
  //             $sum: "$total.store_bp"
  //           }
  //         }
  //       },
  //       {
  //         "$match": {
  //           "amount": { $gt: 30 }
  //         }
  //       }
  //     ]);
  //     console.log("CHECKING", buyer);
  //     const result = await models.User.populate(buyer, {
  //       path: "_id",
  //       select: " profile.first_name"
  //     });
  //     let count = 0;
  //     const level = await models.CommissionPercentage.findOne();
  //     console.log(result, level, "ELIGIBLE USERS");
  //     count = 0;
  //     const tmpstack1 = [];
  //     count = count + 1;
  //     if (result.length) {
  //       const it = await models.Commissiontree.find({ parent: result[0]._id._id });
  //       const parent = result[0]._id._id;
  //       const TEMP1 = it;
  //       console.log("LEVEL-01", TEMP1);
  //       TEMP1.forEach(async element => {
  //         const result1 = await calculateSales(element._id, fromDate, toDate);
  //         console.log("RESULT" , result1);
  //         if (result1.length) {
  //           SalesAmount += result1[0].profit * (level.l1 / 100);
  //           console.log("SALES-11111", SalesAmount );
  //         }
  //       });

  //       tmpstack1.push(it);
  //       const counter = 0;
  //       const child = [];
  //       const FETCH2 = [];

  //       for (let i = 0; i < TEMP1.length; i++) {
  //         console.log("TEMP1-02", TEMP1[i]._id);
  //         const it = await models.Commissiontree.find({ parent: TEMP1[i]._id });
  //         FETCH2.push(it);
  //         console.log("IT", it);
  //         for (let j = 0; j < it.length; j++) {
  //           const result1 = await calculateSales(it[j]._id, fromDate, toDate);
  //           if (result1.length) {
  //             SalesAmount += result1[0].profit * (level.l2 / 100);
  //           }
  //         }
  //       }

  //       const FETCH3 = [];
  //       const temp2 = FETCH2[0];
  //       if (temp2 && temp2.length) {
  //         for (let i = 0; i < temp2.length; i++) {
  //           const it = await models.Commissiontree.find({ parent: temp2[i]._id });
  //           FETCH3.push(it);
  //           for (let j = 0; j < it.length; j++) {
  //             const result1 = await calculateSales(it[j]._id, fromDate, toDate);
  //             if (result1.length) {
  //               SalesAmount += result1[0].profit * (level.l3 / 100);
  //             }
  //           }
  //         }
  //       }
  //       const FETCH4 = [];
  //       const temp3 = FETCH3[0];
  //       if (temp3 && temp3.length) {
  //         for (let i = 0; i < temp3.length; i++) {
  //           const it = await models.Commissiontree.find({ parent: temp2[i]._id });
  //           FETCH4.push(it);
  //           for (let j = 0; j < it.length; j++) {
  //             const result1 = await calculateSales(it[j]._id, fromDate, toDate);
  //             if (result1.length) {
  //               SalesAmount += result1[0].profit * (level.l3 / 100);
  //             }
  //           }
  //         }
  //       }

  //       const temp4 = FETCH3[0];
  //       if (temp4 && temp4.length) {
  //         for (let i = 0; i < temp4.length; i++) {
  //           const it = await models.Commissiontree.find({ parent: temp2[i]._id });
  //           for (let j = 0; j < it.length; j++) {
  //             const result1 = await calculateSales(it[j]._id, fromDate, toDate);
  //             if (result1.length) {
  //               SalesAmount += result1[0].profit * (level.l3 / 100);
  //             }
  //           }
  //         }
  //       }

  //     }

  //     return res.status(200).send({ amount: SalesAmount });
  //     // return res.status(200).send({ amount: 10});
  //   } catch (error) {
  //     return next(error);
  //   }
  // }

  // async function getEstimatedCredit(req: Request, res: Response, next: NextFunction) {
  //   // TO GET THE DESCENDANTS OF A PATRICULAR NODE.
  //   try {
  //     console.log(req.query);
  //     const { buyer_id } = req.query;
  //     const toDate = new Date(Date.now());
  //     const fromDate = new Date();
  //     const SalesAmount = 0;
  //     fromDate.setDate(1);
  //     fromDate.setHours(0, 0, 0, 0);
  //     console.log(fromDate, toDate);
  //     const Result = [];
  //     const tmpstack1 = [];
  //     let TEMP2 = [];
  //     let totalCommission = 0;

  //     // const userdata = await models.User.findById(buyer_id);
  //     const it = await models.Commissiontree.find({ parent: buyer_id });
  //     // console.log("Result1", it);
  //     const Result2 = [];
  //     const Result3 = [];
  //     const Result4 = [];
  //     const Result5 = [];
  //     const Resultfull_1 = [];
  //     const Resultfull_2 = [];
  //     const Resultfull_3 = [];
  //     const Resultfull_4 = [];
  //     const Resultfull_5 = [];

  //     Resultfull_1.push(it);


  //     for (let w = 0; w < it.length; w++) {
  //       const item = it[w];
  //       const userOrders = [];
  //       const totalPurchase = await models.Order.aggregate([
  //         {
  //           $project: {
  //             purchase: "$total.store_ap",
  //             createdAt: "$createdAt",
  //             buyer: "$buyer_id",
  //             OrderNo: "$OrderNo",
  //             status: "$status",
  //             products: "$products"
  //           }
  //         },
  //         { $match: { "buyer": new ObjectId(item._id), "createdAt": { $gt: fromDate, $lt: toDate }, status: "Paid" } }
  //       ]);

  //       if (totalPurchase.length != 0) {
  //         for (let j = 0; j < totalPurchase.length; j++) {
  //           const purchase = totalPurchase[j];
  //           let cost = 0, price = 0, margin = 0;

  //           for (let l = 0; l < purchase.products.length; l++) {
  //             const product = purchase.products[l];
  //             console.log(product.product._id);
  //             const rateDt = await models.Product.findById(product.product._id);
  //             const qty = product.variants[0].order_qty;
  //             cost = cost + rateDt.brief.cost * qty;
  //             price = price + product.product.brief.price * qty;
  //             const teCost = rateDt.brief.cost * qty;
  //             const tePrice = product.product.brief.price * qty;
  //             margin = margin + tePrice - teCost;
  //             console.log("rateDt", rateDt.brief.price);
  //           }
  //           const dataset = {
  //             OrderNo: purchase.OrderNo, user_name: item.user_name, price, cost,
  //             commission: (margin * 10) / 100, _id: item.parent, margin
  //           };
  //           totalCommission = totalCommission + dataset.commission;
  //           userOrders.push(dataset);

  //         }

  //       }
  //       if (userOrders.length != 0) {
  //         tmpstack1.push(userOrders);
  //       }

  //     }
  //     Result.push({ leval1: tmpstack1 }); // Level 1

  //     TEMP2 = Resultfull_1.pop();
  //     // console.log(TEMP2)
  //     for (let i = 0; i < TEMP2.length; i++) {
  //       const TEMP3 = await models.Commissiontree.find({
  //         parent: TEMP2[i]._id
  //       }); // Level 2
  //       Resultfull_2.push(TEMP3);
  //       let userOrders = [];
  //       for (let k = 0; k < TEMP3.length; k++) {
  //         const item = TEMP3[k];
  //         userOrders = [];
  //         const totalPurchase = await models.Order.aggregate([
  //           {
  //             $project: {
  //               purchase: "$total.store_ap",
  //               createdAt: "$createdAt",
  //               buyer: "$buyer_id",
  //               OrderNo: "$OrderNo",
  //               status: "$status",
  //               products: "$products"
  //             }
  //           },
  //           { $match: { "buyer": new ObjectId(item._id), "createdAt": { $gt: fromDate, $lt: toDate }, status: "Paid" } }
  //         ]);
  //         if (totalPurchase.length != 0) {
  //           for (let j = 0; j < totalPurchase.length; j++) {
  //             const purchase = totalPurchase[j];
  //             let cost = 0, price = 0, margin = 0;

  //             for (let l = 0; l < purchase.products.length; l++) {
  //               const product = purchase.products[l];
  //               console.log(product.product._id);
  //               const rateDt = await models.Product.findById(product.product._id);
  //               const qty = product.variants[0].order_qty;
  //               cost = cost + rateDt.brief.cost * qty;
  //               price = price + product.product.brief.price * qty;
  //               const teCost = rateDt.brief.cost * qty;
  //               const tePrice = product.product.brief.price * qty;
  //               margin = margin + tePrice - teCost;
  //               console.log("rateDt", rateDt.brief.price);
  //             }
  //             const dataset = {
  //               OrderNo: purchase.OrderNo, user_name: item.user_name, price, cost,
  //               commission: (margin * 8) / 100, _id: item.parent, margin
  //             };
  //             totalCommission = totalCommission + dataset.commission;
  //             userOrders.push(dataset);
  //           }

  //         }
  //         if (userOrders.length != 0) {
  //           Result2.push(userOrders);
  //         }

  //       }


  //     }
  //     Result.push({ leval2: Result2 });

  //     for (let m = 0; m < Resultfull_2.length; m++) {
  //       const temp = Resultfull_2[m];
  //       for (let i = 0; i < temp.length; i++) {
  //         const TEMP3 = await models.Commissiontree.find({
  //           parent: temp[i]._id
  //         }); // Level 3
  //         Resultfull_3.push(TEMP3);
  //         let userOrders = [];
  //         for (let k = 0; k < TEMP3.length; k++) {
  //           const item = TEMP3[k];
  //           userOrders = [];
  //           const totalPurchase = await models.Order.aggregate([
  //             {
  //               $project: {
  //                 purchase: "$total.store_ap",
  //                 createdAt: "$createdAt",
  //                 buyer: "$buyer_id",
  //                 OrderNo: "$OrderNo",
  //                 status: "$status",
  //                 products: "$products"
  //               }
  //             },
  //             { $match: { "buyer": new ObjectId(item._id), "createdAt": { $gt: fromDate, $lt: toDate }, status: "Paid" } }

  //           ]);
  //           if (totalPurchase.length != 0) {
  //             for (let j = 0; j < totalPurchase.length; j++) {
  //               const purchase = totalPurchase[j];
  //               let cost = 0, price = 0, margin = 0;

  //               for (let l = 0; l < purchase.products.length; l++) {
  //                 const product = purchase.products[l];
  //                 console.log(product.product._id);
  //                 const rateDt = await models.Product.findById(product.product._id);
  //                 const qty = product.variants[0].order_qty;
  //                 cost = cost + rateDt.brief.cost * qty;
  //                 price = price + product.product.brief.price * qty;
  //                 const teCost = rateDt.brief.cost * qty;
  //                 const tePrice = product.product.brief.price * qty;
  //                 margin = margin + tePrice - teCost;
  //                 console.log("rateDt", rateDt.brief.price);
  //               }
  //               const dataset = {
  //                 OrderNo: purchase.OrderNo, user_name: item.user_name, price, cost,
  //                 commission: (margin * 6) / 100, _id: item.parent, margin
  //               };
  //               totalCommission = totalCommission + dataset.commission;
  //               userOrders.push(dataset);
  //             }

  //           }
  //           if (userOrders.length != 0) {
  //             Result3.push(userOrders);
  //           }
  //         }



  //       }
  //     }
  //     Result.push({ leval3: Result3 });
  //     for (let m = 0; m < Resultfull_3.length; m++) {
  //       const temp = Resultfull_3[m];
  //       for (let i = 0; i < temp.length; i++) {
  //         const TEMP3 = await models.Commissiontree.find({
  //           parent: temp[i]._id
  //         }); // Level 4
  //         Resultfull_4.push(TEMP3);

  //         let userOrders = [];
  //         for (let k = 0; k < TEMP3.length; k++) {
  //           const item = TEMP3[k];
  //           userOrders = [];
  //           const totalPurchase = await models.Order.aggregate([
  //             {
  //               $project: {
  //                 purchase: "$total.store_ap",
  //                 createdAt: "$createdAt",
  //                 buyer: "$buyer_id",
  //                 OrderNo: "$OrderNo",
  //                 status: "$status",
  //                 products: "$products"
  //               }
  //             },
  //             { $match: { "buyer": new ObjectId(item._id), "createdAt": { $gt: fromDate, $lt: toDate }, status: "Paid" } }

  //           ]);
  //           if (totalPurchase.length != 0) {
  //             for (let j = 0; j < totalPurchase.length; j++) {
  //               const purchase = totalPurchase[j];
  //               let cost = 0, price = 0, margin = 0;

  //               for (let l = 0; l < purchase.products.length; l++) {
  //                 const product = purchase.products[l];
  //                 console.log(product.product._id);
  //                 const rateDt = await models.Product.findById(product.product._id);
  //                 const qty = product.variants[0].order_qty;
  //                 cost = cost + rateDt.brief.cost * qty;
  //                 price = price + product.product.brief.price * qty;
  //                 const teCost = rateDt.brief.cost * qty;
  //                 const tePrice = product.product.brief.price * qty;
  //                 margin = margin + tePrice - teCost;
  //                 console.log("rateDt", rateDt.brief.price);
  //               }
  //               const dataset = {
  //                 OrderNo: purchase.OrderNo, user_name: item.user_name, price, cost,
  //                 commission: (margin * 4) / 100, _id: item.parent, margin
  //               };
  //               totalCommission = totalCommission + dataset.commission;
  //               userOrders.push(dataset);
  //             }

  //           }
  //           if (userOrders.length != 0) {
  //             Result4.push(userOrders);
  //           }
  //         }


  //       }
  //     }
  //     Result.push({ leval4: Result4 });

  //     for (let m = 0; m < Resultfull_4.length; m++) {
  //       const temp = Resultfull_4[m];
  //       for (let i = 0; i < temp.length; i++) {
  //         const TEMP3 = await models.Commissiontree.find({
  //           parent: temp[i]._id
  //         });
  //         let userOrders = [];
  //         for (let k = 0; k < TEMP3.length; k++) {
  //           const item = TEMP3[k];
  //           userOrders = [];
  //           const totalPurchase = await models.Order.aggregate([
  //             {
  //               $project: {
  //                 purchase: "$total.store_ap",
  //                 createdAt: "$createdAt",
  //                 buyer: "$buyer_id",
  //                 OrderNo: "$OrderNo",
  //                 status: "$status",
  //                 products: "$products"
  //               }
  //             },
  //             { $match: { "buyer": new ObjectId(item._id), "createdAt": { $gt: fromDate, $lt: toDate }, status: "Paid" } }

  //           ]);
  //           if (totalPurchase.length != 0) {
  //             for (let j = 0; j < totalPurchase.length; j++) {
  //               const purchase = totalPurchase[j];
  //               let cost = 0, price = 0, margin = 0;

  //               for (let l = 0; l < purchase.products.length; l++) {
  //                 const product = purchase.products[l];
  //                 console.log(product.product._id);
  //                 const rateDt = await models.Product.findById(product.product._id);
  //                 const qty = product.variants[0].order_qty;
  //                 cost = cost + rateDt.brief.cost * qty;
  //                 price = price + product.product.brief.price * qty;
  //                 const teCost = rateDt.brief.cost * qty;
  //                 const tePrice = product.product.brief.price * qty;
  //                 margin = margin + tePrice - teCost;
  //                 console.log("rateDt", rateDt.brief.price);
  //               }
  //               const dataset = {
  //                 OrderNo: purchase.OrderNo, user_name: item.user_name, price, cost,
  //                 commission: (margin * 2) / 100, _id: item.parent, margin
  //               };
  //               totalCommission = totalCommission + dataset.commission;
  //               userOrders.push(dataset);
  //             }

  //           }
  //           if (userOrders.length != 0) {

  //             Result5.push(userOrders);
  //           }
  //         }
  //       }
  //     }
  //     Result.push({ leval5: Result5 });
  //     return res.status(200).send({
  //       //  Result,
  //       amount: totalCommission
  //     });
  //   } catch (error) {
  //     console.log(error);
  //     return next(error);
  //   }
  // }

  // async function getEstimatedCredit(req: Request, res: Response, next: NextFunction) {
  //   // TO GET THE DESCENDANTS OF A PATRICULAR NODE.
  //   try {
  //     console.log("req.query", req.query);
  //     const { buyer_id } = req.query;
  //     console.log("buyer_id", buyer_id);
  //     const toDate = new Date(Date.now());
  //     const fromDate = new Date();
  //     const SalesAmount = 0;
  //     fromDate.setDate(1);
  //     fromDate.setHours(0, 0, 0, 0);
  //     console.log(fromDate, toDate);
  //     const Result = [];
  //     const tmpstack1 = [];
  //     let TEMP2 = [];
  //     let totalCommission = 0;
  //     const Result1 = [];
  //     const Result2 = [];
  //     const Result3 = [];
  //     const Result4 = [];
  //     const Result5 = [];
  //     const Resultfull_1 = [];
  //     const Resultfull_2 = [];
  //     const Resultfull_3 = [];
  //     const Resultfull_4 = [];
  //     const Resultfull_5 = [];
  //     // const item = it[w];
  //     const buyer = await models.Commissiontree.findOne({ _id: buyer_id });
  //     console.log("buyer", buyer);
  //       const userOrders = [];
  //       const totalPurchase = await models.Order.aggregate([
  //         {
  //           $project: {
  //             purchase: "$total.store_ap",
  //             createdAt: "$createdAt",
  //             buyer: "$buyer_id",
  //             OrderNo: "$OrderNo",
  //             status: "$status",
  //             products: "$products"
  //           }
  //         },
  //         { $match: { "buyer": new ObjectId(buyer_id), "createdAt": { $gt: fromDate, $lt: toDate }, status: "Paid" } }
  //       ]);

  //       if (totalPurchase.length != 0) {
  //         for (let j = 0; j < totalPurchase.length; j++) {
  //           const purchase = totalPurchase[j];
  //           let cost = 0, price = 0, margin = 0;

  //           for (let l = 0; l < purchase.products.length; l++) {
  //             const product = purchase.products[l];
  //             console.log(product.product._id);
  //             const rateDt = await models.Product.findById(product.product._id);
  //             const qty = product.variants[0].order_qty;
  //             cost = cost + rateDt.brief.cost * qty;
  //             price = price + product.product.brief.price * qty;
  //             const teCost = rateDt.brief.cost * qty;
  //             const tePrice = product.product.brief.price * qty;
  //             margin = margin + tePrice - teCost;
  //             console.log("rateDt", rateDt.brief.price);
  //           }
  //           const dataset = {
  //             OrderNo: purchase.OrderNo, user_name: buyer.user_name, price, cost,
  //             commission: (margin * 10) / 100, _id: buyer.parent, margin
  //           };
  //           totalCommission = totalCommission + dataset.commission;
  //           userOrders.push(dataset);

  //         }

  //       }
  //       if (userOrders.length != 0) {
  //       Result1.push(userOrders);
  //       }
  //       Result.push({ leval1: Result1 });
  //     // const userdata = await models.User.findById(buyer_id);
  //     const it = await models.Commissiontree.find({ parent: buyer_id });
  //     // console.log("Result1", it);


  //     Resultfull_1.push(it);


  //     for (let w = 0; w < it.length; w++) {
  //       const item = it[w];
  //       const userOrders = [];
  //       const totalPurchase = await models.Order.aggregate([
  //         {
  //           $project: {
  //             purchase: "$total.store_ap",
  //             createdAt: "$createdAt",
  //             buyer: "$buyer_id",
  //             OrderNo: "$OrderNo",
  //             status: "$status",
  //             products: "$products"
  //           }
  //         },
  //         { $match: { "buyer": new ObjectId(item._id), "createdAt": { $gt: fromDate, $lt: toDate }, status: "Paid" } }
  //       ]);

  //       if (totalPurchase.length != 0) {
  //         for (let j = 0; j < totalPurchase.length; j++) {
  //           const purchase = totalPurchase[j];
  //           let cost = 0, price = 0, margin = 0;

  //           for (let l = 0; l < purchase.products.length; l++) {
  //             const product = purchase.products[l];
  //             console.log(product.product._id);
  //             const rateDt = await models.Product.findById(product.product._id);
  //             const qty = product.variants[0].order_qty;
  //             cost = cost + rateDt.brief.cost * qty;
  //             price = price + product.product.brief.price * qty;
  //             const teCost = rateDt.brief.cost * qty;
  //             const tePrice = product.product.brief.price * qty;
  //             margin = margin + tePrice - teCost;
  //             console.log("rateDt", rateDt.brief.price);
  //           }
  //           const dataset = {
  //             OrderNo: purchase.OrderNo, user_name: item.user_name, price, cost,
  //             commission: (margin * 8) / 100, _id: item.parent, margin
  //           };
  //           totalCommission = totalCommission + dataset.commission;
  //           userOrders.push(dataset);

  //         }

  //       }
  //       if (userOrders.length != 0) {
  //         tmpstack1.push(userOrders);
  //       }

  //     }
  //     Result.push({ leval2: tmpstack1 }); // Level 1

  //     TEMP2 = Resultfull_1.pop();
  //     // console.log(TEMP2)
  //     for (let i = 0; i < TEMP2.length; i++) {
  //       const TEMP3 = await models.Commissiontree.find({
  //         parent: TEMP2[i]._id
  //       }); // Level 2
  //       Resultfull_2.push(TEMP3);
  //       let userOrders = [];
  //       for (let k = 0; k < TEMP3.length; k++) {
  //         const item = TEMP3[k];
  //         userOrders = [];
  //         const totalPurchase = await models.Order.aggregate([
  //           {
  //             $project: {
  //               purchase: "$total.store_ap",
  //               createdAt: "$createdAt",
  //               buyer: "$buyer_id",
  //               OrderNo: "$OrderNo",
  //               status: "$status",
  //               products: "$products"
  //             }
  //           },
  //           { $match: { "buyer": new ObjectId(item._id), "createdAt": { $gt: fromDate, $lt: toDate }, status: "Paid" } }
  //         ]);
  //         if (totalPurchase.length != 0) {
  //           for (let j = 0; j < totalPurchase.length; j++) {
  //             const purchase = totalPurchase[j];
  //             let cost = 0, price = 0, margin = 0;

  //             for (let l = 0; l < purchase.products.length; l++) {
  //               const product = purchase.products[l];
  //               console.log(product.product._id);
  //               const rateDt = await models.Product.findById(product.product._id);
  //               const qty = product.variants[0].order_qty;
  //               cost = cost + rateDt.brief.cost * qty;
  //               price = price + product.product.brief.price * qty;
  //               const teCost = rateDt.brief.cost * qty;
  //               const tePrice = product.product.brief.price * qty;
  //               margin = margin + tePrice - teCost;
  //               console.log("rateDt", rateDt.brief.price);
  //             }
  //             const dataset = {
  //               OrderNo: purchase.OrderNo, user_name: item.user_name, price, cost,
  //               commission: (margin * 6) / 100, _id: item.parent, margin
  //             };
  //             totalCommission = totalCommission + dataset.commission;
  //             userOrders.push(dataset);
  //           }

  //         }
  //         if (userOrders.length != 0) {
  //           Result3.push(userOrders);
  //         }

  //       }


  //     }
  //     Result.push({ leval3: Result3 });

  //     for (let m = 0; m < Resultfull_2.length; m++) {
  //       const temp = Resultfull_2[m];
  //       for (let i = 0; i < temp.length; i++) {
  //         const TEMP3 = await models.Commissiontree.find({
  //           parent: temp[i]._id
  //         }); // Level 3
  //         Resultfull_3.push(TEMP3);
  //         let userOrders = [];
  //         for (let k = 0; k < TEMP3.length; k++) {
  //           const item = TEMP3[k];
  //           userOrders = [];
  //           const totalPurchase = await models.Order.aggregate([
  //             {
  //               $project: {
  //                 purchase: "$total.store_ap",
  //                 createdAt: "$createdAt",
  //                 buyer: "$buyer_id",
  //                 OrderNo: "$OrderNo",
  //                 status: "$status",
  //                 products: "$products"
  //               }
  //             },
  //             { $match: { "buyer": new ObjectId(item._id), "createdAt": { $gt: fromDate, $lt: toDate }, status: "Paid" } }

  //           ]);
  //           if (totalPurchase.length != 0) {
  //             for (let j = 0; j < totalPurchase.length; j++) {
  //               const purchase = totalPurchase[j];
  //               let cost = 0, price = 0, margin = 0;

  //               for (let l = 0; l < purchase.products.length; l++) {
  //                 const product = purchase.products[l];
  //                 console.log(product.product._id);
  //                 const rateDt = await models.Product.findById(product.product._id);
  //                 const qty = product.variants[0].order_qty;
  //                 cost = cost + rateDt.brief.cost * qty;
  //                 price = price + product.product.brief.price * qty;
  //                 const teCost = rateDt.brief.cost * qty;
  //                 const tePrice = product.product.brief.price * qty;
  //                 margin = margin + tePrice - teCost;
  //                 console.log("rateDt", rateDt.brief.price);
  //               }
  //               const dataset = {
  //                 OrderNo: purchase.OrderNo, user_name: item.user_name, price, cost,
  //                 commission: (margin * 4) / 100, _id: item.parent, margin
  //               };
  //               totalCommission = totalCommission + dataset.commission;
  //               userOrders.push(dataset);
  //             }

  //           }
  //           if (userOrders.length != 0) {
  //             Result4.push(userOrders);
  //           }
  //         }



  //       }
  //     }
  //     Result.push({ leval4: Result4 });
  //     for (let m = 0; m < Resultfull_3.length; m++) {
  //       const temp = Resultfull_3[m];
  //       for (let i = 0; i < temp.length; i++) {
  //         const TEMP3 = await models.Commissiontree.find({
  //           parent: temp[i]._id
  //         }); // Level 4
  //         Resultfull_4.push(TEMP3);

  //         let userOrders = [];
  //         for (let k = 0; k < TEMP3.length; k++) {
  //           const item = TEMP3[k];
  //           userOrders = [];
  //           const totalPurchase = await models.Order.aggregate([
  //             {
  //               $project: {
  //                 purchase: "$total.store_ap",
  //                 createdAt: "$createdAt",
  //                 buyer: "$buyer_id",
  //                 OrderNo: "$OrderNo",
  //                 status: "$status",
  //                 products: "$products"
  //               }
  //             },
  //             { $match: { "buyer": new ObjectId(item._id), "createdAt": { $gt: fromDate, $lt: toDate }, status: "Paid" } }

  //           ]);
  //           if (totalPurchase.length != 0) {
  //             for (let j = 0; j < totalPurchase.length; j++) {
  //               const purchase = totalPurchase[j];
  //               let cost = 0, price = 0, margin = 0;

  //               for (let l = 0; l < purchase.products.length; l++) {
  //                 const product = purchase.products[l];
  //                 console.log(product.product._id);
  //                 const rateDt = await models.Product.findById(product.product._id);
  //                 const qty = product.variants[0].order_qty;
  //                 cost = cost + rateDt.brief.cost * qty;
  //                 price = price + product.product.brief.price * qty;
  //                 const teCost = rateDt.brief.cost * qty;
  //                 const tePrice = product.product.brief.price * qty;
  //                 margin = margin + tePrice - teCost;
  //                 console.log("rateDt", rateDt.brief.price);
  //               }
  //               const dataset = {
  //                 OrderNo: purchase.OrderNo, user_name: item.user_name, price, cost,
  //                 commission: (margin * 2) / 100, _id: item.parent, margin
  //               };
  //               totalCommission = totalCommission + dataset.commission;
  //               userOrders.push(dataset);
  //             }

  //           }
  //           if (userOrders.length != 0) {
  //             Result5.push(userOrders);
  //           }
  //         }


  //       }
  //     }
  //     Result.push({ leval5: Result5 });

  //     console.log("totalCommission", totalCommission);
  //     console.log("amount", totalCommission);

  //     return res.status(200).send({
  //        Result,
  //       amount: totalCommission
  //     });
  //   } catch (error) {
  //     console.log(error);
  //     return next(error);
  //   }
  // }

  async function getEstimatedCredit(req: Request, res: Response, next: NextFunction) {
    // TO GET THE DESCENDANTS OF A PATRICULAR NODE.
    try {
      console.log(req.query);
      const { buyer_id } = req.query;
      // const buyer = await models.Commissiontree.findOne({ _id: buyer_id });
      // console.log("buyer", buyer);


      const toDate = new Date(Date.now());
      const fromDate = new Date();
      const SalesAmount = 0;
      fromDate.setDate(1);
      fromDate.setHours(0, 0, 0, 0);
      console.log("fromDate", fromDate, toDate);
      const Result = [];
      const tmpstack1 = [];
      let TEMP2 = [];
      let totalCommission = 0;
      const Result1 = [];
      const Result2 = [];
      const Result3 = [];
      const Result4 = [];
      const Result5 = [];
      const Resultfull_1 = [];
      const Resultfull_2 = [];
      const Resultfull_3 = [];
      const Resultfull_4 = [];
      const Resultfull_5 = [];

      const buyer = await models.Commissiontree.findOne({ _id: buyer_id });
      if (!buyer) {
        const user  =  await models.User.findById(buyer_id);
        const parent = await models.Commissiontree.findOne({ parent: null });
        const node = new models.Commissiontree({
          _id: buyer_id,
          user_name: user.profile.first_name,
          parent: parent._id,
          someadditionalattr: "Gstock",
          order: 8
        });
        console.log("console2");
        console.log("not savedd");
        console.log("ERRORRRRRRRRR", node);
        const newNode = await node.save();

      }
        const userOrders = [];
        const totalPurchase = await models.Order.aggregate([
          {
            $project: {
              purchase: "$total.store_ap",
              createdAt: "$createdAt",
              buyer: "$buyer_id",
              OrderNo: "$OrderNo",
              status: "$status",
              products: "$products"
            }
          },
          { $match: { "buyer": new ObjectId(buyer_id), "createdAt": { $gt: fromDate, $lt: toDate }, status: "Paid" } }
        ]);
        let sum = 0;
       // if (totalPurchase.length != 0) {
          for (const purchase of totalPurchase) {
            sum += purchase.purchase;
          }
          console.log("sum", sum);
          if (sum >= 30) {
            if (totalPurchase.length != 0) {
              for (let j = 0; j < totalPurchase.length; j++) {
                const purchase = totalPurchase[j];
                let cost = 0, price = 0, margin = 0;

                for (let l = 0; l < purchase.products.length; l++) {
                  const product = purchase.products[l];
                  console.log(product.product._id);
                  const rateDt = await models.Product.findById(product.product._id);
                  const qty = product.variants[0].order_qty;
                  cost = cost + rateDt.brief.cost * qty;
                  price = price + product.product.brief.price * qty;
                  const teCost = rateDt.brief.cost * qty;
                  const tePrice = product.product.brief.price * qty;
                  margin = margin + tePrice - teCost;
                  console.log("rateDt", rateDt.brief.price);
                }
                const dataset = {
                  OrderNo: purchase.OrderNo, user_name: buyer.user_name, price, cost,
                  commission: (margin * 10) / 100, _id: buyer.parent, margin
                };
                totalCommission = totalCommission + dataset.commission;
                userOrders.push(dataset);

              }

            }
            if (userOrders.length != 0) {
            Result1.push(userOrders);
            }
            Result.push({ leval1: Result1 });
          // const userdata = await models.User.findById(buyer_id);
          const it = await models.Commissiontree.find({ parent: buyer_id });
          // console.log("Result1", it);


          Resultfull_1.push(it);


          for (let w = 0; w < it.length; w++) {
            const item = it[w];
            const userOrders = [];
            const totalPurchase = await models.Order.aggregate([
              {
                $project: {
                  purchase: "$total.store_ap",
                  createdAt: "$createdAt",
                  buyer: "$buyer_id",
                  OrderNo: "$OrderNo",
                  status: "$status",
                  products: "$products"
                }
              },
              { $match: { "buyer": new ObjectId(item._id), "createdAt": { $gt: fromDate, $lt: toDate }, status: "Paid" } }
            ]);
            const sum = 0;
            if (totalPurchase.length != 0) {

                for (let j = 0; j < totalPurchase.length; j++) {
                  const purchase = totalPurchase[j];
                  let cost = 0, price = 0, margin = 0;

                  for (let l = 0; l < purchase.products.length; l++) {
                    const product = purchase.products[l];
                    console.log(product.product._id);
                    const rateDt = await models.Product.findById(product.product._id);
                    const qty = product.variants[0].order_qty;
                    cost = cost + rateDt.brief.cost * qty;
                    price = price + product.product.brief.price * qty;
                    const teCost = rateDt.brief.cost * qty;
                    const tePrice = product.product.brief.price * qty;
                    margin = margin + tePrice - teCost;
                    console.log("rateDt", rateDt.brief.price);
                  }
                  const dataset = {
                    OrderNo: purchase.OrderNo, user_name: item.user_name, price, cost,
                    commission: (margin * 8) / 100, _id: item.parent, margin
                  };
                  totalCommission = totalCommission + dataset.commission;
                  userOrders.push(dataset);

                }



            }
            if (userOrders.length != 0) {
              tmpstack1.push(userOrders);
            }

          }
          Result.push({ leval2: tmpstack1 }); // Level 1

          TEMP2 = Resultfull_1.pop();
          // console.log(TEMP2)
          for (let i = 0; i < TEMP2.length; i++) {
            const TEMP3 = await models.Commissiontree.find({
              parent: TEMP2[i]._id
            }); // Level 2
            Resultfull_2.push(TEMP3);
            let userOrders = [];
            for (let k = 0; k < TEMP3.length; k++) {
              const item = TEMP3[k];
              userOrders = [];
              const totalPurchase = await models.Order.aggregate([
                {
                  $project: {
                    purchase: "$total.store_ap",
                    createdAt: "$createdAt",
                    buyer: "$buyer_id",
                    OrderNo: "$OrderNo",
                    status: "$status",
                    products: "$products"
                  }
                },
                { $match: { "buyer": new ObjectId(item._id), "createdAt": { $gt: fromDate, $lt: toDate }, status: "Paid" } }
              ]);
              if (totalPurchase.length != 0) {
                for (let j = 0; j < totalPurchase.length; j++) {
                  const purchase = totalPurchase[j];
                  let cost = 0, price = 0, margin = 0;

                  for (let l = 0; l < purchase.products.length; l++) {
                    const product = purchase.products[l];
                    console.log(product.product._id);
                    const rateDt = await models.Product.findById(product.product._id);
                    const qty = product.variants[0].order_qty;
                    cost = cost + rateDt.brief.cost * qty;
                    price = price + product.product.brief.price * qty;
                    const teCost = rateDt.brief.cost * qty;
                    const tePrice = product.product.brief.price * qty;
                    margin = margin + tePrice - teCost;
                    console.log("rateDt", rateDt.brief.price);
                  }
                  const dataset = {
                    OrderNo: purchase.OrderNo, user_name: item.user_name, price, cost,
                    commission: (margin * 6) / 100, _id: item.parent, margin
                  };
                  totalCommission = totalCommission + dataset.commission;
                  userOrders.push(dataset);
                }

              }
              if (userOrders.length != 0) {
                Result3.push(userOrders);
              }

            }


          }
          Result.push({ leval3: Result3 });

          for (let m = 0; m < Resultfull_2.length; m++) {
            const temp = Resultfull_2[m];
            for (let i = 0; i < temp.length; i++) {
              const TEMP3 = await models.Commissiontree.find({
                parent: temp[i]._id
              }); // Level 3
              Resultfull_3.push(TEMP3);
              let userOrders = [];
              for (let k = 0; k < TEMP3.length; k++) {
                const item = TEMP3[k];
                userOrders = [];
                const totalPurchase = await models.Order.aggregate([
                  {
                    $project: {
                      purchase: "$total.store_ap",
                      createdAt: "$createdAt",
                      buyer: "$buyer_id",
                      OrderNo: "$OrderNo",
                      status: "$status",
                      products: "$products"
                    }
                  },
                  { $match: { "buyer": new ObjectId(item._id), "createdAt": { $gt: fromDate, $lt: toDate }, status: "Paid" } }

                ]);
                if (totalPurchase.length != 0) {
                  for (let j = 0; j < totalPurchase.length; j++) {
                    const purchase = totalPurchase[j];
                    let cost = 0, price = 0, margin = 0;

                    for (let l = 0; l < purchase.products.length; l++) {
                      const product = purchase.products[l];
                      console.log(product.product._id);
                      const rateDt = await models.Product.findById(product.product._id);
                      const qty = product.variants[0].order_qty;
                      cost = cost + rateDt.brief.cost * qty;
                      price = price + product.product.brief.price * qty;
                      const teCost = rateDt.brief.cost * qty;
                      const tePrice = product.product.brief.price * qty;
                      margin = margin + tePrice - teCost;
                      console.log("rateDt", rateDt.brief.price);
                    }
                    const dataset = {
                      OrderNo: purchase.OrderNo, user_name: item.user_name, price, cost,
                      commission: (margin * 4) / 100, _id: item.parent, margin
                    };
                    totalCommission = totalCommission + dataset.commission;
                    userOrders.push(dataset);
                  }

                }
                if (userOrders.length != 0) {
                  Result4.push(userOrders);
                }
              }



            }
          }
          Result.push({ leval4: Result4 });
          for (let m = 0; m < Resultfull_3.length; m++) {
            const temp = Resultfull_3[m];
            for (let i = 0; i < temp.length; i++) {
              const TEMP3 = await models.Commissiontree.find({
                parent: temp[i]._id
              }); // Level 4
              Resultfull_4.push(TEMP3);

              let userOrders = [];
              for (let k = 0; k < TEMP3.length; k++) {
                const item = TEMP3[k];
                userOrders = [];
                const totalPurchase = await models.Order.aggregate([
                  {
                    $project: {
                      purchase: "$total.store_ap",
                      createdAt: "$createdAt",
                      buyer: "$buyer_id",
                      OrderNo: "$OrderNo",
                      status: "$status",
                      products: "$products"
                    }
                  },
                  { $match: { "buyer": new ObjectId(item._id), "createdAt": { $gt: fromDate, $lt: toDate }, status: "Paid" } }

                ]);
                if (totalPurchase.length != 0) {
                  for (let j = 0; j < totalPurchase.length; j++) {
                    const purchase = totalPurchase[j];
                    let cost = 0, price = 0, margin = 0;

                    for (let l = 0; l < purchase.products.length; l++) {
                      const product = purchase.products[l];
                      console.log(product.product._id);
                      const rateDt = await models.Product.findById(product.product._id);
                      const qty = product.variants[0].order_qty;
                      cost = cost + rateDt.brief.cost * qty;
                      price = price + product.product.brief.price * qty;
                      const teCost = rateDt.brief.cost * qty;
                      const tePrice = product.product.brief.price * qty;
                      margin = margin + tePrice - teCost;
                      console.log("rateDt", rateDt.brief.price);
                    }
                    const dataset = {
                      OrderNo: purchase.OrderNo, user_name: item.user_name, price, cost,
                      commission: (margin * 2) / 100, _id: item.parent, margin
                    };
                    totalCommission = totalCommission + dataset.commission;
                    userOrders.push(dataset);
                  }

                }
                if (userOrders.length != 0) {
                  Result5.push(userOrders);
                }
              }


            }
          }
          Result.push({ leval5: Result5 });

          }




      return res.status(200).send({
         Result,
        amount: totalCommission
      });
    } catch (error) {
      console.log(error);
      return next(error);
    }
  }
  // async function getBuyerFavorites(
  //   req: Request,
  //   res: Response,
  //   next: NextFunction
  // ) {
  //   try {
  //     const productList = [];
  //     console.log("getBuyerFavorites");
  //     const orders = await models.Order.find(
  //       {
  //         buyer_id: req.query.buyer_id,
  //       }
  //       // { distinct: "products.product._id" }
  //     );

  //     const products1 = await models.Product.populate(orders, {
  //       path: "products.product._id",
  //       // select: "brief detail pricing stock category_id",
  //       distinct: "products.product._id",
  //     });
  //     // console.log("15042020", products1);
  //     for (const key in products1) {
  //       for (let i = 0; i < products1[key].products.length; i++) {
  //         productList.push(
  //                          products1[key].products[i].product["_id"]
  //           ,
  //         );
  //       }
  //     }
  //     return res.status(200).send(productList);
  //   } catch (error) {
  //     console.log(error);
  //     throw "Failed to get order details";
  //   }
  // }

  async function getBuyerFavorites(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const productList = [];
      console.log("getBuyerFavorites");
      const orders = await models.Order.find(
        {
          buyer_id: req.query.buyer_id,
        }
        // { distinct: "products.product._id" }
      );

      const products1 = await models.Product.populate(orders, {
        path: "products.product._id",
        match: { is_active: true },
        // select: "brief detail pricing stock category_id",
        // distinct: "products.product._id",
      });

      // console.log("15042020", products1);
      for (const key in products1) {
        for (let i = 0; i < products1[key].products.length; i++) {
          productList.push(products1[key].products[i].product["_id"]);
        }
      }

      const uniqueArray = (productList) =>
        productList.filter((ar, index) => {
          return (
            index ===
            productList.findIndex((obj) => {
              return JSON.stringify(obj) === JSON.stringify(ar);
            })
          );
        });

      const filtered = uniqueArray(productList).filter(function (el) {
        return el != null;
      });

      // console.log("productList", uniqueArray(productList));

      return res.status(200).send(filtered);
    } catch (error) {
      console.log(error);
      throw "Failed to get order details";
    }
  }
  return {
    createNewUser,
    getUserDetail,
    updateUserDetail,
    updateMerchantAssignedCommission,
    deleteMerchantAssignedCommission,
    getBuyerCreditWalletList,
    getBuyerCreditWallet,
    addNewEntryToBuyerCreditWallet,
    changeStatus,
    // updateMerchantDetails,
    merchantnameFetch,
    deleteAccount,
    activateAccount,
    suspendAccount,
    addnewSupplier,
    getallSuppliers,
    getSupplierData,
    updatesupplier,
    buildLink,
    updateCommission,
    createBuyerSalesReturn,
    getSalesReturnOrders,
    buildLinkTree,
    showtrees,
    getOrderStatus,
    getActiveUser,
    getTotalOrderamnt,
    saveBuyerBankInfo,
    getWalletWithdrawalDetails,
    createImage,
    getEstimatedCredit,
    getBuyerFavorites
  };
};

import * as del from "del";
import { NextFunction, Request, Response } from "express";
import * as mkdirp from "mkdirp";
import { Types } from "mongoose";
import * as multer from "multer";
import * as fs from "fs-extra";
import paths from "../config/path";
import { isArrayUnique } from "../_lib/array";
import { ObjectID } from "bson";
import { UserModel } from "../models/User";
import { flatten } from "../_lib/flatObjOnly";
import { isVariantPriceInRange } from "../_global/business";
import { Config } from "../types/app";
import { Passport } from "passport";
import { RSA_NO_PADDING } from "constants";
import * as nodemailer from "nodemailer";
import * as Handlebars from "handlebars";
import * as dateFormat from "dateformat";
import { MonthlyCommissionController } from ".";


export class OrderApprovalController {
    public models;
    public userPassport: Passport;

    constructor(private config) {
        this.models = config.models;
        this.userPassport = config.passport.userPassport.getUserPassport();
    }

     // Approve Pending Orders
    //  public async create(req: Request,
    //     res: Response,
    //     next: NextFunction) {
    //         try {
    //             const order_approval = await this.models.OrderApproval(req.body).save();
    //             const order = await this.models.Order.update({OrderNo: req.body.order_no}, {"$set": {
    //                 status: "Paid"}
    //             });
    //             const orders = await this.models.Order.findOne({OrderNo: req.body.order_no}).populate({
    //                 path: "buyer_id merchant_id",
    //                 select: "contact profile credential.email",
    //                 model: this.models.User
    //               });
    //             console.log(orders.buyer_id.credential);
    //             const root = "./src/emailTemplate";
    //             console.log(root);
    //             const imagesPath = `${root}/welcome/images`;
    //             const templatePath = `${root}/order.html`;
    //             let mailOptions = {};
    //             const smtpTransport = nodemailer.createTransport({
    //               port: 587,
    //               host: "smtp.office365.com",
    //               secure: false,
    //               auth: {
    //                 user: "no-reply@gstock.sg",
    //                 pass: "GSto2819"
    //               },
    //               tls: {
    //                 rejectUnauthorized: false
    //               }
    //             });
    //             fs.readFile(templatePath, (err, chunk) => {
    //               if (err) {
    //                 console.log(err);
    //                 return next(err);
    //               }
    //               const html = chunk.toString("utf8");
    //               const template = Handlebars.compile(html);
    //               const current_time = new Date();
    //               const someDate = new Date();
    //               const numberOfDaysToAdd = 3;
    //               someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
    //               const dd = someDate.getDate();
    //               const mm = someDate.getMonth() + 1;
    //               const y = someDate.getFullYear();
    //               const deliveryDate = dd + "/" + mm + "/" + y;

    //               const imgSrc =
    //                 "https://gstockservices.glux.sg/uploads/user/undefined/products/";
    //               console.log(orders);
    //               const data = {
    //                 fromEmail: "no-reply@gstock.sg",
    //                 mail_obj: orders,
    //                 paypal: req.body.paypal,
    //                 productArray: orders.products,
    //                 time: dateFormat(current_time, "shortTime"),
    //                 order_date: dateFormat(orders.createdAt, "mediumDate"),
    //                 imgSrc,
    //                 deliveryDate
    //               };
    //               const result = template(data);
    //               console.log(orders.buyer_id.credential.email, "email");
    //               mailOptions = {
    //                 to: [orders.buyer_id.credential.email],
    //                // bcc: ["dessa@dinghe.sg", "toh@dinghe.sg"],
    //                bcc: ["dessa@dinghe.sg", "angi@dinghe.sg", "dhania@dinghe.sg", "pratheesh@dinghe.sg", "toh@dinghe.sg", "jovi@dinghe.sg", "eric@dinghe.sg"],
    //                 from: "no-reply@gstock.sg",
    //                 subject: "Thank you for shopping on GSTOCK",
    //                 html: result
    //               };
    //               const sendEmail = new Promise((resolve, reject) => {
    //                 smtpTransport.sendMail(mailOptions, (error, response: any) => {
    //                   if (error) {
    //                     console.log(error);
    //                     reject(error);
    //                   } else {
    //                     console.log(response);
    //                     resolve(response);
    //                   }
    //                 });
    //               });
    //             });
    //             return res.status(200).send(order_approval);

    //         } catch (error) {
    //             return next(error);
    //         }
    // }
    public async create(req: Request,
      res: Response,
      next: NextFunction) {
      try {
        const order_approval = await this.models.OrderApproval(req.body).save();
        const order = await this.models.Order.update({ OrderNo: req.body.order_no }, {
          "$set": {
            status: "Paid"
          }
        });
        const orders = await this.models.Order.findOne({ OrderNo: req.body.order_no }).populate({
          path: "buyer_id merchant_id",
          select: "contact profile credential.email",
          model: this.models.User
        });
        console.log(orders.buyer_id.credential);
        const root = "./src/emailTemplate";
        console.log(root);
        const imagesPath = `${root}/welcome/images`;
        const templatePath = `${root}/order.html`;
        let mailOptions = {};

        const smtpTransport = nodemailer.createTransport({
          port: 587,
          host: "smtp.office365.com",
          secure: false,
          auth: {
            user: "no-reply@gstock.sg",
            pass: "GSto2819"
          },
          tls: {
            rejectUnauthorized: false
          }
        });
        fs.readFile(templatePath, (err, chunk) => {
          if (err) {
            console.log(err);
            return next(err);
          }
          const html = chunk.toString("utf8");
          const template = Handlebars.compile(html);
          Handlebars.registerHelper("distanceFixed", function(distance) {
                   return distance.toFixed(2);
          });
          const current_time = new Date();
          const someDate = new Date();
          const numberOfDaysToAdd = 3;
          someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
          const dd = someDate.getDate();
          const mm = someDate.getMonth() + 1;
          const y = someDate.getFullYear();
          const deliveryDate = dd + "/" + mm + "/" + y;
          const paypal = "-";
          const Ptype = "Pay Now";
          console.log("Pay DAA", Ptype);
          let shipping_date;
          if (orders.delivery.shipping_type === "normal")
             shipping_date = orders.delivery.Shipping_date;
          else
            shipping_date = this.convertUTCDateToLocalDate(orders.delivery.Shipping_date);
          const imgSrc =
            "https://gstockservices.glux.sg/uploads/user/undefined/products/";
          console.log(orders);
          const data = {
            fromEmail: "no-reply@gstock.sg",
            mail_obj: orders,
            paypal: req.body.paypal,
            type: orders.payment_method,
            productArray: orders.products,
            time: dateFormat(current_time, "shortTime"),
            order_date: dateFormat(orders.createdAt, "mediumDate"),
            shipping_date: dateFormat(shipping_date, "mediumDate"),
            imgSrc,
            deliveryDate
          };
          const result = template(data);
          console.log("DATA", result);
          console.log(orders.buyer_id.credential.email, "email");
          mailOptions = {
            to: [orders.buyer_id.credential.email],
            bcc: ["dhania@dinghe.sg", "pratheesh@dinghe.sg", "prithwijith@gmail.com"],
            //  bcc: ["dessa@dinghe.sg", "angi@dinghe.sg", "dhania@dinghe.sg", "pratheesh@dinghe.sg", "jovi@dinghe.sg", "eric@dinghe.sg", "concierge@dinghe.sg"],
            from: "no-reply@gstock.sg",
            subject: "Thank you for shopping on GSTOCK",
            html: result
          };
          const sendEmail = new Promise((resolve, reject) => {
            smtpTransport.sendMail(mailOptions, (error, response: any) => {
              if (error) {
                console.log(error);
                reject(error);
              } else {
                console.log(response);
                resolve(response);
              }
            });
          });
        });
        return res.status(200).send(order_approval);

      } catch (error) {
        return next(error);
      }
    }
    convertUTCDateToLocalDate(date) {
      if (date) {
        const newDate = new Date(date.getTime() - date.getTimezoneOffset() *  60 *  1000);
        return newDate;
      } return "";

    }
    public async getOrderApproval(req: Request,
        res: Response,
        next: NextFunction) {
            try {
                const order_approval = await this.models.OrderApproval.find();
                return res.status(200).send( order_approval);
            } catch (error) {
                return next(error);
            }
    }



}
import * as del from "del";
import { NextFunction, Request, Response } from "express";
import * as mkdirp from "mkdirp";
import { Types } from "mongoose";
import * as multer from "multer";
import * as fs from "fs-extra";
import paths from "../config/path";
import commentController from "./comment";
import { isArrayUnique } from "../_lib/array";
import { PRODUCT_STATUS } from "../_status/status";
import { ProductModel } from "../models/Product";
import { ObjectID } from "bson";
import { UserModel } from "../models/User";
import { flatten } from "../_lib/flatObjOnly";
import { isVariantPriceInRange } from "../_global/business";
import { Config } from "../types/app";
import { Passport } from "passport";
import * as nodemailer from "nodemailer";
import * as Handlebars from "handlebars";
import * as dateFormat from "dateformat";

/**
 * Product app controller.
 */
export class SalesPromotionController {
    public models;
    public userPassport: Passport;
    // config;
    constructor(private config) {
        // this.config = config;
        this.models = config.models;
        this.userPassport = config.passport.userPassport.getUserPassport();
    }

    public async createSalesPromotion(req: Request, res: Response, next: NextFunction) {
        try {
            console.log(req.body);
            const promotion = await new this.models.SalesPromotion(
              {
                  promo_name: req.body.promoName,
                  validity_from: req.body.from,
                  validity_to: req.body.to,
                  desc: req.body.desc,
                  item: req.body.item
              }
            ).save();

            return res.status(201).send({ "_id": promotion._id });
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }

    public async getSalesPromitionList(req: Request, res: Response, next: NextFunction) {
        try {

            return res.status(200).send(await this.models.SalesPromotion.find({status: "active"}));
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }
    public async getSalesPromitionByDate(req: Request, res: Response, next: NextFunction) {
        try {
            console.log("body", req.body);

            const today  = new Date();
            const promo = await this.models.SalesPromotion.find({validity_from: {$lte: today}, validity_to: {$gte: today}}).lean();
            console.log("PROMO-PRICE", promo);
            const products = [];
            for (const elem of promo) {
                const pendingStock = await this.models.Order.find({ "products.variants": { "$elemMatch": { "promo_id": elem._id } } }).lean();
               console.log("abc", pendingStock);
                    for (const product of elem.item) {
                         let stock = product.stock;
                         console.log(stock);
                        if (pendingStock.length) {
                            for (const order of  pendingStock) {
                                for (const prod of order.products) {
                                    if (String(prod.product._id) === String(product._id) && prod.variants[0].promo_id) {
                                        stock -= prod.variants[0].order_qty;
                                    console.log("stock1", prod.product.brief.name, stock);
                                    }
                                }
                            }
                        }
                        console.log("stock", stock);
                        if (stock > 0 && product.criteria <= req.body.amount) {
                            const img = await this.models.Product.findOne({ "brief.code": product.code }).lean();
                            products.push({ promo_id: elem._id, product, image: img.brief.images });
                        }
                    }
                }


            return res.status(200).send(products);

        } catch (e) {
            console.log(e);
            return next(e);
        }
    }
    public async getSalesPromition(req: Request, res: Response, next: NextFunction) {
        try {
            const query = req.query;
            console.log(req.query);
            if (!ObjectID.isValid(query._id)) return res.status(400).send({ message: "Invalid Purchase Order id" });
            return res.status(200).send(await this.models.SalesPromotion.findOne({ _id: new ObjectID(query._id) }));
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }
    public async updateSalesPromotion(req: Request, res: Response, next: NextFunction) {
        try {
            const { _id } = req.query;
            console.log(req.body);
            await this.models.SalesPromotion.findByIdAndUpdate(req.query, { $set: {
                promo_name: req.body.promoName,
                validity_from: req.body.from,
                validity_to: req.body.to,
                desc: req.body.desc,
                item: req.body.item
            }});

            return res.status(200).send({ message: "Sales Promotion updated" });
        } catch (e) {
            console.log(e);
            return next(e);
        }
    }

    public async deleteSalesPromotion(req: Request, res: Response, next: NextFunction) {

        try {
            const { _id } = req.query;
             await this.models.SalesPromotion.findByIdAndUpdate(_id, { $set: { status: "Cancelled" } });
            return res.status(200).json({message: "Sales Promotion cancelled"  });
        } catch (err) {

            res.status(500);
            return next(err);
        }
    }

}
import { NextFunction, Request, Response } from "express";
import { IVerifyOptions } from "passport-local";
import { sendEmail } from "./../emailTemplate/email_manager";
import * as jwt from "jsonwebtoken";
import { ObjectID } from "bson";
import { Config } from "../types/app";
import * as flat from "flat";
import { Passport } from "passport";
import { More } from "../_lib/decorators";
import { BaseController } from "../_lib/crud";
import { PaginateModel } from "mongoose";
import * as nodemailer from "nodemailer";
import * as fs from "fs";
import * as Handlebars from "handlebars";
import * as dateFormat from "dateformat";
import { getMaxListeners } from "cluster";
import * as bcrypt from "bcrypt-nodejs";
// import round as; from ("mongo-round");
const qrcode = require("qrcode");
export class UserController extends BaseController {
  public models;
  public userPassport: Passport;
  // ecrypt_val: any;
  constructor(private config: Config) {
    super(config.models.User);
    this.models = config.models;
    this.userPassport = config.passport.userPassport.getUserPassport();
  }
  @More({
    "100000": {
      message: "Login successful.",
      detail: "Feel like being talktive?"
    },
    "100001": {
      message: "Account does not exist.",
      detail: "Does not look good mate."
    },
    "100002": {
      message: "Invalid password.",
      detail: "Does not look good mate."
    }
  })
  public async login(req: Request, res: Response, next: NextFunction) {
    try {
      console.log("login");
      // const user: UserModel = await this.create({});
      console.log("login");
      req.sanitize("email").normalizeEmail({ gmail_remove_dots: false });
      const { email, user_group } = req.body;
      console.log("body", req.body);
      // console.log("this.userPassport", this.userPassport);
      return await new Promise((resolve, reject) =>
        this.userPassport.authenticate(
          "local",
          { session: false },
          (err: Error, user, info: IVerifyOptions) => {
            console.log("info", info);
            if (err) return reject(err);
            if (!user) return reject({ ...info });
            const payload = {
              id: user.id,
              user_group,
              timestamp: new Date(),
              expired: 900
            };
            console.log("process.env.JWTKEY", process.env.JWTKEY);
            const token = jwt.sign(payload, process.env.JWTKEY);
            console.log("token", token);
            resolve({
              status_code: 200,
              detail_code: 100000,
              token,
              user_id: user._id,
              //  Uen_no: user.profile.Uen_no,
              email: user.credential.email
            });
          }
        )(req, res, next)
      );
    } catch (error) {
      return error.status_code ? error : next(error);
    }
  }

  // public async fbRegister(req: Request, res: Response, next: NextFunction) {
  //   const { email, contact_no, first_name, last_name, id: fb_id } = req.body;
  //   const errors = await req.getValidationResult();
  //   if (!errors.isEmpty()) {
  //     return res.status(400).json(errors.array({ onlyFirstError: true }));
  //   }
  //   const oldUser = await this.models.User.findOne({
  //     "credential.fb_id": fb_id
  //   });
  //   if (oldUser) {
  //     // pass old user info to next handler
  //     res.locals.user = oldUser;
  //     // if old user, set welcome email flag to false
  //     res.locals.welcome = false;
  //     return next();
  //   }
  //   // check if a new user has fb email in conflict
  //   if (!email) return res.status(404).json("Email is needed!");
  //   const emailConflict = await this.models.User.findOne({
  //     "credential.email": email
  //   }).lean();
  //   if (emailConflict)
  //     return res.status(409).json({ message: "Account existed" });
  //   const newUser = await new this.models.User({
  //     credential: {
  //       email,
  //       // by default fb regsiter is only open to buyer
  //       user_group: "buyer",
  //       fb_id
  //     },
  //     profile: {
  //       first_name,
  //       last_name
  //     }
  //   }).save();
  //   res.locals.user = newUser;
  //   // if new user, set welcome email flag to true
  //   res.locals.welcome = true;
  //   return next();
  // }
  // public async register(req: Request, res: Response, next: NextFunction) {
  //   console.log("register");
  //   req.sanitize("email").normalizeEmail({ gmail_remove_dots: false });
  //   const {
  //     email,
  //     contact_no,
  //     password,
  //     store_name,
  //     user_group,
  //     first_name,
  //     last_name
  //   } = req.body;
  //   console.log("register body", req.body);
  //   if ("buyer" !== user_group && "merchant" !== user_group) {
  //     return res.status(400).send({ message: "Unknown user group!" });
  //   }
  //   if (await this.models.User.findOne({ "credential.email": email }).lean()) {
  //     return res.status(409).json({ message: "Account existed" });
  //   }
  //   const userBase = {
  //     credential: {
  //       email,
  //       user_group,
  //       password
  //     },
  //     profile: {
  //       first_name,
  //       last_name
  //     },
  //     contact: {
  //       mobile_no: contact_no
  //     }
  //   };
  //   let newUser;
  //   if ("merchant" === user_group) {
  //     if (!store_name)
  //       return res.status(400).json({ message: "Store name cannot be empty." });
  //     if (await this.models.User.findOne({ "store.name": store_name }).lean())
  //       return res.status(409).json({ message: "Store name existed" });
  //     newUser = {
  //       ...userBase,
  //       store: {
  //         name: store_name
  //       }
  //     };
  //   } else {
  //     newUser = userBase;
  //   }
  //   console.log("newUser", newUser);
  //   // const user = await new this.models.User(newUser).save();
  //   const user = await new this.models.User(newUser).save();
  //   res.locals.user = user;
  //   return next();
  // }

  // public async register(req: Request, res: Response, next: NextFunction) {
  //   console.log("register");
  //   req.sanitize("email").normalizeEmail({ gmail_remove_dots: false });
  //   const {
  //     email,
  //     contact_no,
  //     password,
  //     store_name,
  //     user_group,
  //     first_name,
  //     last_name,
  //     qrcode,
  //   } = req.body;
  //   console.log("register body", req.body);
  //   if ("buyer" !== user_group && "merchant" !== user_group) {
  //     return res.status(400).send({ message: "Unknown user group!" });
  //   }
  //   if (await this.models.User.findOne({ "credential.email": email }).lean()) {
  //     return res.status(409).json({ message: "Account existed" });
  //   }
  //   const userBase = {
  //     credential: {
  //       email,
  //       user_group,
  //       password
  //     },
  //     profile: {
  //       first_name,
  //       last_name
  //     },
  //     contact: {
  //       mobile_no: contact_no
  //     },
  //     qrcode
  //   };
  //   let newUser;
  //   if ("merchant" === user_group) {
  //     if (!store_name)
  //       return res.status(400).json({ message: "Store name cannot be empty." });
  //     if (await this.models.User.findOne({ "store.name": store_name }).lean())
  //       return res.status(409).json({ message: "Store name existed" });
  //     newUser = {
  //       ...userBase,
  //       store: {
  //         name: store_name
  //       }
  //     };
  //   } else {
  //     newUser = userBase;
  //   }
  //   console.log("newUser", newUser);
  //   // const user = await new this.models.User(newUser).save();
  //   const user = await new this.models.User(newUser).save();
  //   res.locals.user = user;
  //   return next();
  // }
  // public async fbRegister(req: Request, res: Response, next: NextFunction) {
  //   const type = req.body.type;
  //   console.log(type);
  //   if (type === 1) {
  //     const { email, contact_no, first_name, last_name, id: fb_id } = req.body;
  //     const errors = await req.getValidationResult();
  //     if (!errors.isEmpty()) {
  //       return res.status(400).json(errors.array({ onlyFirstError: true }));
  //     }
  //     const oldUser = await this.models.User.findOne({
  //       "credential.fb_id": fb_id
  //     });
  //     console.log(req.body);

  //     if (oldUser) {
  //       // pass old user info to next handler
  //       res.locals.user = oldUser;
  //       // if old user, set welcome email flag to false
  //       res.locals.welcome = false;
  //       return next();
  //     }
  //     // check if a new user has fb email in conflict
  //     if (!email) return res.status(404).json("Email is needed!");
  //     const emailConflict = await this.models.User.findOne({
  //       "credential.email": email
  //     }).lean();

  //     if (emailConflict) {
  //       console.log(emailConflict, "emailConflict");
  //       const exsting = await this.models.User.findOne({
  //         "credential.email": email
  //       });
  //       await this.models.User.findOneAndUpdate({
  //         "credential.email": email
  //       }, { fb_id });
  //       res.locals.user = exsting;
  //       return next();
  //     }
  //     // return res.status(409).json({ message: "Account existed" });
  //     const newUser = await new this.models.User({
  //       credential: {
  //         email,
  //         // by default fb regsiter is only open to buyer
  //         user_group: "buyer",
  //         fb_id
  //       },
  //       profile: {
  //         first_name,
  //         last_name
  //       }
  //     }).save();
  //     res.locals.user = newUser;
  //     // if new user, set welcome email flag to true
  //     res.locals.welcome = true;
  //     return next();
  //   } else {
  //     console.log( req.body);
  //     const { email, firstName, lastName,  uid } = req.body;
  //     const errors = await req.getValidationResult();
  //     if (!errors.isEmpty()) {
  //       return res.status(400).json(errors.array({ onlyFirstError: true }));
  //     }
  //     const oldUser = await this.models.User.findOne({
  //       "credential.fb_id": uid
  //     });
  //     if (oldUser) {
  //       // pass old user info to next handler
  //       res.locals.user = oldUser;
  //       // if old user, set welcome email flag to false
  //       res.locals.welcome = false;
  //       return next();
  //     }
  //     // check if a new user has fb email in conflict
  //     if (!email) return res.status(404).json("Email is needed!");
  //     const emailConflict = await this.models.User.findOne({
  //       "credential.email": email
  //     }).lean();
  //     if (emailConflict) {
  //       console.log(emailConflict, "emailConflict");
  //       const exsting = await this.models.User.findOne({
  //         "credential.email": email
  //       });
  //       await this.models.User.findOneAndUpdate({
  //         "credential.email": email
  //       }, { fb_id: uid });
  //       res.locals.user = exsting;
  //       return next();
  //     }
  //     // if (emailConflict)
  //     //   return res.status(409).json({ message: "Account existed" });
  //     const newUser = await new this.models.User({
  //       credential: {
  //         email,
  //         // by default fb regsiter is only open to buyer
  //         user_group: "buyer",
  //         fb_id: uid
  //       },
  //       profile: {
  //         first_name: firstName,
  //         last_name: lastName
  //       }
  //     }).save();
  //     const qr = await this.models.qrcode.toDataURL(email);
  //     await this.models.User.update({ _id: newUser._id }, {
  //       $set: { qrcode: qr }
  //     });
  //     res.locals.user = newUser;
  //     // if new user, set welcome email flag to true
  //     res.locals.welcome = true;
  //     return next();
  //   }

  // }

  public async fbRegister(req: Request, res: Response, next: NextFunction) {
    const type = req.body.type;
    console.log(type);
    if (type === 1) {
      const { email, contact_no, first_name, last_name, id: fb_id } = req.body;
      const errors = await req.getValidationResult();
      if (!errors.isEmpty()) {
        return res.status(400).json(errors.array({ onlyFirstError: true }));
      }
      const oldUser = await this.models.User.findOne({
        "credential.fb_id": fb_id
      });
      console.log(req.body);

      if (oldUser) {
        // pass old user info to next handler
        res.locals.user = oldUser;
        // if old user, set welcome email flag to false
        res.locals.welcome = false;
        return next();
      }
      // check if a new user has fb email in conflict
      if (!email) return res.status(404).json("Email is needed!");
      const emailConflict = await this.models.User.findOne({
        "credential.email": email
      }).lean();

      if (emailConflict) {
        console.log(emailConflict, "emailConflict");
        const exsting = await this.models.User.findOne({
          "credential.email": email
        });
        await this.models.User.findOneAndUpdate({
          "credential.email": email
        }, { fb_id });
        res.locals.user = exsting;
        return next();
      }
      // return res.status(409).json({ message: "Account existed" });
      const newUser = await new this.models.User({
        credential: {
          email,
          // by default fb regsiter is only open to buyer
          user_group: "buyer",
          fb_id
        },
        profile: {
          first_name,
          last_name
        }
      }).save();
      res.locals.user = newUser;
      // if new user, set welcome email flag to true
      res.locals.welcome = true;
      return next();
    } else {
      console.log( req.body);
      const { email, firstName, lastName,  uid } = req.body;
      const errors = await req.getValidationResult();
      if (!errors.isEmpty()) {
        return res.status(400).json(errors.array({ onlyFirstError: true }));
      }
      const oldUser = await this.models.User.findOne({
        "credential.fb_id": uid
      });
      if (oldUser) {
        // pass old user info to next handler
        res.locals.user = oldUser;
        // if old user, set welcome email flag to false
        res.locals.welcome = false;
        return next();
      }
      // check if a new user has fb email in conflict
      if (!email) return res.status(404).json("Email is needed!");
      const emailConflict = await this.models.User.findOne({
        "credential.email": email
      }).lean();
      if (emailConflict) {
        console.log(emailConflict, "emailConflict");
        const exsting = await this.models.User.findOne({
          "credential.email": email
        });
        await this.models.User.findOneAndUpdate({
          "credential.email": email
        }, { fb_id: uid });
        res.locals.user = exsting;
        return next();
      }
      // if (emailConflict)
      //   return res.status(409).json({ message: "Account existed" });
      const newUser = await new this.models.User({
        credential: {
          email,
          // by default fb regsiter is only open to buyer
          user_group: "buyer",
          fb_id: uid
        },
        profile: {
          first_name: firstName,
          last_name: lastName
        }
      }).save();
      const qr = await this.models.qrcode.toDataURL(email);
      await this.models.User.update({ _id: newUser._id }, {
        $set: { qrcode: qr }
      });
      res.locals.user = newUser;
      // if new user, set welcome email flag to true
      res.locals.welcome = true;
      return next();
    }

  }
  public async register(req: Request, res: Response, next: NextFunction) {
    req.sanitize("email").normalizeEmail({ gmail_remove_dots: false });
    const {
      email,
      contact_no,
      password,
      store_name,
      user_group,
      first_name,
      last_name,
      country
    } = req.body;
    console.log("register body", req.body);
    const qr = await qrcode.toDataURL(email);
    if ("buyer" !== user_group && "merchant" !== user_group) {
      return res.status(400).send({ message: "Unknown user group!" });
    }
    if (await this.models.User.findOne({ "credential.email": email }).lean()) {
      return res.status(409).json({ message: "Account existed" });
    }
    const userBase = {
      credential: {
        email,
        user_group,
        password
      },
      profile: {
        first_name,
        last_name
      },
      contact: {
        mobile_no: contact_no
      },
      qrcode: qr,
      location: {
        country
      }
    };
    let newUser;
    if ("merchant" === user_group) {
      if (!store_name)
        return res.status(400).json({ message: "Store name cannot be empty." });
      if (await this.models.User.findOne({ "store.name": store_name }).lean())
        return res.status(409).json({ message: "Store name existed" });
      newUser = {
        ...userBase,
        store: {
          name: store_name
        }
      };
    } else {
      newUser = userBase;
    }
    console.log("newUser", newUser);
    // const user = await new this.models.User(newUser).save();
    const user = await new this.models.User(newUser).save();
    res.locals.user = user;
    return next();
  }

  public async reg(req: Request, res: Response, next: NextFunction) {
    req.sanitize("email").normalizeEmail({ gmail_remove_dots: false });
    const { email, password, first_name, Uen_no } = req.body;
    console.log("user", req.body);

    if (await this.models.User.findOne({ "credential.email": email }).lean()) {
      return res.status(409).json({ message: "Account existed" });
    }
    const userBase = {
      credential: {
        email,
        password
      },
      profile: {
        first_name,
        Uen_no
      }
    };
    let newUser;
    newUser = userBase;
    const user = await new this.models.User(newUser).save();
    res.locals.user = user;
    return next();
  }

  public async reset(req: Request, res: Response, next: NextFunction) {
    try {
      const { password, old_password } = req.body;
      console.log("RESET PASSWORD");
      console.log("new reset", res.locals.user.id);
      const user = await this.models.User.findById(res.locals.user.id).select(
        "credential"
      );
      console.log("new reset1", user);
      if (!user) return res.status(400).send({ message: "No such user." });
      user.comparePassword(
        old_password,
        user.credential.password,
        async (err, isMatched) => {
          if (!isMatched)
            return res.status(400).send({ message: "Wrong password!" });
          user.credential.password = password;
          await user.save();
          return res.status(200).send({ message: "Password changed" });
        }
      );
    } catch (error) {
      return next(error);
    }
  }

  public async forgot(req: Request, res: Response, next: NextFunction) {
    const { email, token, new_pw } = req.body;
    try {
      const user = await this.models.User.findOne({
        "credential.email": email
      });

      if (user.credential.email_verif_token !== token)
        return res.status(400).send({ message: "Invalid token!" });
      user.credential.password = new_pw;
      console.log("new user check", user);
      await user.save();
      return res.status(200).send({ message: "Password updated!" });
    } catch (error) {
      return next(error);
    }
  }

  /***********Email************* */
  public async forget(req: Request, res: Response, next: NextFunction) {
    try {
      const { email } = req.body;
      console.log("EMAIL", req.body);
      const user1 = await this.models.User.find({
        "credential.email": email
      }).select("credential.email profile.first_name");
      if (user1.length > 0) {
        const root = "./src/emailTemplate";
        //   const imagesPath = `${root}/welcome/images`;
        const templatePath = `${root}/forgotpassword/forgotpass.html`;

        let mailOptions = {};
        const smtpTransport = nodemailer.createTransport({
          port: 587,
          host: "smtp.office365.com",
          secure: false,
          auth: {
            user: "no-reply@gstock.sg",
            pass: "GSto2819"
          },
          tls: {
            rejectUnauthorized: false
          }
        });
        fs.readFile(templatePath, (err, chunk) => {
          if (err) {
            return next(err);
          }
          const html = chunk.toString("utf8");
          const template = Handlebars.compile(html);
          // gta
          //    const mail_link = " http://localhost:4200/#/reset/" + email;
          const mail_link = "https://gstock.sg/#/user/reset/" + email;

          const current_time = new Date();
          const data = {
            fromEmail: "no-reply@gstock.sg",
            mail_obj: user1[0].profile.first_name,
            mail_linkId: mail_link,
            time: dateFormat(current_time, "shortTime")
          };
          const result = template(data);
          mailOptions = {
            to: email,
            from: "no-reply@gstock.sg",
            subject: "Password Reset For Gstock",
            html: result
          };
          const sendEmail = new Promise((resolve, reject) => {
            smtpTransport.sendMail(mailOptions, (error, response: any) => {
              if (error) {
                //
                console.log(error);
                reject(error);
              } else {
                console.log(response);
                resolve(response);
              }
            });
          });
        });
        // -------------------------------------------
        return res.status(200).send({ message: "Please check your mail!" });
      } else {
        return res.status(400).send({ message: "Please enter valid email id" });
      }
    } catch (error) {
      return next(error);
    }
  }

  /*********************End***************** */
  public async ecrypt_val(pass) {
    return new Promise((resolve, reject) => {
      bcrypt.genSalt(10, (err, salt) => {
        console.log("encryption", salt);
        if (err) {
          reject(err);
        }
        bcrypt.hash(pass, salt, undefined, (err: Error, hash) => {
          if (err) {
            reject(err);
          }

          resolve(hash);
        });
      });
    });
  }

  // const user = this;
  // if (!user.isModified("credential.password")) {
  //   return next();
  // }
  // bcrypt.genSalt(10, (err, salt) => {
  //   if (err) {
  //     return next(err);
  //   }
  //   bcrypt.hash(
  //     user.credential.password,
  //     salt,
  //     undefined,
  //     (err: Error, hash) => {
  //       if (err) {
  //         return next(err);
  //       }
  //       user.credential.password = hash;
  //       next();
  //     }
  //   );
  // });

  /**********************Reset******************** */
  // public async Reset_pass(req: Request, res: Response, next: NextFunction) {
  //   try {
  //     const { new_pass, email } = req.body;
  //     console.log("RQ", req.body);
  //     // console.log("**************************************************************************");
  //     let encrpt_pass;
  //     await this.ecrypt_val(new_pass)
  //       .then(function(result) {
  //         encrpt_pass = result;
  //       })
  //       .catch(function(error) {
  //         return res.status(500).send({ error });
  //       });

  //     console.log("Email------- ", email);
  //     console.log("New Pass------- ", encrpt_pass);

  //     await this.models.User.update(
  //       { "credential.email": email },
  //       {
  //         $set: {
  //           "credential.password": encrpt_pass,
  //           updatedAt: new Date()
  //         }
  //       },
  //       function(err, doc) {
  //         if (err) {
  //           return res.status(500).send("Failed");
  //         }
  //         // return res
  //         //   .status(200)
  //         //   .send({ message: "Password Updated Successfully" });
  //         return  res.end({ message: "Password updated successfully!" });
  //       }
  //     );
  //     // return res
  //     //   .status(200)
  //     //   .send({ message: "Password updated successfully!" });
  //     return  res.end({ message: "Password updated successfully!" });
  //       // return res
  //       // .status(200)
  //       // .send({ message: "Password updated successfully!" });

  //     // console.log("**************************************************************************");
  //   } catch (error) {
  //     console.log(error);
  //     // console.log(error);
  //     return next(error);
  //   }
  // }

  public async Reset_pass_old(req: Request, res: Response, next: NextFunction) {
    try {
      const { new_pw, email } = req.body;
      console.log("RQ", req.body);
      let encrpt_pass;
      const user = await this.models.User.findOne({ "credential.email": email });
      const walletEntry = await this.models.CreditWallet.findOne({   buyer_id: user._id, });
      console.log("entry", walletEntry);
      if (walletEntry)  return res
      .status(200)
      .send({ message: "Validity Expired!" });
      await this.ecrypt_val(new_pw)
        .then(function (result) {
          encrpt_pass = result;
        })
        .catch(function (error) {
          return res.status(500).send({ error });
        });
      console.log("password", encrpt_pass);
      await this.models.User.findOneAndUpdate(
        { "credential.email": email },
        { "credential.password": encrpt_pass, updatedAt: new Date() },
        function (err, doc) {
          if (err) {
            return res.status(500).send("Failed");
          }
        }
      );

      const newEntry = await this.models.CreditWallet({
        buyer_id: user._id,
        memo: "Old Gstock Member",
        amount: 5,
        status: "Earned",
        ref_id: null
      });
      console.log("CREDIT", email, user._id);
      await newEntry.save();
      return res
        .status(200)
        .send({ message: "Password updated successfully!" });

    } catch (error) {
      console.log(error);
      return next(error);
    }
  }


  public async Reset_pass(req: Request, res: Response, next: NextFunction) {
    try {
      // const { new_pass, email } = req.body;
      const { new_pw, email } = req.body;
      console.log("RQ", req.body);
      // console.log("**************************************************************************");
      let encrpt_pass;
      await this.ecrypt_val(new_pw)
        .then(function(result) {
          encrpt_pass = result;
        })
        .catch(function(error) {
          return res.status(500).send({ error });
        });

      await this.models.User.findOneAndUpdate(
        { "credential.email": email },
        { "credential.password": encrpt_pass, updatedAt: new Date() },
        function(err, doc) {
          if (err) {
            return res.status(500).send("Failed");
          }
          // return res
          //   .status(200)
          //   .send({ message: "Password Updated Successfully" });
        }
      );
      return res
        .status(200)
        .send({ message: "Password updated successfully!" });

      // console.log("**************************************************************************");
    } catch (error) {
      console.log(error);
      // console.log(error);
      return next(error);
    }
  }
  /*******************************End***************************** */
  /*---------------------------------Encrypt--------------------------------*/
  /*---------------------------------------end-----------------------------------*/

  public async generateToken(req: Request, res: Response, next: NextFunction) {
    const {
      user: {
        id,
        credential: { email, user_group }
      }
    } = res.locals;
    // send token only on registration
    const payload = {
      id,
      user_group,
      timestamp: new Date(),
      expired: 900
    };
    const token = jwt.sign(payload, process.env.JWTKEY);
    return res.status(200).send({
      token,
      email,
      user_id: id
    });
  }

  public async updateEmailVerifToken(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    const { email, token } = req.query;
    const user = await this.models.User.findOneAndUpdate(
      {
        "credential.email": email,
        "credential.email_verif_expires": { $gt: Date.now() },
        "credential.email_verif_token": token
      },
      {
        $set: { "credential.email_verified": true }
      }
    ).lean();
    return user
      ? res.status(200).json({ message: "Email verfied" })
      : res.status(404).json({ message: "No valid user found" });
  }
  @More()
  public async getUserAccountDetail(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    return { status_code: 200, detail_code: 0, ...res.locals.user.toJSON() };
  }

  @More()
  public async updateUserAccountDetail(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      console.log(req.locals.user.id, req.body);
      const result = await this.models.User.findByIdAndUpdate(
        req.locals.user.id,
        { $set: flat.flatten(req.body) },
        { new: true }
      );
      return {
        status_code: 200,
        detail_code: 0,
        ...result.toJSON()
      };
    } catch (error) {
      return error.status_code ? error : next(error);
    }
  }

  @More()
  public async getRewardPts(req: Request, res: Response, next: NextFunction) {
    try {
      return {
        status_code: 200,
        detail_code: 0,
        reward_points: await this.models.RewardPts.find({
          buyer_id: req.locals.user.id
        })
      };
    } catch (error) {
      return error.status_code ? error : next(error);
    }
  }
  // public async getStore(req: Request, res: Response, next: NextFunction) {
  //   console.log("getStore");

  //   console.log("MERCHANT ", req.query);

  //   try {
  //     const { merchant_id } = req.query;

  //     const detail = await this.models.User.findById({
  //       _id: new ObjectID(merchant_id),
  //       "credential.user_group": "merchant"
  //     }).select("store createdAt");

  //     console.log("MERCHANT DETAILSSSS", detail);
  //     if (!detail)
  //       return res.status(200).json({ message: "No such merchant!" });
  //     const products = await this.models.Product.find({
  //       merchant_id: new ObjectID(merchant_id),
  //       status: "Approved"
  //     }).select("brief pricing");
  //     return res.status(200).send({
  //       detail,
  //       products
  //     });
  //   } catch (e) {
  //     return next(e);
  //   }
  // }

  // public async getStore(req: Request, res: Response, next: NextFunction) {
  //   console.log("getStore");
  //   console.log("MERCHANT ", req.query);

  //   try {
  //     const { merchant_id } = req.query;

  //     console.log("MERCHANT IDDDD", merchant_id);
  //     const detail = await this.models.User.findOne({
  //       _id: new ObjectID(merchant_id),
  //       "credential.user_group": "merchant"
  //     }).select("store createdAt");

  //     console.log("MERCHANT DETAILSSSS", detail);
  //     if (!detail)
  //       return res.status(200).json({ message: "No such merchant!" });
  //     const products = await this.models.Product.find({
  //       merchant_id: new ObjectID(merchant_id),
  //       status: "Approved"
  //     }).select("brief pricing");
  //     return res.status(200).send({
  //       detail,
  //       products
  //     });
  //   } catch (e) {
  //     return next(e);
  //   }
  // }

  public async getStore(req: Request, res: Response, next: NextFunction) {
    console.log("getStore");
    console.log("MERCHANT ", req.query);

    try {
      const { merchant_id } = req.query;

      console.log("MERCHANT IDDDD", merchant_id);
      const detail = () =>
        this.models.User.findOne({
          _id: new ObjectID(merchant_id),
          "credential.user_group": "merchant"
        }).select("store createdAt");

      console.log("MERCHANT DETAILSSSS", detail);
      if (!detail)
        return res.status(200).json({ message: "No such merchant!" });
      const products = () =>
        this.models.Product.find({
          merchant_id: new ObjectID(merchant_id),
          status: "Approved"
        }).select("brief pricing");
      return res.status(200).send({
        detail,
        products
      });
    } catch (e) {
      console.log(e);
      return next(e);
    }
  }

//     public async getCreditWalletHistory(
//     req: Request,
//     res: Response,
//     next: NextFunction
//   ) {
//     try {
//       console.log("CREDIT HISTORY", req.locals.user.id);
//       const findTotal = await this.models.CreditWallet.aggregate([
//         {
//           $match: {
//             buyer_id: new ObjectID(req.locals.user.id)
//           }
//         },
//         {
//           $group: {
//             _id: "",
//             total: { $sum: "$amount" }
//           }
//         },
//         {
//           $project: {
//             _id: 0,
//             total: "$total"
//           }
//         },

//       ]);
// console.log("findTotal", findTotal);
//       return res.status(200).send({
//         data: {
//           history: await this.models.CreditWallet.find({
//             buyer_id: new ObjectID(req.locals.user.id),
//             amount: {"$gt": 0}
//           }),
//           ...findTotal[0]
//         }
//       });
//     } catch (error) {
//       return next(error);
//     }
//   }

public async getCreditWalletHistory(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    console.log("CREDIT HISTORY 12", req.locals.user.id);
    const findTotal = await this.models.CreditWallet.aggregate([
      {
        $match: {
          buyer_id: new ObjectID(req.locals.user.id)
           }
      },
      {
        $group: {
          _id: "",
          total: { $sum: "$amount" }
        }
      },
      {
        $project: {
          _id: 0,
          total: "$total"
        }
      }
    ]);

    return res.status(200).send({
      data: {
        history: await this.models.CreditWallet.find({
          $and: [ { status: { $nin: "Pending" } }, { buyer_id: new ObjectID(req.locals.user.id) } ]
          }),
        ...findTotal[0]
      }
    });
  } catch (error) {
    return next(error);
  }
}
public async updateQR(req: Request, res: Response, next: NextFunction) {

  const users = await this.models.User.find({ "qrcode": { "$exists": false } }).lean();
  for (const user of users) {
    console.log(user.credential.email);
    const qr = await qrcode.toDataURL(user.credential.email);
    await this.models.User.update({ _id: user._id }, {
      $set: { qrcode: qr }
    });
  }
  return res.status(200).send({ message: ["QR code updated successfully"] });
}
}

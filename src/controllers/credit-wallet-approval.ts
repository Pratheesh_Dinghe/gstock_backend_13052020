import * as del from "del";
import { NextFunction, Request, Response } from "express";
import * as mkdirp from "mkdirp";
import { Types } from "mongoose";
import * as multer from "multer";
import * as fs from "fs-extra";
import paths from "../config/path";
import { isArrayUnique } from "../_lib/array";
import { ObjectID } from "bson";
import { UserModel } from "../models/User";
import { flatten } from "../_lib/flatObjOnly";
import { isVariantPriceInRange } from "../_global/business";
import { Config } from "../types/app";
import { Passport } from "passport";
import { RSA_NO_PADDING } from "constants";
import * as nodemailer from "nodemailer";
import * as Handlebars from "handlebars";
import * as dateFormat from "dateformat";

export class CreditWalletApprovalController {
  public models;
  public userPassport: Passport;

  constructor(private config) {
    this.models = config.models;
    this.userPassport = config.passport.userPassport.getUserPassport();
  }

  // Approve Pending Orders
  public async save(req: Request, res: Response, next: NextFunction) {
    try {
      req.body.buyer_id = req.body["buyer_id._id"];
      const wallet_approval = await this.models
        .CreditWalletApproval({
          approve_id: req.body.approve_id,
          date: req.body.date,
          email: req.body.email,
          Acc_No: req.body.Acc_No,
          Mobile_No: req.body.Mobile_No,
          Withdrawal_Amnt: req.body.Withdrawal_Amnt,
          status: req.body.status,
          bank_name: req.body.bank_name,
          buyer_id: req.body.buyer_id,
          address: req.body.address,
          remarks: req.body.remarks,
          Name: req.body.Name
        })
        .save();
      const wallet_withdrawal = await this.models.WalletWithdrawal.update(
        { _id: req.body._id },
        {
          $set: {
            Status: req.body.status
          }
        }
      );
      await this.CreditWalletUpdate(
        req.body.Withdrawal_Amnt,
        wallet_approval._id,
        req.body.buyer_id
      );
      return res.status(200).send(wallet_approval);
    } catch (error) {
      return next(error);
    }
  }

  // -----------------Withdrawed entry to Crediwallets Table---------------------------

  public async CreditWalletUpdate(walletamnt, approvedId, user_id) {
    try {
      console.log("UpdateCreditWallet checking");
      console.log("UpdateCreditWallet checking1", walletamnt);
      console.log("UpdateCreditWallet checking2", approvedId);
      console.log("UpdateCreditWallet checking3", user_id);
      const newEntry = await this.models.CreditWallet({
        buyer_id: user_id,
        memo: "Wallet",
        amount: walletamnt,
        status: "Withdrawed",
        ref_id: approvedId
      });
      console.log("CREDIT", newEntry);
      await newEntry.save();
    } catch (error) {
      // return next(error);
    }
  }

  public async getCreditWalletApproval(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const wallet_approval = await this.models.CreditWalletApproval.find();
      return res.status(200).send(wallet_approval);
    } catch (error) {
      return next(error);
    }
  }

  public async getWalletWithdrawal(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const wallet_withdrawal = await this.models.WalletWithdrawal.find({
        Status: "Requested"
      })
        .populate({
          path: "buyer_id",
          select: "contact profile credential.email",
          model: this.models.User
        })
        .lean();
      (async () => {
        for (const key in wallet_withdrawal) {
          const address = await this.models.DeliveryAddress.findOne({
            buyer_id: wallet_withdrawal[key].buyer_id._id
          }).lean();
          wallet_withdrawal[key].address = address;
        }
        return res.status(200).send(wallet_withdrawal);
      })();
    } catch (error) {
      return next(error);
    }
  }

  public async getLatest(req: Request, res: Response, next: NextFunction) {
    try {
      const result = await this.models.CreditWalletApproval.findOne()
        .sort({ date: -1 })
        .limit(1);
      if (result) return res.status(200).send(result);
      else return res.status(200).send({});
    } catch (err) {
      return next(err);
    }
  }
}

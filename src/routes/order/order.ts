import { NextFunction, Request, Response, Router } from "express";
import { checkSchema } from "express-validator/check";
import { validationResponse } from "../../_middleware/validationRes";
import { orderController, rewardProductController } from "../../controllers";
import * as nodemailer from "nodemailer";
import * as fs from "fs";
import * as Handlebars from "handlebars";
import * as dateFormat from "dateformat";
import { type } from "os";

export default config => {
  const {
    getDeliveryDetail,
    getOrderDetail,
    // getSalesReturnOrders,
    getPromotionDetailFromPromoCode,
    processStore,
    placeOrder,
    buyerMadePayment,
    getPendingStock,
    substractPendingStock,
    pendingStock,
    updateOrderStatus,
    loadAddress,
    load_Address,
    save_bill,
    save_ship,
    getAllOrder,
    convertUTCDateToLocalDate,
    getPaidOrder,
    // getSalesReturnOrders
    // GetDeliveryCharge
    //    AddshipTo
  } = orderController(config);
  const { Order, User } = config.models;
  const populate = {
    path: "buyer_id merchant_id",
    select: "contact profile credential.email",
    model: User
  };
  console.log("PRATHEESH_02");
  const { purchaseRewardProduct } = rewardProductController(config);
  console.log("PRATHEESH_04");
  return (
    Router()
      // .get("/", async (req: Request, res: Response, next: NextFunction) => {
      //     const orders = await models.Order.find({});

      //     // { $and: [{ "createdAt": { $gte: req.body.start } }, { "createdAt": { $gte: req.body.end } }]

      //     // if (err) return next(err);
      //     return res.status(200).send(orders);
      // })
      // Commented By Pratheesh : 281118
      //  .post("/place", checkSchema({
      //     "orders.*.products.*.variants.*.qty": {
      //         in: ["body"],
      //         isInt: {
      //             options: { min: 1 },
      //             errorMessage: "Product quantity must not be lower than 1.",
      //         }
      //     }
      // }), [validationResponse, getDeliveryDetail, getOrderDetail, getPromotionDetailFromPromoCode, processStore, placeOrder])
      .post("/place", [
        validationResponse,
        getDeliveryDetail,
        getOrderDetail,
        getPromotionDetailFromPromoCode,
        processStore,
        placeOrder
      ])


      .post(
        "/success",
        [buyerMadePayment, getPendingStock, substractPendingStock],
        async (req: Request, res: Response, next: NextFunction) => {
          try {
            console.log("REQ", req.body);
            const { order_id } = req.body;
            console.log("ORDER", order_id);
            const orders = await Order.findById(order_id).populate(populate);
            const root = "./src/emailTemplate";
            console.log(root);
            const imagesPath = `${root}/welcome/images`;
            const templatePath = `${root}/order.html`;
            let mailOptions = {};
            const smtpTransport = nodemailer.createTransport({
              port: 587,
              host: "smtp.office365.com",
              secure: false,
              auth: {
                user: "no-reply@gstock.sg",
                pass: "GSto2819"
              },
              tls: {
                rejectUnauthorized: false
              }
            });
            fs.readFile(templatePath, (err, chunk) => {
              if (err) {
                console.log(err);
                return next(err);
              }
              const html = chunk.toString("utf8");
              const template = Handlebars.compile(html);
              Handlebars.registerHelper("distanceFixed", function(distance) {
                      return distance.toFixed(2);
              });
              const current_time = new Date();
              const someDate = new Date();
              const numberOfDaysToAdd = 3;
              someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
              const dd = someDate.getDate();
              const mm = someDate.getMonth() + 1;
              const y = someDate.getFullYear();
              const paypal = "-";
              let payment_type;
              const deliveryDate = dd + "/" + mm + "/" + y;
              // if (req.body.paypal) {
              //    paypal = req.body.paypal.paymentID;
              //    payment_type = "Paypal"; }
              // else {payment_type = "Credit Wallet"; }

              payment_type = orders.payment_method;
              const imgSrc =
                "https://gstockservices.glux.sg/uploads/user/undefined/products/";
              console.log(orders);
              // const shippingdate = convertUTCDateToLocalDate(orders.delivery.Shipping_date);
              // console.log(shippingdate, "SHIPPING DATE");
              let shipping_date;
              let shipping_type;

              if (orders.delivery.shipping_type === "self") shipping_type = "Self Collect";
              else shipping_type = "";
console.log("shipping_type1", shipping_type);
              if (orders.delivery.shipping_type === "normal")
                shipping_date = convertUTCDateToLocalDate(orders.delivery.Shipping_date);
              else
                shipping_date = convertUTCDateToLocalDate(orders.delivery.Shipping_date);
              const data = {
                fromEmail: "no-reply@gstock.sg",
                mail_obj: orders,
                paypal,
                type: payment_type,
                productArray: orders.products,
                time: dateFormat(current_time, "shortTime"),
                order_date: dateFormat(orders.createdAt, "mediumDate"),
                // shipping_date: dateFormat(orders.delivery.Shipping_date, "mediumDate"),
                // shippingdate,
                shipping_date: dateFormat(shipping_date, "mediumDate"),
                imgSrc,
                deliveryDate
              };
              const result = template(data);
              console.log(orders.buyer_id.credential.email, "email");
              mailOptions = {
                to: orders.buyer_id.credential.email,
                // to: "pratheesh@dinghe.sg",
                // bcc: ["manju.caxigo@gmail.com"],
                // to: 'vishnupuliyarakkal@gmail.com',
                // bcc: ["dessa@dinghe.sg", "toh@dinghe.sg"],
                bcc: ["pratheesh@dinghe.sg", "dhania@dinghe.sg"],
                // bcc: ["dessa@dinghe.sg", "angi@dinghe.sg", "dhania@dinghe.sg", "pratheesh@dinghe.sg", "jovi@dinghe.sg", "eric@dinghe.sg", "concierge@dinghe.sg"],
                from: "no-reply@gstock.sg",
                subject: "Thank you for shopping on GSTOCK",
                html: result
              };
              const sendEmail = new Promise((resolve, reject) => {
                smtpTransport.sendMail(mailOptions, (error, response: any) => {
                  if (error) {
                    console.log(error);
                    reject(error);
                  } else {
                    console.log(response);
                    resolve(response);
                  }
                });
              });
            });
            return res.status(200).send(orders);
          } catch (error) {
            console.log(error);
          }
        }
      )


      .get("/pending", pendingStock)
      .get("/loadAddress/:userid", loadAddress)
      .get("/load_Address/:userid", load_Address)
      .post("/save", save_bill)
      .post("/saveing", save_ship)
      .get("/getorder", getAllOrder)
      // .get("/deliverycharge", GetDeliveryCharge)
      // .get("/getOrder-SalesReturn", getSalesReturnOrders)
      //   .get("/SalesReturnOrders", getSalesReturnOrders)
      .get("/allpaid", getPaidOrder)
      .post("/reward-product/order", purchaseRewardProduct)
  );
};

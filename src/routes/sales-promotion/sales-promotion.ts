import { Request, Response, NextFunction, Router } from "express";
import * as _ from "lodash";
import {  SalesPromotionController } from "../../controllers";

export default config => {
    const salesPromotionCtrl = new SalesPromotionController(config);
    const {createSalesPromotion, getSalesPromition, getSalesPromitionList,
      updateSalesPromotion, deleteSalesPromotion, getSalesPromitionByDate
    } = _.bindAll(
        salesPromotionCtrl,
      Object.getOwnPropertyNames(SalesPromotionController.prototype)
    );
    return Router()

      .post("/", createSalesPromotion)
      .get("/getById", getSalesPromition)
      .get("/", getSalesPromitionList)
      .post("/update", updateSalesPromotion)
      .post("/getByDate", getSalesPromitionByDate)
      .delete("/", deleteSalesPromotion);

  };

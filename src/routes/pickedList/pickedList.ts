import { Request, Response, NextFunction, Router } from "express";
import * as _ from "lodash";
import { RefundController, PickedListController } from "../../controllers";

export default config => {
    const pickedListCtrl = new PickedListController(config);
    const {savePickedList, generatePdf, getSupplieremail, getPicked,
      getPickedList, getPacked, getOnDelivery, getDelivered, updateDeliveryStatus, getPackedOrHold, getOrderStatus
    } = _.bindAll(
        pickedListCtrl,
      Object.getOwnPropertyNames(PickedListController.prototype)
    );
    return Router()
      .post("/", savePickedList)
      .get("/", getPicked)
      .get("/getList", getPickedList)
      .get("/packed", getPacked)
      .get("/ondelivery", getOnDelivery)
      .get("/delivered", getDelivered)
      .post("/generate", generatePdf)
      .get("/packedOrHold", getPackedOrHold)
      .get("/getstatus", getOrderStatus)
      .post("/update", updateDeliveryStatus);

  };

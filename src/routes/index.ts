import systemRoutes from "./system/system";
import orderRoutes from "./order/order";
import { ProductRoutes } from "./product/product";
import mgmtRoute from "./user/admin/management";
import { BuyerRoutes } from "./user/buyer/buyer";
import merchantRoutes from "./user/merchant/merchant";
import { UserRoutes } from "./user/user";
import logisticRoute from "./order/logistics";
import adminRoute from "./../controllers/user/admin/admin";
import topupMgmtRoute from "./user/admin/management/Topup";
import transferMgmtRoute from "./user/admin/management/Transfer";
import pickedListMgmtRoute from "./pickedList/pickedList";
import salesPromotionMgmtRoute from "./sales-promotion/sales-promotion";
// import salesreturnRoutes from "./salesreturn/buyersalesreturn";
export default {
  systemRoutes,
  orderRoutes,
  ProductRoutes,
  ...mgmtRoute,
  BuyerRoutes,
  merchantRoutes,
  adminRoute,
  logisticRoute,
  UserRoutes,
  topupMgmtRoute,
  transferMgmtRoute,
  pickedListMgmtRoute,
  salesPromotionMgmtRoute
  //   salesreturnRoutes
};

"use strict";
import { Router } from "express";

import {
    categoryController,
    ProductController,
    promotionController,
    UserController,
    attributeController,
    rewardProductController
} from "./../../controllers";
import { check, checkSchema } from "express-validator/check";
import { validationResponse } from "./../../_middleware/validationRes";
import * as _ from "lodash";
/**
 * Product app routes.
 */
export class ProductRoutes {
    constructor(private config) {

    }
    register() {
        const { passport: { userPassport } } = this.config;
        const {
            getAllCategory
        } = categoryController(this.config);
        const productCtrl = new ProductController(this.config);
        const {
            getApprovedProducts,
            getSingleProductDetail,
            getProductsVariants,
            getRelevantProductFromStore,
            getProductSearch
        } = _.bindAll(productCtrl, Object.getOwnPropertyNames(ProductController.prototype));
        const {
            getRewardProduct
        } = rewardProductController(this.config);
        const {
            getUIFloors,
            getBrands,
            getAdsLink
        } = attributeController(this.config);
        const {
            getStore
        } = new UserController(this.config);
        const {
            verifyPromoCode
        } = promotionController(this.config);
        return Router()
            .get("/list", getApprovedProducts())
            .get("/search",getProductSearch())
            .get("/product",
                checkSchema({
                    product_id: {
                        in: "query",
                        exists: { errorMessage: "Product ID is required." }
                    }
                }),
                validationResponse,
                getSingleProductDetail("buyer"))
            .get("/category", getAllCategory)
            .get("/variants/:id", getProductsVariants)
            .get("/ui/floor", getUIFloors)
            .get("/ui/home/brands", getBrands)
            .get("/ui/home/ad-links", getAdsLink)
            .get("/store", getStore)
            .get("/store/relevant", getRelevantProductFromStore)
            .get("/promo/verify",
                userPassport.isJWTValid,
                checkSchema({
                    promo_code: {
                        // The location of the field, can be one or more of body, cookies, headers, params or query.
                        // If omitted, all request locations will be checked
                        in: ["query"],
                        exists: {
                            errorMessage: "Promo code not found.",
                        }
                    }
                }),
                validationResponse,
                verifyPromoCode)
            .get("/reward-product", getRewardProduct);

    }
}
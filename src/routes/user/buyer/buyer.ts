"use strict";
import { NextFunction, Request, Response, Router } from "express";
import { Types } from "mongoose";
import * as _ from "lodash";
import {
  UserController,
  CartController,
  orderController,
  rewardProductController,
  EmailController
} from "./../../../controllers";
import { checkSchema } from "express-validator/check";
import { validationResponse } from "../../../_middleware/validationRes";

class Greeter {
  greeting: string;
  constructor(message: string) {
    this.greeting = message;
  }
  greet() {
    return "Hello, " + this.greeting;
  }
}
export class BuyerRoutes {
  constructor(private config) {}
  register() {
    const cartCtrl = new CartController(this.config);
    const userCtrl = new UserController(this.config);
    const {
      buyerGetCart,
      buyerAddProductToCart,
      buyerAddToCart,
      buyerUpdateProductQtyInCart,
      buyerRemoveProductFromCart
    } = _.bindAll(
      cartCtrl,
      Object.getOwnPropertyNames(CartController.prototype)
    );
    const { getCreditWalletHistory, getRewardPts } = _.bindAll(
      userCtrl,
      Object.getOwnPropertyNames(UserController.prototype)
    );

    const {
      buyerGetOrder,
      buyerConfirmOrder,
      buyerPostComment,
      buyerPostComplaint,
      buyerGetOrderNo,
      getOrderNos,
   //   getSalesReturnOrders,
      saveShiptoaddress,
      saveBilltoaddress
    } = orderController(this.config);
    const { purchaseRewardProduct } = rewardProductController(this.config);
    const emailCtrl = new EmailController(this.config);
    const { sendEmail } = _.bindAll(
      emailCtrl,
      Object.getOwnPropertyNames(EmailController.prototype)
    );
    return (
      Router()
        .post("/welcome", sendEmail("WELCOME"))
        .get("/cart", buyerGetCart)
        .post(
          "/cart",
          checkSchema({
            product_id: {
              in: ["body"],
              exists: { errorMessage: "Product id is required." }
            },
            qty: {
              in: ["body"],
              exists: { errorMessage: "Quantity is required." }
            }
          }),
          validationResponse,
          buyerAddProductToCart
        )
        .post(
          "/addToCart",
          checkSchema({
            product_id: {
              in: ["body"],
              exists: { errorMessage: "Product id is required." }
            },
            qty: {
              in: ["body"],
              exists: { errorMessage: "Quantity is required." }
            }
          }),
          validationResponse,
          buyerAddToCart
        )
        .put("/cart", buyerUpdateProductQtyInCart)
        .delete(
          "/cart/:cart_id",
          checkSchema({
            cart_id: {
              in: ["params"],
              exists: { errorMessage: "Cart id is required." }
            }
          }),
          buyerRemoveProductFromCart
        )

        .get("/order", buyerGetOrder)
        .post("/order/confirm", buyerConfirmOrder)
        .get("/rewardpts", getRewardPts)
        .post("/comment", buyerPostComment)
        .get("/credit", getCreditWalletHistory)
        .post("/order/complaint", buyerPostComplaint)
        .get("/orderno", buyerGetOrderNo)
         .get("/odernos", getOrderNos)
        .post("/saveshipto", saveShiptoaddress)
        .post("/savebillto", saveBilltoaddress)
    );
    // .get("/salesreturnorders", getSalesReturnOrders);
  }
}

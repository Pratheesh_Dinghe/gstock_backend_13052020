import { Request, Response, NextFunction, Router } from "express";
import { ReturnController, ContactController } from "../../../../controllers";
import * as _ from "lodash";

export default config => {
   const contactCtrl = new ContactController(config);
    const {createContact, loadAddress, // send_email
    } = _.bindAll(
      contactCtrl,
      Object.getOwnPropertyNames(ContactController.prototype)
    );
    return Router()
      .post("/", createContact)
      .get("/load_contact/:user_id", loadAddress);
      // .post("/emailsend",send_email)

  };
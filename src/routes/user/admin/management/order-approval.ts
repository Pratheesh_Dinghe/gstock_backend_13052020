import { Request, Response, NextFunction, Router } from "express";
import * as _ from "lodash";
import {  OrderApprovalController } from "../../../../controllers";

export default config => {
    const orderApprovalCtrl = new OrderApprovalController(config);
    const {create, getOrderApproval
    } = _.bindAll(
        orderApprovalCtrl,
      Object.getOwnPropertyNames(OrderApprovalController.prototype)
    );
    return Router()

      .post("/", create)
      .get("/", getOrderApproval);
  };

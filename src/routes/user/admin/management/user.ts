import { NextFunction, Request, Response, Router } from "express";
import { check } from "express-validator/check";
import { userMgmtController } from "../../../../controllers";
/**
 * User management
 */

export default config => {
  const {
    createNewUser,
    getUserDetail,
    updateUserDetail,
    updateMerchantAssignedCommission,
    deleteMerchantAssignedCommission,
    getBuyerCreditWalletList,
    getBuyerCreditWallet,
    addNewEntryToBuyerCreditWallet,
    changeStatus,
    deleteAccount,
    activateAccount,
    suspendAccount,
    addnewSupplier,
    getallSuppliers,
    getSupplierData,
    updatesupplier,
    buildLink,
    getOrderStatus,
    updateCommission,
    buildLinkTree,
    showtrees,
    getActiveUser,
    getSalesReturnOrders,
    createBuyerSalesReturn,
    getTotalOrderamnt,
    saveBuyerBankInfo,
    getWalletWithdrawalDetails,
    createImage,
    merchantnameFetch,
    getEstimatedCredit,
    getBuyerFavorites
  } = userMgmtController(config);
  return (
    Router()
      .delete("/", deleteAccount())
      .post(
        "/create",
        [
          check("credential.email", "Email is not valid").isEmail(),
          check(
            "credential.password",
            "Password must be at least 4 characters long"
          ).isLength({ min: 4 }),
          // check("credential.confirm_password", "Passwords do not match").equals(req.body.credential.password),
          check("credential.user_group", "User group must be defined").exists()
        ],
        createNewUser
      )
      .get("/detail", getUserDetail)
      .put("/detail", updateUserDetail)
      .put("/commission", updateMerchantAssignedCommission)
      .post("/commission/delete", deleteMerchantAssignedCommission)
      .put("/activate/merchant", activateAccount())
      .put("/suspend/merchant", suspendAccount())
      .put("/activate/buyer", activateAccount())
      .put("/suspend/buyer", suspendAccount())
      .get("/user/credit_list", getBuyerCreditWalletList)
      .get("/creditwallet", getBuyerCreditWallet)
      // .post("/user/credit/:buyer_id", addNewEntryToBuyerCreditWallet)
      .post("/addcreditwallet", addNewEntryToBuyerCreditWallet)
      .post("/newsupplier", addnewSupplier)
      .get("/getallSuppliers", getallSuppliers)
      .post("/getSupplierData", getSupplierData)
      .post("/updatesupplier", updatesupplier)
      .post("/buildLink", buildLink)
      .post("/showtrees", showtrees)
      .get("/getOrderStatus", getOrderStatus)
      .post("/updateCommission", updateCommission)
      .get("/getsalesreturnorder", getSalesReturnOrders)
      .post("/buyersalesreturn", createBuyerSalesReturn)
      .post("/image/:id", createImage)
      .get("/checkActiveornot/:buyer", getActiveUser)
      .get("/getTotalOrderAmnt/:buyer", getTotalOrderamnt)
      .post("/saveBuyerBankInfo", saveBuyerBankInfo)
      .get("/getWalletWithdrawalDetails", getWalletWithdrawalDetails)
      .get("/getWalletWithdrawalDetails", getWalletWithdrawalDetails)
      .post("/getname", merchantnameFetch)
      .get("/getEstimatedCredit", getEstimatedCredit)
      .get("/getBuyerFavorites", getBuyerFavorites)
      // .get("/getPurchase/:buyer", getPurchase)
  );
};

import { Request, Response, NextFunction, Router } from "express";
import * as _ from "lodash";
import { MonthlyCommissionController } from "../../../../controllers";

export default config => {
    const monthlyCommissionCtrl = new MonthlyCommissionController(config);
    const {getCommission,
      approveCommission,
      withdrawFromWallet, setCommissionPercentage, getCommissionPercentage

    } = _.bindAll(
        monthlyCommissionCtrl,
      Object.getOwnPropertyNames(MonthlyCommissionController.prototype)
    );
    return Router()
      .post("/getCommission", getCommission)
      .post("/", approveCommission)
      .post("/withdraw", withdrawFromWallet)
      .post("/settings", setCommissionPercentage)
      .get("/settings", getCommissionPercentage);

  };

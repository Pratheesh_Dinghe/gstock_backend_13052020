import { Request, Response, NextFunction, Router } from "express";
import { ReturnController, purchaseReturnController } from "../../../../controllers";
import * as _ from "lodash";

export default config => {
   const purchaaseReturnCtrl = new purchaseReturnController(config);
    const {createPurchaseReturn,getReturnOrderNo,getAllPRDetails,deletePR,updatePR,getSinglePRDetail
    } = _.bindAll(
        purchaaseReturnCtrl,
      Object.getOwnPropertyNames(purchaseReturnController.prototype)
    );
    return Router()
      .post("/", createPurchaseReturn)
      .post('/purchaseReturn', updatePR)
      .get("/getOrderNo",getReturnOrderNo)
      .get("/getAllPRDetails",getAllPRDetails)
      .delete('/purchaseReturn', deletePR)
      .get('/getSinglePRDetail', getSinglePRDetail)

   
     
  };
  
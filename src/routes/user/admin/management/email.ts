import { Request, Response, NextFunction, Router } from "express";
import { EmailController } from "../../../../controllers";
import { Config } from "../../../../types/app";
import * as _ from "lodash";
export class EmailMgmtRoutes {
    constructor(private config: Config) {
    }
    /**
     * /admin/email
     */
    register() {
        const emailCtrl = new EmailController(this.config);
        const {
            getEmailContent,
            updateEmailContent,
            sendEmail,
            getHtmlStorage,
            getCampaignTemplates,
            saveCampaignTemplate,
            sendCampaignEmail,
            getCampaignTemplateDetails,
            updateCampaignTemplate
        } = _.bindAll(emailCtrl, Object.getOwnPropertyNames(EmailController.prototype));
        return Router()
            .get("/", getEmailContent)
            .put("/", updateEmailContent)
            .post("/test", sendEmail("WELCOME"))
            .post("/upload",
                getHtmlStorage().single("html"),
                (req: Request, res: Response, next: NextFunction) => {
                    return res.status(200).send({ message: "Upload succesfull" });
                })
                .get("/campaign-templates", getCampaignTemplates)
                .post("/campaign-templates", saveCampaignTemplate)
                .post("/campaign-templates/:id/send", sendCampaignEmail)
                .post("/getCampaignTemplateDetails", getCampaignTemplateDetails)
      .post("/updateCampaignTemplate", updateCampaignTemplate);
    }
}

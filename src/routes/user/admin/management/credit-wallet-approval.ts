import { Request, Response, NextFunction, Router } from "express";
import * as _ from "lodash";
import { CreditWalletApprovalController } from "../../../../controllers";

export default config => {
  const orderApprovalCtrl = new CreditWalletApprovalController(config);
  const {
    save,
    CreditWalletUpdate,
    getCreditWalletApproval,
    getWalletWithdrawal,
    getLatest
  } = _.bindAll(
    orderApprovalCtrl,
    Object.getOwnPropertyNames(CreditWalletApprovalController.prototype)
  );
  return Router()
    .post("/", save)
    .post("/CreditWalletUpdate", CreditWalletUpdate)
    .get("/", getCreditWalletApproval)
    .get("/withdraw", getWalletWithdrawal)
    .get("/latest", getLatest);
};

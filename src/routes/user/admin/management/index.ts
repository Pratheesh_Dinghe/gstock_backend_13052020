import attributeMgmtRoute from "./attribute";
import categoryMgmtRoute from "./category";
import productMgmtRoute from "./product";
import promotionMgmtRoute from "./promotion";
import rewardProductMgmtRoute from "./reward_product";
import userMgmtRoute from "./user";
import { EmailMgmtRoutes } from "./email";
import orderMgmtRoute from "./order";
import countryMgmtRoute from "./country";
import invoiceMgmtRoute from "./invoice";
import returnMgmtRoute from "./return";
import dailystockMgmtRoute from "./dailystock";
import stockMgmtRoute from "./stock";
import stockverifyMgmtRoute from "./stockverify";
import monthlyCommissionMgmtRoute from "./monthly-commission";
import deliveryProcessMgmtRoute from "./delivery-process";
import inventoryMgmtRoute from "./inventory";
import purchaseMgmtRoute from "./Purchasereturn";
import imageMgmtRoute from "./Image" ;
import refundMgmtRoute from "./refund";
import orderApprovalMgmtRoute from "./order-approval";
import creditWalletApprovalMgmtRoute from "./credit-wallet-approval";
import contactMgmtRoute from "./contact" ;
import updateWinnersMgmtRoute from "./update-winners";
export default {
    attributeMgmtRoute,
    categoryMgmtRoute,
    productMgmtRoute,
    promotionMgmtRoute,
    rewardProductMgmtRoute,
    userMgmtRoute,
    EmailMgmtRoutes,
    orderMgmtRoute,
    countryMgmtRoute,
    invoiceMgmtRoute,
    returnMgmtRoute,
    dailystockMgmtRoute,
    stockverifyMgmtRoute,
    stockMgmtRoute,
    deliveryProcessMgmtRoute,
    monthlyCommissionMgmtRoute,
    inventoryMgmtRoute,
    purchaseMgmtRoute,
    imageMgmtRoute,
    refundMgmtRoute,
    orderApprovalMgmtRoute,
    contactMgmtRoute,
    creditWalletApprovalMgmtRoute,
    updateWinnersMgmtRoute

};
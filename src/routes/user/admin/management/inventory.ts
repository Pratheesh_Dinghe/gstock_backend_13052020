// import { validatePageAndLimit } from "../../../config/validation";
import { Request, Response, NextFunction, Router } from "express";
import * as _ from "lodash";
import { InventoryController } from "../../../../controllers/inventory";
/*
 * Inventory
 * /admin/inventory
 */
export default (config: any) => {
    console.log("HAI--02");
    const inventoryCtrl = new InventoryController(config);
    const {
        createPO,
        approvePO,
        sendEmail,
        getOrderNo,
        getAllPODetails,
        getSinglePODetail,
        getAllActivePo,
        updatePO,
        deletePO,
        getAllSuppliers,
        getGrnNo,
        createGrn,
        getAllGrn,
        deleteGrn,
        getGrnById,
        getAllActivePoAuto
    } = _.bindAll(inventoryCtrl, Object.getOwnPropertyNames(InventoryController.prototype));
    return Router()
        .post("/",
            async (req: Request, res: Response, next: NextFunction) => { })
        .post("/purchaseOrder", createPO)
        .post("/purchaseOrder/approve", approvePO)
        .post("/purchaseOrder/sendEmail", sendEmail)
        .get("/getOrderNo", getOrderNo)
        .get("/getAllPODetails", getAllPODetails)
        .get("/getAllActivePo", getAllActivePo)
        .get("/getAllActivePoAuto", getAllActivePoAuto)
        .get("/getSinglePODetail", getSinglePODetail)
        .put("/purchaseOrder", updatePO)
        .delete("/purchaseOrder", deletePO)
        .get("/getSuppliers", getAllSuppliers)
        .get("/getGrnNo", getGrnNo)
        .post("/createGrn", createGrn)
        .get("/getAllGrn", getAllGrn)
        .delete("/deleteGrn", deleteGrn)
        .get("/grnById", getGrnById);

    // .post("/putimage", productCtrl.putStoreName)
    // .get("/getEmail", adminController.getEmail)
    // .get("/emailauth", adminController.authorize)

};


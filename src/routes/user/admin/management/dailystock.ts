import { Request, Response, NextFunction, Router } from "express";
import { DailystockController } from "../../../../controllers";
import * as _ from "lodash";

export default config => {
   const dailystockCtrl = new DailystockController(config);
    const {dailyStock
    } = _.bindAll(
         dailystockCtrl,
      Object.getOwnPropertyNames(DailystockController.prototype)
    );
    return Router()
      .post("/", dailyStock);
    };

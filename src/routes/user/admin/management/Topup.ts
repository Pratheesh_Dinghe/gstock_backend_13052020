import { Request, Response, NextFunction, Router } from "express";
import * as _ from "lodash";
import { TopupController } from "../../../../controllers";

export default config => {
    const topupCtrl = new TopupController(config);
    const {
         save
    } = _.bindAll(
      topupCtrl,
      Object.getOwnPropertyNames(TopupController.prototype)
    );
    return Router()

      .post("/", save);

  };
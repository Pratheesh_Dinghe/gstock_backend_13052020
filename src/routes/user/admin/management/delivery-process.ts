import { Request, Response, NextFunction, Router } from "express";
import {  DeliveryProcessController } from "../../../../controllers";
import * as _ from "lodash";

export default config => {
    const deliveryCtrl = new DeliveryProcessController(config);
    const {getAllPickedList, updateDeliveryStatus , getOrderStatus , updateProduct
    } = _.bindAll(
      deliveryCtrl,
      Object.getOwnPropertyNames(DeliveryProcessController.prototype)
    );
    return Router()

    .post("/", getAllPickedList)
    .post("/update", updateDeliveryStatus)
    .post("/getstatus", getOrderStatus)
      .post("/updateProduct", updateProduct);
   };



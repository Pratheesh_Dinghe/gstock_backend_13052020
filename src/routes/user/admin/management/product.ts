// import { validatePageAndLimit } from "../../../config/validation";
import { Request, Response, NextFunction, Router } from "express";
import { ProductController } from "../../../../controllers";
import * as _ from "lodash";
import { check, checkSchema, oneOf, param } from "express-validator/check";
import { validationResponse } from "../../../../_middleware/validationRes";
/*
 * Product management
 * /admin/product
 */

export default config => {
  const productCtrl = new ProductController(config);
  const {
    adminGetProducts,
    adminCreateProduct /** Admin Create product */,
    adminUpdateProducts,
    isProductActiveOrInPendingOrder,
    adminDeleteSingleProduct,
    getSingleProductDetail,
    adminUpdateProductStatus,
    merchantUpdateProduct,
    merchantRemoveProductImage,
    getProductsVariants,
    merchantUpdateProductActive,
    getproduct,
    // upload,
    productList,
    getProductBySupplier,
    SaveStockManaging,
    getAllSupplierProduct,
    getNormalSupplierProduct,
    getConsignmentSupplierProduct,
    getProductByName,
    getProductBySku
  } = _.bindAll(
    productCtrl,
    Object.getOwnPropertyNames(ProductController.prototype)
  );
  return (
    Router()
      .post("/", adminCreateProduct)
      .post("/getlist", productList)
      .post("/SaveStockManaging", SaveStockManaging)
      .get("/search", adminGetProducts())
      .put("/update", adminUpdateProducts)
      .delete("/", [isProductActiveOrInPendingOrder, adminDeleteSingleProduct])
      .get("/detail", getSingleProductDetail("admin"))
      .put("/status", adminUpdateProductStatus)
      .get("/variant", getProductsVariants)
      .get("/getproduct", getproduct)
      .post("/getProductBySupplier", getProductBySupplier)
      // .post("/upload", upload)
      // .post("/putimage", productCtrl.putStoreName)
      // .get("/getEmail", adminController.getEmail)
      // .get("/emailauth", adminController.authorize)
      .get("/getAllSupplierProduct", getAllSupplierProduct)
      .get("/getNormalSupplierProduct", getNormalSupplierProduct)
      .get("/getConsignmentSupplierProduct", getConsignmentSupplierProduct)
      .post("/getProductByName", getProductByName)
      .post("/getProductBySku", getProductBySku)
      .put(
        "/product",
        checkSchema({
          "brief.short_description": {
            in: ["body"],
            isLength: {
              options: { max: 150 },
              errorMessage:
                "Product short description must not exceed 150 characters."
            }
          },
          "stock.qty": {
            in: ["body"],
            isInt: {
              options: { min: 1 },
              errorMessage: "Product quantity must at least be 1."
            }
          },
          "brief.name": {
            in: ["body"],
            isLength: {
              options: { max: 50 },
              errorMessage: "Product name must not exceed 50 characters."
            }
          },
          "detail.brand": {
            in: ["body"],
            isLength: {
              options: { max: 50 },
              errorMessage: "Product brand name must not exceed 50 characters."
            }
          }
        }),
        [validationResponse, merchantUpdateProduct]
      )
      .post(
        "/thumbnail/delete/:productId",
        checkSchema({
          productId: {
            in: "params",
            errorMessage: "Product id is required"
          },
          remove: {
            in: "body",
            isString: {
              errorMessage: "Invalid file name"
            },
            errorMessage: "Files to remove is required"
          }
        }),
        merchantRemoveProductImage
      )
      .put("/active", merchantUpdateProductActive)
  );
};

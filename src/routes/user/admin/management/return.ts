import { Request, Response, NextFunction, Router } from "express";
import { ReturnController } from "../../../../controllers";
import * as _ from "lodash";

export default config => {
   const returnCtrl = new ReturnController(config);
    const {createSalesReturn, getLatest, getAllReturn, getReturnById, updateSalesReturn
    } = _.bindAll(
        returnCtrl,
      Object.getOwnPropertyNames(ReturnController.prototype)
    );
    return Router()
      .post("/", createSalesReturn)
      .get("/latest", getLatest)
      .get("/", getAllReturn)
      .get("/search/:id", getReturnById)
      .post("/update", updateSalesReturn);

  };

import { Request, Response, NextFunction, Router } from "express";
import * as _ from "lodash";
import { UpdateWinnersController } from "../../../../controllers";

export default config => {
    const updateWinnersCtrl = new UpdateWinnersController(config);
    const {
        checkEmail, save, getUpdateWinners, updateById, deleteById, bulkEmail
    } = _.bindAll(
        updateWinnersCtrl,
      Object.getOwnPropertyNames(UpdateWinnersController.prototype)
    );
    return Router()

      .post("/checkEmail", checkEmail)
      .post("/", save)
      .get("/", getUpdateWinners)
      .post("/update", updateById)
      .post("/bulkEmail", bulkEmail)
      .delete("/delete", deleteById);


  };
import { Request, Response, NextFunction, Router } from "express";
import { InvoiceController } from "../../../../controllers";
import * as _ from "lodash";

export default config => {
  const invoicetCtrl = new InvoiceController(config);
  const { createSalesInvoice, updateSalesInvoice,
    getLatest, getAllInvoices, getInvoiceById,
    getAllSalesReturns, getSalesReturnbyid, updateReturnStatus

  } = _.bindAll(
    invoicetCtrl,
    Object.getOwnPropertyNames(InvoiceController.prototype)
  );
  return Router()
    .post("/", createSalesInvoice)
    .get("/latest", getLatest)
    .get("/", getAllInvoices)
    .get("/search/:id", getInvoiceById)
    .post("/update", updateSalesInvoice)
    .get("/getReturnbyid/:id", getSalesReturnbyid)
    .get("/getReturns", getAllSalesReturns)
    .post("/updateReturn", updateReturnStatus);

};

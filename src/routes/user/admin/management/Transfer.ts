import { Request, Response, NextFunction, Router } from "express";
import * as _ from "lodash";
import { TransferController } from "../../../../controllers";

export default config => {
    const transferCtrl = new TransferController(config);
    const {
         save,
    } = _.bindAll(
        transferCtrl,
      Object.getOwnPropertyNames(TransferController.prototype)
    );
    return Router()

      .post("/", save);


  };
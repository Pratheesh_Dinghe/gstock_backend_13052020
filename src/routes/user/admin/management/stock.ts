import { Request, Response, NextFunction, Router } from "express";
import { InvoiceController, StockController } from "../../../../controllers";
import * as _ from "lodash";

export default config => {
  const stockCtrl = new StockController(config);
  const {
    getStockRefNo,
    createStock,
    getAllStockDetails,
    getSingleStockDetail,
    updateStock,
    deleteStock,
    category_fetch,
    category,
    getStockReport,
    getAllOrder,
    getAllOrderdetails,
    getJoinReport,
    checkUserTree,
    loadJoinedUnderCompany,
    loadUserList,
    generatecommissionReport,
    hotitems,
    gethotitems,
    gethotitemsList,
    deletehotitems,
    getCreditWallet
  } = _.bindAll(
    stockCtrl,
    Object.getOwnPropertyNames(StockController.prototype)
  );
  return Router()

    .post("/stock", createStock)
    .get("/getStockRefNo", getStockRefNo)
    .get("/getAllStockDetails", getAllStockDetails)
    .get("/getSingleStockDetail", getSingleStockDetail)
    .post("/getcategory", category_fetch)
    .get("/category", category)
    .put("/stock", updateStock)

    .delete("/stock", deleteStock)
    .get("/getstockreport", getStockReport)
    .post("/getsalesreport", getAllOrder)
    .post("/getsalesdetais", getAllOrderdetails)
    .post("/getJoineReport", getJoinReport)
    .post("/checkUserTree", checkUserTree)
    .post("/loadJoinedUnderCompany", loadJoinedUnderCompany)
  //  .post("/getsalesdetais",getPaidOrders)
  .get("/loadUserList", loadUserList)
  .post("/hotitems", hotitems)
  .get("/gethotitems", gethotitems)
  .get("/gethotitemsList", gethotitemsList)
  .delete("/deletehotitems", deletehotitems)
  .get("/getCreditWallet", getCreditWallet)
    .post("/generatecommissionReport", generatecommissionReport);
};


import { Request, Response, NextFunction, Router } from "express";
import { countryController } from "../../../../controllers";
/**
 * Category Management
 */
export default config => {
  const { getAllCountry } = countryController(config);

  return Router().get("/allcountry", getAllCountry);
};

import { Request, Response, NextFunction, Router } from "express";
import { StockverifyController } from "../../../../controllers";
import * as _ from "lodash";

export default config => {
    const stockverifyCtrl = new StockverifyController(config);
    const {
        createStockverify, getStockVerfNo, getAllStockverifyDetail, getStockverifyDetailById
    } = _.bindAll(
        stockverifyCtrl,
      Object.getOwnPropertyNames(StockverifyController.prototype)
    );
    return Router()
           .post("/", createStockverify)
           .get("/getStockVerfNo", getStockVerfNo)
           .get("/getAllStockverifyDetails", getAllStockverifyDetail)
           .get("/getSingleStockveifyDetail", getStockverifyDetailById);


  };

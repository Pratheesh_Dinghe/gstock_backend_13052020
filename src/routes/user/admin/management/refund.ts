import { Request, Response, NextFunction, Router } from "express";
import * as _ from "lodash";
import { RefundController } from "../../../../controllers";

export default config => {
  const refundCtrl = new RefundController(config);
  const { saveRefund, getRefundNo, getRefund, approveRefund, approveSingleId

  } = _.bindAll(
    refundCtrl,
    Object.getOwnPropertyNames(RefundController.prototype)
  );
  return Router()

    .post("/", saveRefund)
    .get("/latest", getRefundNo)
    .get("/", getRefund)
    .post("/approve", approveRefund)
    .get("/approveSingleId/:id", approveSingleId)
  //  .get("/approveSingleId",approveSingleId)

};

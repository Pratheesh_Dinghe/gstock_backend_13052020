import { Request, Response, NextFunction, Router } from "express";
import * as PDFkit from "pdfkit";
import orderController from "./../../../../controllers/order";
/**
 * Order Management
 */
export default (config) => {
  const {
    deleteOrder,
    getOrderPending,
    adminGetOrder,
    adminSearchOrder,
    generateOrdersList,
    // getpaidlist,
    adminGetOrderComplain,
    adminGetOrderPaid,
    getPaidOrders,
    getSupplierDetails,
    getAllJobs,
    SaveJobs,
    getSavedJobs,
    UpdateJobStatus,
    getPaidOrder
    //  getPickDate

  } = orderController(config);
  return Router()
    .get("/", adminGetOrder)
    .post("/paid", getPaidOrders)
    .post("/generateOrdersList", generateOrdersList)
    .post("/search", adminSearchOrder)
    .put("/status")
    .get("/complaint", adminGetOrderComplain)
    .get("/test", adminGetOrderPaid)
    .post("/bulk/delete", deleteOrder)
    .post("/getSupplierDetails", getSupplierDetails)
    .get("/getOrderPending", getOrderPending)
    .post("/getAllJobs", getAllJobs)
    .post("/SaveJobs", SaveJobs)
    .post("/getSavedJobs", getSavedJobs)
    .get("/allpaid", getPaidOrder)
    .post("/updateJobStatus", UpdateJobStatus);


};


import { Request, Response, NextFunction, Router } from "express";
import { ReturnController, purchaseReturnController, ImageSaveController } from "../../../../controllers";
import * as _ from "lodash";

export default config => {
  const imageCtrl = new ImageSaveController(config);
  const { createImage
  } = _.bindAll(
    imageCtrl,
    Object.getOwnPropertyNames(ImageSaveController.prototype)
  );

  return Router()
    .post("/", createImage)
};

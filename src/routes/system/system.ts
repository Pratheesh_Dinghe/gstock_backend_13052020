import { Request, Response, Router } from "express";
export default config => {
  const { System } = config.models;
  return (
    Router()
    .get("/", async (req: Request, res: Response) => {
      try {
        const system = await System.findOne({}, "-_v, -_id");
        res.json(system);
      } catch (error) {
       res.json({error: error.message});
      }
    })
  );
};
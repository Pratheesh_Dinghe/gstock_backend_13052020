import { Connection } from "mongoose";
import * as mongoose from "mongoose";
import { dbLogger } from "./logger";
// import { Mockgoose } from "mockgoose";
mongoose.set("debug", (coll, method, query, doc, options) => {
  dbLogger.log({
    level: "info",
    source: "db",
    message: JSON.stringify({
      coll,
      method,
      query,
      doc,
      options
    })
  });
});
(<any>mongoose).Promise = global.Promise;
/**
 * Connect to MongoDB.
 */
// export class DatabaseConnection {
//     constructor(private role: string) {

//     }
//     async connect() {
//             switch (this.role) {
//                 case "admin": {
//                     const admin_uri = process.env.MONGODB_URI_ADMIN;
//                     // console.log("reading", process.env, admin_uri);
//                     console.log("Admin connected");
//                     return await mongoose.createConnection(admin_uri, { useMongoClient: true, });
//                 }
//                 case "merchant": {
//                     const merchant_uri = process.env.MONGODB_URI_MERCHANT;
//                     console.log("reading", merchant_uri);
//                     console.log("Merchant connected");
//                     return await mongoose.createConnection(merchant_uri, { useMongoClient: true, });
//                 }
//                 case "buyer": {
//                     const buyer_uri = process.env.MONGODB_URI_BUYER;
//                     console.log("reading", buyer_uri);
//                     return await mongoose.createConnection(buyer_uri, { useMongoClient: true, })
//                         .on("connected", () => {
//                             console.log("Buyer connected");
//                             console.log("I am opened only once!");
//                         })
//                         .on("error", () => {
//                             console.log("Whoops");
//                             console.log("Something wrong!");
//                         })
//                         .on("closed", () => {
//                             console.log("I am closed!");
//                         });
//                 }
//             }
//         }
//     }

export default async (role: string) => {
  let connection: Connection;
  if (process.env.NODE_ENV === "test") {
    // const mockgoose = new Mockgoose(mongoose);
    // await mockgoose.prepareStorage();
    // connection = await mongoose.connect("mongodb://localhost/", {
    //   useMongoClient: true
    // });
    // connection = await mongoose.createConnection("mongodb://localhost:27017", {
    //   useMongoClient: true
    // });
    // connection = await mongoose.createConnection("mongodb://13.228.151.60/", {
      connection = await mongoose.createConnection("mongodb://54.255.249.222/", {
      useMongoClient: true
    });
    console.log("Mock DB connected");
  } else {
    switch (role) {
      case "admin": {
        const admin_uri =
          // process.env.MONGODB_URI_ADMIN || "mongodb://localhost:27017";
        // process.env.MONGODB_URI_ADMIN || "mongodb://13.228.151.60:15555"; // TEST
        process.env.MONGODB_URI_ADMIN || "mongodb://54.255.249.222:15555"; // LIVE
        // const admin_uri = "mongodb://localhost:27017/";
        // process.env.MONGODB_URI_ADMIN;
        // console.log("reading", process.env, admin_uri);
        connection = await mongoose.createConnection(admin_uri, {
          useMongoClient: true
        });
        console.log("Admin connected");
        break;
      }
      case "merchant": {
        const merchant_uri =
          // process.env.MONGODB_URI_MERCHANT || "mongodb://localhost:27017";
      // process.env.MONGODB_URI_ADMIN || "mongodb://13.228.151.60:15555"; // TEST
      process.env.MONGODB_URI_ADMIN || "mongodb://54.255.249.222:15555"; // LIVE
        // const merchant_uri = "mongodb://localhost:27017/";
        // process.env.MONGODB_URI_MERCHANT;
        console.log("reading", merchant_uri);
        connection = await mongoose.createConnection(merchant_uri, {
          useMongoClient: true
        });
        console.log("Merchant connected");
        break;
      }
      case "buyer": {
        const buyer_uri =
          // process.env.MONGODB_URI_BUYER || "mongodb://localhost:27017";
        //  process.env.MONGODB_URI_ADMIN || "mongodb://13.228.151.60:15555";
         process.env.MONGODB_URI_ADMIN || "mongodb://54.255.249.222:15555";
        // const buyer_uri = "mongodb://localhost:27017/";
        // process.env.MONGODB_URI_BUYER;
        console.log("reading", buyer_uri);
        connection = await mongoose
          .createConnection(buyer_uri, { useMongoClient: true })
          .on("connected", () => {
            console.log("Buyer connected");
            console.log("I am opened only once!");
          })
          .on("error", () => {
            console.log("Whoops");
            console.log("Something wrong!");
          })
          .on("closed", () => {
            console.log("I am closed!");
          });
        break;
      }
    }
  }

  const DATABASE_NAME =
    process.env.NODE_ENV === "development" ? "test" : "production";
  return {
    // live
    userDB: connection.useDb("gstok_new_prod"),
    productDB: connection.useDb("gstok_new_prod"),
    orderDB: connection.useDb("gstok_new_prod"),
    // userDB: connection.useDb("Gstock_041019"),
    // productDB: connection.useDb("Gstock_041019"),
    // orderDB: connection.useDb("Gstock_041019"),
    // userDB: connection.useDb("gstock"),
    // productDB: connection.useDb("gstock"),
    // orderDB: connection.useDb("gstock"),
    role
  };
};

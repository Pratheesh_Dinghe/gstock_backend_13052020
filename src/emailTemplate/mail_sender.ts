import * as nodemailer from "nodemailer";

export function sendMail(content) {
    try {
        const {
            html,
            token,
            subject,
            email
        } = content;
        const smtpTransport = nodemailer.createTransport({
            service: "gmail",
            auth: {
                user: "gstocktest123@gmail.com",
                pass: "CYGA6677"
            }
        });
        const mailOptions = {
            to: email,
            from: "gstore@admin.com",
            subject,
            html
        };
        const sendEmail = new Promise((resolve, reject) => {
            smtpTransport.sendMail(mailOptions,
                (error, response: any) => { if (error) { console.log(error); reject(error); } else { console.log(response); resolve(response); } });
        });
        sendEmail.then(() => {
            console.log('Mail sent')
        }).catch((error) => {
            console.log('***********----Error--------****************', error)
        })

    } catch (error) {
        console.log('***********----Error--------****************', error)
    }
}
import routes from "./routes";
import { Express } from "express";
import { getUserPassport, UserPassport } from "./config/user.passport";
import { getAdminPassport, AdminPassport } from "./config/admin.passport";
import getModels, { Models } from "./models";
import { Config } from "./types/app";
const {
  systemRoutes,
  orderRoutes,
  ProductRoutes,
  attributeMgmtRoute,
  categoryMgmtRoute,
  productMgmtRoute,
  promotionMgmtRoute,
  rewardProductMgmtRoute,
  EmailMgmtRoutes,
  userMgmtRoute,
  BuyerRoutes,
  UserRoutes,
  merchantRoutes,
  adminRoute,
  logisticRoute,
  orderMgmtRoute,
  countryMgmtRoute,
  invoiceMgmtRoute,
  returnMgmtRoute,
  dailystockMgmtRoute,
  stockverifyMgmtRoute,
  stockMgmtRoute,
  deliveryProcessMgmtRoute,
  monthlyCommissionMgmtRoute,
  inventoryMgmtRoute,
  purchaseMgmtRoute,
  imageMgmtRoute,
  refundMgmtRoute,
  orderApprovalMgmtRoute,
  // contactMgmtRoute
  contactMgmtRoute,
  creditWalletApprovalMgmtRoute,
  updateWinnersMgmtRoute,
  topupMgmtRoute,
  transferMgmtRoute,
  pickedListMgmtRoute,
  salesPromotionMgmtRoute
} = routes;
export class API {
  app: Express;
  config: Config;
  constructor(app: Express, config: Config) {
    this.app = app;
    this.config = config;
    console.log("gfgdgsgd", config.role);
    switch (config.role) {
      case "admin": {
        this.regsiterAdmin();
        return;
      }
      case "buyer": {
        this.regsiterBuyer();
        return;
      }
      case "merchant": {
        this.regsiterMerchant();
        return;
      }
      default:
        throw "Invalid user";
    }
  }
  private regsiterAdmin() {
    const config = this.config;
    const app = this.app;
    const { userPassport } = config.passport;
    console.log("Connecting as admin");
    app.use("/admin", adminRoute(config));
    app.use("/admin/attributes", attributeMgmtRoute(config));
    app.use("/admin/category", categoryMgmtRoute(config));
    app.use("/admin/country", countryMgmtRoute(config));
    app.use("/admin/product", productMgmtRoute(config));
    app.use("/admin/dailystock", dailystockMgmtRoute(config));
    app.use("/admin/stockverify", stockverifyMgmtRoute(config));
    app.use("/admin/stock", stockMgmtRoute(config));
    app.use("/admin/invoice", invoiceMgmtRoute(config));
    app.use("/admin/refund", refundMgmtRoute(config));
    app.use("/admin/order-approval", orderApprovalMgmtRoute(config));
    app.use("/admin/credit-wallet-approval", creditWalletApprovalMgmtRoute(config));
    app.use("/admin/Image", imageMgmtRoute(config));
    app.use("/admin/return", returnMgmtRoute(config));
    app.use("/admin/promotion", promotionMgmtRoute(config));
    app.use("/admin/reward-product", rewardProductMgmtRoute(config));
    app.use("/admin/user", userMgmtRoute(config));
    app.use("/admin/contact", contactMgmtRoute(config));
    app.use("/admin/order", orderMgmtRoute(config));
    app.use("/admin/Purchasereturn", purchaseMgmtRoute(config));
    app.use("/admin/delivery-process", deliveryProcessMgmtRoute(config));
    app.use("/admin/monthly-commission", monthlyCommissionMgmtRoute(config));
    app.use("/admin/inventory", inventoryMgmtRoute(config));
    app.use("/admin/logistic", logisticRoute(config));
    app.use("/admin/update-winners", updateWinnersMgmtRoute(config));
    app.use("/admin/email", new EmailMgmtRoutes(config).register());
    app.use("/user", new UserRoutes(config).register());
    app.use("/buyer", new BuyerRoutes(config).register());
    app.use("/merchant", merchantRoutes(config));
    app.use("/product", new ProductRoutes(config).register());
    app.use("/order", orderRoutes(config));
    app.use("/system", systemRoutes(config));
    app.use("/admin/topup", topupMgmtRoute(config));
    app.use("/admin/transfer", transferMgmtRoute(config));
    app.use("/admin/pickedList", pickedListMgmtRoute(config));
    app.use("/admin/sales-promotion", salesPromotionMgmtRoute(config));
    const swaggerUi = require("swagger-ui-express");
    const swaggerJson = require("./swagger.json");
    app.use(
      "/api-docs",
      swaggerUi.serve,
      swaggerUi.setup(swaggerJson, { showExplorer: true })
    );
  }
  private regsiterBuyer() {
    const config = this.config;
    const app = this.app;
    const { userPassport } = config.passport;
    console.log("Connecting as buyer");
    /**
     * POST /user/login
     * POST /user/register
     * POST /user/register/fb
     * POST /user/forgot/reset
     * POST /user/forgot/email
     * POST /user/forgot/token
     * GET /user/account/detail
     * PUT /user/account/detail
     * POST /user/account/reset
     */
    app.use("/user", new UserRoutes(config).register());
    /**
     * GET /buyer/cart
     * POST /buyer/cart
     * PUT /buyer/cart
     * DELETE /buyer/cart/:cart_id
     */
    app.use(
      "/buyer",
      userPassport.isJWTValid.bind(userPassport),
      new BuyerRoutes(config).register()
    );
    /**
     * GET /product/list
     * GET /product/product
     * GET /product/category
     * GET /product/variants/:id
     */
    app.use("/product", new ProductRoutes(config).register());
    /**
     * POST /order/place
     * GET /product/product
     * GET /product/category
     * GET /product/variants/:id
     */
    // app.use("/order", orderRoutes(config));
    app.use(
      "/order",
      userPassport.isJWTValid.bind(userPassport),
      orderRoutes(config)
    );

    // console.log("salesreturnRoutes", userPassport);
    // app.use(
    //   "/return",
    //   userPassport.isJWTValid.bind(userPassport),
    //   salesreturnRoutes(config)
    // );
    return;
  }
  private regsiterMerchant() {
    const config = this.config;
    const app = this.app;
    const { userPassport } = config.passport;
    console.log("Connecting as merchant");
    /**
     * POST /user/login
     * POST /user/register
     * POST /user/forgot/reset
     * POST /user/forgot/email
     * POST /user/forgot/token
     * GET /user/account/detail
     * PUT /user/account/detail
     * POST /user/account/reset
     */
    app.use("/user", new UserRoutes(config).register());
    /*
     * POST /merchant/product
     * POST /user/register
     * POST /user/forgot/reset
     * POST /user/forgot/email
     * POST /user/forgot/token
     * GET /user/account/detail
     * PUT /user/account/detail
     * POST /user/account/reset
     */
    app.use(
      "/merchant",
      userPassport.isJWTValid.bind(userPassport),
      merchantRoutes(config)
    );
    app.use("/product", new ProductRoutes(config).register());
    app.use("/order", orderRoutes(config));
    return;
  }
}

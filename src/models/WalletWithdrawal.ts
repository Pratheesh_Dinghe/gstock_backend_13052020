import { SchemaTypes, Types, Schema, Document } from "mongoose";
import { StopTrainingJobRequest } from "aws-sdk/clients/sagemaker";

export type WalletWithdrawalModel = Document & {
  buyer_id: Types.ObjectId;
  bank_name: string;
  Acc_No: string;
  Name: string;
  Mobile_No: string;
  Withdrawal_Amnt: number;

  Status: string;
};

const WalletWithdrawalSchema = new Schema(
  {
    buyer_id: {
      type: SchemaTypes.ObjectId,
      ref:"User"
    },
    bank_name: String,
    Acc_No: String,
    Name: String,
    Mobile_No: String,
    Withdrawal_Amnt: Number,
    Status: String
  },
  { timestamps: true }
);

export default WalletWithdrawalSchema;

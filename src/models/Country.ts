import { Schema, Types, Document, Error, SchemaTypes } from "mongoose";

export interface CountryModel extends Document {
  code: string;
  name: string;
}

const countrySchema = new Schema({
  code: String,
  name: String
});
export default countrySchema;

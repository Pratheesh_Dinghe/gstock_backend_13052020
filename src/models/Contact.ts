import { SchemaTypes, Document, Schema, Types } from "mongoose";
import * as mongoosePaginate from "mongoose-paginate";
import { NumericDictionary } from "lodash";
export interface ContactModel extends Document {
    buyer_id: Types.ObjectId;
    FirstName: string;
    LastName: string;
    email: string;
    telephone: number;
    message: string;

 

}
const contactSchema = new Schema(
    {
        buyer_id: {
            type: SchemaTypes.ObjectId
          },
        FirstName: String,
        LastName: String,
        email: String,
        telephone: Number,
        message: String,

        
    });


export default contactSchema;

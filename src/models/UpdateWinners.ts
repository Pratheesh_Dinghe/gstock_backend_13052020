import { Schema, Types, Document, Error, SchemaTypes } from "mongoose";
import * as bcrypt from "bcrypt-nodejs";
import * as crypto from "crypto";

export interface UpdateWinnersModel extends Document {
  name: String;
  email: String;
  mobile: String;
  amount: String;
}

const updateWinnersSchema = new Schema(
  {
    name: String,
    email: String,
    mobile: String,
    amount: String,
  },
  {
    timestamps: true,
    minimize: false
  }
);

export default updateWinnersSchema;

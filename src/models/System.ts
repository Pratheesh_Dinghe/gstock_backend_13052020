import { SchemaTypes, Document, Schema, Types } from "mongoose";

export interface SystemModel extends Document {
   androidMinimumVersion: string;
}

const systemSchema = new Schema({
    androidMinimumVersion: {
        type: String,
        default: "3.2.0"
    },
});

export default systemSchema;
import { SchemaTypes, Types, Schema, Document } from "mongoose";

export type commissionPaidModel = Document & {

    year: Number,
    month: String,
    commission: [{
        name: String,
        buyer: String,
        email: String,
        amount: {
            type: String
        }
    }],
    processing_date: Date
};


const commissionPaidSchema = new Schema({
    email: String,
    year: Number,
    month: String,
    commission: [{
        name: String,
        buyer: String,
        email: String,
        amount: {
            type: String
        }
    }],
    processing_date: Date

});




export default commissionPaidSchema;

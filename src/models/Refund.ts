import { SchemaTypes, Document, Schema, Types } from "mongoose";
import * as mongoosePaginate from "mongoose-paginate";
import { NumericDictionary } from "lodash";
export interface RefundModel extends Document {
  refund_no: string;
  refund_date: Date;
  order_no: string;
  order_date: Date;
  buyer_id: string;
  buyer_name: string;
  ref_id: string;
  total: number;
  orderTotalAmount: number;
  shipping_fee: number;
  //  buyer_details:string;
  //  address:string,
  buyer_email: String;
  // contact:Number,
  item: [
    {
      amount: number;
      code: string;
      name: string;
      cost: number;
      qty: number;
      refund_qty: number;
    }
  ];
  status: {
    type: string;
    default: "requested";
  };
}
const refundSchema = new Schema({
  refund_no: String,
  refund_date: Date,
  order_no: String,
  order_date: Date,
  buyer_id: String,
  buyer_name: String,
  ref_id: String,
  // buyer_details:String,
  // address:String,
  buyer_email: String,
  // contact:Number,

  item: [
    {
      amount: Number,
      code: String,
      name: String,
      cost: Number,
      qty: Number,
      refund_qty: Number
    }
  ],

  total: Number,
  orderTotalAmount: Number,
  shipping_fee: Number,
  status: {
    type: String,
    default: "Requested"
  }
});

export default refundSchema;

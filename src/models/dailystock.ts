import { SchemaTypes, Document, Schema, Types } from "mongoose";
import * as mongoosePaginate from "mongoose-paginate";

export interface DailystockModel extends Document {

  
    name: string;
     sku: string;
  //  qty: number;
    Updated_qty:number;
    current_qty:number;

  //  userqty:number;
  
}

export const validatorGroups = {
  unitPriceMustBeGreaterThanDiscountPrice() {
    return this.uP > this.dP;
  }
};
const dailystockSchema = new Schema(
  {
      name: String,
      sku: String,
      Updated_qty:Number,
      current_qty:Number,
      // qty: Number,
      // userqty:Number,

  },
  { timestamps: true, minimize: false }
);
dailystockSchema.plugin(mongoosePaginate);
export default dailystockSchema;

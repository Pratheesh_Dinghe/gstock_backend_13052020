import { SchemaTypes, Document, Schema, Types } from "mongoose";
import * as mongoosePaginate from "mongoose-paginate";
import { NumericDictionary } from "lodash";
export interface StockModel extends Document {
    refNo: string;
    date: Date;
    purchasInvNo: string;
    purchaseDate: Date;
    totalCost: number;
    Status: String;
    item: [{
        brand: string;
        code: string;
        name: string;
        qty: number;
        cost: number;
        totalCost: number
    }];

}

const stockSchema = new Schema({
    refNo: String,
    date: Date,
    purchasInvNo: String,
    purchaseDate: Date,
    totalCost: Number,
    Status: String,
    item: [ {
        slNo: Number,
        brand: String,
        code: String,
        name: String,
        qty: Number,
        cost: Number,
        totalCost: Number
    }]
}, { timestamps: true, minimize: false });
stockSchema.plugin(mongoosePaginate);

export default stockSchema;

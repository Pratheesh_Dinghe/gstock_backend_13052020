import { Schema, Types, Document, Error, SchemaTypes } from "mongoose";
import * as bcrypt from "bcrypt-nodejs";
import * as crypto from "crypto";

export interface TransferModel extends Document {
  name: String;
  email: String;
  mobile: String;
  address: String;
  amount: String;
  transfer_to: Types.ObjectId;
  transfer_from: Types.ObjectId;
}

const transferSchema = new Schema(
  {
    name: String,
    email: String,
    mobile: String,
    address: String,
    amount: String,
    transfer_to: SchemaTypes.ObjectId,
    transfer_from: SchemaTypes.ObjectId,
  },
  {
    timestamps: true,
    minimize: false
  }
);

export default transferSchema;

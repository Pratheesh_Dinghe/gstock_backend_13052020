import { SchemaTypes, Document, Schema, Types } from "mongoose";
import * as mongoosePaginate from "mongoose-paginate";
import { totalmem } from "os";

export interface GoodsReceivedModel extends Document {
    grnNo:string,
    poNo:string,
    poId:Schema.Types.ObjectId,
    grnDate:Date,
    supplier:string,
    item:[ {
        code: string,
        name: string,
        qty: number,
        cost: number,
        discount:number,
        totalCost: number,
        rcvd_qty:number,
        balance:number,
        delivered:Number,
        unit:string,
        status:{
            type:String,
            default:"active"
        }
    }],
}

const goodsReceivedSchema = new Schema({
    grnNo:String,
    poNo:String,
    poId:Schema.Types.ObjectId,
    grnDate:Date,
    supplier:String,
    item:[ {
        code: String,
        name: String,
        qty: Number,
        cost: Number,
        discount:Number,
        totalCost: Number,
        rcvd_qty:Number,
        balance:Number,
        delivered:Number,
        unit:String,
        status:{
            type:String,
            default:"active"
        },
    }], status:{
        type:String,
        default:"active"
    }
}, { timestamps: true, minimize: false });
goodsReceivedSchema.plugin(mongoosePaginate);
export default goodsReceivedSchema;


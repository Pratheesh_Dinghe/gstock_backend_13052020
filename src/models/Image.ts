import { SchemaTypes, Document, Schema, Types } from "mongoose";
import * as mongoosePaginate from "mongoose-paginate";
import { NumericDictionary } from "lodash";
export interface ImageSaveModel extends Document {

    name:string;
    type: string;
}
const imagesaveSchema = new Schema(
    {
       name:String,
      type: String,
        });
export default imagesaveSchema;
import { SchemaTypes, Document, Schema, Types } from "mongoose";
import * as mongoosePaginate from "mongoose-paginate";

export interface ProductModel extends Document {
  merchant_id: Types.ObjectId;
  category_id: Types.ObjectId;
  status: string;
  is_active: boolean;
  visibility: string;
  image: string;
  brief: {
    name: string;
    code: string;
    short_description: string;
    price: number;
    cost: number;
    stock: number;
    discount: boolean;
    category: string;
    images: Array<string>;
  };
  detail: {
    long_description: string;
    product_brand: string;
    barcode: string;
    sku: string;
  };
  pricing: {
    discount_rate: number;
    discount_price: number;
    final_price: number;
    min_price: number;
    max_price: number;
  };
  stock: {
    pending: [
      {
        order_id: Types.ObjectId;
        merchant_id: Types.ObjectId;
        qty: number;
      }
    ];
    qty: number;
    min_qty: number;
    unit: string;
  };
  country: string;

  countryofOrigin: string;
  supplier: {
    _id: Types.ObjectId;
    supplier: String;
    // email1: String;
    // type: String;
  };
  deliveryCost: string;
  international: boolean;
  stock_avilability: boolean;
}

export const validatorGroups = {
  unitPriceMustBeGreaterThanDiscountPrice() {
    return this.uP > this.dP;
  }
};
const productSchema = new Schema(
  {
    merchant_id: SchemaTypes.ObjectId,
    category_id: {
      type: SchemaTypes.ObjectId,
      ref: "Category"
    },
    is_active: {
      type: Boolean,
      default: false
    },
    status: {
      type: String,
      default: "Pending"
    },
    visibility: String,
    image: String,
    brief: {
      name: String,
      code: String,
      short_description: String,
      price: Number,
      cost: Number,
      stock: Number,
      discount: {
        type: Boolean,
        default: false
      },
      category: {
        type: String,
        index: true
      },
      attributes: [
        {
          name: String,
          value: String
        }
      ],
      images: [String]
    },
    detail: {
      long_description: String,
      barcode: String,
      sku: String,
      product_brand: String
    },
    pricing: {
      discount_rate: Number,
      discount_price: Number,
      final_price: Number,
      min_price: Number,
      max_price: Number
    },
    stock: {
      qty: Number,
      min_qty: Number,
      unit: String
    },
    country: String,

    countryofOrigin: String,

    supplier: {
      _id: {
        type: SchemaTypes.ObjectId,
        ref: "supplier"
      },
      supplier: String
      // email1: String,
      // type: String
    },
    deliveryCost: String,
    international: {
      type: Boolean,
      default: false
    },
    stock_avilability: {
      type: Boolean,
      default: true
    }
  },
  { timestamps: true, minimize: false }
);
productSchema.plugin(mongoosePaginate);
export default productSchema;

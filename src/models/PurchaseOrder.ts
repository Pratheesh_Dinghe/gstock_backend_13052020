import { SchemaTypes, Document, Schema, Types } from "mongoose";
import * as mongoosePaginate from "mongoose-paginate";
import { totalmem } from "os";

export interface PurchaseOrderModel extends Document {
    orderNo: string,
    doNo: number,
    orderDate: Date,
    doDate: Date,
    supplier: string,
    invNo: string,
    invDate: Date,
    subTotal: number,
    gst: number,
    totalCost: number,
    item: [{
        code: string,
        name: string,
        qty: number,
        cost: number,
        unit: string,
        discount: number,
        totalCost: number,
        status: {
            type: String,
            default: "active"
        },
        delivered: {
            type: number,
            default: 0
        }
    }],
    status: {
        type: string;
        default: "active";
    },
    po_type: string,
    supplier_type: string;
    approve_status:{
        type:string,
        default:"not-approved"
    },
    email_status:{
        type:string,
        default:"not_sent"
    }
}

const purchaseOrderSchema = new Schema({
    orderNo: String,
    doNo: Number,
    orderDate: Date,
    doDate: Date,
    supplier: String,
    invNo: String,
    invDate: Date,
    subTotal: Number,
    gst: Number,
    totalCost: Number,
    item: [{
        code: String,
        name: String,
        qty: Number,
        cost: Number,
        discount: Number,
        totalCost: Number,
        unit: String,
        status: {
            type: String,
            default: "active"
        },
        delivered: {
            type: Number,
            default: 0
        }
    }], status: {
        type: String,
        default: "active"
    },
    po_type: String,
    supplier_type: String,
    approve_status:{
        type:String,
        default:"not-approved"
    },
    email_status:{
        type:String,
        default:"not_sent"
    }
},
    { timestamps: true, minimize: false }
);
purchaseOrderSchema.plugin(mongoosePaginate);
export default purchaseOrderSchema;

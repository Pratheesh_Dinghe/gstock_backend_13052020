import { SchemaTypes, Document, Schema, Types } from "mongoose";
import * as mongoosePaginate from "mongoose-paginate";
import { NumericDictionary } from "lodash";
export interface SalesPromotionModel extends Document {
    promo_name: String;
    validity_from: Date;
    validity_to: Date;
    desc: String;

    item: [{
        promo_price: Number,
        code: String,
        name: String,
        unit: String,
        qty: Number,
        criteria: Number,
        link: String,
        price: Number,
        stock: Number,

    }]; status: {
        type: String,
        default: "active"
    };



}
const salesPromotionSchema = new Schema(
    {
        promo_name: String,
        validity_from: Date,
        validity_to: Date,
        desc   : String,

        item: [{
            price: Number,
            code: String,
            name: String,
            unit: String,
            qty: Number,
            criteria: Number,
            link: String,
            promo_price: Number,
            stock: Number,

        }], status: {
            type: String,
            default: "active"
        },

    });

    export default salesPromotionSchema;
import { SchemaTypes, Document, Schema, Types } from "mongoose";
import * as mongoosePaginate from "mongoose-paginate";
import { NumericDictionary } from "lodash";
export interface OrderApprovalModel extends Document {

    order_no: string;
    order_date: Date;
    buyer: string;
    buyer_id:Types.ObjectId;
    amount:number;
    contact:number;
    email:string;
    date:Date;
    billing_address:string;
    remarks:string;
    status:string;
}
const orderApprovalSchema = new Schema(
    {
        order_no: String,
        order_date   : Date,
        buyer: String,
        buyer_id:SchemaTypes.ObjectId,
        amount:Number,
        contact:Number,
        email:String,
        date:Date,
        billing_address:String,
        remarks:String,
        status:String


       
    });

    export default orderApprovalSchema;
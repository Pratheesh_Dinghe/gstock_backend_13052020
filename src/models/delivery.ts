import { Schema, Types, Document, Error, SchemaTypes } from "mongoose";

export interface DeliveryModel extends Document {
  //  deli_slno:Number;
    deli_no: String;
    deli_date:Date;
    deli_method: String;
    deli_desc: String;
    deli_status:String;

   

}

const deliverySchema = new Schema(
    {   
       // deli_slno:Number,
        deli_no: String,
        deli_date:Date,
        deli_method: String,
        deli_desc: String,
        deli_status:String,
        deli_tatus: {
            type: String,
            index: true,
            default: "Active"
        },
    },
   );



export default deliverySchema;

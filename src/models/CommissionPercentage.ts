import { Schema, Types, Document, Error, SchemaTypes } from "mongoose";
export interface CommissionPercentageModel extends Document {
    
    l1: Schema.Types.Number,
    l2: Schema.Types.Number,
    l3: Schema.Types.Number,
    l4: Schema.Types.Number,
    l5: Schema.Types.Number

}
const commissionPercentageSchema = new Schema(
{
    l1: Schema.Types.Number,
    l2: Schema.Types.Number,
    l3: Schema.Types.Number,
    l4: Schema.Types.Number,
    l5: Schema.Types.Number
}
   ,
   {
        timestamps: true,
        minimize: false
   });
    export default commissionPercentageSchema;




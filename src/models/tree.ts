import { Schema, Types, Document, Error, SchemaTypes } from "mongoose";
export interface TreeModel extends Document {
    children: [{type: Schema.Types.ObjectId, ref: "Node"}];
    name: string ;
}
const treeSchema = new Schema(
{
    children: [{type: Schema.Types.ObjectId, ref: "Node"}],
    name: String
}
   ,
   {
        timestamps: true,
        minimize: false
   });
    export default treeSchema;


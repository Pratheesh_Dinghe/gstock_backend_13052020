import { SchemaTypes, Document, Schema, Types } from "mongoose";
import * as mongoosePaginate from "mongoose-paginate";
import { NumericDictionary } from "lodash";
export interface MonthlyCommissionModel extends Document {
  updatedAt: Date;
  createdAt: Date;
  Root: Types.ObjectId;
  Level: String;
  From: Date;
  To: Date;
  ParentBuyer: Types.ObjectId;
  ChildBuyer: Types.ObjectId;
  SalesAmount: Number;
  status: String;
}
const monthlyCommissionSchema = new Schema({
  updatedAt: Date,
  createdAt: Date,
  Root: { type: SchemaTypes.ObjectId, ref: "User" },
  Level: String,
  From: Date,
  To: Date,
  ParentBuyer: { type: SchemaTypes.ObjectId, ref: "User" },
  ChildBuyer: SchemaTypes.ObjectId,
  SalesAmount: Number,
  status: {
    type: String,
    default: "active"
  }
});
export default monthlyCommissionSchema;

import { SchemaTypes, Document, Schema, Types } from "mongoose";
import * as mongoosePaginate from "mongoose-paginate";
import { NumericDictionary } from "lodash";
export interface StockManagingModel extends Document {

    product_id: [Types.ObjectId];
    product_name: string;
    product_code: string;
    status: boolean;
    supplier: [Types.ObjectId];
    supplier_name: string;

}
const StockManagingSchema = new Schema(
    {
        product_id: [{
            type: SchemaTypes.ObjectId,
            ref: "product"
        }],
        status: Boolean,
        supplier: [{
            type: SchemaTypes.ObjectId,
            ref: "supplier"
        }],
        product_name: String,
        product_code: String,
        supplier_name: String

    }, { timestamps: true, minimize: false });


export default StockManagingSchema;
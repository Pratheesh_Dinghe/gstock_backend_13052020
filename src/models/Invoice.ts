import { SchemaTypes, Document, Schema, Types } from "mongoose";
import * as mongoosePaginate from "mongoose-paginate";
import { NumericDictionary } from "lodash";
export interface InvoiceModel extends Document {
    invoice_no:string;
    invoice_date: Date;
    name:string;
    address:string,
    city:string;
    phone:number;
    item:[{
        amount: number;
        code: string;
        discount:number;
        name:string;
        price:number;
        qty:number;
        slNo:number;
    }],
    total:number;
    gst:number;
    gst_perc:number;
    discount:number;
    net_total :number;
    status :string

}

    const invoiceSchema = new Schema(
        {
            invoice_no: String,
            invoice_date: Date,
            name: String,
            address: String,
            city: String,
            phone: Number,
            item:[{
                amount: Number,
                code:String,
                discount:Number,
                name:String,
                price:Number,
                qty:Number,
                slNo:Number
            }],
            total:Number,
            gst_perc:Number,
            gst:Number,
            discount:Number,
            net_total :Number,
            status: String,
        });
    
    
export default invoiceSchema;
    
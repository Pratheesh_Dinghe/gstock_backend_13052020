import { SchemaTypes, Document, Schema, Types } from "mongoose";
import * as mongoosePaginate from "mongoose-paginate";
import { NumericDictionary } from "lodash";
export interface ReturnModel extends Document {

    return_no: string;
    return_date: Date;
    invoice_date: Date;
    Customer: string;
    invoice_no: string;

    item: [{
        amount: number;
        code: string;
        discount: number;
        name: string;
        price: number;
        _rtn_amount:number;
        qty: number;
        //  slNo:Number,
        _rtn_qty: number;
        _id: string;
    }],
    total: number;
    gst: number;
    gst_perc:number;
    net_total: number;
    status: string;

}
const returnSchema = new Schema(
    {
        return_no: String,
        return_date: Date,
        invoice_date: Date,
        Customer: String,
        invoice_no: String,

        item: [{
            amount: Number,
            code: String,
            discount: Number,
            name: String,
            price: Number,
            qty: Number,
            _rtn_amount:Number,
            //  slNo:Number,
            _id: String,
            _rtn_qty: Number,




        }],
        total: Number,
        gst: Number,
        net_total: Number,
        gst_perc:Number,
        status: String,
    });


export default returnSchema;

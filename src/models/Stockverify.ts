import { SchemaTypes, Document, Schema, Types } from "mongoose";
import * as mongoosePaginate from "mongoose-paginate";
import { NumericDictionary } from "lodash";
export interface StockverifyModel extends Document {
        verfNo:string;
        brand: string;
        code: string;
        name: string;
        stock: number;
        Discrepancy: number;
        updted_stk:number;   
        verifydate:Date;

}
const stockverifySchema = new Schema({
    verfNo:String,
    brand: String,
    code: String,
    name: String,
    stock: Number,
    Discrepancy: Number,
    updted_stk:Number,
    verifydate:Date

}, { timestamps: true, minimize: false });
stockverifySchema.plugin(mongoosePaginate);
    
export default stockverifySchema;
    
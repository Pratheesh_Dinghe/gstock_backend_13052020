import { SchemaTypes, Document, Schema, Types } from "mongoose";
import * as mongoosePaginate from "mongoose-paginate";
import { NumericDictionary } from "lodash";
export interface HotItemModel extends Document {
    // itemCode:string;
    // itemName:string;
    item: [{
        name: string;
        code: string;
        unit: string
        Price: number
        brand: string
        images: Array<string>;
        product_id: Types.ObjectId;


    }];
}
const hotitemSchema = new Schema(
    {
        // itemCode:String,
        // itemName:String,
        item: [{
            name: String,
            code: String,
            unit: String,
            Price: Number,
            brand: String,
            images: [String],
            product_id: SchemaTypes.ObjectId,


        }],
    });


export default hotitemSchema;
import { Schema, Types, Document, Error, SchemaTypes } from "mongoose";
import * as bcrypt from "bcrypt-nodejs";
import * as crypto from "crypto";
interface PayPalSuccess {
  intent: string;
  payerID: string;
  paymentID: string;
  paymentToken: string;
}
export interface TopupModel extends Document {

    paypal: PayPalSuccess;
    amount: String;
  }

  const topupSchema = new Schema(
    {

      paypal: {},
      amount: String,
    },
    {
      timestamps: true,
      minimize: false
    }
  );

  export default topupSchema;

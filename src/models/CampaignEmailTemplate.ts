import { SchemaTypes, Document, Schema, Types } from "mongoose";

export interface CampaignEmailTemplateModel extends Document {
    title: string;
    name: string;
    subject: string;
    header_image: string;
    body: string;
    footer: string;
    last_sent_at: Date;
}

const campaignEmailTemplateSchema = new Schema({
    title: String,
    name: String,
    subject: String,
    header_image: String,
    body: String,
    footer: String,
    last_sent_at: Date
}, {
    timestamps: true,
    minimize: false
});


export default campaignEmailTemplateSchema;
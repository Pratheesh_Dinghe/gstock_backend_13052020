import { Schema, Types, Document, Error, SchemaTypes } from "mongoose";

export interface DeliveryAddressModel extends Document {
  buyer_id: Types.ObjectId;
  S_location: string;
  S_unitno: string;
  S_postalcode: string;
  S_shipto: string;
  S_contactnumber: string;
  B_location: string;
  B_unitno: string;
  B_postalcode: string;
  B_shipto: string;
  B_contactnumber: string;
}

const DeliveryAddressSchema = new Schema({
  buyer_id: {
    type: SchemaTypes.ObjectId
  },
  S_location: String,
  S_unitno: String,
  S_postalcode: String,
  S_shipto: String,
  S_contactnumber: String,
  B_location: String,
  B_unitno: String,
  B_postalcode: String,
  B_shipto: String,
  B_contactnumber: String
});

export default DeliveryAddressSchema;

import * as mongoosePaginate from "mongoose-paginate";
import { Model, Document, Connection, Types, PaginateModel } from "mongoose";
import productAttributeSchema, {
  ProductAttributeModel
} from "./ProductAttribute";
import rewardPtsSchema, { RewardPtsModel } from "./RewardPts";
import variantSchema, { VariantModel } from "./Variants";
import adminSchema, { AdminModel } from "./Admin";
import userSchema, { UserModel } from "./User";
import buyerSchema, { BuyerModel } from "./Buyer";
import merchantSchema, { MerchantModel } from "./Merchant";
import productSchema, { ProductModel } from "./Product";
import categorySchema, { CategoryModel } from "./Category";
import cartSchema, { CartModel } from "./Cart";
import orderSchema, { OrderModel } from "./Order";
import commentSchema, { CommentModel } from "./Comment";
import promotionSchema, { PromotionModel } from "./Promotion";
import attributeSchema, { AttributeModel } from "./Attribute";
import pendingStockSchema, { PendingStockModel } from "./PendingStock";
import creditWalletSchema, { CreditWalletModel } from "./CreditWallet";
import complaintSchema, { ComplaintModel } from "./Complaint";
import rewardProductSchema, { RewardProductModel } from "./RewardProdcut";
import emailSchema, { EmailModel } from "./Email";
import countrySchema, { CountryModel } from "./Country";
import supplierSchema, { SupplierModel } from "./Supplier";
import deliveryProcessSchema, { DeliveryProcessModel } from "./DeliveryProcess";
import commissiontreeSchema, { CommissiontreeModel } from "./commissiontree";
import jobsSchema, { JobsModel } from "./jobs";

import treeSchema, { TreeModel } from "./tree";

// import countrySchema, { CountryModel } from './Country'
// import supplierSchema, { SupplierModel } from "./Supplier"
import invoiceSchema, { InvoiceModel } from "./Invoice";
import failedEmailsLogSchema, { FailedEmailsLogModel } from "./FailedEmailsLog";
import monthlyCommissionSchema, {
  MonthlyCommissionModel
} from "./monthlycommission";
import commissionPaidSchema, { commissionPaidModel } from "./CommissionPaid";
import purchaseOrderSchema, { PurchaseOrderModel } from "./PurchaseOrder";
import { DeliveryModel } from "./delivery";
import purchasereturnSchema, { PurchaseReturnModel } from "./Purchasereturn";
import imagesaveSchema, { ImageSaveModel } from "./Image";
// import purchasereturnSchema, { PurchaseReturnModel } from "./Purchasereturn";
import commissionPercentageSchema, {
  CommissionPercentageModel
} from "./CommissionPercentage";
import BuyerSalesReturnSchema, {
  BuyerSalesReturnModel
} from "./Buyersalesreturn";
import returnSchema, { ReturnModel } from "./Return";
import dailystockSchema, { DailystockModel } from "./dailystock";
import stockverifySchema, { StockverifyModel } from "./Stockverify";
import stockSchema, { StockModel } from "./Stock";
import DeliveryAddressSchema, { DeliveryAddressModel } from "./Deliveryaddress";
import contactSchema, { ContactModel } from "./Contact";

import goodsReceivedSchema, { GoodsReceivedModel } from "./GoodsReceived";
import refundSchema, { RefundModel } from "./Refund";
import orderApprovalSchema, { OrderApprovalModel } from "./OrderApproval";
import BuyerBankInfoSchema, { BuyerBankInfoModel } from "./BuyerBankInfo";
import WalletWithdrawalSchema, {
  WalletWithdrawalModel
} from "./WalletWithdrawal";
import creditWalletApprovalSchema, { CreditWalletApprovalModel } from "./CreditWalletApproval";
import StockManagingSchema, { StockManagingModel } from "./Stock_managing";
import updateWinnersSchema, { UpdateWinnersModel } from "./UpdateWinners";
import campaignEmailTemplateSchema, { CampaignEmailTemplateModel } from "./CampaignEmailTemplate";
import systemSchema, { SystemModel } from "./System";
import pickedListSchema, { PickedListModel } from "./PickedList";
import { PickedListController } from "../controllers";
import salesPromotionSchema, { SalesPromotionModel } from "./SalesPromotion";
import hotitemSchema, { HotItemModel } from "./HotItems";
export interface AdminModels {
  User: PaginateModel<UserModel>;
  Buyer: PaginateModel<BuyerModel>;
  Admin: Model<AdminModel>;
  Product: PaginateModel<ProductModel>;
  Category: Model<CategoryModel>;
  Order: Model<OrderModel>;
  Cart: Model<CartModel>;
  Promotion: Model<PromotionModel>;
  Attribute: Model<AttributeModel>;
  Variant: Model<VariantModel>;
  PendingStock: Model<PendingStockModel>;
  RewardPts: Model<RewardPtsModel>;
  ProductAttribute: Model<ProductAttributeModel>;
  Comment: Model<CommentModel>;
  CreditWallet: Model<CreditWalletModel>;
  Complaint: Model<ComplaintModel>;
  RewardProdcut: Model<RewardProductModel>;
  Email: Model<EmailModel>;
  Country: Model<CountryModel>;
  Supplier: Model<SupplierModel>;
  DeliveryProcess: Model<DeliveryProcessModel>;
  Commissiontree: Model<CommissiontreeModel>;
  tree: Model<TreeModel>;
  Invoice: Model<InvoiceModel>;
  FailedEmailsLog: Model<FailedEmailsLogModel>;
  MonthlyCommission: Model<MonthlyCommissionModel>;
  CommissionPaid: Model<commissionPaidModel>;
  PurchaseOrder: Model<PurchaseOrderModel>;
  PurchaseReturn: Model<PurchaseReturnModel>;
  GoodsReceived: Model<GoodsReceivedModel>;
  Return: Model<ReturnModel>;
  Contact: Model<ContactModel>;
  Dailystock: Model<DailystockModel>;
  StockManaging: Model<StockManagingModel>;
  Jobs: Model<JobsModel>;

  // Inventory:Model<InventoryModel>;
  Stock: Model<StockModel>;
  Stockverify: Model<StockverifyModel>;
  ImageSave: Model<ImageSaveModel>;
  CommissionPercentage: Model<CommissionPercentageModel>;
  BuyerSalesReturn: Model<BuyerSalesReturnModel>;
  Refund: Model<RefundModel>;
  OrderApproval: Model<OrderApprovalModel>;
  BuyerBankInfo: Model<BuyerBankInfoModel>;
  WalletWithdrawal: Model<WalletWithdrawalModel>;
  UpdateWinners: Model<UpdateWinnersModel>;
  CampaignEmailTemplates: Model<CampaignEmailTemplateModel>;
  System: Model<SystemModel>;
  // import commissionPercentageSchema, {
  //   CommissionPercentageModel
  // } from "./CommissionPercentage";
  // import BuyerSalesReturnSchema, {
  //   BuyerSalesReturnModel
  // } from "./Buyersalesreturn";
  PickedList: Model<PickedListModel>;
  SalesPromotion: Model<SalesPromotionModel>;
  HotItem: Model<HotItemModel>;
}

export interface AdminModels {
  User: PaginateModel<UserModel>;
  Buyer: PaginateModel<BuyerModel>;
  Admin: Model<AdminModel>;
  Product: PaginateModel<ProductModel>;
  Category: Model<CategoryModel>;
  Order: Model<OrderModel>;
  Cart: Model<CartModel>;
  Promotion: Model<PromotionModel>;
  Attribute: Model<AttributeModel>;
  Variant: Model<VariantModel>;
  PendingStock: Model<PendingStockModel>;
  RewardPts: Model<RewardPtsModel>;
  ProductAttribute: Model<ProductAttributeModel>;
  Comment: Model<CommentModel>;
  CreditWallet: Model<CreditWalletModel>;
  Complaint: Model<ComplaintModel>;
  RewardProdcut: Model<RewardProductModel>;
  Email: Model<EmailModel>;
  Country: Model<CountryModel>;
  Supplier: Model<SupplierModel>;
  DeliveryProcess: Model<DeliveryProcessModel>;
  Commissiontree: Model<CommissiontreeModel>;
  tree: Model<TreeModel>;
  Invoice: Model<InvoiceModel>;
  FailedEmailsLog: Model<FailedEmailsLogModel>;
  MonthlyCommission: Model<MonthlyCommissionModel>;
  CommissionPaid: Model<commissionPaidModel>;
  PurchaseOrder: Model<PurchaseOrderModel>;
  GoodsReceived: Model<GoodsReceivedModel>;
  CommissionPercentage: Model<CommissionPercentageModel>;
  BuyerSalesReturn: Model<BuyerSalesReturnModel>;
  DeliveryAddress: Model<DeliveryAddressModel>;
  Refund: Model<RefundModel>;
  OrderApproval: Model<OrderApprovalModel>;
  BuyerBankInfo: Model<BuyerBankInfoModel>;
  WalletWithdrawal: Model<WalletWithdrawalModel>;
  Contact: Model<ContactModel>;
  CreditWalletApproval: Model<CreditWalletApprovalModel>;
  StockManaging: Model<StockManagingModel>;
  UpdateWinners: Model<UpdateWinnersModel>;
  CampaignEmailTemplates: Model<CampaignEmailTemplateModel>;
  PickedList: Model<PickedListModel>;
  SalesPromotion: Model<SalesPromotionModel>;
  HotItem: Model<HotItemModel>;
}
export interface BuyerModels {
  User: PaginateModel<UserModel>;
  Buyer: PaginateModel<BuyerModel>;
  Product: PaginateModel<ProductModel>;
  Category: Model<CategoryModel>;
  Order: Model<OrderModel>;
  Cart: Model<CartModel>;
  Promotion: Model<PromotionModel>;
  Attribute: Model<AttributeModel>;
  Variant: Model<VariantModel>;
  RewardPts: Model<RewardPtsModel>;
  ProductAttribute: Model<ProductAttributeModel>;
  Comment: Model<CommentModel>;
  CreditWallet: Model<CreditWalletModel>;
  Complaint: Model<ComplaintModel>;
  RewardProdcut: Model<RewardProductModel>;
  Email: Model<EmailModel>;
  Country: Model<CountryModel>;
  DeliveryProcess: Model<DeliveryProcessModel>;
  FailedEmailsLog: Model<FailedEmailsLogModel>;
  Commissiontree: Model<CommissiontreeModel>;
  tree: Model<TreeModel>;
  Contact: Model<ContactModel>;
  Invoice: Model<InvoiceModel>;
  MonthlyCommission: Model<MonthlyCommissionModel>;
  CommissionPaid: Model<commissionPaidModel>;
  PurchaseOrder: Model<PurchaseOrderModel>;
  GoodsReceived: Model<GoodsReceivedModel>;
  PurchaseReturn: Model<PurchaseReturnModel>;
  ImageSave: Model<ImageSaveModel>;
  CommissionPercentage: Model<CommissionPercentageModel>;
  BuyerSalesReturn: Model<BuyerSalesReturnModel>;
  Return: Model<ReturnModel>;
  Dailystock: Model<DailystockModel>;
  Stock: Model<StockModel>;
  Stockverify: Model<StockverifyModel>;
  Refund: Model<RefundModel>;
  DeliveryAddress: Model<DeliveryAddressModel>;
  OrderApproval: Model<OrderApprovalModel>;
  BuyerBankInfo: Model<BuyerBankInfoModel>;
  WalletWithdrawal: Model<WalletWithdrawalModel>;
  CreditWalletApproval: Model<CreditWalletApprovalModel>;
  StockManaging: Model<StockManagingModel>;
  UpdateWinners: Model<UpdateWinnersModel>;
  System: Model<SystemModel>;
  PickedList: Model<PickedListModel>;
  SalesPromotion: Model<SalesPromotionModel>;
  HotItem: Model<HotItemModel>;
}
export interface MerchantModels {
  User: PaginateModel<UserModel>;
  Product: PaginateModel<ProductModel>;
  Category: Model<CategoryModel>;
  Order: Model<OrderModel>;
  Attribute: Model<AttributeModel>;
  Variant: Model<VariantModel>;
  PendingStock: Model<PendingStockModel>;
  ProductAttribute: Model<ProductAttributeModel>;
  Comment: Model<CommentModel>;
  Contact: Model<ContactModel>;
  Complaint: Model<ComplaintModel>;
  Email: Model<EmailModel>;
  Country: Model<CountryModel>;
  DeliveryProcess: Model<DeliveryProcessModel>;
  FailedEmailsLog: Model<FailedEmailsLogModel>;
  tree: Model<TreeModel>;
  Invoice: Model<InvoiceModel>;
  MonthlyCommission: Model<MonthlyCommissionModel>;
  CommissionPaid: Model<commissionPaidModel>;
  PurchaseOrder: Model<PurchaseOrderModel>;
  GoodsReceived: Model<GoodsReceivedModel>;
  PurchaseReturn: Model<PurchaseReturnModel>;
  ImageSave: Model<ImageSaveModel>;
  CommissionPercentage: Model<CommissionPercentageModel>;
  BuyerSalesReturn: Model<BuyerSalesReturnModel>;
  Return: Model<ReturnModel>;
  Dailystock: Model<DailystockModel>;
  Stock: Model<StockModel>;
  Stockverify: Model<StockverifyModel>;
  Refund: Model<RefundModel>;
  DeliveryAddress: Model<DeliveryAddressModel>;
  OrderApproval: Model<OrderApprovalModel>;
  BuyerBankInfo: Model<BuyerBankInfoModel>;
  WalletWithdrawal: Model<WalletWithdrawalModel>;
  CreditWalletApproval: Model<CreditWalletApprovalModel>;
  StockManaging: Model<StockManagingModel>;
  UpdateWinners: Model<UpdateWinnersModel>;
  System: Model<SystemModel>;
  PickedList: Model<PickedListModel>;
  SalesPromotion: Model<SalesPromotionModel>;
  HotItem: Model<HotItemModel>;
}
export class Models2 {
  constructor(
    private role: "admin" | "buyer" | "merchant",
    private connection: Connection
  ) { }
  admin(): AdminModels {
    return {
      User: this.connection.model("User", userSchema) as PaginateModel<
        UserModel
      >,
      Buyer: this.connection.model("Buyer", buyerSchema) as PaginateModel<
        BuyerModel
      >,
      Admin: this.connection.model("Admin", adminSchema),
      Cart: this.connection.model("Cart", cartSchema),
      RewardPts: this.connection.model("RewardPts", rewardPtsSchema),
      CreditWallet: this.connection.model("CreditWallet", creditWalletSchema),
      Product: this.connection.model("Product", productSchema) as PaginateModel<
        ProductModel
      >,
      Category: this.connection.model("Category", categorySchema),
      Promotion: this.connection.model("Promotion", promotionSchema),
      Attribute: this.connection.model("Attribute", attributeSchema),
      Variant: this.connection.model("Variant", variantSchema),
      PendingStock: this.connection.model("PendingStock", pendingStockSchema),
      ProductAttribute: this.connection.model(
        "ProductAttribute",
        productAttributeSchema
      ),
      Comment: this.connection.model("Comment", commentSchema),
      RewardProdcut: this.connection.model(
        "RewardProduct",
        rewardProductSchema
      ),
      Email: this.connection.model<EmailModel>("Email", emailSchema),
      Order: this.connection.model("Order", orderSchema),
      Complaint: this.connection.model("Complaint", complaintSchema),
      Country: this.connection.model("Country", countrySchema),
      Supplier: this.connection.model("supplier", supplierSchema),

      Return: this.connection.model("return", returnSchema),
      Contact: this.connection.model("contact", contactSchema),
      Dailystock: this.connection.model("dailystock", dailystockSchema),
      Stockverify: this.connection.model("stockverify", stockverifySchema),
      Stock: this.connection.model("stock", stockSchema),
      PurchaseReturn: this.connection.model("purchaseReturn", purchasereturnSchema),
      ImageSave: this.connection.model("imageSave", imagesaveSchema),
      // Dailystock: this.connection.model("dailystock", dailystockSchema),
      // Stockverify: this.connection.model("stockverify", stockverifySchema),
      // Stock: this.connection.model("stock", stockSchema),
      // PurchaseReturn: this.connection.model("purchaseReturn", purchasereturnSchema),
      // ImageSave: this.connection.model("imageSave", imagesaveSchema),
      Jobs: this.connection.model("Jobs", jobsSchema),
      DeliveryProcess: this.connection.model(
        "deliveryProcess",
        deliveryProcessSchema
      ),
      FailedEmailsLog: this.connection.model(
        "failedEmailsLog",
        failedEmailsLogSchema
      ),
      Commissiontree: this.connection.model(
        "Commissiontree",
        commissiontreeSchema
      ),
      tree: this.connection.model("tree", treeSchema),
      PurchaseOrder: this.connection.model(
        "purchaseOrder",
        purchaseOrderSchema
      ),
      // Country: this.connection.model('Country', countrySchema),,
      Invoice: this.connection.model("invoice", invoiceSchema),
      MonthlyCommission: this.connection.model(
        "monthlyCommission",
        monthlyCommissionSchema
      ),
      CommissionPaid: this.connection.model(
        "commissionPaid",
        commissionPaidSchema
      ),
      CommissionPercentage: this.connection.model(
        "commissionPercentage",
        commissionPercentageSchema
      ),
      BuyerSalesReturn: this.connection.model(
        "BuyerSalesReturn",
        BuyerSalesReturnSchema
      ),
      DeliveryAddress: this.connection.model(
        "DeliveryAddress",
        DeliveryAddressSchema
      ),
      GoodsReceived: this.connection.model(
        "GoodsReceived ",
        goodsReceivedSchema
      ),
      Refund: this.connection.model(
        "Refund",
        refundSchema
      ),
      OrderApproval: this.connection.model(
        "OrderApproval",
        orderApprovalSchema
      ),
      BuyerBankInfo: this.connection.model(
        "BuyerBankInfo ",
        BuyerBankInfoSchema
      ),
      WalletWithdrawal: this.connection.model(
        "WalletWithdrawal ",
        WalletWithdrawalSchema
      ),
      CreditWalletApproval: this.connection.model(
        "CreditWalletApproval",
        creditWalletApprovalSchema
      ),
      StockManaging: this.connection.model("StockManaging", StockManagingSchema),
      UpdateWinners: this.connection.model("UpdateWinners", updateWinnersSchema),
      CampaignEmailTemplates: this.connection.model(
        "CampaignEmailTemplates",
        campaignEmailTemplateSchema,
       ),
      System: this.connection.model("System", systemSchema),
      PickedList: this.connection.model("PickedList", pickedListSchema),
      SalesPromotion: this.connection.model("Sales Promotion", salesPromotionSchema),
      HotItem: this.connection.model("hotitem", hotitemSchema)
    };
 }
  buyer(): BuyerModels {
    return {
      User: this.connection.model("User", userSchema) as PaginateModel<
        UserModel
      >,
      Buyer: this.connection.model("Buyer", buyerSchema) as PaginateModel<
        BuyerModel
      >,
      Cart: this.connection.model("Cart", cartSchema),
      RewardPts: this.connection.model("RewardPts", rewardPtsSchema),
      CreditWallet: this.connection.model("CreditWallet", creditWalletSchema),
      Product: this.connection.model("Product", productSchema) as PaginateModel<
        ProductModel
      >,
      Category: this.connection.model("Category", categorySchema),
      Promotion: this.connection.model("Promotion", promotionSchema),
      Attribute: this.connection.model("Attribute", attributeSchema),
      ProductAttribute: this.connection.model(
        "ProductAttribute",
        productAttributeSchema
      ),
      Variant: this.connection.model("Variant", variantSchema),
      Comment: this.connection.model("Comment", commentSchema),
      RewardProdcut: this.connection.model(
        "RewardProduct",
        rewardProductSchema
      ),
      Contact: this.connection.model("contact", contactSchema),


      Return: this.connection.model("return", returnSchema),
      Dailystock: this.connection.model("dailystock", dailystockSchema),
      Stockverify: this.connection.model("stockverify", stockverifySchema),
      Stock: this.connection.model("stock", stockSchema),
      PurchaseReturn: this.connection.model("purchaseReturn", purchasereturnSchema),
      ImageSave: this.connection.model("imageSave", imagesaveSchema),


      Email: this.connection.model<EmailModel>("Email", emailSchema),
      Order: this.connection.model("Order", orderSchema),
      Complaint: this.connection.model("Complaint", complaintSchema),
      Country: this.connection.model("Country", countrySchema),
      DeliveryProcess: this.connection.model(
        "deliveryProcess",
        deliveryProcessSchema
      ),
      FailedEmailsLog: this.connection.model(
        "failedEmailsLog",
        failedEmailsLogSchema
      ),
      Commissiontree: this.connection.model(
        "Commissiontree",
        commissiontreeSchema
      ),
      tree: this.connection.model("tree", treeSchema),
      PurchaseOrder: this.connection.model(
        "purchaseOrder",
        purchaseOrderSchema
      ),
      // Country: this.connection.model('Country', countrySchema),
      Invoice: this.connection.model("invoice", invoiceSchema),
      MonthlyCommission: this.connection.model(
        "monthlyCommission",
        monthlyCommissionSchema
      ),
      CommissionPaid: this.connection.model(
        "commissionPaid",
        commissionPaidSchema
      ),
      CommissionPercentage: this.connection.model(
        "commissionPercentage",
        commissionPercentageSchema
      ),
      BuyerSalesReturn: this.connection.model(
        "BuyerSalesReturn",
        BuyerSalesReturnSchema
      ),
      DeliveryAddress: this.connection.model(
        "DeliveryAddress",
        DeliveryAddressSchema
      ),
      GoodsReceived: this.connection.model(
        "GoodsReceived ",
        goodsReceivedSchema
      ),
      Refund: this.connection.model(
        "Refund",
        refundSchema
      ),
      OrderApproval: this.connection.model(
        "OrderApproval",
        orderApprovalSchema
      ),
      BuyerBankInfo: this.connection.model(
        "BuyerBankInfo ",
        BuyerBankInfoSchema
      ),
      WalletWithdrawal: this.connection.model(
        "WalletWithdrawal ",
        WalletWithdrawalSchema
      ),
      CreditWalletApproval: this.connection.model(
        "CreditWalletApproval",
        creditWalletApprovalSchema
      ),
      StockManaging: this.connection.model("StockManaging", StockManagingSchema),
      UpdateWinners: this.connection.model("UpdateWinners", updateWinnersSchema),
      System: this.connection.model("System", systemSchema),
      PickedList: this.connection.model("PickedList", pickedListSchema),
      SalesPromotion: this.connection.model("Sales Promotion", salesPromotionSchema),
      HotItem: this.connection.model("hotitem", hotitemSchema),
    };
  }
  merchant(): MerchantModels {
    return {
      User: this.connection.model("User", userSchema) as PaginateModel<
        UserModel
      >,
      Product: this.connection.model("Product", productSchema) as PaginateModel<
        ProductModel
      >,
      Category: this.connection.model("Category", categorySchema),
      Attribute: this.connection.model("Attribute", attributeSchema),
      Variant: this.connection.model("Variant", variantSchema),
      Comment: this.connection.model("Comment", commentSchema),
      Email: this.connection.model<EmailModel>("Email", emailSchema),
      ProductAttribute: this.connection.model(
        "ProductAttribute",
        productAttributeSchema
      ),
      Contact: this.connection.model("contact", contactSchema),
      Order: this.connection.model("Order", orderSchema),
      PendingStock: this.connection.model("PendingStock", pendingStockSchema),
      Complaint: this.connection.model("Complaint", complaintSchema),
      Country: this.connection.model("Country", countrySchema),
      DeliveryProcess: this.connection.model(
        "deliveryProcess",
        deliveryProcessSchema
      ),
      FailedEmailsLog: this.connection.model(
        "failedEmailsLog",
        failedEmailsLogSchema
      ),
      tree: this.connection.model("tree", treeSchema),
      PurchaseOrder: this.connection.model(
        "purchaseOrder",
        purchaseOrderSchema
      ),
      /******* */

      Return: this.connection.model("return", returnSchema),
      Dailystock: this.connection.model("dailystock", dailystockSchema),
      Stockverify: this.connection.model("stockverify", stockverifySchema),
      Stock: this.connection.model("stock", stockSchema),
      PurchaseReturn: this.connection.model(
        "purchaseReturn",
        purchasereturnSchema
      ),
      ImageSave: this.connection.model("imageSave", imagesaveSchema),

      /** */
      // Country: this.connection.model('Country', countrySchema),
      Invoice: this.connection.model("invoice", invoiceSchema),
      MonthlyCommission: this.connection.model(
        "monthlyCommission",
        monthlyCommissionSchema
      ),
      CommissionPaid: this.connection.model(
        "commissionPaid",
        commissionPaidSchema
      ),
      CommissionPercentage: this.connection.model(
        "commissionPercentage",
        commissionPercentageSchema
      ),
      BuyerSalesReturn: this.connection.model(
        "BuyerSalesReturn",
        BuyerSalesReturnSchema
      ),
      DeliveryAddress: this.connection.model(
        "DeliveryAddress",
        DeliveryAddressSchema
      ),
      GoodsReceived: this.connection.model(
        "GoodsReceived ",
        goodsReceivedSchema
      ),
      Refund: this.connection.model(
        "Refund",
        refundSchema
      ),
      OrderApproval: this.connection.model(
        "OrderApproval",
        orderApprovalSchema
      ),
      BuyerBankInfo: this.connection.model(
        "BuyerBankInfo ",
        BuyerBankInfoSchema
      ),
      WalletWithdrawal: this.connection.model(
        "WalletWithdrawal ",
        WalletWithdrawalSchema
      ),
      CreditWalletApproval: this.connection.model(
        "CreditWalletApproval",
        creditWalletApprovalSchema
      ),
       StockManaging: this.connection.model("StockManaging", StockManagingSchema),
       UpdateWinners: this.connection.model("UpdateWinners", updateWinnersSchema),
       System: this.connection.model("System", systemSchema),
       PickedList: this.connection.model("PickedList", pickedListSchema),
       SalesPromotion: this.connection.model("Sales Promotion", salesPromotionSchema),
       HotItem: this.connection.model("hotitem", hotitemSchema),
    };
  }
  models(): AdminModels | BuyerModels | MerchantModels {
    return {
      admin: this.admin(),
      merchant: this.merchant(),
      buyer: this.buyer()
    }[this.role];
  }
}
interface DBS {
  userDB: Connection;
  productDB: Connection;
  orderDB: Connection;
  role: string;
}
const count = 0;
export interface Models {
    User: PaginateModel<UserModel>;
    Buyer: PaginateModel<BuyerModel>;
    Admin: Model<AdminModel>;
    Product: PaginateModel<ProductModel>;
    Category: Model<CategoryModel>;
    Country: Model<CountryModel>;
    Order: Model<OrderModel>;
    Cart: Model<CartModel>;
    Promotion: Model<PromotionModel>;
    Attribute: Model<AttributeModel>;
    Variant: Model<VariantModel>;
    PendingStock: Model<PendingStockModel>;
    RewardPts: Model<RewardPtsModel>;
    ProductAttribute: Model<ProductAttributeModel>;
    Comment: Model<CommentModel>;
    CreditWallet: Model<CreditWalletModel>;
    Complaint: Model<ComplaintModel>;
    RewardProdcut: Model<RewardProductModel>;
    Email: Model<EmailModel>;
    Contact: Model<ContactModel>;
    FailedEmailsLog: Model<FailedEmailsLogModel>;
    DeliveryProcess: Model<DeliveryProcessModel>;
    Supplier: Model<SupplierModel>;
    Commissiontree: Model<CommissiontreeModel>;
    tree: Model<TreeModel>;
    PurchaseOrder: Model<PurchaseOrderModel>;
    GoodsReceived: Model<GoodsReceivedModel>;
    // Supplier:Model<SupplierModel>;
    Invoice: Model<InvoiceModel>;
    MonthlyCommission: Model<MonthlyCommissionModel>;
    CommissionPaid: Model<commissionPaidModel>;
    CommissionPercentage: Model<CommissionPercentageModel>;
    BuyerSalesReturn: Model<BuyerSalesReturnModel>;
    PurchaseReturn: Model<PurchaseReturnModel>;
    ImageSave: Model<ImageSaveModel>;
    Dailystock: Model<DailystockModel>;
    Stock: Model<StockModel>;
    Stockverify: Model<StockverifyModel>;
    Return: Model<ReturnModel>;
    DeliveryAddress: Model<DeliveryAddressModel>;
    Refund: Model<RefundModel>;
    OrderApproval: Model<OrderApprovalModel>;
    BuyerBankInfo: Model<BuyerBankInfoModel>;
    WalletWithdrawal: Model<WalletWithdrawalModel>;
    CreditWalletApproval: Model<CreditWalletApprovalModel>;
    StockManaging: Model<StockManagingModel>;
    UpdateWinners: Model<UpdateWinnersModel>;
    CampaignEmailTemplates: Model<CampaignEmailTemplateModel>;
    System: Model<SystemModel>;
    PickedList: Model<PickedListModel>;
    SalesPromotion: Model<SalesPromotionModel>;
    HotItem: Model<HotItemModel>;
  }


export default (dbs: DBS): Models => {
  const { userDB, productDB, orderDB, role } = dbs;

    return {
        User: userDB.model("User", userSchema) as PaginateModel<UserModel>,
        Buyer: userDB.model("Buyer", buyerSchema) as PaginateModel<BuyerModel>,
        Admin: userDB.model("Admin", adminSchema),
        Cart: userDB.model("Cart", cartSchema),
        RewardPts: userDB.model("RewardPts", rewardPtsSchema),
        CreditWallet: userDB.model("CreditWallet", creditWalletSchema),
        Product: productDB.model("Product", productSchema) as PaginateModel<ProductModel>,
        Category: productDB.model("Category", categorySchema),
        Country: productDB.model("Country", countrySchema),
        Promotion: productDB.model("Promotion", promotionSchema),
        Attribute: productDB.model("Attribute", attributeSchema),
        Variant: productDB.model("Variant", variantSchema),
        PendingStock: productDB.model("PendingStock", pendingStockSchema),
        ProductAttribute: productDB.model("ProductAttribute", productAttributeSchema),
        Comment: productDB.model("Comment", commentSchema),
        RewardProdcut: productDB.model("RewardProduct", rewardProductSchema),
        Email: orderDB.model<EmailModel>("Email", emailSchema),
        Order: orderDB.model("Order", orderSchema),
        Complaint: orderDB.model("Complaint", complaintSchema),
        Supplier: userDB.model("supplier", supplierSchema),
        DeliveryProcess: userDB.model("deliveryProcess", deliveryProcessSchema),
        Commissiontree: userDB.model("Commissiontree", commissiontreeSchema),
        tree: userDB.model("tree", treeSchema),
        Invoice: userDB.model("invoice", invoiceSchema),
        Return: userDB.model("return", returnSchema),
        Dailystock: userDB.model("dailystock", dailystockSchema),
     //   Inventory:userDB.model("inventory",inventorySchema),
        Stockverify: userDB.model("stockverify", stockverifySchema),
      Stock: userDB.model("stock", stockSchema),
        PurchaseOrder: userDB.model("purchaseOrder", purchaseOrderSchema),
        FailedEmailsLog: userDB.model("failedEmailsLog", failedEmailsLogSchema),
        MonthlyCommission: userDB.model("monthlyCommission", monthlyCommissionSchema),
        CommissionPaid: userDB.model("commissionPaid", commissionPaidSchema),
        PurchaseReturn: userDB.model("purchaseReturn", purchasereturnSchema),
        ImageSave: userDB.model("imageSave", imagesaveSchema),
        CommissionPercentage: userDB.model("commissionPercentage", commissionPercentageSchema),
        BuyerSalesReturn: userDB.model("BuyerSalesReturn", BuyerSalesReturnSchema),
        DeliveryAddress: userDB.model("DeliveryAddress", DeliveryAddressSchema),
        GoodsReceived: userDB.model("goodsReceived", goodsReceivedSchema),
        Refund: userDB.model("refund", refundSchema),
        OrderApproval: userDB.model("orderApproval", orderApprovalSchema),
        Contact: userDB.model("contact", contactSchema),
        BuyerBankInfo: userDB.model("BuyerBankInfo", BuyerBankInfoSchema),
        WalletWithdrawal: userDB.model("WalletWithdrawal", WalletWithdrawalSchema),
        StockManaging: userDB.model("StockManaging", StockManagingSchema),
        CreditWalletApproval: userDB.model("CreditWalletApproval", creditWalletApprovalSchema),
        UpdateWinners: userDB.model("UpdateWinners", updateWinnersSchema),
        CampaignEmailTemplates: userDB.model("CampaignEmailTemplates", campaignEmailTemplateSchema),
        System: userDB.model("System", systemSchema),
        PickedList: userDB.model("PickedList", pickedListSchema),
        SalesPromotion: userDB.model("Sales Promotion", salesPromotionSchema),
        HotItem: userDB.model("hotitem", hotitemSchema),
    };
};

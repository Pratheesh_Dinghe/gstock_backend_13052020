import { Document, Schema, SchemaTypes, Types } from "mongoose";
interface PayPalSuccess {
  intent: string;
  payerID: string;
  paymentID: string;
  paymentToken: string;
}
export interface OrderModel extends Document {
  OrderNo: string;
  SequenceNo: Number;
  buyer_id: Types.ObjectId;
  merchant_id: Types.ObjectId;
  memo: string;
  status: string;
  commission_status: string;
  payment_method: string;
  delivery: {
    S_address: string;
    S_unit_no: string;
    S_postal_code: string;
    shipping_fee: number;
    shipping_type: string;
    S_contact_no: string;
    S_recepient: string;
    shipment_id: string;
    Shipping_date: Date;
    Shipping_time: String;
    B_address: string;
    B_unit_no: string;
    B_postal_code: string;
    B_contact_no: string;
    B_recepient: string;
  };
  promotions: [
    {
      _id: Types.ObjectId;
    }
  ];
  products: [
    {
      _id: Types.ObjectId;
      comment_id: Types.ObjectId;
      purchase: {
        product_total_ap: number;
        product_total_bp: number;
        commission: number;
        commission_amount: number;
        reward_pts: number;
        creditwallet_amnt: number;
      };
      product: {
        _id: Types.ObjectId;
        brief: {
          name: string;
          short_description: string;
          price: number;
          cost: number;
          stock: number;
          category: string;
          images: Array<string>;
          discount: boolean;
          code: String;
        };
        pricing: {
          discount_rate: Number;
        };
      };
      variants: [
        {
          _id: Types.ObjectId;
          sku: string;
          stock: number;
          price: number;
          option_name: string;
          option_value: string;
          status: string;
          order_qty: number;
          commission: number;
          reward_pts: number;
        }
      ];
    }
  ];
  paypal: PayPalSuccess;
  total: {
    store: number;
    commission: number;
    reward_pts: number;
    reward_rate: number;
    creditwallet_amnt: Number;
  };
  orderDate: Date;
  orderStatus: string;
  purchasedFrom: string;
  placedFromIP: string;
  customerName: string;
  email: string;
  customerGroup: string;
  prefix: string;
  billingAdd: string;
  giftOpt: {
    from: string;
    to: string;
    msg: string;
  };
  creditMemos: string;
  commentsHistory: string;
  delivery_status: string;
}
const orderSchema = new Schema(
  {
    OrderNo: String,
    SequenceNo: Number,
    buyer_id: {
      type: SchemaTypes.ObjectId,
      ref: "User"
    },
    merchant_id: SchemaTypes.ObjectId,
    memo: String,
    status: {
      type: String,
      index: true,
      default: "Pending"
    },
    commission_status: {
      type: String,
      default: "Hold"
    },
    payment_method: String,
    delivery: {
      S_address: String,
      S_unit_no: String,
      S_postal_code: String,
      S_contact_no: String,
      S_recepient: String,

      B_address: String,
      B_unit_no: String,
      B_postal_code: String,
      B_contact_no: String,
      B_recepient: String,

      shipping_fee: Number,
      shipping_type: String,
      shipment_id: String,
      Shipping_date: Date,
      Shipping_time: String
    },
    products: [
      {
        _id: {
          type: SchemaTypes.ObjectId,
          ref: "Product"
        },
        comment_id: SchemaTypes.ObjectId,
        purchase: {
          product_total_bp: Number,
          commission: Number,
          commission_amount: Number,
          reward_pts: Number
        },
        product: {
          _id: SchemaTypes.ObjectId,
          brief: {
            name: String,
            short_description: String,
            price: Number,
            cost: Number,
            stock: Number,
            category: String,
            images: [String],
            discount: Boolean,
            code: String
          },
          pricing: {
            discount_rate: {
              type: Number,
              default: 0
            }
          }
        },
        variants: [
          {
            _id: SchemaTypes.ObjectId,
            sku: String,
            stock: Number,
            price: Number,
            option_name: String,
            option_value: String,
            status: String,
            order_qty: Number,
            commission: Number,
            reward_pts: Number,
            promo_id: String,
          }
        ]
      }
    ],
    promotions: [
      {
        _id: SchemaTypes.ObjectId
      }
    ],
    free_shipping: Boolean,
    total: {
      store_bp: Number,
      store_ap: Number,
      commission: Number,
      reward_pts: Number,
      creditwallet_amnt: Number
    },
    purchasedFrom: String,
    placedFromIP: String,
    customerGroup: String,
    prefix: String,
    billingAdd: String,
    giftOpt: {
      from: String,
      to: String,
      msg: String
    },
    creditMemos: String,
    commentsHistory: String,
    delivery_status: {
      type: String,
      default: "not_picked"
    }
  },
  { timestamps: true }
);

export default orderSchema;

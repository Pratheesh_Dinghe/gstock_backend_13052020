import { Schema, Types, Document, Error, SchemaTypes } from "mongoose";
import * as bcrypt from "bcrypt-nodejs";
import * as crypto from "crypto";

export interface SupplierModel extends Document {
  name: String;
  address1: String;
  address2: String;
  postalcode: String;
  city: String;
  state: String;
  country: String;
  website: String;
  email1: String;
  email2: String;
  Password:String;
  //C_password:Number;
  username:String;

  ic_no:{
    type:String;
    
  }
  contact:Number;
  type: String;
}

const supplierSchema = new Schema(
  {
    name: String,
    address1: String,
    address2: String,
    postalcode: String,
    city: String,
    state: String,
    country: String,
    website: String,
    email1: String,
    email2: String,
    ic_no:String,
    contact:Number,
    type: String,
   Password:String,
        username:String,
  //  C_password:Number
  },
  {
    timestamps: true,
    minimize: false
  }
);
/*supplierSchema.pre("save", function save(next) {
  const user = this;
  if (!user.isModified("credential.password")) {
    return next();
  }
  bcrypt.genSalt(10, (err, salt) => {
    if (err) {
      return next(err);
    }
    bcrypt.hash(
      user.credential.password,
      salt,
      undefined,
      (err: Error, hash) => {
        if (err) {
          return next(err);
        }
        user.credential.password = hash;
        next();
      }
    );
  });
});

supplierSchema.methods.comparePassword = function(
  candidatePassword: string,
  password: string,
  cb: (err: Error, isMatch: boolean) => {}
) {
  bcrypt.compare(candidatePassword, password, function(
    err: Error,
    isMatch: boolean
  ) {
    // console.log("Everythin ok?", candidatePassword, password);
    cb(err, isMatch);
  });
};*/
export default supplierSchema;

import { Document, Schema, SchemaTypes, Types } from "mongoose";

export type JobsModel = Document & {
    orders: [
        {
            OrderNo: string;
            OrderDate: Date;
            OrderId: Types.ObjectId;
            S_Address: string;
            netPrice: number;
            customerPhone: string;
            customerName: string;
            numberOfItems: number;
        }
    ],
    JobId: String;
    status: String;
};


const jobsSchema = new Schema({
    orders: [
        {
            OrderNo: String,
            OrderDate: Date,
            OrderId: {
                type: SchemaTypes.ObjectId,
                ref: "Order"
            },
            S_Address: String,
            netPrice: Number,
            customerPhone: String,
            customerName: String,
            numberOfItems: Number
        }
    ],
    JobId: String,
    status: String
},
    { timestamps: true, minimize: false });


export default jobsSchema;

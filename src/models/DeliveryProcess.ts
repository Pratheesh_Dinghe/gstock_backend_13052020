import { Schema, Types, Document, Error, SchemaTypes } from "mongoose";

export interface DeliveryProcessModel extends Document {
    deli_date: Date;
    from_date: Date;
    to_date: Date;
    ordet_list: [{
        order_id: String,
        order_no: String,
        order_date: Date,
        delivery_type: String,
        deli_status: String
    }];
}

const deliveryProcessSchema = new Schema(
    {
        deli_date: Date,
        from_date: Date,
        to_date: Date,
        order_list: [{
            order_id: String,
            order_no: String,
            order_date: Date,
            delivery_type: String,
            deli_status: String
    }]
    },
   );

export default deliveryProcessSchema;

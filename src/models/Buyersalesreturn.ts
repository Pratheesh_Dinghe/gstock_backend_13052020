import { SchemaTypes, Types, Schema, Document } from "mongoose";
import { StopTrainingJobRequest } from "aws-sdk/clients/sagemaker";

export type BuyerSalesReturnModel = Document & {
  buyer_id: Types.ObjectId;
  ref_id: Types.ObjectId;
  Return_No: string;
  Order_No: string;
  Verify_Date: Date;
  name: string;
  email: string;
  reason: string;
  remarks: string;
  SequenceNo: number;
  // Order_amnt: number;
  // Ret_amnt: number;
  Order_amount,
  Ret_amount,
  status: string;
  uploadimage: Array<string>;
  products: [
    {
      _id: Types.ObjectId;

      name: string;

      price: number;
      OrderQty: number;
      RetQty: number;

      images: Array<string>;
    }
  ];
};




const BuyerSalesReturnSchema = new Schema(
  {
    buyer_id: {
      type: SchemaTypes.ObjectId
    },
    ref_id: {
      type: SchemaTypes.ObjectId
    },
    Return_No: String,
    Order_No: String,
    Verify_Date: Date,
    name: String,
    email: String,
    reason: String,
    remarks: String,
    SequenceNo: Number,
    // Order_amnt: {
    //   type: Number,
    //   default: 0
    // },
    // Ret_amnt: {
    //   type: Number,
    //   default: 0
    // },
    Order_amount: {
         type: Number,
         default: 0
       },
    Ret_amount: {
         type: Number,
         default: 0
       },
    status: String,
    uploadimage: [String],
    products: [
      {
        _id: SchemaTypes.ObjectId,
        name: String,

        price: Number,
        OrderQty: Number,
        RetQty: Number,

        images: [String]
      }
    ]
  },
  { timestamps: true, minimize: false }
);

export default BuyerSalesReturnSchema;

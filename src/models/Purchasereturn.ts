import { SchemaTypes, Document, Schema, Types } from "mongoose";
import * as mongoosePaginate from "mongoose-paginate";
import { NumericDictionary } from "lodash";
export interface PurchaseReturnModel extends Document {

    // return_no: string;
    // return_date: Date;
    // invoice_date: Date;
    // Customer: string;
    // invoice_no: string;
    Invoice_date:Date;
    Invoice_No: string;
    OderDate:Date;
    Return_no: string;
    Invoice_no: string;
    Return_date:Date
    PO_N0: string;

    item: [{
        // amount: number;
        // code: string;
        // discount: number;
        // name: string;
        // price: number;
        // _rtn_amount:number;
        // qty: number;
        // //  slNo:Number,
        // _rtn_qty: number;
        // _id: string;
        code:String,
        cost:Number,
        discount: Number,
        name:String,
        pre: String,
        qty: Number,
        slNo:Number,
        totalCost: Number,
        _id:String,
        _rtn_amount: Number,
        _rtn_qty: Number
    }],
    total: number;
    gst: number;
    gst_perc:number;
    net_total: number;
    Status: string;

}
const purchasereturnSchema = new Schema(
    {
        Invoice_date:Date,
        Invoice_No: String,
        OderDate:Date,
        Return_no: String,
        Invoice_no: String,
        Return_date:Date,
        PO_N0: String,
  



        item: [{
            // amount: Number,
            // code: String,
            // discount: Number,
            // name: String,
            // price: Number,
            // qty: Number,
            // _rtn_amount:Number,
            // //  slNo:Number,
            // _id: String,
            // _rtn_qty: Number,

            code:String,
            cost:Number,
            discount: Number,
            name:String,
            pre: String,
            qty: Number,
            slNo:Number,
            totalCost: Number,
            _id:String,
            _rtn_amount: Number,
            _rtn_qty: Number


        }],
        total: Number,
        gst: Number,
        net_total: Number,
        gst_perc:Number,
        Status: String,
    });


export default purchasereturnSchema;
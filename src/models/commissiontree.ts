import { Schema, Types, Document, Error, SchemaTypes } from "mongoose";
export interface CommissiontreeModel extends Document {
     _id: Schema.Types.String;
     user_name: Schema.Types.String;
     parent: Schema.Types.String;
     someadditionalattr: Schema.Types.String;
     order: Schema.Types.String;
     totalPurchase: Number;
}
const commissiontreeSchema = new Schema(
     {
          _id: Schema.Types.String,
          user_name: Schema.Types.String,
          parent: Schema.Types.String,
          someadditionalattr: Schema.Types.String,
          order: Schema.Types.String,
          level: Schema.Types.Number,
          totalPurchase: Schema.Types.Number
     }
     ,
     {
          timestamps: true,
          minimize: false
     });
export default commissiontreeSchema;




import { SchemaTypes, Document, Schema, Types } from "mongoose";
import * as mongoosePaginate from "mongoose-paginate";
import { NumericDictionary } from "lodash";
import { ObjectId, ObjectID } from "bson";
export interface CreditWalletApprovalModel extends Document {

    approve_id: string;
    date: Date;
    Name: string;
    buyer_id:Types.ObjectId;
    Withdrawal_Amnt:number;
    Mobile_No:string;
    address:string;
    email:string;
    bank_name:string;
    Acc_No:string;
    remarks:string
    status:string;
}
const creditWalletApprovalSchema = new Schema(
    {
        approve_id: String,
        Name: String,
        buyer_id: {
            type: SchemaTypes.ObjectId,
        },
        Withdrawal_Amnt:Number,
        Mobile_No:String,
        address:String,
        email:String,
        date:Date,
        bank_name:String,
        Acc_No:String,
        remarks:String,
        status:String,
       
    });

    export default creditWalletApprovalSchema
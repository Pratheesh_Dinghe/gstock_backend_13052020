import { SchemaTypes, Document, Schema, Types } from "mongoose";
import * as mongoosePaginate from "mongoose-paginate";
import { NumericDictionary } from "lodash";
export interface FailedEmailsLogModel extends Document {

    supplier:String,
    orders:[{
        _id:Types.ObjectId,
    }]
    
}
    const failedEmailsLogSchema = new Schema(
        {
            supplier:String,
            orders:[{
                _id:SchemaTypes.ObjectId,
            }
               
            ]
        });
    
    
export default failedEmailsLogSchema;
    